"""
@Desc: Utility functions for PI-ICR analysis/visualization. All of these functions
       are intended to be as generic as possible, and usable for a large range of
       different applications
@Author: Scott Campbell, campbels@frib.msu.edu
@Version: 30 Apr 2023
"""

import numpy as np
import glob
import os
import scipy.integrate as integrate
from uncertainties import ufloat, umath
import configparser
import matplotlib
from matplotlib.colors import LinearSegmentedColormap

from .DataFile_Reconstructed import DataFile as OldDataFile
from .DataFile_RawTDC import DataFile as NewDataFile
from .AnalysisFile import AnalysisFile
from .System import System

###############################################################################
# General Use Methods
###############################################################################

def cart2pol(x, y):
    x, y = x.astype('float'), y.astype('float')
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    rho, phi = rho.astype('float'), phi.astype('float')
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

def cart2Pol_withUnc(xy, xyErr, center=[0,0], centerErr=[0,0]):
    """ Extracts the angular information for a dataset
    Args:
        xy: the position to be converted 
        xy_err: the uncertainty in the position
        center: the offset of the data
        centerErr: the uncertainty in the offset
    Returns:
        rho, delta rho, phi (deg), delta phi (deg)
    """
    xy, xyErr = xy.astype('float'), xyErr.astype('float')
    xyU = tuple(ufloat(i,j) for i,j in zip(xy,xyErr)) # create an uncertainty object to propogate the errors
    centerU = tuple(ufloat(i,j) for i,j in zip(center,centerErr)) # create an uncertainty object to propogate the errors
    xyU = xyU - centerU
    # Calculate the radius and err
    rhoU = umath.sqrt(xyU[0]**2 + xyU[1]**2)
    phiU = umath.atan2(xyU[1], xyU[0])
    # convert angles into degrees
    phiU = (phiU*180/np.pi) % 360
    
    return rhoU.n, rhoU.s, phiU.n, phiU.s

def extract_configuration(ini_path):
    config = configparser.ConfigParser()
    if not os.path.exists(ini_path):
        raise FileNotFoundError("The configuration file \""+ini_path+"\" does not exist. Please check the location.")
        
    config.read(ini_path)

    mm8DataPath      = config.get("SYSTEM", "mm8_data_dir", fallback="").strip()
    mcpDataPath      = config.get("SYSTEM", "mcp_data_dir", fallback="").strip()
    mm8MainXMLPath   = config.get("SYSTEM","mm8_main_xml_dir", fallback="").strip()
    mm8SetsPath      = config.get("SYSTEM","mm8_sets_dir", fallback="").strip()
    amePath          = config.get("SYSTEM","ame_datafile", fallback="").strip()
    dipole_afg_loc   = config.get("SYSTEM", "dipole_AFG_loc", fallback="None").strip()
    conv_afg_loc     = config.get("SYSTEM", "conv_AFG_loc", fallback="None").strip()
    userFigScale     = config.getfloat("SYSTEM", "figure_scale", fallback=1.0)
    userFontScale    = config.getfloat("SYSTEM","font_scale", fallback=1.0)
    nbinsPosn        = config.getint("SYSTEM","n_bins_posn", fallback=75)
    cntrateHistory   = config.getfloat("SYSTEM","cntrate_history", fallback=100.0)
    useRawDatafile   = config.getboolean("SYSTEM","using_raw_datafile", fallback=True)
    datafileExtens   = config.get("SYSTEM","mcp_datafile_extension",fallback="RAW, *.raw")
    redcyc_timing    = config.get("SYSTEM","redcyc_accum_timing",fallback="1:A:")
    mag_timing       = config.get("SYSTEM","mag_accum_timing",fallback="1:A:")
    radMotionDir     = config.getint("DATA_LIMITS","radial_motion_direction",fallback=-1)
    mcpRadius        = config.getfloat("DATA_LIMITS","mcp_radius", fallback=1)
    absTofMin        = config.getfloat("DATA_LIMITS","tof_min", fallback=0)
    absTofMax        = config.getfloat("DATA_LIMITS","tof_max", fallback=100)
    tdcX1Fall        = config.getint("TDC_INFO","x1_fall")
    tdcX2Fall        = config.getint("TDC_INFO","x2_fall")
    tdcY1Fall        = config.getint("TDC_INFO","y1_fall")
    tdcY2Fall        = config.getint("TDC_INFO","y2_fall")
    tdcMCPFall       = config.getint("TDC_INFO","mcp_fall")
    tdcSrtRise       = config.getint("TDC_INFO","srt_rise")
    tdcSrtFall       = config.getint("TDC_INFO","srt_fall")
    tdcStpFall       = config.getint("TDC_INFO","stp_fall")
    sumXAvg          = config.getfloat("TDC_INFO","sumx_avg", fallback=-1)
    sumXStd          = config.getfloat("TDC_INFO","sumx_std", fallback=-1)
    sumYAvg          = config.getfloat("TDC_INFO","sumy_avg", fallback=-1)
    sumYStd          = config.getfloat("TDC_INFO","sumy_std", fallback=-1)

    if sumXAvg < 0: sumXAvg = None
    if sumXStd < 0: sumXStd = None
    if sumYAvg < 0: sumYAvg = None
    if sumYStd < 0: sumYStd = None 

    tdcInfo = {'x1_fall':tdcX1Fall,'x2_fall':tdcX2Fall,
               'y1_fall':tdcY1Fall,'y2_fall':tdcY2Fall,
               'mcp_fall':tdcMCPFall,
               'srt_rise':tdcSrtRise,'srt_fall':tdcSrtFall,
               'stp_fall':tdcStpFall,
               'sumX':sumXAvg,'sigmaSumX':sumXStd,
               'sumY':sumYAvg,'sigmaSumY':sumYStd}

    # Parse out the datafile extension info
    mcpDataExtName, mcpDataExtFormat = datafileExtens.split(",", maxsplit=2)
    mcpDataExtName, mcpDataExtFormat = mcpDataExtName.strip(), mcpDataExtFormat.strip()
    # Create the system class object
    piSystem = System(mm8DataPath, mcpDataPath, mm8MainXMLPath, mm8SetsPath, amePath, 
                      tdcInfo, absTofMin, absTofMax, mcpRadius, radMotionDir,
                      mcpDataExtName, mcpDataExtFormat, useRawDatafile,  
                      dipole_afg_loc, conv_afg_loc, redcyc_timing, mag_timing)

    # Set data limits: must be in dictionaries so that they can be dynamically updated
    limits =   {'xmin':-mcpRadius, 'xmax':mcpRadius, 
                'ymin':-mcpRadius, 'ymax':mcpRadius, 
                'tofmin':absTofMin, 'tofmax':absTofMax, 
                'tofminview':absTofMin, 'tofmaxview':absTofMax,
                'eventmin':None, 'eventmax':None,
                'zmin':0, 'zmax':10,
                'radialmin':0, 'radialmax':mcpRadius, 
                'angmin':0, 'angmax':360 }
    
    # Check if the user is loading an old datafile format, or new
    if useRawDatafile: piDataFile = NewDataFile(piSystem.getMcpDataDirectory(), piSystem.getLatestMCPDataFile(), piSystem)
    else: piDataFile = OldDataFile(piSystem.getMcpDataDirectory(), piSystem.getLatestMCPDataFile(), piSystem)
    # Create the rest of the needed classes
    piAnalysisFile = AnalysisFile(piDataFile, limits)
    
    return ( tdcInfo, piSystem, limits, 
             piDataFile, piAnalysisFile,
             nbinsPosn, cntrateHistory, 
             userFigScale, userFontScale )

def loadAllDataFiles(PIDataPath, filenums, system, limits):
    all_files = []       
    #Grabs some basic info about every data file in PIDataPath
    files = glob.glob(os.path.join(PIDataPath, '*'+system.getDatafileExtFormat()))
    for fil in files:
        thisFile = os.path.basename(fil)
        try: filNum = int(thisFile.split('_')[1]) # Gets the file number. excpetion is thrown if its a try scan file
        except: filNum = -1
        if filNum in filenums:
            # Create the file object -- it will automatically load all the data it is able to
            if system.getUsesRawDataFile(): currDataFile = NewDataFile(PIDataPath, thisFile, system)
            else: currDataFile = OldDataFile(PIDataPath, thisFile, system)
            all_files.append(AnalysisFile(currDataFile, limits))
    return all_files

###############################################################################
# Generate Colormaps
###############################################################################

def get_mcp_colormap(max_cluster_count, max_noise_count, full_range_color=False, n_color_samples=256):
    """
    Creates a color map split in axes for showing both the cluster and noise on
    independently diverging color maps
    
    Args: 
        max_cluster_count (int): the maximum histogram bin value for the cluster map
        max_noise_count (int): the maximum histogram bin value for the noise map
    """
    if max_cluster_count == 0: return get_reject_colormap()
    if max_noise_count == 0: return get_position_colormap()

    percent_clust = max_cluster_count/(max_cluster_count+max_noise_count) # Define how much of the colormap should use "top" one
    n_color_clust = int(percent_clust*n_color_samples)
    n_color_noise = n_color_samples - n_color_clust
    clust_map = get_position_colormap(n_color_samples=n_color_clust, full_range=full_range_color, return_arr=True)
    noise_map = get_reject_colormap(n_color_samples=n_color_noise, return_arr=True)
    my_map = np.concatenate((noise_map, clust_map),axis=0)
    return LinearSegmentedColormap.from_list('dual_map', my_map, N=256)

def get_reject_colormap(n_color_samples=256, full_range=False, return_arr=False):
    greys = matplotlib.colormaps['Greys']
    # exclude the dark grey/black -> confusing with inferno map!
    if not full_range: vals = np.linspace(0.25, 0.75, n_color_samples)
    else: vals = np.linspace(0, 1, n_color_samples)
    noise_map = [greys(val)[:3] for val in vals]
    if return_arr: return noise_map
    return LinearSegmentedColormap.from_list('noise_map', noise_map, N=n_color_samples) 

def get_position_colormap(n_color_samples=256, full_range=False, return_arr=False):
    inferno = matplotlib.colormaps['magma'] # top colorbar
    # trim off some of the really light yellow since it doesn't show well on white
    if not full_range: clust_map = np.array(inferno.colors)[np.linspace(0.0*len(inferno.colors), 0.80*len(inferno.colors)-1, n_color_samples).astype(int)]
    else: clust_map = np.array(inferno.colors)[np.linspace(0, len(inferno.colors)-1, n_color_samples).astype(int)]
    clust_map = clust_map[::-1] # flip order
    if return_arr: return clust_map
    return LinearSegmentedColormap.from_list('clust_map', clust_map, N=n_color_samples)

###############################################################################
# Datafile/Analysis Specific Methods
###############################################################################

def getEllipseAccumPhase(start_phi, end_phi, freq_direction, ellipse_a, ellipse_b, ellipse_theta):
    """ Computes the phase angle between two points on an ellipse, defined by start_phi and end_phi

    Args: 
        start_phi, end_phi: absolute angles of the two points, in degrees, relative to center of ellipse
        freq_direction (-1 or 1): -1 -> clkwise and 1 -> counterclkwise
        ellipse_a, ellipse_b, ellipse_theta: parameters of the ellipse, ellipse_theta in degrees
    Returns:
        phase between start and end angles, in degrees
    """
    start_phi, end_phi = start_phi%360, end_phi%360
    # calculate perimeter of the ellipse
    perim, perim_err = getEllipseArcLen(0, 360, ellipse_a, ellipse_b, ellipse_theta)
    # calculate the arc length between the two angles
    if start_phi < end_phi:
        arclen, arclen_err = getEllipseArcLen(start_phi, end_phi, ellipse_a, ellipse_b, ellipse_theta)
    else:
        arclen, arclen_err = getEllipseArcLen(end_phi, start_phi, ellipse_a, ellipse_b, ellipse_theta)
        arclen = perim-arclen
    # Adjust for the frequency direction
    if freq_direction < 0: # negative -> clockwise direction
        arclen = perim-arclen

    # NOTE: errors from numerical integration are negligible compared to angle uncertainty!
    return 360 * (arclen/perim)

def getEllipseArcLen(start_phi, end_phi, ellipse_a, ellipse_b, ellipse_theta):
    """ Computes the arc length between two points on an ellipse, defined by start_phi and end_phi

    Args: 
        start_phi, end_phi: absolute angles of the two points, in degrees, relative to center of ellipse
        ellipse_a, ellipse_b, ellipse_theta: parameters of the ellipse, ellipse_theta in degrees
    Returns:
        arc length, and uncertainty in the arc length
    """
    start_phi, end_phi, ellipse_theta = start_phi*np.pi/180, end_phi*np.pi/180, ellipse_theta*np.pi/180
    val = lambda t: np.sqrt( ellipse_a**2 * np.power(np.sin(t-ellipse_theta),2) + \
                             ellipse_b**2 * np.power(np.cos(t-ellipse_theta),2) )
    return integrate.quad(val, start_phi, end_phi)

def calculateBirgeRatio(vals, val_errs):
    if len(vals) <= 1: return 0, 0
    # Calculate the weighted average
    wav = np.average(vals, weights=1/np.square(val_errs))
    # Calculate Chi-squared
    chi_sq = np.sum(((vals - wav) / val_errs) ** 2)
    # Calculate the Birge Ratio and uncertainty
    birge_ratio = np.sqrt(chi_sq / (len(vals)-1) )
    birge_err = 0.4769/np.sqrt(len(vals)) # https://journals.aps.org/pr/abstract/10.1103/PhysRev.40.207
    return birge_ratio, birge_err 

def getSurroundingIdx(midtime, measurement_times):
    """ Finds the indices of measurements in this dataset which bracket the supplied time
        this requires that self.midtimes has at least 2 values
    Args: 
        midtime (float): epoch time of the file of interest
    Returns: lower idx, upper idx which bracket the supplied midtime
    """
    if midtime <= measurement_times[0] or midtime >= measurement_times[-1]:
        raise ValueError("The supplied file time is outside the range of measurments in the Circle Dataset!")
    lo_idx, hi_idx = -1, -1
    for kk, meas_time in enumerate(measurement_times):
        if meas_time < midtime: 
            lo_idx = kk
            continue
        if meas_time > midtime: 
            hi_idx = kk
            break
    if lo_idx > 0 and hi_idx > 0: return lo_idx, hi_idx
    else: raise ValueError("Could not find two measurement times bracketing the supplied time!")

def getNearestUncertainty(x, x_vals, y_errs):
    """ Determines the confidence interval for linear regression from just two datapoints
    Args:
        x (float): the x value for which to determine the uncertainty of its corresp. y value
        x_vals (1x2 list of float): measured x values
        y_vals (1x2 list of float): measured y value uncertainties
    """
    delx = x_vals[1] - x_vals[0]
    variance =  (y_errs[0] * (1 + x_vals[0]/delx ))**2
    variance += (y_errs[1]*x_vals[0]/delx)**2
    variance += (y_errs[0]**2 + y_errs[1]**2)*(x**2)/(delx**2)
    variance -= 2*( (y_errs[1]**2)*x_vals[0]/(delx**2) + (y_errs[0]**2)*(1+ x_vals[0]/delx)/delx ) * x
    return np.sqrt(variance)

###############################################################################
# Methods for Clustering and Fitting Positions
###############################################################################

def clusterSpot(PIClusterObj, positions, threshold, smoothing, return_map=False):
    """
    Args:
        threshold: float in (0,1) defining the confidence in the clustering prediction. 1 is the most restrictive
        smoothing: apply a smoothing to the cluster prediction. Typical spot values are < 1, circle < 2
        MCA (default False): if not false, should be an array of tof cuts: [tof_min, tof_max]
        angular_filter (2 val list): angular filter in degrees, the limits are interpreted in clkwise order
        radial_filter (2 val list): radial filter, the limits are interpreted as [low, high]
    """   
    if len(positions) < 3:
        print("Insufficient positions made the filter criteria in file!")
        return 
    
    clusters, noise, cluster_map = PIClusterObj.cluster_data(positions[:,0], positions[:,1], cluster_thresh=threshold, smoothing_sigma=smoothing, return_map=True)
    noise, clusters = np.array(noise), np.array(clusters) # each value in the clusters array is a 2D list of cluster points!
    # TODO: handle multiple clusters!
    clusterXY = clusters[0]
    noiseXY = noise
    
    if return_map:
        return clusterXY, noiseXY, cluster_map
    return clusterXY, noiseXY

def clusterEllipse(PIClusterObj, positions, clusterThresh, smoothSigma, return_map=False):
    """
    Args:
        threshold: float in (0,1) defining the confidence in the clustering prediction. 1 is the most restrictive
        smoothing: apply a smoothing to the cluster prediction. Typical spot values are < 1, circle < 2
        MCA (default False): if not false, should be an array of tof cuts: [tof_min, tof_max]
    """    
    if len(positions) < 3:
        print("Insufficient positions made the filter criteria in file!")
        return 

    clusterXY, noiseXY, cluster_map = PIClusterObj.cluster_ellipse(positions[:,0], positions[:,1], cluster_thresh=clusterThresh, smoothing_sigma=smoothSigma, return_map=True)
    
    if return_map:
        return clusterXY, noiseXY, cluster_map
    return clusterXY, noiseXY

###############################################################################
# Misc. Methods
###############################################################################

def get_hist_plot_points(heights, bins):
    """ len of bins is len of heights + 1
    """
    # Compute the x and y values: 
    x_values = [ bins[0], bins[0] ]
    y_values = [ 0, heights[0] ]
    for kk in range(1, len(bins)-1):
        x_values.append(bins[kk])
        y_values.append(heights[kk-1])
        x_values.append(bins[kk])
        y_values.append(heights[kk])
    x_values.append(bins[-1])
    x_values.append(bins[-1])
    y_values.append(heights[-1])
    y_values.append(0)

    return x_values, y_values

