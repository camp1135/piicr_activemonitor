import numpy as np

class LinearFitter:
   
    def __init__(self):
        self.fit_completed = False
        # Create placeholder values for results of the fit
        self.x_data = None
        self.y_data = None
        self.y_errs = None
        self.n = None # The number of dimensions of the fitted data
        self.x_weighted_avg = None
        self.y_weighted_avg = None
        self.residual_std = None # standard deviation of residuals (y_fit - y_data)
        self.slope = None
        self.intercept = None
    
    def fit(self, x_data, y_data, y_errs):
        """ Performs a weighted least squares regression to a function of the form y = mx + b
            where the y data has errors associated with it
        Args:
            x_data (list of float): the independent data
            y_data (list of float): the dependent data
            y_errs (list of float): the uncertainties in the dependent data
        """
        self.x_data = x_data
        self.y_data = y_data
        self.y_errs = y_errs
        #--------------------------------------------------------
        # Weighted Least Squares Regression
        w = 1 / y_errs**2  # Inverse variance weighting for y
        # Calculate the weighted averages of x and y
        self.x_weighted_avg = np.sum(w * x_data) / np.sum(w)
        self.y_weighted_avg = np.sum(w * y_data) / np.sum(w)
        # Calculate the slope (m) and intercept (b)
        numerator = np.sum(w * (x_data - self.x_weighted_avg) * (y_data - self.y_weighted_avg))
        denominator = np.sum(w * (x_data - self.x_weighted_avg)**2)
        self.slope = numerator / denominator
        self.intercept = self.y_weighted_avg - self.slope * self.x_weighted_avg
        #--------------------------------------------------------
        # Calculate standard error in slope and intercept
        y_fit = self.slope * x_data + self.intercept # calculate the fitted y values
        residuals = y_data - y_fit # calculate residuals
        self.residual_std = np.sqrt(np.sum(w * residuals**2) / np.sum(w)) # standard deviation of residuals
        self.n = len(x_data)

        self.fit_completed = True
    
    def get_y(self, x_val):
        if not self.fit_completed: return np.nan, np.nan
        # Get the predected value and uncertainty
        y_pred = self.slope * x_val + self.intercept  # Predicted y value at x_new
        se_pred = self.__prediction_std_error__(x_val)  # Standard error of prediction
        ## Critical value from the t-distribution (for 95% confidence interval)
        # confidence_level = 0.68 # 1 sigma confidence interval
        # dof = self.n - 2  # Degrees of freedom
        # t_value = stats.t.ppf((1 + confidence_level) / 2, dof)
        ## Calculate the confidence interval for y_pred at x_new
        # lower_bound = y_pred - t_value * se_pred
        # upper_bound = y_pred + t_value * se_pred 
        return y_pred, se_pred
    
    def __prediction_std_error__(self, x): # Standard error of the prediction
        """ Function to compute the standard error (confidence interval) of the prediction at a given x value
        """
        if not self.fit_completed: return np.nan
        return np.sqrt( self.residual_std**2 * (1 / self.n + (x - self.x_weighted_avg)**2 / np.sum((self.x_data - self.x_weighted_avg)**2)) )
    