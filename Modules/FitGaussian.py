from iminuit import Minuit
from iminuit.cost import LeastSquares
import numpy as np

class GaussianFitter:
    """ The code in this class has been adapted from the work developed in the following paper:
    J. Karthein et. al, Analysis methods and code for very high-precision mass measurements of unstable isotopes, Computer Physics Communications (2021)
    https://www.sciencedirect.com/science/article/pii/S001046552100182X
    """
    
    def __init__(self):
        pass
    
    ##################################################################
    # Fitting methods
    ##################################################################

    def fit1DGaussian(self, data_x, data_y, data_yerr):
        def Gauss1D(x, amp, mean, sig):
            """Negative log likelihood function for (n=1)-dimensional Gaussian distribution."""
            return amp * np.exp( -0.5 * np.power(x-mean,2) / sig**2 )

        if len(data_x) < 4: raise Exception("Insufficient number of data points to perform 1D gaussian fit.")

        # minimize negative log likelihood function
        least_squares = LeastSquares(data_x, data_y, data_yerr, Gauss1D)
        mean = np.average(data_x, weights=data_y)
        sig = max(1e-6, np.average((data_x-mean)**2, weights=data_y) )
        root_res = Minuit(least_squares, amp=np.amax(data_y), mean=mean, sig=sig)
        root_res.migrad() # finds minimum of mle function
        root_res.hesse()  # computes errors

        return { 'amp': root_res.values['amp'],
                 'mean': root_res.values['mean'], 
                 'sigma': np.abs(root_res.values['sig']), 
                 'amp_err': np.abs(root_res.errors['amp']),
                 'mean_err': np.abs(root_res.errors['mean']), 
                 'sigma_err': np.abs(root_res.errors['sig']) } 
