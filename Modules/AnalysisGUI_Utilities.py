import matplotlib
matplotlib.use("TKAgg")
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches

import PySimpleGUI as sg
import configparser
import os
import numpy as np
from time import strftime, localtime

from .PIICR_Utilities import *
from .System import System

#############################################################################
# 'Public' Methods that should be called by a user
#############################################################################

def launchFitGUI(analysisfiles, system, limits, settings, clusterObject, fitFunctionDictionary, nbinsPosn):
    """ Launches a basic GUI that allows the user to perform a fit on a 
        dataset for a given fit function.

    Args:
        analysisfiles (list of AnalysisFile obj): files containing the position/tof data
        limits (dict): initial ranges for tof/z/angle/radius
        settings (dict): inital states for different filtering options
        fitParamNames (list of str): names of the fit parameters for 
                                     the fit function used
        fitFunctionHandle (function): the function used to perform the 
                                      fit. Must have the 2D data list as 
                                      the FIRST argument, followed by 
                                      the params in same order as specif.
                                      in fitParamNames list AS A TUPLE.
                                      Function must be able to handle None
                                      values for some/all of the params.
                                      Must return two tuples: one for the
                                      fitted params, then one for the uncs.
        dataCenter (1D list): the center location of the ion motion
                              important for a polar fit, for example
    Returns:
        datafile with updated cluster/noise lists, and a dictionary of 
        the found parameters (keys are fitParamNames values). If the 
        user "Ignores" or exits before a fit is saved, instead of a
        dict, None is returned instead. 
    """
    # create the GUI layout
    layout, mcp_fig, tof_fig, ang_fig = __create_GUI_layout__(fitFunctionDictionary, system, limits, settings)
    window = sg.Window('PI-ICR Data Fitting', layout, finalize=True)

    # draw the initial mcp/data plot in the window
    mcpcanvas = window['mcpcanvas'].TKCanvas
    mcp_ax = mcp_fig.add_subplot(111)
    mcp_fig_agg = __draw_figure__(mcpcanvas, mcp_fig)
    # draw the inital tof plot in the window
    tofcanvas = window['tofcanvas'].TKCanvas
    tof_ax = tof_fig.add_subplot(111)
    tof_ax2 = tof_ax.twinx() # make separate axis for plotting mca gate 
    tof_ax2.set_yticklabels([])
    tof_fig_agg = __draw_figure__(tofcanvas, tof_fig)
    tof_ax.set_position([0.10, 0.20, 0.80, 0.75])
    # draw the angular spectrum
    angcanvas = window['angcanvas'].TKCanvas
    ang_ax = ang_fig.add_subplot(111)
    ang_ax.set_xlabel(r'Angle (deg)', fontsize=10)
    ang_ax.set_position([0.10, 0.20, 0.80, 0.75])
    ang_fig_agg = __draw_figure__(angcanvas, ang_fig)

    fitparamdict_list, fitparamerrdict_list = [], []

    for analysisfile in analysisfiles:
        
        fileinfo_dict = { "info_str":"" }
        paramval_dict = { 'fitmethod':None, 'param1':None, 'param2':None, 'param3':None, 'param4':None, 'param5':None, 'param6':None, 'param7':None, 'param8':None }
        paramerr_dict = { 'param1err':None, 'param2err':None, 'param3err':None, 'param4err':None, 'param5err':None, 'param6err':None, 'param7err':None, 'param8err':None }
        fitplotter = None  
        center, centerErr = analysisfile.getCenter(), analysisfile.getCenterErr()
        if None in center: center, centerErr = [0,0], [0,0]
        # Define a starting cluster map that includes everything
        cluster_map = np.ones((clusterObject.nx,clusterObject.nx))
        # Do the initial updates/plotting
        window['currentfile'].update("Current File: "+str(analysisfile.getFileName()))
        window['centerText'].update("("+str(round(center[0],2))+", "+str(round(center[1],2))+")")

        # Enter into the control loop
        counter = -1
        is_waiting = True
        while is_waiting:
            counter += 1
            event, values = window.read(timeout=10)

            # See if the user updated any of the limits/filters
            limits = __extract_new_limits__(limits, values)
            settings = __extract_new_settings__(settings, values)
            if settings['hideRejectedPoints']: window['hide_rejected_points'].update(background_color = 'indianred')
            else: window['hide_rejected_points'].update(background_color = None)

            # Update the center to use for any spatial filterings
            if settings['overrideFilterCenter']: curr_filter_x0, curr_filter_y0 = settings['filterCenterXY'][0], settings['filterCenterXY'][1]
            else: 
                curr_filter_x0, curr_filter_y0 = center[0], center[1]
                window['filter_center_x'].update(round(curr_filter_x0,2))
                window['filter_center_y'].update(round(curr_filter_y0,2))
            window['filter_center_x'].update(disabled=(not settings['overrideFilterCenter']))
            window['filter_center_y'].update(disabled=(not settings['overrideFilterCenter']))
            window['thresh_slider_val'].update(round(settings['clusterthresh'],2))
            window['smooth_slider_val'].update(round(settings['clustersmooth'],2))

            # Determine how the plot should be shown
            if event == 'image_button':
                settings['plotstyle'] = 'image'
            elif event == 'contour_button':
                settings['plotstyle'] = 'contour'
            elif event == 'scatter_button':
                settings['plotstyle'] = 'scatter'

            # Ocassionally Update the plots and spectra (applying new limits if any were adjusted)
            if counter%5 == 0:
                # Apply any spatial/tof/z filters specified
                current_pos, noise_pos = analysisfile.getGatedPositions(limits, 
                    applyTOF=settings['toffilter'], 
                    applyZ=settings['zfilter'], 
                    applyRadial=settings['radialfilter'], 
                    applyAngular=settings['angularfilter'], 
                    allowRecovered=settings['allowRecovered'], 
                    filterCenter=[curr_filter_x0,curr_filter_y0],
                    returnRejected=True)
                num_filtered = len(current_pos)
                # Apply any clustering if that was done
                if len(current_pos) > 0:
                    cluster_x, cluster_y, noise_x, noise_y = clusterObject.__get_data_from_cluster__(current_pos[:,0], current_pos[:,1], cluster_map)
                    current_pos = np.transpose([cluster_x,cluster_y])
                    if len(noise_pos) == 0:
                        noise_pos = np.transpose([noise_x,noise_y])
                    elif len(noise_x) >= 0:
                        noise_pos = np.concatenate((noise_pos, np.transpose([noise_x,noise_y])))
                    else:
                        noise_pos = []
                num_clustered = len(cluster_x)
                if len(current_pos) > 0 or len(noise_pos) > 0:
                    # Update the mcp and tof data
                    __plot_mcp_data__(mcp_fig, mcp_ax, mcp_fig_agg, current_pos, noise_pos, fitplotter, system, limits, settings, filter_center=[curr_filter_x0, curr_filter_y0], n_bins=nbinsPosn)
                    __plot_tof_spectrum__(tof_fig, tof_ax, tof_ax2, tof_fig_agg, analysisfile.getDataFile(), limits, settings)
                    # Update the angular spectrum plot
                    if settings['angularfilter']: __plot_ang_spectrum__(ang_fig, ang_ax, ang_fig_agg, current_pos-center, ang_start=limits['angmin'], ang_end=limits['angmax'])
                    else: __plot_ang_spectrum__(ang_fig, ang_ax, ang_fig_agg, current_pos-center)
                
                # Update the file info piece
                try: n_recovered = analysisfile.dataFile.getRecoveredCount() if values['allow_recovered_check'] else None
                except: n_recovered = None
                line0 = "MM8 Start Time: "+strftime('%Y-%m-%d %H:%M:%S', localtime(analysisfile.getStartTime()))+"\n"
                line1 = "{0:<26} {1:>10}".format("Total trigger events:", analysisfile.dataFile.CurrentEventID)
                line2 = "{0:<26} {1:>10}".format("Total reconstructed hits:", len(analysisfile.dataFile.ReconstructedEvents))
                line3 = "{0:<26} {1:>10}".format(" → recovered hits:", str(n_recovered))
                line4 = "{0:<26} {1:>10}".format("Number of filtered hits:", str(num_filtered))
                line5 = "{0:<26} {1:>10}".format("Number of clustered hits:", str(num_clustered))
                window['file_info'].update(value=line0+"\n"+line1+"\n"+line2+"\n"+line3+"\n"+line4+"\n"+line5)

            # Handle the clustering of data if requested
            if event == 'cluster_button':
                # Apply any spatial/tof/z filters specified
                current_pos, noise_pos = analysisfile.getGatedPositions(limits, 
                    applyTOF=settings['toffilter'], 
                    applyZ=settings['zfilter'], 
                    applyRadial=settings['radialfilter'], 
                    applyAngular=settings['angularfilter'], 
                    allowRecovered=settings['allowRecovered'],
                    filterCenter=[curr_filter_x0,curr_filter_y0], 
                    returnRejected=True)
                # Apply any clustering if that was done
                if len(current_pos) > 0:
                    cluster_map = __handle_cluster_button__(current_pos, settings, clusterObject)
                    cluster_x, cluster_y, _, _ = clusterObject.__get_data_from_cluster__(current_pos[:,0], current_pos[:,1], cluster_map)
                    current_pos = np.transpose([cluster_x,cluster_y])
                num_original = len(analysisfile.getDataFile().getPositions())
                num_cluster = len(current_pos)
            if event == 'ignore':
                is_waiting = False
                fitparamdict_list.append(None)
                fitparamerrdict_list.append(None)
            if event == 'fit':
                # Apply any spatial/tof/z filters specified
                current_pos, noise_pos = analysisfile.getGatedPositions(limits, 
                    applyTOF=settings['toffilter'], 
                    applyZ=settings['zfilter'], 
                    applyRadial=settings['radialfilter'], 
                    applyAngular=settings['angularfilter'], 
                    allowRecovered=settings['allowRecovered'], 
                    filterCenter=[curr_filter_x0,curr_filter_y0],
                    returnRejected=True)
                # Apply any clustering if that was done
                cluster_x, cluster_y, _, _ = clusterObject.__get_data_from_cluster__(current_pos[:,0], current_pos[:,1], cluster_map)
                current_pos = np.transpose([cluster_x,cluster_y])
                analysisfile.setCounts(len(current_pos))
                # get the current state of the values
                paramval_dict = __extract_param_values__(paramval_dict, values)
                fit_param_guesses = tuple([paramval_dict['param'+str(kk+1)] for kk in range(8)])
                fit_names, fit_values, fit_errors, fitplotter = fitFunctionDictionary[settings['fitfunctionkey']](current_pos, fit_param_guesses, center, centerErr=centerErr)
                # update the GUI with the new values
                for kk in range(len(fit_values)):
                    window['param'+str(kk+1)+'name'].update(fit_names[kk])
                    window['param'+str(kk+1)+'value'].update(np.round(fit_values[kk],2))
                    window['param'+str(kk+1)+'error'].update(np.round(fit_errors[kk],2))
                paramval_dict['fitmethod'] = settings['fitfunctionkey']
            if event == 'save':
                # get the current state of the values
                paramval_dict = __extract_param_values__(paramval_dict, values)
                paramerr_dict = __extract_param_errors__(paramerr_dict, values)
                is_waiting = False
                fitparamdict_list.append(paramval_dict)
                fitparamerrdict_list.append(paramerr_dict)
            if event == 'reset':
                current_pos = analysisfile.getDataFile().getPositions()
                analysisfile.setCounts(len(current_pos))
                fitplotter = None 
                cluster_map = np.ones((clusterObject.nx, clusterObject.nx))

            if event in ('Exit', None):
                is_waiting = False
                fitparamdict_list.append(None)
                fitparamerrdict_list.append(None)

    window.close()
    return fitparamdict_list, fitparamerrdict_list

#############################################################################
# 'Private' Methods that don't need to be called by a user
#############################################################################

def __create_GUI_layout__(fitmethod_dict, system, limits, settings, attribute_scale=1):
    xlarge_font_size = str(int(15*attribute_scale))
    large_font_size = str(int(13*attribute_scale))
    med_font_size = str(int(11*attribute_scale))
    small_font_size = str(int(9*attribute_scale))
    xlarge_font = "Helvetica "+xlarge_font_size
    large_font = "Helvetica "+large_font_size
    med_font = "Helvetica "+med_font_size
    small_font = "Helvetica "+small_font_size

    # Define the GUI figure dimensions
    mcp_width, mcp_height = mcp_figsize = (6, 6)
    mcp_fig = Figure(figsize=mcp_figsize)
    mcp_dpi = mcp_fig.get_dpi()
    mcp_canvsize = (mcp_width*mcp_dpi, mcp_height*mcp_dpi)

    tof_width, tof_height = tof_figsize = (6, 1.75)
    tof_fig = Figure(figsize=tof_figsize)
    tof_dpi = tof_fig.get_dpi()
    tof_canvsize = (tof_width*tof_dpi, tof_height*tof_dpi)

    ang_width, ang_height = ang_figsize = (5, 2.5)
    ang_fig = Figure(figsize=ang_figsize)
    ang_dpi = ang_fig.get_dpi()
    ang_canvsize = (ang_width*ang_dpi, ang_height*ang_dpi)

    # Create helper-/sub- layouts
    ang_spectrum = sg.Frame("Angular Spectrum", [
        [sg.Canvas(size=ang_canvsize, key='angcanvas')],
    ], expand_x=True, expand_y=True, font=large_font)

    cluster_layout = sg.Frame("Clustering",[[
        sg.Column([
            [ sg.Combo(values=["Spot","Ellipse"], default_value="Spot", readonly=True, size=15, font=small_font, key="cluster_combo") ],
            [ sg.Button("Cluster Data!",size=(15,1),key='cluster_button') ]
        ]),
        sg.VerticalSeparator(),
        sg.Column([
            [
                sg.Text('Threshold:',expand_x=True,font=small_font,size=10),
                sg.Text('', size=4, font=small_font, key='thresh_slider_val'),
                sg.Slider(range=(0.01,1), size=(22,10), expand_x=True, disable_number_display=True, default_value=str(settings['clusterthresh']), resolution=.01, orientation='h', key='threshold'),
                sg.Push()
            ],
            [
                sg.Text('Smoothing:',expand_x=True,font=small_font,size=10),
                sg.Text('', size=4, font=small_font, key='smooth_slider_val'),
                sg.Slider(range=(0.1,3), size=(22,10), expand_x=True, disable_number_display=True, default_value=str(settings['clustersmooth']), resolution=.1, orientation='h', key='smoothing'),
                sg.Push()
            ]
        ])
    ]], expand_x=True, font=large_font)

    filters_layout = sg.Frame("Filters", [
        [
            sg.Checkbox("Override Suggested Filter Center (x,y)?", default=False, font=small_font, key='override_filter_center'), 
            sg.Input("0",size=(5,1), disabled=True, font=small_font, key="filter_center_x"),
            sg.Input("0",size=(5,1), disabled=True, font=small_font, key="filter_center_y")
        ],
        [
            sg.Column([
                [
                    sg.Checkbox("TOF Gate", default=True, size=12, font=small_font, key='tof_bounds_check'),
                    sg.In(str(limits['tofmin']), size=5, font=small_font, key='tof_min'),
                    sg.Text("to", size=2, font=small_font),
                    sg.In(str(limits['tofmax']), size=5, font=small_font, key='tof_max'),
                    sg.Push()
                ], 
                [
                    sg.Checkbox("Max Cnts/Shot", default=True, size=12, font=small_font, key='z_limit_check'), 
                    sg.In(str(limits['zmax']), size=5, font=small_font, key='z_max'),
                    sg.Push()
                ],
                [
                    sg.Checkbox("Radial Gate", default=False, size=12, font=small_font, key='radial_check'),
                    sg.In(str(limits['radialmin']), size=5, font=small_font, key='radial_min'),
                    sg.Text("to", size=2, font=small_font),
                    sg.In(str(limits['radialmax']), size=5, font=small_font, key='radial_max'),
                    sg.Push()
                ], 
                [
                    sg.Checkbox("Angular Gate", default=False, size=12, font=small_font, key='ang_check'),
                    sg.In(str(limits['angmin']), size=5, font=small_font, key='ang_min'),
                    sg.Text("to", size=2, font=small_font),
                    sg.In(str(limits['angmax']), size=5, font=small_font, key='ang_max'),
                    sg.Push()
                ]
            ], expand_x=True),
            sg.VerticalSeparator(),
            sg.Column([
                [ sg.Text("Set Delay-line Sums, Raw Only (ns):",font=small_font) ],
                [
                    sg.Text("    Sum X (avg, std):", size=16, font=small_font),
                    sg.In(str(system.getTDCInfo(key='sumX')),size=(5,1),disabled=True,font=small_font, key='sumx_avg'),
                    sg.In(str(system.getTDCInfo(key='sigmaSumX')),size=(5,1),disabled=True,font=small_font, key='sumx_std')
                ],
                [
                    sg.Text("    Sum Y (avg, std):", size=16, font=small_font),
                    sg.In(str(system.getTDCInfo(key='sumY')),size=(5,1),disabled=True,font=small_font, key='sumy_avg'),
                    sg.In(str(system.getTDCInfo(key='sigmaSumY')),size=(5,1),disabled=True,font=small_font, key='sumy_std')
                ],
                [sg.Checkbox("Allow Recovered Positions?", default=settings['allowRecovered'], font=small_font, key='allow_recovered_check')],
                [sg.VPush()]
        ])
        ]
    ], expand_x=True, font=large_font)

    fit_layout = sg.Frame("Fitting", [
        [
            sg.Text("All parameters are relative to the center: ", font=small_font), sg.Text("(None,None)", font=small_font, key='centerText'),
            sg.Combo(values=list(fitmethod_dict.keys()), expand_x=True, readonly=True, default_value=settings['fitfunctionkey'], key='fit_method')
        ],
        [
            sg.Column([
                [sg.Text("Name", justification='center', font=small_font, expand_x=True)],
                [sg.Text("Param 1:", justification='right', font=small_font, size=(10,1), expand_x=True, key='param1name')], 
                [sg.Text("Param 3:", justification='right', font=small_font, size=(10,1), expand_x=True, key='param3name')], 
                [sg.Text("Param 5:", justification='right', font=small_font, size=(10,1), expand_x=True, key='param5name')], 
                [sg.Text("Param 7:", justification='right', font=small_font, size=(10,1), expand_x=True, key='param7name')]
            ]),
            sg.Column([
                [sg.Text("Value", justification='center', font=small_font, expand_x=True)],
                [sg.Input(size=(7,1), font=small_font, expand_x=False, disabled=False, key='param1value')], 
                [sg.Input(size=(7,1), font=small_font, expand_x=False, disabled=False, key='param3value')], 
                [sg.Input(size=(7,1), font=small_font, expand_x=False, disabled=False, key='param5value')], 
                [sg.Input(size=(7,1), font=small_font, expand_x=False, disabled=False, key='param7value')]
            ]),
            sg.Column([
                [sg.Text("Error", justification='center', font=small_font, expand_x=True)],
                [sg.Input(size=(5,1), font=small_font, expand_x=False, disabled=True, key='param1error')], 
                [sg.Input(size=(5,1), font=small_font, expand_x=False, disabled=True, key='param3error')], 
                [sg.Input(size=(5,1), font=small_font, expand_x=False, disabled=True, key='param5error')], 
                [sg.Input(size=(5,1), font=small_font, expand_x=False, disabled=True, key='param7error')]
            ]),
            sg.VerticalSeparator(),
            sg.Column([
                [sg.Text("Name", justification='center', font=small_font, expand_x=True)],
                [sg.Text("Param 2:", font=small_font, justification='right', size=(10,1), expand_x=True, key='param2name')], 
                [sg.Text("Param 4:", font=small_font, justification='right', size=(10,1), expand_x=True, key='param4name')], 
                [sg.Text("Param 6:", font=small_font, justification='right', size=(10,1), expand_x=True, key='param6name')], 
                [sg.Text("Param 8:", font=small_font, justification='right', size=(10,1), expand_x=True, key='param8name')]
            ]),
            sg.Column([
                [sg.Text("Value", justification='center', font=small_font, expand_x=True)],
                [sg.Input(size=(7,1), font=small_font, expand_x=False, disabled=False, key='param2value')], 
                [sg.Input(size=(7,1), font=small_font, expand_x=False, disabled=False, key='param4value')], 
                [sg.Input(size=(7,1), font=small_font, expand_x=False, disabled=False, key='param6value')], 
                [sg.Input(size=(7,1), font=small_font, expand_x=False, disabled=False, key='param8value')]
            ]),
            sg.Column([
                [sg.Text("Error", justification='center', font=small_font, expand_x=True)],
                [sg.Input(size=(5,1), font=small_font, expand_x=False, disabled=True, key='param2error')], 
                [sg.Input(size=(5,1), font=small_font, expand_x=False, disabled=True, key='param4error')], 
                [sg.Input(size=(5,1), font=small_font, expand_x=False, disabled=True, key='param6error')], 
                [sg.Input(size=(5,1), font=small_font, expand_x=False, disabled=True, key='param8error')]
            ]),
        ],
        [ sg.Push(), sg.Button("Fit", size=(25,1), font=med_font,  key='fit'), sg.Push() ]
    ], expand_x=True, font=large_font)

    # Build the final layout
    left_layout = sg.Column([
        [sg.Push(), sg.Text("Current File", expand_x=True, font=xlarge_font, key='currentfile'), sg.Push()],
        [sg.Canvas(size=mcp_canvsize, key='mcpcanvas')],
        [
            sg.Push(), 
            sg.Button("Image", pad=(0,0), font=small_font, key='image_button'), 
            sg.Button("Contour", pad=(0,0), font=small_font, key='contour_button'), 
            sg.Button("Scatter", pad=(0,0), font=small_font, key='scatter_button'), 
            sg.Push(),
            sg.Checkbox("Hide Rejected Points?", default=False, font=small_font, key='hide_rejected_points'),
            sg.Push()
        ],
        [sg.Canvas(size=tof_canvsize, key='tofcanvas')],
        [
            sg.Text("TOF Window Range:", font=small_font), 
            sg.Input(settings['tofwindowmin'], font=small_font, size=(8), key='tof_window_min'), 
            sg.Text("to", font=small_font), 
            sg.Input(settings['tofwindowmax'], font=small_font, size=(8), key='tof_window_max')]
    ])

    right_layout = sg.Column([
        [ang_spectrum],
        [filters_layout],
        [cluster_layout], 
        [fit_layout],
        [
            sg.Push(), 
            sg.Button("Reset", size=(10,2), key='reset'),  
            sg.Button("Ignore", size=(10,2), key='ignore'),  
            sg.Button("Save Fit", size=(10,2), key='save'),
            sg.Push()
        ]
    ])

    info_layout = sg.Column([[
        sg.Frame("File Information",[
            [sg.Multiline("", size=(40,15), disabled=True, expand_x=True, no_scrollbar=True, font='Courier '+small_font_size, key='file_info')]
        ], expand_x=True, font=large_font)
    ]])

    return [ [left_layout, right_layout, info_layout] ], mcp_fig, tof_fig, ang_fig

def __draw_figure__(canvas, figure, loc=(0, 0)):
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    return figure_canvas_agg

def __plot_mcp_data__(mcp_fig, mcp_ax, mcp_fig_agg, XY_pos, XY_noise, fitplotter, system, limits, settings, filter_center=[0,0], n_bins=50):
    mcp_ax.cla()
    mcp_ax.xaxis.tick_top()
    mcp_ax.set_position([0.08, 0.08, 0.88, 0.85]) # left, bot, width, height
    
    __plot_MCP_boundary__(mcp_ax, system)

    range = [[limits['xmin']-1, limits['xmax']+1], [limits['ymin']-1, limits['ymax']+1]]
    extent = (limits['xmin']-1, limits['xmax']+1, limits['ymin']-1, limits['ymax']+1)

    # We need to check if either the cluster and/or noise has no data. If so, need to handle the errors
    position_data, noise_data = np.zeros((n_bins,n_bins)), np.zeros((n_bins,n_bins))
    if XY_pos.size > 0 and XY_noise.size > 0: # both cluster and noise positions
        position_data, xbins, ybins = np.histogram2d(XY_pos[:,1], XY_pos[:,0], bins=n_bins, range=range)
        noise_data, xbins, ybins = np.histogram2d(XY_noise[:,1], XY_noise[:,0], bins=n_bins, range=range)
    elif XY_pos.size > 0: # pure cluster--no noise
        position_data, xbins, ybins = np.histogram2d(XY_pos[:,1], XY_pos[:,0], bins=n_bins, range=range)
    elif XY_noise.size > 0: # pure noise--no clusters
        noise_data, xbins, ybins = np.histogram2d(XY_noise[:,1], XY_noise[:,0], bins=n_bins, range=range)
    # If the user wants to hide the rejected points, update that here
    if settings['hideRejectedPoints']: noise_data *= 0
    # Find the difference of the position and noise data
    # Use the position data to form a 'map', and ignore noise on this map
    # That way, ANY position data is shown, and unique noise is still highlighted.
    # This is more truthful to the real data that we are analysing
    nonzero_pos_idrow, nonzero_pos_idcol = np.where(position_data > 0)
    noise_data[nonzero_pos_idrow, nonzero_pos_idcol] = 0
    pos_minus_noise = position_data-noise_data
    pos_minus_noise[pos_minus_noise == 0] = np.nan # This way the colorbar won't show up for zeros

    my_cmap = get_mcp_colormap(np.amax(position_data), np.amax(noise_data))
    if settings['plotstyle'] == 'contour':
        xcenters, ycenters = 0.5*(xbins[1:] + xbins[:-1]), 0.5*(ybins[1:] + ybins[:-1])
        CS = mcp_ax.contourf(xcenters, ycenters, pos_minus_noise, levels=30, cmap=my_cmap, origin='lower', extent=extent)
    elif settings['plotstyle'] == 'image':
        CS = mcp_ax.imshow(pos_minus_noise, cmap=my_cmap, origin='lower', extent=extent)
    else: # by default just do a scatter plot
        if XY_pos.size > 0: mcp_ax.plot(XY_pos[:,0], XY_pos[:,1], 'o', markeredgewidth='0', markersize=4, alpha=0.3, color='red', label='Data')
        if XY_noise.size > 0 and not settings['hideRejectedPoints']:
            mcp_ax.plot(XY_noise[:,0], XY_noise[:,1], 'o', markeredgewidth='0', markersize=4, alpha=0.3, color='grey', label='Noise')
        mcp_ax.legend()
        my_cmap = None
    
    # Update the colorbar if using a colormap
    try: mcp_fig.delaxes(mcp_fig.axes[1])
    except: pass
    if settings['plotstyle'] == 'contour' or settings['plotstyle'] == 'image':
        divider = make_axes_locatable(mcp_ax)
        cax = divider.append_axes('bottom', size='3%', pad=0.05)
        temp = position_data-noise_data
        abs_min, abs_max = np.amin(temp), np.amax(temp)
        if abs_min == 0 and abs_max == 0: bounds = np.arange(-1.5,2,1)
        elif len(XY_noise) == 0 and len(XY_pos) > 0: bounds = np.arange(0.5, abs_max+1, 1) # No noise present
        elif len(XY_noise) > 0 and len(XY_pos) == 0: bounds = np.arange(abs_min-0.5, 0, 1) # only noise present
        elif len(XY_noise) == 0 and len(XY_pos) == 0: bounds = np.arange(-1.5,2,1) # no noise or signal
        else: bounds = np.arange(abs_min-0.5, abs_max+1, 1) # both noise and position are present

        norm = matplotlib.colors.BoundaryNorm(bounds, my_cmap.N)
        cbar_ticks = np.linspace( abs_min, abs_max, 9)
        cbar_ticks = list(set([int(t) for t in cbar_ticks]))
        cbar = mcp_fig.colorbar(matplotlib.cm.ScalarMappable(norm=norm, cmap=my_cmap), cax=cax, shrink=0.9, orientation='horizontal', ticks=cbar_ticks)

    if not fitplotter == None:
        fitplotter(mcp_ax)

    __plot_filter_locations__(mcp_fig, mcp_ax, system, limits, settings, filter_center=filter_center)
    
    mcp_ax.set_xlim(xmin=limits['xmin']-1, xmax=limits['xmax']+1)
    mcp_ax.set_ylim(ymin=limits['ymin']-1, ymax=limits['ymax']+1)
    mcp_fig_agg.draw()

def __plot_MCP_boundary__(ax, system, user_font_scale=1):
    """
    Draws the MCP boundary on the plot as well as directions of f+/f-
    """
    # draw the initial plot in the window
    MCPBounds = plt.Circle((0.0,0.0), system.getRadiusMCP(), color='cadetblue', linewidth=4, fill=False)
    ax.add_patch(MCPBounds)
    style = "Simple, tail_width=3, head_width=10, head_length=12"
    kw = dict(arrowstyle=style, color="r")
    scale = system.getRadiusMCP()/15
    a3 = patches.FancyArrowPatch((-13.27*scale, -7*scale), (-7*scale, -13.27*scale), connectionstyle="arc3,rad=.149", **kw)
    a4 = patches.FancyArrowPatch((13.27*scale, -7*scale), (7*scale, -13.27*scale), connectionstyle="arc3,rad=-.149", **kw)
    ax.add_patch(a3)
    ax.add_patch(a4)
    if system.getDirectionOfRadialMotion() > 0: # redcyc motion is in the counterclockwise direction
        ax.text(-12*scale, -12*scale, r"$\mathcal{\phi}_{p}$", ha='center', va='center', rotation=0, color='k', fontsize=int(user_font_scale*25))
        ax.text(12*scale, -12*scale, r"$\mathcal{\phi}_{m}$", ha='center', va='center', rotation=0, color='k', fontsize=int(user_font_scale*25))
        ax.text(12*scale, -14*scale, r"$(apparent)$", ha='center', va='center', rotation=0, color='k', fontsize=int(user_font_scale*14))
    else: # redcyc motion is in the clockwise direction
        ax.text(-12*scale, -12*scale, r"$\mathcal{\phi}_{m}$", ha='center', va='center', rotation=0, color='k', fontsize=int(user_font_scale*25))
        ax.text(-12*scale, -14*scale, r"$(apparent)$", ha='center', va='center', rotation=0, color='k', fontsize=int(user_font_scale*12))
        ax.text(12*scale, -12*scale, r"$\mathcal{\phi}_{p}$", ha='center', va='center', rotation=0, color='k', fontsize=int(user_font_scale*25))

def __plot_filter_locations__(fig, ax, system, limits, settings, filter_center=[0,0]):
    apply_ang_filter = settings['angularfilter']
    if (limits['angmin']%360) == (limits['angmax']%360): apply_ang_filter = False
    apply_radial_filter = settings['radialfilter']
    if (limits['radialmin'] == limits['radialmax']): apply_radial_filter = False

    # Wedge width: inner_radius=r-width, outer_radius=r
    if apply_radial_filter and apply_ang_filter:
        filterWedgeA = patches.Wedge(
            filter_center, 2.5*system.getRadiusMCP(), 
            limits['angmax'], limits['angmin'], 
            facecolor='indianred', edgecolor=None, 
            fill=True, alpha=0.3, linewidth=None)
        filterWedgeB = patches.Wedge(
            filter_center, 2.5*system.getRadiusMCP(), 
            limits['angmin'], limits['angmax'], 
            width=(2.5*system.getRadiusMCP()-limits['radialmax']), 
            facecolor='indianred', edgecolor=None, 
            fill=True, alpha=0.3, linewidth=None)
        filterWedgeC = patches.Wedge(
            filter_center, limits['radialmin'], 
            limits['angmin'], limits['angmax'], 
            facecolor='indianred', edgecolor=None, 
            fill=True, alpha=0.3, linewidth=None)
        ax.add_patch(filterWedgeA)
        ax.add_patch(filterWedgeB)
        ax.add_patch(filterWedgeC)
    elif apply_radial_filter:
        filterWedgeA = patches.Wedge(
            filter_center, limits['radialmin'], 
            0, 360, 
            facecolor='indianred', edgecolor=None, 
            fill=True, alpha=0.3, linewidth=None)
        filterWedgeB = patches.Wedge(
            filter_center, 2.5*system.getRadiusMCP(), 
            0, 360, width=(2.5*system.getRadiusMCP()-limits['radialmax']),
            facecolor='indianred', edgecolor=None, 
            fill=True, alpha=0.3, linewidth=None)
        ax.add_patch(filterWedgeA)
        ax.add_patch(filterWedgeB)
    elif apply_ang_filter:
        filterWedge = patches.Wedge(
            filter_center, 2.5*system.getRadiusMCP(), 
            limits['angmax'], limits['angmin'], 
            facecolor='indianred', edgecolor=None, 
            fill=True, alpha=0.3, linewidth=None)
        ax.add_patch(filterWedge)

def __plot_tof_spectrum__(tof_fig, tof_ax, tof_ax2, tof_fig_agg, datafile, limits, settings):
    """
    Plots the TOF spectrum histogram
    
    Args: 
        cspp_out: datafile containing all time/position data
        mca_min: the minimum mca bin/tof value to filter data
        mca_max: the maximum mca bin/tof value to filter data
        tof_min_view: the minimum tof to show in the plot
        tof_max_view: the maximum tof to show in the plot
    """
    tof_min_view, tof_max_view = limits['tofminview'], limits['tofmaxview']
    tof_ax.cla()
    
    __plot_mca_gate__(tof_fig, tof_ax, tof_ax2, tof_fig_agg, limits, settings)
    # First need to extract all tofs into one list
    all_tofs = datafile.getToFs()
    if len(all_tofs) > 0:
        try:
            bins = np.arange(tof_min_view, tof_max_view, 0.01)
            heights, bins = np.histogram(all_tofs, bins=bins)
            plot_x, plot_y = get_hist_plot_points(heights, bins)
            tof_ax.plot(plot_x, plot_y, '-', color='dimgray', zorder=1)
            tof_ax.set_xlim([tof_min_view,tof_max_view])
            tof_ax.set_ylim(ymin=0, ymax=np.amax(plot_y)+1)
        except: pass
    else: tof_ax.plot([tof_min_view,tof_max_view], [0,0], color='dimgray', zorder=1)
    tof_ax.set_xlabel(r"TOF ($\mu$s)")
    tof_fig_agg.draw()

def __plot_mca_gate__(tof_fig, tof_ax, tof_ax2, tof_fig_agg, limits, settings):
    """
    Plots the mca spectrum limits
    
    Args: 
        mca_min: the minimum mca bin/tof value to filter data
        mca_max: the maximum mca bin/tof value to filter data
        tof_min_view: the minimum tof to show in the plot
        tof_max_view: the maximum tof to show in the plot
    """
    mca_min, mca_max, tof_min_view, tof_max_view = limits['tofmin'], limits['tofmax'], settings['tofwindowmin'], settings['tofwindowmax']

    tof_ax2.cla()
    mca_ymax = tof_ax.get_ylim()[1]
    tof_ax2.fill_between([tof_min_view, mca_min], [0,0], [mca_ymax,mca_ymax], color='indianred', alpha=0.3, zorder=0)
    tof_ax2.fill_between([mca_max, tof_max_view], [0,0], [mca_ymax,mca_ymax], color='indianred', alpha=0.3, zorder=0)
    tof_ax2.set_xlim([tof_min_view,tof_max_view])
    tof_ax2.set_ylim(tof_ax.get_ylim())
    tof_ax2.get_yaxis().set_visible(False)
    tof_fig_agg.draw()

def __plot_ang_spectrum__(ang_fig, ang_ax, ang_fig_agg, positions, ang_start=0, ang_end=360):
    """
    Plots the angular spectrum histogram
    
    Args: 
        angles_list (1D list): angles for the filtered positions, in deg
        ang_min: the minimum angle to show in plot
        ang_max: the maximum angle to show in plot
    """
    if len(positions) < 3:
        return
    
    ang_ax.cla()
    ang_ax.grid()
    
    rho, phi = cart2pol(positions[:,0], positions[:,1])
    phi = (phi * 180/np.pi) % 360

    # check if there is a zero crossing
    if ang_start > ang_end:
        bins1, bins2 =  np.arange(ang_start, 360, 0.5), np.arange(0, ang_end, 0.5)
        vals1, _ = np.histogram(phi, bins=bins1)
        vals2, _ = np.histogram(phi, bins=bins2)
        vals = np.concatenate((vals1, vals2))
        bins = np.concatenate((bins1[:-1]-360, bins2))
        ang_ax.set_xlim(ang_start-360, ang_end)
    else:
        bins = np.arange(ang_start, ang_end, 0.5)
        vals, _ = np.histogram(phi, bins)
        ang_ax.set_xlim(ang_start, ang_end)

    plot_x, plot_y = get_hist_plot_points(vals, (bins[:-1]+bins[1:])/2)
    ang_ax.plot(plot_x, plot_y, '-', color='steelblue')
    ang_ax.set_xlabel("Angle (deg)")
    ang_fig_agg.draw()

def __extract_new_settings__(settings, window_values):
    # Extract out the filter settings and update the plot
    settings['toffilter'] = window_values['tof_bounds_check']
    settings['zfilter'] = window_values['z_limit_check']
    settings['radialfilter'] = window_values['radial_check']
    settings['angularfilter'] = window_values['ang_check']
    settings['allowRecovered'] = window_values['allow_recovered_check'] 
    settings['overrideFilterCenter'] = window_values['override_filter_center']
    settings['fitfunctionkey'] = window_values['fit_method']
    settings['hideRejectedPoints'] = window_values['hide_rejected_points']
    settings['clustertype'] = window_values['cluster_combo'].lower()
    try: settings['clusterthresh'] = float(window_values['threshold'])
    except: pass
    try: settings['clustersmooth'] = float(window_values['smoothing'])
    except: pass
    try: settings['tofwindowmin'] = float(window_values['tof_window_min'])
    except: pass
    try: settings['tofwindowmax'] = float(window_values['tof_window_max'])
    except: pass
    try: settings['filterCenterXY'][0] = float(window_values['filter_center_x'])
    except: pass
    try: settings['filterCenterXY'][1] = float(window_values['filter_center_y'])
    except: pass

    return settings

def __extract_new_limits__(limits, window_values):
    try: 
        limits['tofmin'] = float(window_values['tof_min'])
    except: 
        pass
    try: 
        limits['tofmax'] = float(window_values['tof_max'])
    except: 
        pass
    try: 
        limits['zmax'] = float(window_values['z_max'])
    except: 
        pass
    try:
        limits['angmin'] = float(window_values['ang_min'])
    except: 
        pass
    try:    
        limits['angmax'] = float(window_values['ang_max'])
    except: 
        pass
    try:    
        limits['radialmin'] = float(window_values['radial_min'])
    except: 
        pass
    try:    
        limits['radialmax'] = float(window_values['radial_max'])
    except: 
        pass

    return limits

def __extract_param_values__(paramval_dict, window_values):
    try: 
        paramval_dict['param1'] = float(window_values['param1value'])
    except: 
        paramval_dict['param1'] = None
    try: 
        paramval_dict['param2'] = float(window_values['param2value'])
    except: 
        paramval_dict['param2'] = None
    try: 
        paramval_dict['param3'] = float(window_values['param3value'])
    except: 
        paramval_dict['param3'] = None
    try:
        paramval_dict['param4'] = float(window_values['param4value'])
    except: 
        paramval_dict['param4'] = None
    try:    
        paramval_dict['param5'] = float(window_values['param5value'])
    except: 
        paramval_dict['param5'] = None
    try:    
        paramval_dict['param6'] = float(window_values['param6value'])
    except: 
        paramval_dict['param6'] = None
    try:    
        paramval_dict['param7'] = float(window_values['param7value'])
    except: 
        paramval_dict['param7'] = None
    try:    
        paramval_dict['param8'] = float(window_values['param8value'])
    except: 
        paramval_dict['param8'] = None
    
    return paramval_dict

def __extract_param_errors__(paramerr_dict, window_values):
    try: 
        paramerr_dict['param1err'] = float(window_values['param1error'])
    except: 
        paramerr_dict['param1err'] = None
    try: 
        paramerr_dict['param2err'] = float(window_values['param2error'])
    except: 
        paramerr_dict['param2err'] = None
    try: 
        paramerr_dict['param3err'] = float(window_values['param3error'])
    except: 
        paramerr_dict['param3err'] = None
    try:
        paramerr_dict['param4err'] = float(window_values['param4error'])
    except: 
        paramerr_dict['param4err'] = None
    try:    
        paramerr_dict['param5err'] = float(window_values['param5error'])
    except: 
        paramerr_dict['param5err'] = None
    try:    
        paramerr_dict['param6err'] = float(window_values['param6error'])
    except: 
        paramerr_dict['param6err'] = None
    try:    
        paramerr_dict['param7err'] = float(window_values['param7error'])
    except: 
        paramerr_dict['param7err'] = None
    try:    
        paramerr_dict['param8err'] = float(window_values['param8error'])
    except: 
        paramerr_dict['param8err'] = None
    
    return paramerr_dict

def __handle_cluster_button__(positions, settings, clusterObject):
    cluster_thresh, cluster_smooth = settings['clusterthresh'], settings['clustersmooth']
  
    if settings['clustertype'] == 'spot':
        _, _, cluster_map = clusterSpot(clusterObject, positions, cluster_thresh, cluster_smooth, return_map=True)
    else:
        _, _, cluster_map = clusterEllipse(clusterObject, positions, cluster_thresh, cluster_smooth, return_map=True)

    cluster_map = clusterObject.__apply_filters_to_map__(cluster_map, cluster_thresh, cluster_smooth)

    return cluster_map
