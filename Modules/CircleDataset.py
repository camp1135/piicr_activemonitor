"""
@Desc: This class holds all information relevant to the circle files.
       Each instance should have only files related to each other, 
       such as all being of the same ion. Trends WILL be infered and
       mixing together different ions may give incorrect results.
@Author: Scott Campbell, campbels@nscl.msu.edu
@Version: 22 February 2023
"""
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')
import copy

from .FitLinear import LinearFitter
from .PIICR_Utilities import calculateBirgeRatio, getSurroundingIdx, getNearestUncertainty

class CircleDataset:
    def __init__(self, datafile_list, interp_method='linear', print_results=True, plot_results=True):
        """
        Args:
            datafile_list (list of DataSet obj): list of all PI-ICR datafiles to be used as circles
            interp_method (str): how to interpolate the values: 'constant', 'linear' (in time), 'nearest' (weighted by time)
            print_results (bool): whether to print out the results of the dataset
        """
        #-----------------------------------------------------------
        # Sort datafile_list by their midtimes
        if len(datafile_list) == 0: raise ValueError("Dataset is empty. Cannot run any interpolation.")
        self.datafile_list = sorted(datafile_list, key=lambda x: x.getMidTime(), reverse=False)
        #-----------------------------------------------------------
        # extract the sorted midtimes, center x and y values
        self.midtimes = np.array([file.getMidTime() for file in datafile_list])
        self.centerX = np.array([file.getCenter()[0] for file in datafile_list])
        self.centerY = np.array([file.getCenter()[1] for file in datafile_list])
        self.centerXErr = np.array([file.getCenterErr()[0] for file in datafile_list])
        self.centerYErr = np.array([file.getCenterErr()[1] for file in datafile_list])
        #-----------------------------------------------------------
        # Check the requested interpolation method with the dataset size 
        if len(self.datafile_list) == 1 and not interp_method == 'constant':
            print("*** WARNING: Dataset only contains 1 sample. Defaulting interpolation method to \'constant\'.\n")
            self.interp_method = 'constant'
        elif len(self.datafile_list) < 3 and interp_method == 'linear':
            print("*** WARNING: Dataset contains less than 3 samples. Changing interpolation method from \'linear\' to \'constant\'.\n")
            self.interp_method = 'constant'
        else: self.interp_method = interp_method        
        #-----------------------------------------------------------
        # If the interpolation method is the linear regression, create a fit object for both the center x and center y
        if self.interp_method == 'linear':
            # Perform the linear regression for the center X values
            self.linearFit_centerX = LinearFitter()
            self.linearFit_centerX.fit(self.midtimes, self.centerX, self.centerXErr)
            # Perform the linear regression for the center Y values
            self.linearFit_centerY = LinearFitter()
            self.linearFit_centerY.fit(self.midtimes, self.centerY, self.centerYErr)
            if plot_results: self.__plotLinearFits__(self.linearFit_centerX, self.linearFit_centerY)
            if print_results:
                print("-"*50)
                print("Aggregate Dataset Fit:")
                print("-"*50)
                print("\tFitted Center X Slope [arb./hr]:", np.round(self.linearFit_centerX.slope*3600,6))
                print("\tFitted Center Y Slope [arb./hr]:", np.round(self.linearFit_centerY.slope*3600,6))
                print("")
        #-----------------------------------------------------------
        # If the interpolation method is the constant, determine the value by weighted average
        elif self.interp_method == 'constant':
            if len(self.datafile_list) == 1:
                self.constant_centerX = self.centerX[0]
                self.constant_centerY = self.centerY[0]
                self.constant_centerXErr = self.centerXErr[0]
                self.constant_centerYErr = self.centerYErr[0]
            else:
                x_weights, y_weights = np.divide(1, np.power(self.centerXErr, 2)), np.divide(1, np.power(self.centerYErr, 2))
                self.constant_centerX = np.sum( np.multiply(x_weights, self.centerX) )/np.sum(x_weights)
                self.constant_centerY = np.sum( np.multiply(y_weights, self.centerY) )/np.sum(y_weights)
                self.constant_centerXErr = 1 / np.sqrt( np.sum( x_weights ) )
                self.constant_centerYErr = 1 / np.sqrt( np.sum( y_weights ) )
    
            if plot_results: 
                self.__plotConstantValues__(self.midtimes, self.centerX, self.centerY, self.centerXErr, self.centerYErr, 
                                           self.constant_centerX, self.constant_centerY, self.constant_centerXErr, self.constant_centerYErr)
            if print_results:
                print("-"*50)
                print("Aggregate Dataset Fit:")
                print("-"*50)
                print("\tWeighted Center X:", np.round(self.constant_centerX,3), np.round(self.constant_centerXErr,3))
                print("\tWeighted Center y:", np.round(self.constant_centerY,3), np.round(self.constant_centerYErr,3))
                print("")
        #-----------------------------------------------------------
        # If the interpolation method is the nearest, determine the value using the two closest values (one on either side) weighted by time difference
        # This will be done on a case by case basis later in the code
        elif self.interp_method == 'nearest':
            if plot_results: self.__plotRawData__(self.midtimes, self.centerX, self.centerY, self.centerXErr, self.centerYErr)
        else: raise ValueError("The interpolation method was not one of the defined values... Expected \'constant\', \'linear\' or \'nearest_neighbors\'") 

    def interpCenter(self, midtime):
        """ Interpolates the circle center for a given time
        Args:
            midTime (float): the time for which the center should be interpolated at
        Returns:
            interpolated center (1d list: [x,y]), interp center err (1d list: [deltax, deltay])
        """
        if self.interp_method == 'constant':
            return [self.constant_centerX, self.constant_centerY], [self.constant_centerXErr,self.constant_centerYErr]
        elif self.interp_method == 'linear':
            fit_centerX, fit_centerXErr = self.linearFit_centerX.get_y(midtime)
            fit_centerY, fit_centerYErr = self.linearFit_centerY.get_y(midtime)
            return [fit_centerX, fit_centerY], [fit_centerXErr, fit_centerYErr]
        elif self.interp_method == 'nearest':
            # check if there are measurements on either side of the time
            # from the constructor, we are gauranteed to have at least two measurements
            lo_idx, hi_idx = getSurroundingIdx(midtime, self.midtimes)
            x1, y1 = self.centerX[lo_idx], self.centerY[lo_idx]
            x1err, y1err = self.centerXErr[lo_idx], self.centerYErr[lo_idx]
            t1 = self.midtimes[lo_idx]
            x2, y2 = self.centerX[hi_idx], self.centerY[hi_idx]
            x2err, y2err = self.centerXErr[hi_idx], self.centerYErr[hi_idx]
            t2 = self.midtimes[hi_idx]
            # Fit and get the predicted center x value
            slope_centerX = (x2-x1)/(t2-t1)
            inter_centerX = x1 - slope_centerX*t1
            pred_centerX = slope_centerX*midtime + inter_centerX
            pred_centerXErr = getNearestUncertainty(midtime, [t1,t2], [x1err,x2err])
            # Fit and get the predicted center y value
            slope_centerY = (y2-y1)/(t2-t1)
            inter_centerY = y1 - slope_centerY*t1
            pred_centerY = slope_centerY*midtime + inter_centerY
            pred_centerYErr = getNearestUncertainty(midtime, [t1,t2], [y1err,y2err])
            return [pred_centerX, pred_centerY], [pred_centerXErr, pred_centerYErr]

    def interpEllipseParams(self, midtime):
        """ 'Interpolates' the ellipse parameters by returning those at the closest
            time is used instead of a true linear interp (step fxn instead)
        Args:
            midtime (float): the time for which the center should be interpolated at
        Returns:
            'closest' ellipse params
        """
        # If there is only one start phase file, use that value!
        if len(self.datafile_list) == 1:
            return self.datafile_list[0].getEllipseParams()
        # If the mid time is shorter than min start phase file, use that value
        if midtime <= min(self.midtimes):
            return self.datafile_list[0].getEllipseParams()
        # If the mid time is larger than the max start phase file, use that value
        if midtime >= max(self.midtimes):
            return self.datafile_list[-1].getEllipseParams()
        # Otherwise use the interpolated slope and int to calculate the value
        idx = self.__getInterpIdx__(midtime) # index of interp params to use
        return self.datafile_list[idx].getEllipseParams()
    
    def __getInterpIdx__(self, midtime):
        for kk in range(len(self.midtimes)):
            if self.midtimes[kk] > midtime:
                return kk-1
        return 0
    
    def __plotLinearFits__(self, linearFit_centerX, linearFit_centerY):
        #------------------------------------------------------------
        # Plot the center X data and fit first
        if not linearFit_centerX.fit_completed: return 
        x_pred = np.linspace(np.amin(linearFit_centerX.x_data),np.amax(linearFit_centerX.x_data),500)
        y_pred = linearFit_centerX.slope * x_pred + linearFit_centerX.intercept  # Predicted y value at x_new
        se_pred = linearFit_centerX.__prediction_std_error__(x_pred)  # Standard error of prediction 
        y_lower = y_pred - se_pred
        y_upper = y_pred + se_pred

        # transform the epoch times to elapsed time in hrs
        x_pred -= np.amin(x_pred)
        x_pred /= 3600
        raw_x = copy.deepcopy(linearFit_centerX.x_data)
        raw_x -= np.amin(raw_x)
        raw_x /= 3600

        fig, axs = plt.subplots(1, 2, figsize=(12, 3))
        axs[0].errorbar(raw_x, linearFit_centerX.y_data, linearFit_centerX.y_errs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        axs[0].fill_between(x_pred, y_upper, y_lower, color='indianred', alpha=0.3)
        axs[0].plot(x_pred, y_pred, '--k')
        axs[0].set_xlabel("Measurement Time [hrs]")
        axs[0].set_ylabel("Center X Value [arb.]")

        #------------------------------------------------------------
        # Plot the center Y data and fit next
        if not linearFit_centerY.fit_completed: return 
        x_pred = np.linspace(np.argmin(linearFit_centerY.x_data),np.argmax(linearFit_centerY.x_data),500)
        y_pred = linearFit_centerY.slope * x_pred + linearFit_centerY.intercept  # Predicted y value at x_new
        se_pred = linearFit_centerY.__prediction_std_error__(x_pred)  # Standard error of prediction 
        y_lower = y_pred - se_pred
        y_upper = y_pred + se_pred

        # transform the epoch times to elapsed time in hrs
        x_pred -= np.amin(x_pred)
        x_pred /= 3600
        raw_x = copy.deepcopy(linearFit_centerY.x_data)
        raw_x -= np.amin(raw_x)
        raw_x /= 3600

        axs[1].errorbar(raw_x, linearFit_centerY.y_data, linearFit_centerY.y_errs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        axs[1].fill_between(x_pred, y_upper, y_lower, color='indianred', alpha=0.3)
        axs[1].plot(x_pred, y_pred, '--k')
        axs[1].set_xlabel("Measurement Time [hrs]")
        axs[1].set_ylabel("Center Y Value [arb.]")
        plt.show()

    def __plotConstantValues__(self, mid_times, centerX_data, centerY_data, centerXErrs, centerYErrs, centerX_wav, centerY_wav, centerX_wavErr, centerY_wavErr):
        # convert the times from epoch time to elapsed time in hrs
        raw_x = copy.deepcopy(mid_times)
        raw_x -= np.amin(raw_x)
        raw_x /= 3600
        
        fig, axs = plt.subplots(1, 2, figsize=(12, 3))

        birge, birge_err = calculateBirgeRatio(centerX_data, centerXErrs)
        axs[0].set_title('Birge Ratio: '+str(np.round(birge,3))+'('+str(np.round(birge_err,3))+')')
        axs[0].errorbar(raw_x, centerX_data, centerXErrs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        axs[0].plot([np.amin(raw_x), np.amax(raw_x)], [centerX_wav,centerX_wav], '--k')
        axs[0].fill_between([np.amin(raw_x), np.amax(raw_x)], centerX_wav+centerX_wavErr, centerX_wav-centerX_wavErr, color='indianred', alpha=0.3)
        axs[0].set_xlabel("Measurement Time [hrs]")
        axs[0].set_ylabel("Center X Value")

        birge, birge_err = calculateBirgeRatio(centerY_data, centerYErrs)
        axs[1].set_title('Birge Ratio: '+str(np.round(birge,3))+'('+str(np.round(birge_err,3))+')')
        axs[1].errorbar(raw_x, centerY_data, centerYErrs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        axs[1].plot([np.amin(raw_x), np.amax(raw_x)], [centerY_wav,centerY_wav], '--k')
        axs[1].fill_between([np.amin(raw_x), np.amax(raw_x)], centerY_wav+centerY_wavErr, centerY_wav-centerY_wavErr, color='indianred', alpha=0.3)
        axs[1].set_xlabel("Measurement Time [hrs]")
        axs[1].set_ylabel("Center Y Value")
        plt.show()

    def __plotRawData__(self, mid_times, centerX_data, centerY_data, centerXErrs, centerYErrs):
        # convert the times from epoch time to elapsed time in hrs
        raw_x = copy.deepcopy(mid_times)
        raw_x -= np.amin(raw_x)
        raw_x /= 3600
        
        fig, axs = plt.subplots(1, 2, figsize=(12, 3))

        birge, birge_err = calculateBirgeRatio(centerX_data, centerXErrs)
        axs[0].set_title('Birge Ratio: '+str(np.round(birge,3))+'('+str(np.round(birge_err,3))+')')
        axs[0].errorbar(raw_x, centerX_data, centerXErrs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        axs[0].set_xlabel("Measurement Time [hrs]")
        axs[0].set_ylabel("Center X Value")

        birge, birge_err = calculateBirgeRatio(centerY_data, centerYErrs)
        axs[1].set_title('Birge Ratio: '+str(np.round(birge,3))+'('+str(np.round(birge_err,3))+')')
        axs[1].errorbar(raw_x, centerY_data, centerYErrs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        axs[1].set_xlabel("Measurement Time [hrs]")
        axs[1].set_ylabel("Center Y Value")
        plt.show()
