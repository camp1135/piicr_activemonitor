"""
Code courtesy of: https://github.com/affanadly/Ellipse-Fit-Uncertainty/tree/main
References:
    
    Fitzgibbon, A., Pilu, M., & Fisher, R. B. (1996). Direct least square fitting of ellipses. IEEE Transactions on Pattern Analysis and Machine Intelligence, 21(5), 476-480). https://doi.org/10.1109/34.765658
    Halir, R., & Flusser, J. (1998). Numerically stable direct least squares fitting of ellipses. Retrieved from https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.1.7559
    Hammel, B., & Sullivan-Molina, N. (2020). bdhammel/least-squares-ellipse-fitting (v2.0.0) [Computer software]. https://doi.org/10.5281/ZENODO.3723294
    McDonald, K. T. (2014). Error Estimation in Fitting of Ellipses [Lab note]. https://scholar.google.com/citations?view_op=view_citation&hl=en&user=W9iFjWQAAAAJ&cstart=400&pagesize=100&citation_for_view=W9iFjWQAAAAJ:SgM-ki2adj0C
    Lebigot, E. O. (2019). Uncertainties: a Python package for calculations with uncertainties (v3.0.3) [Computer software]. Retrieved from http://pythonhosted.org/uncertainties/
    O'Leary, P. L., & Zsombor-Murray, P. J. (2004). Direct and specific least-square fitting of hyperbolæ and ellipses. Journal of Electronic Imaging, 13 (3), 492 - 503. https://doi.org/10.1117/1.1758951

Performs a least squares regression of the ellipse parameters
"""

import numpy as np
from uncertainties import ufloat,umath

class EllipseFitter:
    
    def __init__(self):
        pass

    def fit(self, x, y, conf_int=None):
        """ Fits a rotated and off-center ellipse using a numerically 
            stable least squares algorithm of Halir and Flusser, 
            "Numerically stable direct least squares fitting of ellipses'.
        Args:
            x (1D list): x values of the data points to fit
            y (1D list): y values of the data points to fit
            conf_int (float, default None): confidence interval to 
                calculate for the fitted parameters
        Returns:
            params (1D list): x-center, y-center, major axis, minor axis, phi (deg)
            uncs (1D list): fit uncertainties for the above parameters
          ***IF conf_int is defined:
            conf_up: param values for the upper bound of the confidence interval
            conf_down: param values for the lower bound of the confidence interval
        """
        if len(x) < 5 or len(y) < 5: raise Exception("Insufficient number of data points to perform an ellipse fit!")

        D1 = np.vstack([x**2,x*y,y**2]).T
        D2 = np.vstack([x,y,np.ones_like(x)]).T
        S1,S2,S3 = D1.T @ D1, D1.T @ D2, D2.T @ D2
        C1 = np.array([[0,0,2],[0,-1,0],[2,0,0]])
        M = np.linalg.inv(C1) @ (S1 - S2 @ np.linalg.inv(S3) @ S2.T)
        vec = np.linalg.eig(M)[1]
        cond = 4*(vec[0]*vec[2]) - vec[1]**2
        a1 = vec[:,np.nonzero(cond > 0)[0]]
        return np.vstack([a1,np.linalg.inv(-S3) @ S2.T @ a1]).flatten() # fitted coeffs a,b,c,d,e,f

    def errors(self, x, y, coeffs):
        """ Computes the errors for the ellipse parameters
        """
        z = np.vstack((x**2,x*y,y**2,x,y,np.ones_like(x)))
        numerator = np.sum(((coeffs @ z)-1)**2)
        denominator = (len(x)-6)*np.sum(z**2,axis=1)
        unc = np.sqrt(numerator/denominator)
        return tuple(ufloat(i,j) for i,j in zip(coeffs,unc))

    def convert(self, coeffs):
        """ Converts the ellipse coefficents from the general conic
            form (a,b,c,d,e,f) to the more familiar (x,y,a,b,theta)
            format
        """
        a,b,c,d,e,f = coeffs
        b /= 2
        d /= 2
        e /= 2
        x0 = (c*d - b*e) / (b**2 - a*c)
        y0 = (a*e - b*d) / (b**2 - a*c)
        center = (x0, y0)
        numerator = 2 * (a*e**2 + c*d**2 + f*b**2 - 2*b*d*e - a*c*f)
        denominator1 = (b*b-a*c)*((c-a)*umath.sqrt(1+4*b*b/((a-c)*(a-c)))-(c+a))
        denominator2 = (b*b-a*c)*((a-c)*umath.sqrt(1+4*b*b/((a-c)*(a-c)))-(c+a))
        major = umath.sqrt(numerator/denominator1) if numerator/denominator1 > 0 else 0
        minor = umath.sqrt(numerator/denominator2) if numerator/denominator2 > 0 else 0
        phi = .5*umath.atan((2*b)/(a-c))
        major, minor, phi = (major, minor, phi) if major > minor else (minor, major, np.pi/2+phi)
        return center, major, minor, phi

    def confidence_area(self, x, y, coeffs, f=1):
        """ Computes the confidence interval for the fitted params
        """
        c = coeffs
        res = c[0]*x**2 + c[1]*x*y + c[2]*y**2 + c[3]*x + c[4]*y + c[5]
        c_up = np.array([c[0],c[1],c[2],c[3],c[4],c[5] + f*np.std(res)])
        c_do = np.array([c[0],c[1],c[2],c[3],c[4],c[5] - f*np.std(res)])
        if self.convert(c_do) > self.convert(c_up):
            c_do, c_up = c_up, c_do
        return c_up, c_do


        
if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from matplotlib.patches import Ellipse

    def generate(a,b,center=(0,0),n=100,phi=0,weight=0.2):
        t = np.linspace(0,2*np.pi,n)[:-1]
        x = a*np.cos(t)*np.cos(phi) - b*np.sin(t)*np.sin(phi) + center[0]
        y = a*np.cos(t)*np.sin(phi) + b*np.sin(t)*np.cos(phi) + center[1]
        x += np.random.randn(len(t))*weight
        y += np.random.randn(len(t))*weight
        return x,y
    def ellipse(x,y,a,b,t):
        t *= (np.pi/180)
        phi = np.linspace(0,2*np.pi,100)
        x = a*np.cos(phi)*np.cos(t) - b*np.sin(phi)*np.sin(t) + x
        y = a*np.cos(phi)*np.sin(t) + b*np.sin(phi)*np.cos(t) + y
        return x, y
    def line(coeffs,n=100):
        t = np.linspace(0,2*np.pi,n)
        center,major,minor,phi = fitter.convert(coeffs)
        x = major*np.cos(t)*np.cos(phi) - minor*np.sin(t)*np.sin(phi) + center[0]
        y = major*np.cos(t)*np.sin(phi) + minor*np.sin(t)*np.cos(phi) + center[1]
        return x,y
    def artist(coeffs,*args,**kwargs):
        center,major,minor,phi = fitter.convert(coeffs)
        return Ellipse(xy=(center[0],center[1]),width=2*major,height=2*minor,
                        angle=np.rad2deg(phi),*args,**kwargs)
                        
    fitter = EllipseFitter()

    # generate a noisy ellipse with random parameters
    minor = np.random.uniform(1,5)
    major = np.random.uniform(minor,8)
    center = np.random.uniform(-5,5,size=2)
    angle = np.random.uniform(0,np.pi)
    print("Inputs:", major,minor,center,angle)

    x,y = generate(minor,major,center,100,angle)

    params = fitter.fit(x,y)
    params = fitter.errors(x,y,params)

    center, a, b, phi = fitter.convert(params)
    print("Fitted:", center, a, b, phi)

    c_up, c_do = fitter.confidence_area(x,y,[i.n for i in params],f=2)

    # plot of fit and confidence area
    fig,ax = plt.subplots()
    ax.plot(x,y,'xr',label='Data')
    ax.plot(*line([i.n for i in params]),'--b',lw=1,label='Fit')
    ax.add_patch(artist(c_up,ec='none',fc='b',alpha=0.2,label=r'2$\sigma$'))
    ax.add_patch(artist(c_do,ec='none',fc='white'))
    ax.legend()
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_aspect('equal')
    plt.show()
