from iminuit import Minuit
import numpy as np
from scipy.stats import norm, multivariate_normal, linregress
from scipy.stats._multivariate import _squeeze_output
from scipy.optimize import minimize

def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

class SpotFitter:
    """ The code in this class has been adapted from the work developed in the following paper:
    J. Karthein et. al, Analysis methods and code for very high-precision mass measurements of unstable isotopes, Computer Physics Communications (2021)
    https://www.sciencedirect.com/science/article/pii/S001046552100182X
    """
    
    def __init__(self):
        pass
    
    ##################################################################
    # Fitting methods
    ##################################################################

    def fit2DCartesianGaussian(self, data, dataErr=[0,0]):
        def NegLogLikelihood_2DGauss(meanx, meany, sigx, sigy, theta):
            '''Negative log likelihood function for (n=2)-dimensional Gaussian distribution for Minuit.'''
            cov = self.__Rot__(theta) @ np.array([[np.power(sigx,2)+np.power(dataErr[0],2),0],[0,np.power(sigy,2)+np.power(dataErr[1],2)]]) @ self.__Rot__(theta).T
            return( -np.sum(multivariate_normal.logpdf(x=data, mean=np.array([meanx, meany]), cov=cov, allow_singular=True)) )

        if len(data) < 5: raise Exception("Insufficient number of data points to perform 2D cartesian gaussian fit.")

        # minimize negative log likelihood function
        root_res = Minuit( NegLogLikelihood_2DGauss, 
                           meanx=self.__getStartParams2DCartGauss__(data)[0], 
                           meany=self.__getStartParams2DCartGauss__(data)[1],
                           sigx=self.__getStartParams2DCartGauss__(data)[2], 
                           sigy=self.__getStartParams2DCartGauss__(data)[3],
                           theta=self.__getStartParams2DCartGauss__(data)[4] )
        root_res.errors = (0.1, 0.1, 0.1, 0.1, 0.1) # initital step size
        root_res.limits =[(None, None), (None, None), (None, None), (None, None), (None, None)] # fit ranges
        root_res.errordef = Minuit.LIKELIHOOD # MLE definition (instead of Minuit.LEAST_SQUARES)
        root_res.migrad() # finds minimum of mle function
        root_res.hesse()  # computes errors

        param_dict = {
            'mean_x': root_res.values['meanx'], 
            'mean_y': root_res.values['meany'],
            'sigma_x': root_res.values['sigx'], 
            'sigma_y': root_res.values['sigy'],
            'rot_theta': np.degrees(root_res.values['theta']),
            'mean_x_err': root_res.errors['meanx'], 
            'mean_y_err': root_res.errors['meany'],
            'sigma_x_err': root_res.errors['sigx'], 
            'sigma_y_err': root_res.errors['sigy'],
            'rot_theta_err': np.degrees(root_res.errors['theta']),
            'n_points': len(data)
        }

        return param_dict

    def fit2DPolarGaussian(self, data, dataErr=[0,0]):
        """ Fits data to a 2D Gaussian in Polar Coordinates.
        Args:
            data (2D list): positions to fit, in POLAR coordinates
        
        """
        def NegLogLikelihood_2DGauss(meanrho, meanphi, sigrho, sigphi):
            '''Negative log likelihood function for (n=2)-dimensional Gaussian distribution for Minuit.'''
            cov = np.array([[np.power(sigrho,2)+np.power(dataErr[0],2),0],[0,np.power(sigphi,2)+np.power(dataErr[1],2)]])
            return( -np.sum(multivariate_normal.logpdf(x=data, mean=np.array([meanrho, meanphi]), cov=cov, allow_singular=True)) )

        if len(data) < 5: raise Exception("Insufficient number of data points to perform 2D polar gaussian fit.")

        # First, rotate the data such that the highest desity is at 180 deg
        data_x, data_y = pol2cart(data[:,0], data[:,1])
        mean_x, mean_y = np.mean(data_x), np.mean(data_y)
        offset_phi = np.pi - np.arctan2(mean_y, mean_x)
        data[:,1] = np.mod(data[:,1]+offset_phi, 2*np.pi)

        # minimize negative log likelihood function
        root_res = Minuit( NegLogLikelihood_2DGauss, 
                           meanrho=self.__getStartParams2DPolGauss__(data)[0], 
                           meanphi=self.__getStartParams2DPolGauss__(data)[1],
                           sigrho=self.__getStartParams2DPolGauss__(data)[2], 
                           sigphi=self.__getStartParams2DPolGauss__(data)[3])
        root_res.errors = (0.1, 0.1, 0.1, 0.1) # initital step size
        root_res.limits = [(None, None), (0, 2*np.pi), (None, None), (0, np.pi)] # fit ranges
        root_res.errordef = Minuit.LIKELIHOOD # MLE definition (instead of Minuit.LEAST_SQUARES)
        root_res.migrad() # finds minimum of mle function
        root_res.hesse()  # computes errors

        param_dict = {
            'mean_rho': root_res.values['meanrho'], 
            'mean_phi': root_res.values['meanphi']-offset_phi,
            'sigma_rho': root_res.values['sigrho'], 
            'sigma_phi': root_res.values['sigphi'],
            'mean_rho_err': root_res.errors['meanrho'], 
            'mean_phi_err': root_res.errors['meanphi'],
            'sigma_rho_err': root_res.errors['sigrho'], 
            'sigma_phi_err': root_res.errors['sigphi'],
            'n_points': len(data)
        }

        return param_dict

    def fit2DCartesianSkewNorm(self, data):
        def NegLogLikelihood_2DSkewNorm(meanx, meany, sigx, sigy, alpx, alpy):
            '''Negative log likelihood function for (n=2)-dimensional Gaussian distribution for Minuit.'''
            cov = np.array([[np.power(sigx,2),0],[0,np.power(sigy,2)]])
            return( -np.sum( multivariate_skewnorm(alpha=[alpx, alpy], mean=[meanx, meany], cov=cov).logpdf(x=data) ) )
        
        if len(data) < 5: raise Exception("Insufficient number of data points to perform 2D skewed cartesian gaussian fit.")

        # minimize negative log likelihood function
        root_res = Minuit( NegLogLikelihood_2DSkewNorm, 
                           meanx=self.__getStartParams2DCartSkewNorm__(data)[0], 
                           meany=self.__getStartParams2DCartSkewNorm__(data)[1],
                           sigx=self.__getStartParams2DCartSkewNorm__(data)[2], 
                           sigy=self.__getStartParams2DCartSkewNorm__(data)[3],
                           alpx=self.__getStartParams2DCartSkewNorm__(data)[4],
                           alpy=self.__getStartParams2DCartSkewNorm__(data)[5])
        root_res.errors = (0.1, 0.1, 0.1, 0.1, 0.1, 0.1) # initital step size
        root_res.limits =[(None, None), (None, None), (None, None), (None, None), (None, None), (None, None)] # fit ranges
        root_res.errordef = Minuit.LIKELIHOOD # MLE definition (instead of Minuit.LEAST_SQUARES)
        root_res.migrad() # finds minimum of mle function
        root_res.hesse()  # computes errors

        param_dict = {
            'mean_x': root_res.values['meanx'], 
            'mean_y': root_res.values['meany'],
            'sigma_x': root_res.values['sigx'], 
            'sigma_y': root_res.values['sigy'],
            'alpha_x': root_res.values['alpx'],
            'alpha_y': root_res.values['alpy'],
            'mean_x_err': root_res.errors['meanx'], 
            'mean_y_err': root_res.errors['meany'],
            'sigma_x_err': root_res.errors['sigx'], 
            'sigma_y_err': root_res.errors['sigy'],
            'alpha_x_err': root_res.errors['alpx'],
            'alpha_y_err': root_res.errors['alpy'],
            'n_points': len(data)
        }

        return param_dict
        
    ##################################################################
    # Methods for 1D theta fit
    ##################################################################

    # fit gaussian to time distribution using unbinned maximum likelihood estimation
    # def NegLogLikelihood_1DGauss(mean, sig):
    #     """Negative log likelihood function for (n=1)-dimensional Gaussian distribution."""
    #     return( -np.sum(norm.logpdf(x=data_t, loc=mean, scale=sig)))

    ##################################################################
    # Methods for 2D Gaussian fits in Cartesian/Polar Coordinates
    ##################################################################

    def __Rot__(self, theta):
        '''Rotation (matrix) of angle theta to cartesian coordinates.'''
        return np.array([[np.cos(theta), -np.sin(theta)],
                        [np.sin(theta), np.cos(theta)]])

    def __getStartParams2DCartGauss__(self, data):
        '''Starting parameter based on simple linear regression and 2D numpy array.'''

        def NegLogLikelihood_2DGauss_scipy(param):
            '''Negative log likelihood function for (n=2)-dimensional Gaussian distribution for SciPy.'''
            meanx, meany, sigx, sigy, theta = param
            cov = self.__Rot__(theta) @ np.array([[np.power(sigx,2),0],[0,np.power(sigy,2)]]) @ self.__Rot__(theta).T
            return( -np.sum(multivariate_normal.logpdf(x=data, mean=np.array([meanx, meany]), cov=cov, allow_singular=True)) )

        # simple linear regression to guess the rotation angle based on slope
        #slope, intercept, r_value, p_value, std_err = linregress(data[:, 0], data[:, 1])
        try: slope, yint = np.polyfit(data[:,0], data[:,1], 1)
        except: slope, yint = 1, 0
        theta_guess = -np.arctan(slope)
        # data rotated based on theta guess
        data_rotated_guess = np.dot(self.__Rot__(theta_guess), [data[:,0], data[:,1]])
        first_guess = np.array([data[:,0].mean()+0.2, # meanx
                                data[:,1].mean()+0.2, # meany
                                data_rotated_guess[:,0].std(), # sigma-x
                                data_rotated_guess[:,1].std(), # sigma-y
                                theta_guess]) # rot. angle based on slope of lin. reg.
        
        first_guess[first_guess==np.nan] = 0
        # based on a first guess, a minimization based on a robust simplex is performed
        start_par = minimize(NegLogLikelihood_2DGauss_scipy, first_guess, method='Nelder-Mead')
        return(start_par['x'])
    
    def __getStartParams2DPolGauss__(self, data):
        ''' Starting parameter based on simple linear regression and 2D numpy array.
        Args:
            data (2D list): data to fit, in POLAR coordinates!    
        '''

        def NegLogLikelihood_2DGauss_scipy(param):
            '''Negative log likelihood function for (n=2)-dimensional Gaussian distribution for SciPy.'''
            meanrho, meanphi, sigrho, sigphi = param
            cov = np.array([[np.power(sigrho,2),0],[0,np.power(sigphi,2)]])
            return( -np.sum(multivariate_normal.logpdf(x=data, mean=np.array([meanrho, meanphi]), cov=cov, allow_singular=True)) )

        # data rotated based on theta guess
        first_guess = np.array([data[:,0].mean()+0.2, # mean rho
                                data[:,1].mean()+0.2, # mean phi
                                data[:,0].std(), # sigma-rho
                                data[:,1].std()]) # sigma-phi
        
        first_guess[first_guess==np.nan] = 0
        # based on a first guess, a minimization based on a robust simplex is performed
        start_par = minimize(NegLogLikelihood_2DGauss_scipy, first_guess, method='Nelder-Mead')
        return(start_par['x'])

    def __getStartParams2DCartSkewNorm__(self, data):
        '''Starting parameter based on simple linear regression and 2D numpy array.'''
        def NegLogLikelihood_2DSkewNorm_scipy(param):
            '''Negative log likelihood function for (n=2)-dimensional Gaussian distribution for SciPy.'''
            meanx, meany, sigx, sigy, alpx, alpy = param
            cov = np.array([[np.power(sigx,2),0],[0,np.power(sigy,2)]])
            return( -np.sum( multivariate_skewnorm(alpha=[alpx, alpy], mean=[meanx, meany], cov=cov).logpdf(x=data) ) )

        # simple linear regression to guess the rotation angle based on slope
        slope, intercept, r_value, p_value, std_err = linregress(data[:, 0], data[:, 1])
        theta_guess = -np.arctan(slope)
        theta_guess = 0 
        # data rotated based on theta guess
        data_rotated_guess = np.dot(self.__Rot__(theta_guess), [data[:,0], data[:,1]])
        first_guess = np.array([data[:,0].mean()+0.2, # meanx
                                data[:,1].mean()+0.2, # meany
                                data[:,0].std(), # sigma-x
                                data[:,1].std(), # sigma-y
                                0, # alpha-x
                                0]) # alpha-y
        
        first_guess[first_guess==np.nan] = 0
        # based on a first guess, a minimization based on a robust simplex is performed
        start_par = minimize(NegLogLikelihood_2DSkewNorm_scipy, first_guess, method='Nelder-Mead')
        return(start_par['x'])


class multivariate_skewnorm:
    """ Probability density functions for a multivariate skew-normal distribution
        https://gregorygundersen.com/blog/2020/12/29/multivariate-skew-normal/
        https://stackoverflow.com/questions/52975883/creating-a-multivariate-skew-normal-distribution-python
    """
    
    def __init__(self, alpha, mean=None, cov=None):
        """ alpha is the shape/skewness parameter list (one value for each dimension)
        """
        self.dim   = len(alpha)
        self.alpha = np.asarray(alpha)
        self.mean  = np.zeros(self.dim) if mean is None else np.asarray(mean)
        self.cov   = np.eye(self.dim) if cov is None else np.asarray(cov)

    def pdf(self, x):
        return np.exp(self.logpdf(x))
        
    def logpdf(self, x):
        x    = multivariate_normal._process_quantiles(x, self.dim)
        pdf  = multivariate_normal(self.mean, self.cov).logpdf(x)
        cdf  = norm(0, 1).logcdf(np.dot(x, self.alpha))
        return np.log(2) + pdf + cdf

    def sample(self, size=1):
        std_mvn = multivariate_normal(np.zeros(self.dim), np.eye(self.dim))
        x = np.empty((size, self.dim))
        
        # Apply rejection sampling.
        n_samples = 0
        while n_samples < size:
            z = std_mvn.rvs(size=1)
            u = np.random.uniform(0, 2*std_mvn.pdf(z))
            if not u > self.pdf(z):
                x[n_samples] = z
                n_samples += 1
        
        # Rescale based on correlation matrix.
        chol = np.linalg.cholesky(self.cov)
        x = (chol @ x.T).T
        
        return x + self.mean




if __name__ == "__main__":
    import matplotlib.pyplot as plt

    fitter = SpotFitter()

    ###################################################################
    # Example of fitting a 2D Gaussian in Cartesian Coordinates
    ###################################################################


    ###################################################################
    # Example of fitting a 2D Gaussian in Polar Coordinates
    ###################################################################
    # rho_data, phi_data = np.random.normal(loc=5, scale=0.5, size=1000), np.random.normal(loc=np.pi, scale=np.pi/12, size=1000)
    # RP_data = np.transpose(np.concatenate(([rho_data],[phi_data]), axis=0))
    # x_data, y_data = pol2cart(rho_data, phi_data)
    # XY_data = np.transpose(np.concatenate(([x_data],[y_data]), axis=0))

    # param_dict = fitter.fit2DPolarGaussian(RP_data)
    # mean_rho, mean_phi, sigma_rho, sigma_phi = np.abs(param_dict['mean_rho']), param_dict['mean_phi']%(2*np.pi), param_dict['sigma_rho'], param_dict['sigma_phi']

    # print("Found parameters")
    # print("\tMean Rho, Sigma Rho:", mean_rho, sigma_rho)
    # print("\tMean Phi, Sigma Phi:", mean_phi, sigma_phi)

    # n_points = 500
    # t_list = np.linspace(0,2*np.pi,n_points)
    # phi_list = mean_phi + sigma_phi*np.cos(t_list)
    # rho_list = mean_rho + sigma_rho*np.sin(t_list)
    # x_list, y_list = pol2cart(rho_list, phi_list)

    # fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(5,5))
    # axes.plot(XY_data[:,0], XY_data[:,1], 'ok', alpha=0.2)
    # axes.plot(x_list, y_list, '-', color='indianred')
    # axes.set_aspect('equal', 'box')
    # plt.show()

    ###################################################################
    # Example of fitting a 2D Skew-norm in Cartesian Coordinates
    ###################################################################
    # mean_x, mean_y = 0.5, 0.5
    # sigma_x, sigma_y = 1.5, 1
    # alpha_x, alpha_y = 0, 0.2
    # cov = np.array([[np.power(sigma_x,2),0],[0,np.power(sigma_y,2)]])
    # mvs = multivariate_skewnorm(alpha=[alpha_x, alpha_y], mean=[mean_x, mean_y], cov=cov)
    # XY_data = mvs.sample(5000)
    
    # param_dict = fitter.fit2DCartesianSkewNorm(XY_data)
    # mean_x, mean_y, sigma_x, sigma_y, alpha_x, alpha_y = param_dict['mean_x'], param_dict['mean_y'], param_dict['sigma_x'], param_dict['sigma_y'], param_dict['alpha_x'], param_dict['alpha_y']

    # print("Found parameters")
    # print("\tMean x, Sigma x, Alpha x:", mean_x, sigma_x, alpha_x)
    # print("\tMean y, Sigma y, Alpha y:", mean_y, sigma_y, alpha_y)

    # fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(5,5))
    # axes.plot(XY_data[:,0], XY_data[:,1], 'ok', alpha=0.2)
    # # axes.plot(x_list, y_list, '-', color='indianred')
    # axes.set_aspect('equal', 'box')
    # plt.show()
