import os
import numpy as np
import warnings 

from .PyEva import MM8DataFile

""" Class to hold information for a TDC trigger event. A "Trigger Event" is defined here as
    all the signals recieved by the TDC between one start signal to a stop signal. 
    Each event is uniquely identified by the time of its start signal (in ns).
    """
class TriggerEvent:
    #############################################################################
    # Constructor
    #############################################################################
    def __init__(self, trigger_id, start_time, mcp_list=[], x1_list=[], x2_list=[], y1_list=[], y2_list=[]):
        # Raw information for the trigger event
        self.trigger_id = trigger_id
        self.start_time = start_time
        self.mcp_list   = np.array(mcp_list)
        self.x1_list    = np.array(x1_list)
        self.x2_list    = np.array(x2_list)
        self.y1_list    = np.array(y1_list)
        self.y2_list    = np.array(y2_list)

    #############################################################################
    # Public Methods
    #   User should call these methods to operate/access the stored information
    #############################################################################
    def sortIonHits(self, sum_x_bounds=[None,None], sum_y_bounds=[None,None]):
        """ Takes a trigger event and attempts to correlate an mcp signal with the 
            possible x/y anode signals in the event. If the user supplies limits for 
            the summd x/y anode signal times (relative to mcp hit) then this is used 
            as a gate. If no value is supplied, only x/y signals before the next mcp 
            hit are considered. 
            NOTE: the provided sum bounds will be updated to the time of the next mcp
                  hit IF that hit occurs before the specified bound time.
        Args: 
            sum_x_bounds (arr of float): time in ns for the minimum and maximum 
                                         allowable time for the x anode sum signal
            sum_y_bounds (arr of float): time in ns for the minimum and maximum 
                                         allowable time for the y anode sum signal
        Returns: list of dictionaries containing all x/y hit information for an mcp hit
        """
        hits_list = []
        for mcp_idx, mcp_time in enumerate(self.mcp_list):
            err_msgs = "Errors for Trigger ID "+str(self.trigger_id)+", MCP Hit #"+str(mcp_idx)+", MCP timestamp of "+str(mcp_time)+" ns:"
            can_find_pos = True # flag whether to try and sort hits for position determination
            #-------------------------------------------------------------------
            # Determine the min/max allowable times for the summed x/y signals
            try: next_mcp_time = self.mcp_list[mcp_idx+1]
            except: next_mcp_time = np.inf
            if None in sum_x_bounds: # no user specified values, use mcp hits as bounds
                temp_max_x = next_mcp_time 
            else: # use either the user bounds OR the next mcp hit, whichever is first
                temp_max_x = min( mcp_time+sum_x_bounds[1] , next_mcp_time )
                if temp_max_x == next_mcp_time: 
                    can_find_pos = False
                    err_msgs += "\n\tNext mcp hit overlaps with the specified x anode sum bounds. Cannot uniquely determine position."
            if None in sum_y_bounds: # no user specified values, use mcp hits as bounds
                temp_max_y = next_mcp_time
            else: # use either the user bounds OR the next mcp hit, whichever is first
                temp_max_y = min( mcp_time+sum_y_bounds[1] , next_mcp_time )
                if temp_max_y == next_mcp_time: 
                    can_find_pos = False
                    err_msgs += "\n\tNext mcp hit overlaps with the specified y anode sum bounds. Cannot uniquely determine position."
            #-------------------------------------------------------------------
            # Sort the MCP hits using the above time bounds for the signals
            if can_find_pos:
                x1_idx, x2_idx, x_errs, recover_x = self.__sortAnodeSignals__(mcp_time, self.x1_list, self.x2_list, [mcp_time,temp_max_x], anode_sum_bounds=sum_x_bounds, anode_label="x")
                y1_idx, y2_idx, y_errs, recover_y = self.__sortAnodeSignals__(mcp_time, self.y1_list, self.y2_list, [mcp_time,temp_max_y], anode_sum_bounds=sum_y_bounds, anode_label="y")
                for msg in x_errs: err_msgs += "\n\t"+msg
                for msg in y_errs: err_msgs += "\n\t"+msg
                # Check if the position can be found still, and update the flag if not
                if x1_idx == None and x2_idx == None: can_find_pos = False
                if y1_idx == None and y2_idx == None: can_find_pos = False
            else:
                recover_x, recover_y = False, False
                x1_idx, x2_idx, y1_idx, y2_idx = None, None, None, None
            #-------------------------------------------------------------------
            # Save the data to a dictionary
            curr_dict = {'start_time':self.start_time, 
                         'mcp_idx':mcp_idx, 
                         'x1_idx':x1_idx, 'x2_idx':x2_idx, 'recover_x':recover_x,
                         'y1_idx':y1_idx, 'y2_idx':y2_idx, 'recover_y':recover_y,
                         'can_reconstruct':can_find_pos, 'errors':err_msgs }
            hits_list.append(curr_dict)
        return hits_list

    #############################################################################
    # Pseudo-private Methods
    #   User should not call or modify these methods, unless they know what they
    #   are doing. These are called internally by the 'public' methods
    #############################################################################
    def __sortAnodeSignals__(self, mcp_time, anode1_times, anode2_times, anode_time_bounds, anode_sum_bounds=[None,None], anode_label="anode"):
        a1_idx, a2_idx = None, None
        err_msg_list = []
        recovery_flag = False
        #-------------------------------------------------------------------
        # Find all reasonable anode signals for an mcp hit
        #   This is a coarse filter, any signal outside these bounds can't
        #   be correlated to an ion hit. Some signals that meet the criteria
        #   may not belong to a true ion hit.
        a1_idx = np.where( (anode1_times >= anode_time_bounds[0]) & (anode1_times <= anode_time_bounds[1]) )[0]
        a2_idx = np.where( (anode2_times >= anode_time_bounds[0]) & (anode2_times <= anode_time_bounds[1]) )[0]
        #-------------------------------------------------------------------
        # Find all anode1/anode2 combinations for position determination
        a1_time_idx, a2_time_idx = None, None
        if a1_idx.size == 0 and a2_idx.size == 0: 
            err_msg_list.append("No "+anode_label+"1/"+anode_label+"2 signals for anode pair. Cannot determine position.")
        elif None in anode_sum_bounds and (a1_idx.size > 1 or a2_idx.size > 1):
            # If the user did not specify reasonable summed anode bounds, then 
            #   the position can only be found if there is only one of each signal
            err_msg_list.append("Too many possible summed "+anode_label+" times to determine unique position.")
        elif a1_idx.size > 0 and a2_idx.size > 0: 
            # get the sum time for all permutations of the indices
            a1a2_idx_perms = np.array([[ii,jj] for ii in a1_idx for jj in a2_idx])
            anode_sums = anode1_times[a1a2_idx_perms[:,0]] + anode2_times[a1a2_idx_perms[:,1]] - 2*mcp_time
            # if specified, get the indices where the sum is within limits
            anode_sum_ok = np.ones(anode_sums.size, dtype=bool)
            if not None in anode_sum_bounds:
                anode_sum_ok = anode_sum_ok & (anode_sums >= anode_sum_bounds[0])
                anode_sum_ok = anode_sum_ok & (anode_sums <= anode_sum_bounds[1])
            # get the indices in the master time lists that meet the sum criteria
            anode_sum_ok_idx = np.where(anode_sum_ok)[0]
            a1_sum_ok_idx, a2_sum_ok_idx = a1a2_idx_perms[anode_sum_ok_idx,0], a1a2_idx_perms[anode_sum_ok_idx,1]
            a1_unique_idx, a1_dup_counts = np.unique(a1_sum_ok_idx, return_counts=True)
            a2_unique_idx, a2_dup_counts = np.unique(a2_sum_ok_idx, return_counts=True)
            # add in any error warnings
            if len(anode_sum_ok_idx) == 0: 
                err_msg_list.append("No "+anode_label+"1/"+anode_label+"2 signal combinations yield reasonable summed values.")
            if len(a1_dup_counts) > 0 and np.amax(a1_dup_counts) > 1: 
                err_msg_list.append("Multiple "+anode_label+"2 signals yield reasonable summed values. Cannot uniquely determine position.")
            if len(a2_dup_counts) > 0 and np.amax(a2_dup_counts) > 1: 
                err_msg_list.append("Multiple "+anode_label+"1 signals yield reasonable summed values. Cannot uniquely determine position.")
            if len(a1_unique_idx) > 1 or len(a2_unique_idx) > 1: 
                err_msg_list.append("Multiple possible "+anode_label+" positions found for one mcp hit. Cannot determine position.")
            # save the unique position if no errors occured
            if len(err_msg_list) == 0: 
                a1_time_idx, a2_time_idx = a1_sum_ok_idx[0], a2_sum_ok_idx[0]
        elif (a1_idx.size > 0) or (a2_idx.size > 0):
            # Check if the anode1 signal can be used to recover the position
            if a1_idx.size == 1: 
                a1_time_idx = a1_idx[0]
                recovery_flag = True
            elif a1_idx.size > 1: 
                err_msg_list.append("Multiple "+anode_label+"1 signals present. Cannot recover the "+anode_label+" position uniquely.")
            # Check if the anode2 signal can be used to recover the position
            if a2_idx.size == 1: 
                a2_time_idx = a2_idx[0]
                recovery_flag = True
            elif a1_idx.size > 1: 
                err_msg_list.append("Multiple "+anode_label+"2 signals present. Cannot recover the "+anode_label+" position uniquely.")
        return a1_time_idx, a2_time_idx, err_msg_list, recovery_flag

""" Class to hold all relevent information for the PS-MCP data read by a TDC
    as well as the data saved by the MM8 program. Additionally, the positions 
    for data that can be calculated are stored here.
"""
class DataFile:
    #############################################################################
    # Constructor
    #############################################################################
    def __init__(self, filepath, filename, system, loadData=True):
        """ Constructor for the DataFile object
        Args:
            filepath (str): the location where the datafiles are stored, e.g. "C:/Desktop/"
            filename (str): the name of the datafile WITHOUT file extension, e.g. "myfile"
            system (System obj): the class object for the PI-ICR sytem used to collect the data
            loadData (bool, True): whether to load in the data immediately upon creation of the object
        """
        self.filepath = filepath
        self.filename = filename
        self.system = system
        # system variables for important properties of the datafile
        self.firstLoadFinished = False
        self.CurrentEventID = 0
        self.EventsList = np.array([]) # list of Event objects
        self.ReconstructedEvents = np.array([]) # 2D list, [trigger_id, z_count, trigger_time, tof, x_pos, y_pos, recovered_bool]
        self.UniqueShotTrigid = None # The trigger id number for each MM8 shot
        self.UniqueShotStartIdx = None # List of indices in reconstructed events where a new MM8 shot occured
        self.UniqueShotCounts = None # Number of hits in each MM8 shot
        self.UniqueShotTimes = None # Times for each trigger event
        # information for the position reconstruction
        self.AbsMinTOF, self.AbsMaxTOF = self.system.getAbsTofLimits()
        self.TDC_Info = self.system.getTDCInfo()
        self.SumX, self.SumY, self.SigmaSumX, self.SigmaSumY = self.TDC_Info["sumX"], self.TDC_Info["sumY"], self.TDC_Info["sigmaSumX"], self.TDC_Info["sigmaSumY"]
        if not (self.SumX==None or self.SigmaSumX==None): self.SumX_Bounds = [self.SumX-5*self.SigmaSumX, self.SumX+5*self.SigmaSumX]
        else: self.SumX_Bounds = [None, None]
        if not (self.SumY==None or self.SigmaSumY==None): self.SumY_Bounds = [self.SumY-5*self.SigmaSumY, self.SumY+5*self.SigmaSumY]
        else: self.SumY_Bounds = [None, None]
        # Information about the MM8 settings that are being used
        self.mm8DataFile = MM8DataFile()
        self.redcyc_accum_loc = None
        self.mag_accum_loc = None
        # If the user wants to load the data, do it now
        if loadData and not filename == None: self.updateDataFile()
   
    #############################################################################
    # Public Methods
    #   User should call these methods to operate/access the stored information
    #############################################################################
    def loadData(self, start_line=0):
        """ Reads the data written by the TDC and reconstructs the events to position & tof information
        Args:
            start_line (str): integer of line in file to start reading from. Will continue reading until blank line
        Returns: (int) last line number in the file which was read and saved in a trigger event
        """
        full_path = os.path.join(self.filepath, os.path.splitext(self.filename)[0]+self.system.getDatafileExt())
        if not os.path.isfile(full_path):
            raise FileNotFoundError("The file "+str(full_path)+" does not exist. Please check the location.")
        # Read in any new data
        tdc_info = self.system.getTDCInfo()
        new_data = [] # 2d list holding all the channel ids and timestamps
        try: 
            with warnings.catch_warnings(): # supress the empty file warning
                warnings.simplefilter("ignore")
                new_data = np.loadtxt(full_path, delimiter=',', dtype=float, skiprows=start_line)
        except: pass # try block suppresses a warning if datafile is completely empty    
        # If there is no new data, don't continue
        if len(new_data) < 5: # len 5 since that is the min num of signals for an event
            return start_line
        new_data[:,1] /= (1e3) # transform the trigger times from ps to ns
        # Get the events from the new data
        new_events, num_lines_read = self.__extractEventsInReadWindow__(new_data, self.CurrentEventID)
        self.EventsList += new_events
        # Update the CurrentEventID variable
        if len(self.EventsList) > 0: self.CurrentEventID = self.EventsList[-1].trigger_id
        else: self.CurrentEventID = 0
        # Get the reconstructions from the new data and append them to the list
        new_recons = self.__reconstructEventData__(new_events)
        if len(new_recons) > 0 and len(self.ReconstructedEvents) == 0: self.ReconstructedEvents = np.array(new_recons)
        elif len(new_recons) > 0: self.ReconstructedEvents = np.concatenate( (self.ReconstructedEvents, new_recons) )
        # Determine the indices of the unique hits
        if not len(self.ReconstructedEvents) == 0:
            unique_trigid, idx_start = np.unique(self.ReconstructedEvents[:,0], return_index=True)
            self.UniqueShotTrigid = unique_trigid
            self.UniqueShotStartIdx = idx_start
            self.UniqueShotCounts = self.ReconstructedEvents[idx_start,1] # Get the saved z count for each unique event 
            self.UniqueShotTimes = self.ReconstructedEvents[idx_start,2] # Get the trigger time for each unique event
            self.firstLoadFinished = True
        # return both the last line read in, and the last line used to make an event
        return start_line+num_lines_read
    
    def flushData(self):
        self.CurrentEventID = 0
        self.firstLoadFinished = False
        self.EventsList = []
        self.ReconstructedEvents = [] 
        self.UniqueTrigid = None
        self.UniqueIdxStart = None
        self.UniqueShotCounts = None
        self.mm8HeaderInfo = None
    
    def updateDataFile(self, start_line=0):
        """ Dummy method to be used by the Visualization GUI"""
        if start_line == 0:
            self.flushData()
            mm8_filepath = self.system.findMM8File(os.path.join(self.filepath, self.filename))
            self.mm8DataFile.loadFile(mm8_filepath)
        try:   
            lastStartLine = self.loadData(start_line=start_line)
            return lastStartLine
        except FileNotFoundError as e:
            return start_line

    #---------------------------------------------------------------------------
    # Getter methods for file/hit information
    #---------------------------------------------------------------------------
    def getFilePath(self): return self.filepath
    def getFileName(self): return self.filename
    def getAllEvents(self): return self.EventsList
    def getAllReconstructedEvents(self): return self.ReconstructedEvents
    def getZ(self, unique=True): 
        """ NOTE: will return the z count for each UNIQUE trigid """
        if len(self.ReconstructedEvents) == 0: return []
        if unique: return self.UniqueShotCounts
        else: return self.ReconstructedEvents[:,1]
    def getTriggerTimes(self, unique=True):
        """ NOTE: will return the trigger time for each UNIQUE trigid """
        if len(self.ReconstructedEvents) == 0: return []
        if unique: return self.UniqueShotTimes
        else: return self.ReconstructedEvents[:,2]
    def getTriggerIDs(self, unique=True):
        if len(self.ReconstructedEvents) == 0: return []
        if unique: return self.UniqueShotTrigid
        else: return self.ReconstructedEvents[:,0]
    def getUniqueTriggerIDsAndTimes(self):
        return self.UniqueShotTrigid, self.UniqueShotTimes
    def getPositions(self):
        if len(self.ReconstructedEvents) == 0: return []
        return self.ReconstructedEvents[:,4:6]
    def getToFs(self):
        if len(self.ReconstructedEvents) == 0: return []
        return self.ReconstructedEvents[:,3]
    def getRecoveredFlag(self, indices=None):
        if len(self.ReconstructedEvents) == 0: return []
        if not indices == None and len(indices) > 0: return self.ReconstructedEvents[indices,8] 
        return self.ReconstructedEvents[:,8]
    def getRecoveredCount(self, indices=None):
        if len(self.ReconstructedEvents) == 0: return 0
        if not indices == None and len(indices) > 0: return np.count_nonzero( self.ReconstructedEvents[indices,8] )
        return np.count_nonzero( self.ReconstructedEvents[:,8] )
    def getMissingX1Count(self, indices=None):
        if len(self.ReconstructedEvents) == 0: return 0
        if not indices == None and len(indices) > 0: return len(indices) - np.count_nonzero( self.ReconstructedEvents[indices,9] )
        return len(self.ReconstructedEvents) - np.count_nonzero( self.ReconstructedEvents[:,9] )
    def getMissingX2Count(self, indices=None):
        if len(self.ReconstructedEvents) == 0: return 0
        if not indices == None and len(indices) > 0: return len(indices) - np.count_nonzero( self.ReconstructedEvents[indices,10] )
        return len(self.ReconstructedEvents) - np.count_nonzero( self.ReconstructedEvents[:,10] )
    def getMissingY1Count(self, indices=None):
        if len(self.ReconstructedEvents) == 0: return 0
        if not indices == None and len(indices) > 0: return len(indices) - np.count_nonzero( self.ReconstructedEvents[indices,11] )
        return len(self.ReconstructedEvents) - np.count_nonzero( self.ReconstructedEvents[:,11] )
    def getMissingY2Count(self, indices=None):
        if len(self.ReconstructedEvents) == 0: return 0
        if not indices == None and len(indices) > 0: return len(indices) - np.count_nonzero( self.ReconstructedEvents[indices,12] )
        return len(self.ReconstructedEvents) - np.count_nonzero( self.ReconstructedEvents[:,12] )
    def getSumXSumY(self):
        if len(self.ReconstructedEvents) == 0: return [], []
        sumx_indices = np.where(self.ReconstructedEvents[:,6] != None)[0]
        sumy_indices = np.where(self.ReconstructedEvents[:,7] != None)[0]
        sumx_list, sumy_list = [], []
        if len(sumx_indices) > 0: sumx_list = self.ReconstructedEvents[sumx_indices,6]
        if len(sumy_indices) > 0: sumy_list = self.ReconstructedEvents[sumy_indices,7]
        return sumx_list, sumy_list
    def getSignalCounts(self):
        if len(self.EventsList) == 0: return 0,0,0,0,0
        x1_cnt, x2_cnt, y1_cnt, y2_cnt, mcp_cnt = 0, 0, 0, 0, 0
        for evt in self.EventsList:
            x1_cnt += len(evt.x1_list)
            x2_cnt += len(evt.x2_list)
            y1_cnt += len(evt.y1_list)
            y2_cnt += len(evt.y2_list)
            mcp_cnt += len(evt.mcp_list)
        return x1_cnt, x2_cnt, y1_cnt, y2_cnt, mcp_cnt
    def getEventIsEmpty(self):
        if len(self.EventsList) == 0: return []
        result_arr = []
        for evt in self.EventsList:
            result_arr.append( len(evt.x1_list)+len(evt.x2_list)+len(evt.y1_list)+len(evt.y2_list)+len(evt.mcp_list) > 0)
        return result_arr
    def getStartTimeTDC(self):
        if len(self.ReconstructedEvents) < 1: return None 
        return self.ReconstructedEvents[0][2]
    def getElapsedTimeTDC(self): 
        """ Returns the elapsed time in seconds"""
        if len(self.EventsList) < 1: return None
        if len(self.EventsList) == 1: return 0
        return (self.EventsList[-1].start_time - self.EventsList[0].start_time)*(1e-12) # convert from ps to s

    #---------------------------------------------------------------------------
    # Getter methods for MM8 related information
    #---------------------------------------------------------------------------
    def getStartTime(self): return self.mm8DataFile.getStartTime()
    def getEndTime(self): return self.mm8DataFile.getEndTime()
    def getMidTime(self): return (self.getStartTime() + self.getEndTime()) / 2

    def getSpecies(self): return self.mm8DataFile.getSpecies()
    def getCharge(self): return self.mm8DataFile.getCharge()
    def getMM8FilePath(self): return self.mm8DataFile.getFileName()
    def getMM8Info(self): return self.mm8DataFile.getMM8Info()
    def getMM8Scan1Vals(self): return self.mm8DataFile.getMM8Scan1Vals()
    def getMM8Scan2Vals(self): return self.mm8DataFile.getMM8Scan2Vals()
    def getMM8Scan1Len(self): return self.mm8DataFile.getMM8Scan1Len()
    def getMM8Scan2Len(self): return self.mm8DataFile.getMM8Scan2Len()
    def getMagAccumTime(self): return self.mm8DataFile.getTimingValue(self.system.getMagAccumTimingLoc())
    def getRedcycAccumTime(self): return self.mm8DataFile.getTimingValue(self.system.getRedCycAccumTimingLoc())
    def getMM8CyclotronFrequency(self): return self.mm8DataFile.getFrequencyValue(self.system.getConvAfgLoc())
    def getMM8ReducedCyclotronFrequency(self): self.mm8DataFile.getFrequencyValue(self.system.getDipoleAfgLoc())
    def getMM8MagnetronFrequency(self): return self.getMM8CyclotronFrequency() - self.getMM8ReducedCyclotronFrequency()
    
    #############################################################################
    # Pseudo-private Methods
    #   User should not call or modify these methods, unless they know what they
    #   are doing. These are called internally by the 'public' methods
    #############################################################################
    def __extractEventsInReadWindow__(self, tdc_data, last_event_number):
        """ Goes through the time signal data and pulls out any signals that
            are in between a valid read start until the next start signal.
            This assumes that the array has been sorted by the trigger time (in us)!
        Args:
            tdc_data (2d arr of floats): tdc trigger id (column 0) and timestamp (column 1) data
            last_event_number (int): previous event number read in by the datafile object
        Returns: 
            events_list (list of TriggerEvent obj): trigger event information in the tdc data
            last_idx_read (int): the number of lines read
        """
        events_list = []
        # generate events for every start and stop signal recieved
        all_start_idx = np.where(tdc_data[:,0] == self.TDC_Info["srt_fall"])[0]
        all_stop_idx  = np.where(tdc_data[:,0] == self.TDC_Info["stp_fall"])[0]
        # Choose the last index to read the data from.
        # We don't want to start reading the unfinshed start event if it exists!
        if len(all_start_idx) < 1: return [], 0
        last_start = all_start_idx[-1]
        if len(all_stop_idx) < 1: 
            last_idx_to_read = last_start-1        
        else: # gauranteed to have both start and stop indices here
            if all_stop_idx[-1] > last_start: last_idx_to_read = all_stop_idx[-1]
            if last_start > all_stop_idx[-1]: last_idx_to_read = last_start-1
        if last_idx_to_read < 0: return [], 0 # reasonableness check
        # create the events: defined as all hit info from one start signal to the next
        for kk, srt_idx in enumerate(all_start_idx):
            start_time = tdc_data[srt_idx,1]
            if srt_idx > last_idx_to_read: break
            # use the next start time to set the max read time
            if kk < len(all_start_idx)-1: max_search_idx = all_start_idx[kk+1]
            else: max_search_idx = last_idx_to_read+1
            # if not self.abs_tof_max == None:
            #     max_search_time = min(max_search_time, start_time+self.abs_tof_max)
            xym_data = tdc_data[srt_idx+1:max_search_idx,:]
            # Create the event
            if len(xym_data) > 1:
                events_list.append(TriggerEvent(trigger_id=last_event_number+kk+1, start_time=start_time,
                    x1_list=xym_data[ xym_data[:,0]  == self.TDC_Info["x1_fall"],  1],
                    x2_list=xym_data[ xym_data[:,0]  == self.TDC_Info["x2_fall"],  1],
                    y1_list=xym_data[ xym_data[:,0]  == self.TDC_Info["y1_fall"],  1],
                    y2_list=xym_data[ xym_data[:,0]  == self.TDC_Info["y2_fall"],  1],
                    mcp_list=xym_data[ xym_data[:,0] == self.TDC_Info["mcp_fall"], 1]))
            else: events_list.append(TriggerEvent(trigger_id=last_event_number+kk+1, start_time=start_time))
        return events_list, last_idx_to_read+1
    
    def __reconstructEventData__(self, event_list):
        """ Attempt to reconstruct the data. The algorithm here will save data
            for an event with complete information: all x1/x2/y1/y2/mcp signals 
            are present and in reasonable windows. Cuts based on the time of
            flight and position are made as well
        Args:
            event_list (list of TriggerEvent obj): trigger event information for reconstructing positions
        Returns (2d arr of float): postion/time/flags for the data which was successfully reconstructed
        """
        reconstr_data = []
        for ii, event in enumerate(event_list):
            hits_list = event.sortIonHits(self.SumX_Bounds, self.SumY_Bounds)
            for hit in hits_list:
                curr_errs = hit["errors"]
                # Check if it is possible to reconstruct the position, otherwise skip it
                if not hit['can_reconstruct']: continue
                # Begin the reconstruction process
                x1_time = event.x1_list[hit["x1_idx"]] if not hit["x1_idx"] == None else None 
                x2_time = event.x2_list[hit["x2_idx"]] if not hit["x2_idx"] == None else None 
                y1_time = event.y1_list[hit["y1_idx"]] if not hit["y1_idx"] == None else None 
                y2_time = event.y2_list[hit["y2_idx"]] if not hit["y2_idx"] == None else None 
                mcp_time = event.mcp_list[hit["mcp_idx"]]
        
                x_raw, y_raw = None, None
                sumx_used, sumy_used = None, None # None if couldn't calculate from both X1/X2 and Y1/Y2 signals
                #------------------------------------------------------------------------
                # Get the x-position of the ion from the event information
                if hit["recover_x"] and not self.SumX == None: # recover the x-position
                    if   x1_time != None: x_raw = (2*x1_time - self.SumX - 2*mcp_time) / self.SumX 
                    elif x2_time != None: x_raw = (self.SumX + 2*mcp_time - 2*x2_time) / self.SumX
                else: # do the full position reconstruction
                    sumx_used = x1_time + x2_time - 2*mcp_time
                    x_raw = (x1_time - x2_time) / sumx_used 
                #------------------------------------------------------------------------
                # Get the y-position of the ion from the event information
                if hit["recover_y"] and not self.SumY == None: # recover the y-position
                    if   y1_time != None: y_raw = (2*y1_time - self.SumY - 2*mcp_time) / self.SumY 
                    elif y2_time != None: y_raw = (self.SumY + 2*mcp_time - 2*y2_time) / self.SumY
                else: # do the full position reconstruction
                    sumy_used = y1_time + y2_time - 2*mcp_time
                    y_raw = (y1_time - y2_time) / sumy_used 
                #------------------------------------------------------------------------
                # If we still have both the x and y position, save the reconstruction!
                if (not x_raw == None) and (not y_raw == None):
                    # normalize them to the approx MCP radius
                    x = self.system.getRadiusMCP() * x_raw
                    y = self.system.getRadiusMCP() * y_raw
                    tof = (mcp_time - event.start_time) / (1e3) # time is in ns, convert to us
                    trig_time = mcp_time/(1e9) # time is is ns, convert to seconds
                    # Determine if a recovery was performed
                    recover_flag = hit['recover_x'] or hit['recover_y']
                    x1_found_flag, x2_found_flag = x1_time!=None, x2_time!=None
                    y1_found_flag, y2_found_flag = y1_time!=None, y2_time!=None
                    # Do a reasonableness check with set absolute limits for the system
                    if tof >= self.AbsMinTOF and tof <= self.AbsMaxTOF and np.sqrt(x_raw**2 + y_raw**2) <= self.system.getRadiusMCP():
                        reconstr_data.append([event.trigger_id, len(event.mcp_list), trig_time, tof, x, y, sumx_used, sumy_used, \
                                              recover_flag, x1_found_flag, x2_found_flag, y1_found_flag, y2_found_flag])
        return reconstr_data
    