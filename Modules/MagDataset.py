"""
@Desc: This class holds all information relevant to the spot files
       which have undergone some amount of magnetron accumulation.
       Each instance should have only files related to each other, 
       such as all being of the same ion. Trends WILL be infered and
       mixing together different ions may give incorrect results.
@Author: Scott Campbell, campbels@nscl.msu.edu
@Version: 22 February 2023
"""
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')
import copy

from .PIICR_Utilities import getEllipseAccumPhase, calculateBirgeRatio, getSurroundingIdx, getNearestUncertainty
from .FitLinear import LinearFitter

class MagDataset:
    def __init__(self, analysisfile_list, startDataset, interp_method='linear', print_results=True, plot_results=True, initial_Fm_guess=None, initial_Fp_guess=None):
        """
        Args:
            filenames (list of str): filenames of all raw datafiles for these mag spots
            startDataset (StartDataset obj): the reference spot dataset relevant to these mag spots
            interp_method (str): how to interpolate the values: 'constant', 'linear' (in time), 'nearest' (weighted by time)
            print_results (bool): whether to print out the results of the dataset
            initial_Fm_guess (float): allows you to provide a starting guess of Fm for the analysis code. If None, the MM8 value will be used
            initial_Fp_guess (float): allows you to provide a starting guess of Fp for the analysis code. If None, the MM8 value will be used
        """
        self.startDataset = startDataset
        if len(analysisfile_list) == 0: raise ValueError("Dataset is empty. Cannot run any interpolation.")
        #------------------------------------------------------------------------
        # Sort analysis_list by their midtimes
        self.analysisfile_list = sorted(analysisfile_list, key=lambda x: x.getMidTime(), reverse=False)
        self.midtimes = np.array([file.getMidTime() for file in analysisfile_list])
        #------------------------------------------------------------------------
        # Extract the relevent information
        self.MagFreqGuess = initial_Fm_guess if not initial_Fm_guess == None else analysisfile_list[0].getMM8MagnetronFrequency()
        self.RedFreqGuess = initial_Fp_guess if not initial_Fp_guess == None else analysisfile_list[0].getMM8ReducedCyclotronFrequency()
        self.magAccumTime =      np.array([self.__calcNetMagAccumTime__(file) for file in self.analysisfile_list])
        self.redcycAccumTime =   np.array([self.__calcNetRedcycAccumTime__(file) for file in self.analysisfile_list])
        self.correctedMagPhase = np.array([self.__calcCorrectedMagPhase__(kk) for kk in range(len(self.analysisfile_list))])
        self.nTurnsFreqList =    np.array([self.__calcNTurns__(kk) for kk in range(len(self.analysisfile_list))])
        #------------------------------------------------------------------------
        # Fit the extracted magnetron frequencies to interpolate in the future
        self.magFreqs = np.array([val[1] for val in self.nTurnsFreqList])
        self.magFreqErrs = np.array([val[2] for val in self.nTurnsFreqList])
        if len(self.analysisfile_list) < 3 and not interp_method == 'constant':
            print("\n*** WARNING: Dataset contains less than 3 samples. Defaulting interpolation method to \'constant\'.\n")
            self.interp_method = 'constant'
        else: self.interp_method = interp_method
        #------------------------------------------------------------------------
        # If the interpolation method is the linear regression, create a fit object
        if self.interp_method == 'linear':
            # Perform the linear regression for the magnetron frequency
            self.linearFit_magFreq = LinearFitter()
            self.linearFit_magFreq.fit(self.midtimes, self.magFreqs, self.magFreqErrs)
            if plot_results: self.__plotLinearFits__(self.linearFit_magFreq)
            if print_results:
                print("-"*50)
                print("Aggregate Dataset Fit:")
                print("-"*50)
                print("\tFitted Magnetron Frequency Slope [Hz/hr]:", np.round(self.linearFit_magFreq.slope*3600,6))
                print("")
        #------------------------------------------------------------------------
        # If the interpolation method is the constant, determine the value by weighted average
        elif self.interp_method == 'constant':
            if len(self.analysisfile_list) == 1:
                self.constant_magFreq = self.magFreqs[0]
                self.constant_magFreqErr = self.magFreqErrs[0]
            else:
                mag_weights = np.divide(1, np.power(self.magFreqErrs, 2))
                self.constant_magFreq = np.sum( np.multiply(mag_weights, self.magFreqs) )/np.sum(mag_weights)
                self.constant_magFreqErr = 1 / np.sqrt( np.sum( mag_weights ) )
            if plot_results:
                self.__plotConstantValues__(self.midtimes, self.magFreqs, self.magFreqErrs, self.constant_magFreq, self.constant_magFreqErr)
            if print_results:
                print("-"*50)
                print("Aggregate Dataset Fit:")
                print("-"*50)
                print("\tWeighted Mag Freq:", np.round(self.constant_magFreq,3), np.round(self.constant_magFreqErr,3))
                print("")
        #-----------------------------------------------------------
        # If the interpolation method is the nearest, determine the value using the two closest values (one on either side) weighted by time difference
        # This will be done on a case by case basis later in the code
        elif self.interp_method == 'nearest':
            if plot_results: self.__plotRawData__(self.midtimes, self.magFreqs, self.magFreqErrs)        
        else: raise ValueError("The interpolation method was not one of the defined values... Expected \'constant\', \'linear\' or \'nearest_neighbors\'") 
        # print a short summary of the results
        if print_results: self.printSummary()
                     
    def printSummary(self):
        for kk in range(len(self.nTurnsFreqList)):
            vals = self.nTurnsFreqList[kk]
            analysisfile = self.analysisfile_list[kk]
            print("-"*80)
            print("File:", analysisfile.getFileName(), "    Species:", analysisfile.getSpecies() )
            print("-"*80)
            print("\t" + "{0:>25} {1}".format( "Counts:", str(int(analysisfile.getCounts()))) )
            #print("\t" + "{0:>25} {1}".format( "Net F+ Accum Time [μs]:", str(round(self.redcycAccumTime[kk],3))) )
            print("\t" + "{0:>25} {1}".format( "Net F- Accum Time [μs]:", str(round(self.magAccumTime[kk],3))) )
            print("\t" + "{0:>25} {1}".format( "Ang Location [deg]:", str(round(analysisfile.getSpotPhi(),3))+" ± "+str(round(analysisfile.getSpotPhiErr(),3))) )
            #print("\t" + "{0:>25} {1}".format( "F- Full Turns Guess:", str(int(vals[0]))) )
            print("\t" + "{0:>25} {1}, (n±1: {2} Hz)".format( "F- Frequency [Hz]:", str(round(vals[1],3))+" ± "+str(round(vals[2],3)), str(round((1e6)/self.magAccumTime[kk],3)) ) )   

    def interpMagFreq(self, midtime):
        """ Interpolates the magnetron frequency for a given time
        Args:
            midTime (float): the time for which the frequency should be interpolated at
        Returns:
            interpolated frequency and uncertainty
        """
        if self.interp_method == 'constant':
            return self.constant_magFreq, self.constant_magFreqErr
        elif self.interp_method == 'linear':
            return self.linearFit_magFreq.get_y(midtime)
        elif self.interp_method == 'nearest':
            # check if there are measurements on either side of the time
            # from the constructor, we are gauranteed to have at least two measurements
            lo_idx, hi_idx = getSurroundingIdx(midtime, self.midtimes)
            mag1, mag1err = self.magFreqs[lo_idx], self.magFreqErrs[lo_idx]
            t1 = self.midtimes[lo_idx]
            mag2, mag2err = self.magFreqs[hi_idx], self.magFreqErrs[hi_idx]
            t2 = self.midtimes[hi_idx]
            # Fit and get the predicted center x value
            slope = (mag2-mag1)/(t2-t1)
            inter = mag1 - slope*t1
            pred_mag = slope*midtime + inter
            pred_magErr = getNearestUncertainty(midtime, [t1,t2], [mag1err,mag2err])
            return pred_mag, pred_magErr
        
    def getMagnetronFrequencies(self):
        all_vals = []
        for line in self.nTurnsFreqList:
            all_vals.append(line[1:])
        return all_vals


    def __calcNetMagAccumTime__(self, analysisfile):
        return analysisfile.getMagAccumTime() - self.startDataset.getMagWaitTime()
    
    def __calcNetRedcycAccumTime__(self, analysisfile):
        return analysisfile.getRedcycAccumTime() - self.startDataset.getPhaseAccumTime()

    def __calcCorrectedMagPhase__(self, file_idx):
        # First, correct the angular positon by any magnetron motion that may have accumulated
        analysisfile = self.analysisfile_list[file_idx]

        spotPhi, spotRho = analysisfile.getSpotPhi(), analysisfile.getSpotRho()
        # Find the new angle for the spot
        redCycFreq = analysisfile.getReducedCyclotronDirection()*self.RedFreqGuess
        redCycPhase = ((self.redcycAccumTime[file_idx]/(10.**6))*redCycFreq*360)%360
        spotPhi = (spotPhi-redCycPhase)%360
        magPhaseDiff, magPhaseDiffErr = self.__calcNetPhaseTravelled__(file_idx, spotPhi)
        
        return [magPhaseDiff, magPhaseDiffErr]

    def __calcNetPhaseTravelled__(self, file_idx, spotPhi):
        """ Determine the amount of phase accumulated over the ellipse for the spot,
            accounting for the elliptisity
        Args:
            datafile (DataFile obj): a PI-ICR datafile object containing magnetron spot data
        """
        analysisfile = self.analysisfile_list[file_idx]
        startPhasePhi, startPhasePhiErr = self.startDataset.interpStartPhase(analysisfile.getMidTime()) 
        freqSign = analysisfile.getMagnetronDirection()

        ellipseParams = analysisfile.getEllipseParams()
        if not ellipseParams == None: # If there is an ellipse, use it to determine the phase
            # Determine the arc length between the two angles, ellipse assumption
            ellipse_a, ellipse_b, ellipse_theta = ellipseParams['a'], ellipseParams['b'], ellipseParams['theta']
            phaseDiff = getEllipseAccumPhase(startPhasePhi, spotPhi, freqSign, ellipse_a, ellipse_b, ellipse_theta)
        else: # Old method, use circle assumption
            phaseDiff = ((spotPhi-startPhasePhi)*freqSign)%360 # Depending on the direction of the frequency, you have to adjust sign!  
        
        phaseDiffErr = np.sqrt(startPhasePhiErr**2 + analysisfile.getSpotPhiErr()**2)

        return [phaseDiff, phaseDiffErr] 

    def __calcNTurns__(self, file_idx, n_diff=3, return_all=False):
        """ Calculates the number of turns that would correspond to 
            the observed magnetron spot
        Args:
            datafile (Datafile obj): the datafile with spot information for which number of turns is to be calculated
            n_diff (int, default 2): number of turns to consider beyond expected MM8 predicted value (e.g. n-2, ..., n+2)
        Returns: 
            number of turns that most closely agrees between observed frequency and MM8 value
        """
        analysisfile = self.analysisfile_list[file_idx]

        # Determine the number of turns to check
        n = np.floor((self.magAccumTime[file_idx]*self.MagFreqGuess)/10.**6)
        n_list = [n-(n_diff-kk) for kk in range(n_diff)] + [n+kk for kk in range(n_diff+1)]
        # Determine the magnetron frequencies corresp to the num of turns
        fm_PI_list = [(n_val + self.correctedMagPhase[file_idx][0]/360)/(self.magAccumTime[file_idx]/10.**6) for n_val in n_list]
        fmErr_PI = np.abs(self.correctedMagPhase[file_idx][1]/(360*self.magAccumTime[file_idx]/10.**6))

        if return_all:
            all_arr = np.transpose([n_list, fm_PI_list, fmErr_PI*np.ones(len(n_list))])
            return all_arr
        
        # See how far each freq is from MM8 value, find the one which agrees the closest
        fm_diff_list = [np.abs(fm_PI-self.MagFreqGuess) for fm_PI in fm_PI_list]
        closest_idx = fm_diff_list.index(np.amin(fm_diff_list))
      
        # print("\tn guess:", n)
        # print("\tacc phase (deg):", self.correctedMagPhase[file_idx][0], self.correctedMagPhase[file_idx][1])
        # print("\tacc time (us):", self.magAccumTime[file_idx])
        # print("\tMag Freq (Hz):", fm_PI_list[closest_idx], fmErr_PI)

        # return the guess that best agrees with MM8 value
        return [n_list[closest_idx], fm_PI_list[closest_idx], fmErr_PI]

    def __plotLinearFits__(self, linearFit_magFreq):
        #------------------------------------------------------------
        # Plot the center X data and fit first
        if not linearFit_magFreq.fit_completed: return 
        x_pred = np.linspace(np.amin(linearFit_magFreq.x_data),np.amax(linearFit_magFreq.x_data),500)
        y_pred = linearFit_magFreq.slope * x_pred + linearFit_magFreq.intercept  # Predicted y value at x_new
        se_pred = linearFit_magFreq.__prediction_std_error__(x_pred)  # Standard error of prediction 
        y_lower = y_pred - se_pred
        y_upper = y_pred + se_pred

        # transform the epoch times to elapsed time in hrs
        x_pred -= np.amin(x_pred)
        x_pred /= 3600
        raw_x = copy.deepcopy(linearFit_magFreq.x_data)
        raw_x -= np.amin(raw_x)
        raw_x /= 3600

        plt.figure(figsize=(8,3))
        plt.errorbar(raw_x, linearFit_magFreq.y_data, linearFit_magFreq.y_errs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        plt.fill_between(x_pred, y_upper, y_lower, color='indianred', alpha=0.3)
        plt.plot(x_pred, y_pred, '--k')
        plt.xlabel("Measurement Time [hrs]")
        plt.ylabel("Magnetron Frequency [Hz]")
        plt.show()

    def __plotConstantValues__(self, mid_times, magFreq_data, magFreqErrs, magFreq_wav, magFreq_wavErr):
        # convert the times from epoch time to elapsed time in hrs
        raw_x = copy.deepcopy(mid_times)
        raw_x -= np.amin(raw_x)
        raw_x /= 3600

        birge, birge_err = calculateBirgeRatio(magFreq_data, magFreqErrs)

        plt.figure(figsize=(8, 3))
        plt.title('Birge Ratio: '+str(np.round(birge,3))+'('+str(np.round(birge_err,3))+')')
        plt.errorbar(raw_x, magFreq_data, magFreqErrs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        plt.plot([np.amin(raw_x), np.amax(raw_x)], [magFreq_wav,magFreq_wav], '--k')
        plt.fill_between([np.amin(raw_x), np.amax(raw_x)], magFreq_wav+magFreq_wavErr, magFreq_wav-magFreq_wavErr, color='indianred', alpha=0.3)
        plt.xlabel("Measurement Time [hrs]")
        plt.ylabel("Magnetron Frequency [Hz]")
        plt.show()

    def __plotRawData__(self, mid_times, magFreq_data, magFreqErrs):
        # convert the times from epoch time to elapsed time in hrs
        raw_x = copy.deepcopy(mid_times)
        raw_x -= np.amin(raw_x)
        raw_x /= 3600

        birge, birge_err = calculateBirgeRatio(magFreq_data, magFreqErrs)

        plt.figure(figsize=(8, 3))
        plt.title('Birge Ratio: '+str(np.round(birge,3))+'('+str(np.round(birge_err,3))+')')
        plt.errorbar(raw_x, magFreq_data, magFreqErrs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        plt.xlabel("Measurement Time [hrs]")
        plt.ylabel("Magnetron Frequency [Hz]")
        plt.show()
