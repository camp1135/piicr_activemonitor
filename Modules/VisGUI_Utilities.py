import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
plt.rcParams['figure.constrained_layout.use'] = True
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
from matplotlib.colors import LinearSegmentedColormap

import PySimpleGUI as sg
import os
import xml.etree.ElementTree as ET
import numpy as np

#################################################
# Miscelaneous Helper Methods
#################################################

def draw_figure(canvas, figure, loc=(0,0)):
    """ Updates the figure on the GUI interface
    """
    if canvas.children:
        for child in canvas.winfo_children():
            child.destroy()
    figure_canvas_agg = FigureCanvasTkAgg(figure, master=canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    return figure_canvas_agg

class Toolbar(NavigationToolbar2Tk):
    def __init__(self, *args, **kwargs):
        super(Toolbar, self).__init__(*args, **kwargs)

def draw_figure_w_toolbar(canvas, fig, canvas_toolbar):
    if canvas.children:
        for child in canvas.winfo_children():
            child.destroy()
    if canvas_toolbar.children:
        for child in canvas_toolbar.winfo_children():
            child.destroy()
    figure_canvas_agg = FigureCanvasTkAgg(fig, master=canvas)
    figure_canvas_agg.draw()
    toolbar = Toolbar(figure_canvas_agg, canvas_toolbar)
    toolbar.update()
    figure_canvas_agg.get_tk_widget().pack(side='right', fill='both', expand=1)
    return figure_canvas_agg

def get_resolution():
    # called before window created
    root = sg.tk.Tk()
    scaling = root.winfo_fpixels('1i')/72
    root.destroy()
    return scaling
   
def plot_signal_xy(ax, hi_times, start_time, end_time, plot_color, y_offset=0, hi_val=0.75, signal_width=1):
    if len(hi_times) == 0: return ax.plot([start_time, end_time], [y_offset, y_offset], '-', color=plot_color)
    
    pts = [[start_time, 0+y_offset]]
    for t in hi_times:
        if t < start_time or t > end_time: continue # only add in points within the specified time bounds
        pts.append([t, 0+y_offset])
        pts.append([t, hi_val+y_offset])
        pts.append([t+signal_width, hi_val+y_offset])
        pts.append([t+signal_width, 0+y_offset])
    pts.append([end_time, 0+y_offset])
    pts = np.array(pts)
    return ax.plot(pts[:,0], pts[:,1], '-', color=plot_color)

#################################################
# Methods for creating the GUI pages
#################################################

def create_main_page(piSystem, window_width, window_height, limits, runstate, PLOT_DPI, attribute_scale, figure_scale):
    large_font_size = str(int(13*attribute_scale))
    med_font_size = str(int(11*attribute_scale))
    small_font_size = str(int(9*attribute_scale))
    large_font = "Helvetica "+large_font_size
    med_font = "Helvetica "+med_font_size
    small_font = "Helvetica "+small_font_size
    #----------------------------------------------------------------
    # Set up the left-most column in the GUI
    #----------------------------------------------------------------
    left_col_width = int(0.38*window_width)

    # Set up the plot `for the raw data canvas
    raw_width, raw_height = raw_canvsize = (left_col_width, 0.7*window_height) # Set the figure size in pixel width
    raw_figsize = (0.9*raw_width*figure_scale/PLOT_DPI, 0.8*raw_height*figure_scale/PLOT_DPI)
    raw_fig = Figure(figsize=raw_figsize)
    raw_ax = raw_fig.add_subplot(111)
    raw_ax.xaxis.tick_top()
    raw_ax.set_aspect('equal', 'box')
    
    
    # Set up the plot for the count rate figure
    rate_width, rate_height = rate_canvsize = (left_col_width, 0.22*window_height) # Set the figure size in pixel width
    rate_figsize = (rate_width*figure_scale/PLOT_DPI, rate_height*figure_scale/PLOT_DPI)
    rate_fig = Figure(figsize=rate_figsize)
    rate_allax = rate_fig.add_subplot(111)
    rate_allax.set_ylabel('counts/s')
    rate_allax.set_ylim([0,0.01])
    #rate_allax.set_position([0.15, 0.20, 0.80, 0.75])

    raw_data = sg.Frame("Raw Positions", [
        [sg.Canvas(size=raw_canvsize, expand_x=True, expand_y=True, key='raw_canvas')],
        [
            sg.Push(), 
            sg.Button("Image",   size=(9,1), pad=(0,0), font=small_font, key='imageplot_button'), 
            sg.Button("Contour", size=(9,1), pad=(0,0), font=small_font, key='contourplot_button'), 
            sg.Button("Scatter", size=(9,1), pad=(0,0), font=small_font, key='scatterplot_button'), 
            sg.Push(),
            sg.Text("Min Cnts/Bin:",font=small_font),
            sg.Input("1", size=(3,1), font=small_font, key='min_cnts_per_bin'),
            sg.Push(),
            sg.Checkbox("Hide Rejected Points?", default=False, font=small_font, key='hide_rejected_points'),
            sg.Push()
        ],
    ], expand_x=True, expand_y=True, font=large_font)

    count_rate_layout = sg.Frame("Count Rate",[[
        sg.Canvas(size=rate_canvsize, expand_x=True, expand_y=True, key='rate_canvas'),
    ]], expand_x=True, expand_y=True, font=large_font)

    leftcol = sg.Column([
        [raw_data],
        [count_rate_layout]
    ], expand_x=True, expand_y=True) #, size=(left_col_width, window_height))

    #----------------------------------------------------------------
    # Set up the middle column in the GUI
    #----------------------------------------------------------------
    mid_col_width = int(0.4*window_width)

    # Set up the plot for the TOF spectrum plot
    tof_width, tof_height = tof_canvsize = (mid_col_width, 0.28*window_height) # Set the figure size in pixel width
    tof_figsize = (tof_width*figure_scale/PLOT_DPI, tof_height*figure_scale/PLOT_DPI)
    tof_fig = Figure(figsize=tof_figsize)
    tof_ax = tof_fig.add_subplot(111)
    tof_ax2 = tof_ax.twinx() # make separate axis for plotting mca gate 
    tof_ax2.set_yticklabels([])
    tof_ax2.set_xlabel(r'ToF (us)', fontsize=int(small_font_size))
    #tof_ax.set_position([0.15, 0.20, 0.80, 0.75])

    # Set up the plot for the angular spectrum plot
    ang_width, ang_height = ang_canvsize = (mid_col_width, 0.28*window_height) # Set the figure size in pixel width
    ang_figsize = (ang_width*figure_scale/PLOT_DPI, ang_height*figure_scale/PLOT_DPI)
    ang_fig = Figure(figsize=ang_figsize)
    ang_ax = ang_fig.add_subplot(111)
    ang_ax.set_xlabel(r'Angle (deg)', fontsize=int(small_font_size))
    #ang_ax.set_position([0.15, 0.20, 0.80, 0.75])

    cluster_layout = sg.Frame("Clustering and Fitting",[
        [
            sg.Column([
                [ sg.Checkbox("Run Clustering?", default=False, font=small_font, key="enable_cluster_check") ],
                [ sg.Combo(values=["Spot","Ellipse"], default_value="Spot", readonly=True, size=15, font=small_font, key="cluster_combo") ],
            ]),
            sg.VerticalSeparator(),
            sg.Column([
                [
                    sg.Text('Threshold:',expand_x=True,font=small_font,size=10),
                    sg.Text('', size=4, font=small_font, key='thresh_slider_val'),
                    sg.Slider(range=(0.01,1), size=(30,10), expand_x=True, disable_number_display=True, default_value='0.15', resolution=.01, orientation='h', key='threshold'),
                    sg.Push()
                ],
                [
                    sg.Text('Smoothing:',expand_x=True,font=small_font,size=10),
                    sg.Text('', size=4, font=small_font, key='smooth_slider_val'),
                    sg.Slider(range=(0.1,3), size=(30,10), expand_x=True, disable_number_display=True, default_value='1', resolution=.1, orientation='h', key='smoothing'),
                    sg.Push()
                ]
            ])
        ],
        [sg.HorizontalSeparator()],
        [
            sg.Checkbox("Run 2D Fitting?", default=False, font=small_font, key="enable_fitting_check"),
            sg.Combo(values=["Polar Spot","Cart Spot","Ellipse"], default_value="Polar Spot", readonly=True, size=9, font=small_font, key="fitting_combo"),
        ],
    ], expand_x=True, font=large_font)

    tof_spectrum = sg.Frame("", [
        [sg.Canvas(size=tof_canvsize, expand_x=True, expand_y=True, key='tof_canvas')],
    ], expand_x=True, expand_y=True, font=large_font)

    ang_spectrum = sg.Frame("", [
        [sg.Canvas(size=ang_canvsize, expand_x=True, expand_y=True, key='ang_canvas')],
    ], expand_x=True, expand_y=True, font=large_font)

    filters_layout = sg.Frame("Filters", [[
        sg.Column([
            [
                sg.Checkbox("Max Cnts/Shot", default=True, size=15, font=small_font, key='z_limit_check'), 
                sg.In(str(limits['zmax']), size=5, font=small_font, key='z_max'),
                sg.Push()
            ],
            [
                sg.Checkbox("TOF Gate", default=True, size=15, font=small_font, key='tof_bounds_check'),
                sg.In(str(limits['tofmin']), size=5, font=small_font, key='gated_tof_min'),
                sg.Text("to", size=2, font=small_font),
                sg.In(str(limits['tofmax']), size=5, font=small_font, key='gated_tof_max'),
                sg.Push()
            ], 
            [
                sg.Checkbox("Radial Gate", default=False, size=15, font=small_font, key='radial_check'),
                sg.In(str(limits['radialmin']), size=5, font=small_font, key='radial_min'),
                sg.Text("to", size=2, font=small_font),
                sg.In(str(limits['radialmax']), size=5, font=small_font, key='radial_max'),
                sg.Push()
            ], 
            [
                sg.Checkbox("Angular Gate", default=False, size=15, font=small_font, key='ang_check'),
                sg.In(str(limits['angmin']), size=5, font=small_font, key='ang_min'),
                sg.Text("to", size=2, font=small_font),
                sg.In(str(limits['angmax']), size=5, font=small_font, key='ang_max'),
                sg.Push()
            ],
            [
                sg.Checkbox("Event Range", default=False, size=15, font=small_font, key='event_range_check'),
                sg.In(str(limits['eventmin']),size=(5,1), font=small_font, key='event_range_min'),
                sg.Text("to", size=2, font=small_font),
                sg.In(str(limits['eventmax']),size=(5,1),font=small_font, key='event_range_max'),
                sg.Push()
            ],
        ], expand_x=True),
        sg.VerticalSeparator(),
        sg.Column([
            [
                sg.Text("Set Trap Center (x,y):", font=small_font, size=16), 
                sg.Input("0",size=(5,1), font=small_font, key="trap_center_x"),
                sg.Input("0",size=(5,1), font=small_font, key="trap_center_y")
            ],
            [ sg.Text("Set Delay-line Sums, Raw Only (ns):",font=small_font) ],
            [
                sg.Text("    Sum X (avg, std):", size=16, font=small_font),
                sg.Text(str(piSystem.getTDCInfo(key='sumX')),size=(5,1),font=small_font, key='sumx_avg'),
                sg.Text(str(piSystem.getTDCInfo(key='sigmaSumX')),size=(5,1),font=small_font, key='sumx_std')
            ],
            [
                sg.Text("    Sum Y (avg, std):", size=16, font=small_font),
                sg.Text(str(piSystem.getTDCInfo(key='sumY')),size=(5,1),font=small_font, key='sumy_avg'),
                sg.Text(str(piSystem.getTDCInfo(key='sigmaSumY')),size=(5,1),font=small_font, key='sumy_std')
            ],
            [sg.Checkbox("Allow Recovered Positions?", default=runstate['allow_recovered_pos'], font=small_font, key='allow_recovered_check')],
            [sg.VPush()]
        ])
    ]], expand_x=True, font=large_font)

    midcol = sg.Column([
        [cluster_layout],
        [filters_layout],
        [ang_spectrum],
        [tof_spectrum]
    ], expand_x=True, expand_y=True) #, size=(mid_col_width, window_height))

    #----------------------------------------------------------------
    # Set up the right-most column in the GUI
    #----------------------------------------------------------------
    right_col_width = int(0.3*window_width)

    filetype_search_key = (piSystem.getDatafileExtName(), piSystem.getDatafileExtFormat())
    load_layout = sg.Frame("Load Data", [
        [sg.Text("Default Directory:", font=small_font),sg.Push()],
        [
            sg.In(runstate['pi_data_directory'], size=(40,1), enable_events=True, disabled=True, font=small_font, key='load_dir'), 
            sg.FolderBrowse(size=(8,1), target='load_dir', font=small_font, key='load_dir_browser')
        ],
        [sg.Push(), sg.Button("Load Latest File", size=(40,1), font=small_font, key='load_latest_button'), sg.Push()],

        [sg.Text("Current Filename:", font=small_font),sg.Push()],
        [
            sg.In(os.path.split(runstate['pi_filename'])[1], size=(40,1), disabled=True, enable_events=True, font=small_font, key='load_filepath'),
            sg.FileBrowse(size=(8,1), file_types=(filetype_search_key,), target='load_filepath', font=small_font, initial_folder=piSystem.getMcpDataDirectory(), key='load_file_browser')
        ],
        [
            sg.Text("Using MM8 Datafile:", font=small_font),
            sg.In(os.path.split(runstate['mm8_filename'])[1], size=(30,1), disabled=True, font=small_font, key='mm8_datafile')
        ]
    ], expand_x=True, font=large_font)

    # save_layout = sg.Frame("Save Reconstructed Data", [
    #     [ sg.Push(), sg.Button("Save All Data", size=(40,1), font=small_font, key='save_all_button'), sg.Push() ],
    #     [ sg.Push(), sg.Button("Save Filtered Data", size=(40,1), font=small_font, key='save_filtered_button'), sg.Push() ],
    # ], expand_x=True, font=large_font)

    align_layout = sg.Frame("Spot Alignment Tools",[
        [
            sg.Text("Reference:",font=med_font),
            sg.In("",size=18, font=small_font, disabled=True, key='align_ref_name'),
            sg.Text("Q:", font=small_font),
            sg.In("1",size=3, font=small_font, disabled=True, key='align_ref_charge')
        ],
        [ sg.Checkbox("Use latest MM8 Calibration?",font=small_font,default=True,key='use_mm8_calib') ],
        [
            sg.Text("   F+ Freq [Hz]:",size=13, font=small_font),
            sg.In("",size=15, font=small_font, disabled=True, key='align_ref_fp'),
            sg.In("0",size=4, font=small_font, key='align_ref_fp_unc')
        ],
        [
            sg.Text("   F- Freq [Hz]:",size=13, font=small_font),
            sg.In("",size=15, font=small_font, disabled=True, key='align_ref_fm'),
            sg.In("0",size=4, font=small_font, key='align_ref_fm_unc')
        ],
        [sg.HorizontalSeparator()],
        [
            sg.Text("Ion of Interest:", justification='center',font=med_font),
            sg.In("",size=12,font=small_font,key='align_ioi_name'),
            sg.Text("Q:",font=small_font),
            sg.In("1",size=3,font=small_font,key='align_ioi_charge'),
            sg.Text("Angle:",size=6,font=small_font),
            sg.Text("",size=8,font=small_font,key='ioi_exp_angle')
        ],
        [
            sg.Checkbox("F+ Override [Hz]:", default=False, font=small_font, key='align_ioi_freq_override'),
            sg.In("",size=15, font=small_font, disabled=True, key='align_ioi_fp'),
            sg.In("0",size=4, font=small_font, disabled=True, key='align_ioi_fp_unc')
        ],
        [
            sg.Text("   Start Spot Angle (deg):",size=22,font=small_font),
            sg.In("",size=5, font=small_font, key='align_start_angle'),
            sg.In("0",size=5, font=small_font, key='align_start_angle_unc')
        ],
        [
            sg.Text("   Mag Rel. to Start (μs):",size=22,font=small_font),
            sg.In("0",size=15,font=small_font,key='align_ioi_magacc')
        ],
        [
            sg.Text("   RedCyc Rel. to Start (μs):",size=22,font=small_font),
            sg.In("0",size=15,font=small_font,key='align_ioi_redcycacc')
        ],
        [sg.Text("Contaminants", justification='center',font=med_font)],
        [   
            sg.Text("  ",font=small_font),
            sg.Checkbox("Name:",font=small_font,key='align_cont0_check'),
            sg.In("",size=12,font=small_font,key='align_cont0_name'),
            sg.Text("Q:",font=small_font),
            sg.In("1",size=3,font=small_font,key='align_cont0_charge'),
            sg.Text("Angle:",size=6,font=small_font),
            sg.Text("",size=4,font=small_font,key='cont0_exp_angle')
        ],
        [
            sg.Text("  ",font=small_font),
            sg.Checkbox("Name:",font=small_font,key='align_cont1_check'),
            sg.In("",size=12,font=small_font,key='align_cont1_name'),
            sg.Text("Q:",font=small_font),
            sg.In("1",size=3,font=small_font,key='align_cont1_charge'),
            sg.Text("Angle:",size=6,font=small_font),
            sg.Text("",size=4,font=small_font,key='cont1_exp_angle')
        ],
        [
            sg.Text("  ",font=small_font),
            sg.Checkbox("Name:",font=small_font,key='align_cont2_check'),
            sg.In("",size=12,font=small_font,key='align_cont2_name'),
            sg.Text("Q:",font=small_font),
            sg.In("1",size=3,font=small_font,key='align_cont2_charge'),
            sg.Text("Angle:",size=6,font=small_font),
            sg.Text("",size=4,font=small_font,key='cont2_exp_angle')
        ],
    ], expand_x=True, font=large_font)

    info_layout = sg.Multiline("", size=(45,13), disabled=True, expand_x=True, no_scrollbar=True, font='Courier '+small_font_size, key='run_info')
    

    rightcol = sg.Column([
        [info_layout],
        [load_layout],
        [sg.Text("", font=small_font)],
        [align_layout],
        [ sg.Push(), sg.Button("Save Reconstructed Data", size=(40,1), font=small_font, key='save_all_button'), sg.Push() ],
        [ sg.Push(), sg.Button("Save Filtered Data", size=(40,1), font=small_font, key='save_filtered_button'), sg.Push() ],
    ], expand_x=True, size=(right_col_width, window_height))

    #----------------------------------------------------------------
    # Set up the final GUI Window
    #----------------------------------------------------------------
    layout = [ [leftcol, midcol, rightcol] ]
    plot_dict = {
        "raw_fig":raw_fig, "raw_ax":raw_ax,
        "rate_fig":rate_fig, "rate_allax":rate_allax,
        "ang_fig":ang_fig, "ang_ax":ang_ax,
        "tof_fig":tof_fig, "tof_ax":tof_ax, "tof_ax2":tof_ax2
    }

    return layout, plot_dict

def create_adv_page(piSystem, mm8_scan1_vals, mm8_scan2_vals, window_width, window_height, figure_scale, attribute_scale, PLOT_DPI):
    large_font_size = str(int(13*attribute_scale))
    med_font_size = str(int(11*attribute_scale))
    small_font_size = str(int(9*attribute_scale))
    xsmall_font_size = str(int(8*attribute_scale))
    large_font = "Helvetica "+large_font_size
    med_font = "Helvetica "+med_font_size
    small_font = "Helvetica "+small_font_size
    xsmall_font = "Helvetica"+xsmall_font_size
    
    #----------------------------------------------------------------
    # Set up the advanced plot windows
    #----------------------------------------------------------------
    plot_size = int(0.365*window_width)
    # Set up the plot for the first advanced data canvas
    adv1_width, adv1_height = adv1_canvsize = (plot_size, plot_size) # Set the figure size in pixel width
    adv1_figsize = (adv1_width*figure_scale/PLOT_DPI, adv1_height*figure_scale/PLOT_DPI)
    adv1_fig = Figure(figsize=adv1_figsize)
    adv1_ax = adv1_fig.add_subplot(111)
    adv1_ax.set_aspect('equal', adjustable='box')
    adv1_ax.xaxis.tick_top()
    #adv1_ax.set_position([0.15, 0.20, 0.80, 0.75])
    # Set up the plot for the angular spectrum plot
    adv1ang_width, adv1ang_height = adv1ang_canvsize = (plot_size, 0.3*window_height) # Set the figure size in pixel width
    adv1ang_figsize = (adv1ang_width*figure_scale/PLOT_DPI, adv1ang_height*figure_scale/PLOT_DPI)
    adv1ang_fig = Figure(figsize=adv1ang_figsize)
    adv1ang_ax = adv1ang_fig.add_subplot(111)
    adv1ang_ax.set_xlabel(r'Angle (deg)', fontsize=int(small_font_size))
    #adv1ang_ax.set_position([0.15, 0.20, 0.80, 0.75])
    # Set up the plot for the first advanced data canvas
    adv2_width, adv2_height = adv2_canvsize = (plot_size, plot_size) # Set the figure size in pixel width
    adv2_figsize = (adv2_width*figure_scale/PLOT_DPI, adv2_height*figure_scale/PLOT_DPI)
    adv2_fig = Figure(figsize=adv2_figsize)
    adv2_ax = adv2_fig.add_subplot(111)
    adv2_ax.set_aspect('equal', adjustable='box')
    adv2_ax.xaxis.tick_top()
    #adv2_ax.set_position([0.15, 0.20, 0.80, 0.75])
    # Set up the plot for the angular spectrum plot
    adv2ang_width, adv2ang_height = adv2ang_canvsize = (plot_size, 0.3*window_height) # Set the figure size in pixel width
    adv2ang_figsize = (adv2ang_width*figure_scale/PLOT_DPI, adv2ang_height*figure_scale/PLOT_DPI)
    adv2ang_fig = Figure(figsize=adv2ang_figsize)
    adv2ang_ax = adv2ang_fig.add_subplot(111)
    adv2ang_ax.set_xlabel(r'Angle (deg)', fontsize=int(small_font_size))
    #adv2ang_ax.set_position([0.15, 0.20, 0.80, 0.75])

    dblpattern_list = ["First", "Second"]

    adv1_settings = sg.Column([
        [sg.Push(),sg.Text("Advanced Plot 1", font=large_font),sg.Push()],
        [
            sg.Column([
                [
                    sg.Checkbox("Dbl Pattern?", default=False, font=small_font, key='adv1_dbl_check'), 
                    sg.Combo(dblpattern_list, default_value=dblpattern_list[0], readonly=True, font=small_font, key='adv1_dbl_combo')
                ],
                [
                    sg.Column([
                        [sg.Text("MM8 Scan 1", font=small_font)],
                        [sg.Listbox(mm8_scan1_vals, size=(5,10), expand_x=True, select_mode=sg.LISTBOX_SELECT_MODE_MULTIPLE, font=small_font, key='adv1_scan1_listbox')],
                        [sg.Button("Select All", expand_x=True, font=small_font, key='adv1_scan1_selectall')]
                    ], expand_y=True),
                    sg.Column([
                        [sg.Text("MM8 Scan 2",font=small_font)],
                        [sg.Listbox(mm8_scan2_vals, size=(5,10), expand_x=True, select_mode=sg.LISTBOX_SELECT_MODE_MULTIPLE, font=small_font, key='adv1_scan2_listbox')],
                        [sg.Button("Select All", expand_x=True, font=small_font, key='adv1_scan2_selectall')],
                    ], expand_y=True)
                ],
                [sg.Push(),sg.Button("Clear Selection", size=(15,1), key='adv1_clearselection'),sg.Push()],
                [sg.VPush()]
            ], expand_x=True, expand_y=True),
            sg.VerticalSeparator(),
            sg.Column([
                [
                    sg.Checkbox("Clust?",default=False,font=small_font,key='adv1_cluster_check'),
                    sg.Combo(values=["Spot","Ellipse"],default_value="Spot",readonly=True,size=7,expand_x=True,font=small_font,key="adv1_cluster_combo")
                ],
                [
                    sg.Checkbox("Fit?",default=False,font=small_font,key='adv1_fitting_check'),
                    sg.Combo(values=["Polar Spot","Cart Spot","Ellipse"],default_value="Polar Spot",readonly=True,size=10,expand_x=True,font=small_font,key="adv1_fitting_combo")
                ],
                [sg.Multiline("Plot 1 Information",size=(21,10),expand_y=True,expand_x=True,disabled=True,no_scrollbar=True,font='Courier '+xsmall_font_size,key="adv1_info")],
            ], expand_x=True, expand_y=True)
        ] 
    ], expand_x=True, expand_y=True, size=(0.3*window_width, 0.45*window_height))
    adv2_settings = sg.Column([
        [sg.Push(),sg.Text("Advanced Plot 2", font=large_font),sg.Push()],
        [
            sg.Column([
                [
                    sg.Checkbox("Dbl Pattern?", default=False, font=small_font, key='adv2_dbl_check'), 
                    sg.Combo(dblpattern_list, default_value=dblpattern_list[1], readonly=True, font=small_font, key='adv2_dbl_combo')
                ],
                [
                    sg.Column([
                        [sg.Text("MM8 Scan 1", font=small_font)],
                        [sg.Listbox(mm8_scan1_vals, size=(5,10), expand_x=True, select_mode=sg.LISTBOX_SELECT_MODE_MULTIPLE, font=small_font, key='adv2_scan1_listbox')],
                        [sg.Button("Select All", expand_x=True, font=small_font, key='adv2_scan1_selectall')],
                        [sg.Checkbox("Opp. 1?",font=small_font,default=False,key='adv2_scan1_oppadv1_check')]
                    ], expand_y=True),
                    sg.Column([
                        [sg.Text("MM8 Scan 2", font=small_font)],
                        [sg.Listbox(mm8_scan2_vals, size=(5,10), expand_x=True, select_mode=sg.LISTBOX_SELECT_MODE_MULTIPLE, font=small_font, key='adv2_scan2_listbox')],
                        [sg.Button("Select All", expand_x=True, font=small_font, key='adv2_scan2_selectall')],
                        [sg.Checkbox("Opp. 1?",font=small_font,default=False,key='adv2_scan2_oppadv1_check')]
                    ], expand_y=True)
                ],
                [sg.Push(),sg.Button("Clear Selection", size=(15,1), key='adv2_clearselection'),sg.Push()],
                [sg.VPush()]
            ], expand_x=True, expand_y=True),
            sg.VerticalSeparator(),
            sg.Column([
                [
                    sg.Checkbox("Clust?",default=False,font=small_font,key='adv2_cluster_check'),
                    sg.Combo(values=["Spot","Ellipse"],default_value="Spot",readonly=True,size=7,font=small_font,key="adv2_cluster_combo")
                ],
                [   
                    sg.Checkbox("Fit?",default=False,font=small_font,key='adv2_fitting_check'),
                    sg.Combo(values=["Polar Spot","Cart Spot","Ellipse"],default_value="Polar Spot",readonly=True,size=10,font=small_font,key="adv2_fitting_combo")
                ],
                [sg.Multiline("Plot 2 Information",size=(21,10),expand_y=True,expand_x=True,disabled=True,no_scrollbar=True,font='Courier '+xsmall_font_size,key="adv2_info")],
            ],expand_x=True,expand_y=True)
        ] 
    ], expand_x=True, expand_y=True, size=(0.3*window_width, 0.5*window_height))

    adv_layout = sg.Frame("", [[
        sg.Column([
            [sg.Push(),sg.Text("Advanced Plot 1",font=large_font), sg.Push()],
            [sg.Canvas(size=adv1_canvsize, expand_x=True, expand_y=True, key='adv1_canvas')],
            [sg.Canvas(size=adv1ang_canvsize, expand_x=True, expand_y=True, key='adv1ang_canvas')]
        ], expand_x=True, expand_y=True),
        sg.Column([
            [sg.Push(),sg.Text("Advanced Plot 2",font=large_font), sg.Push()],
            [sg.Canvas(size=adv2_canvsize, expand_x=True, expand_y=True, key='adv2_canvas')],
            [sg.Canvas(size=adv2ang_canvsize, expand_x=True, expand_y=True, key='adv2ang_canvas')]
        ],expand_x=True, expand_y=True),
        sg.VerticalSeparator(),
        sg.Column([
            [adv1_settings],
            [sg.HorizontalSeparator()],
            [adv2_settings]
        ], expand_x=True, expand_y=True)
    ]], expand_x=True, expand_y=True, font=large_font)

    layout = [[ adv_layout ]]
    plot_dict = {
        "adv1_fig":adv1_fig, "adv1_ax":adv1_ax,
        "adv2_fig":adv2_fig, "adv2_ax":adv2_ax,
        "adv1ang_fig":adv1ang_fig, "adv1ang_ax":adv1ang_ax,
        "adv2ang_fig":adv2ang_fig, "adv2ang_ax":adv2ang_ax
    }
    return layout, plot_dict

def create_calib_page(window_width, window_height, figure_scale, attribute_scale, PLOT_DPI):
    large_font_size = str(int(13*attribute_scale))
    med_font_size = str(int(11*attribute_scale))
    small_font_size = str(int(9*attribute_scale))
    large_font = "Helvetica "+large_font_size
    med_font = "Helvetica "+med_font_size
    med_font_bold = ("Helvetica", med_font_size, "bold")
    small_font = "Helvetica "+small_font_size

    #----------------------------------------------------------------
    # Create the Sum X/Y Layout
    #----------------------------------------------------------------   
    sumx_width, sumx_height = sumx_canvsize = (0.25*window_width, 0.3*window_height) # Set the figure size in pixel width
    sumx_figsize = (sumx_width*figure_scale/PLOT_DPI, sumx_height*figure_scale/PLOT_DPI)
    sumx_fig = Figure(figsize=sumx_figsize)
    sumx_ax = sumx_fig.add_subplot(111)

    sumy_width, sumy_height = sumy_canvsize = (0.25*window_width, 0.3*window_height) # Set the figure size in pixel width
    sumy_figsize = (sumy_width*figure_scale/PLOT_DPI, sumy_height*figure_scale/PLOT_DPI)
    sumy_fig = Figure(figsize=sumy_figsize)
    sumy_ax = sumy_fig.add_subplot(111)

    sumxy_layout = sg.Frame("Sum X/Y Fitting", [
        [
            sg.Column([
                [sg.Canvas(size=sumx_canvsize, expand_x=True, expand_y=True, key='sumx_canvas')],
                [
                    sg.Push(),
                    sg.Text("Avg. Sum X (ns):", font=small_font), 
                    sg.Input("", size=9, font=small_font, disabled=True, key='sumx_avg_val'),
                    sg.Text("    ",font=small_font),
                    sg.Text("Std. Sum X (ns):", font=small_font), 
                    sg.Input("", size=9, font=small_font, disabled=True, key='sumx_sig_val'),
                    sg.Push()
                ],
            ], expand_x=True, expand_y=True),
            sg.Column([
                [sg.Canvas(size=sumy_canvsize, expand_x=True, expand_y=True, key='sumy_canvas')],
                [
                    sg.Push(),
                    sg.Text("Avg. Sum Y (ns):", font=small_font), 
                    sg.Input("", size=9, font=small_font, disabled=True, key='sumy_avg_val'), 
                    sg.Text("    ",font=small_font),
                    sg.Text("Std. Sum Y (ns):", font=small_font), 
                    sg.Input("", size=9, font=small_font, disabled=True, key='sumy_sig_val'),
                    sg.Push()
                ],
            ], expand_x=True, expand_y=True),
        ],
        [sg.HorizontalSeparator()],
        [
            sg.Push(),
            sg.Text("Min Time (ns):", font=small_font),
            sg.Input("", size=8, font=small_font, key="sumxy_min_time"),
            sg.Text("    ",font=small_font),
            sg.Text("Max Time (ns):", font=small_font),
            sg.Input("", size=8, font=small_font, key="sumxy_max_time"),
            sg.Push()
        ]
    ], expand_x=True, expand_y=True, font=large_font)

    #----------------------------------------------------------------    
    # Create the Oscilloscope layout
    #----------------------------------------------------------------
    trig_width, trig_height = trig_canvsize = (0.6*window_width, 0.5*window_height) # Set the figure size in pixel width
    trig_figsize = (trig_width*figure_scale/PLOT_DPI, trig_height*figure_scale/PLOT_DPI)
    trig_fig = Figure(figsize=trig_figsize)
    trig_fig.tight_layout()
    trig_ax = trig_fig.add_subplot(111)

    oscilloscope_layout = sg.Frame("Oscilloscope View", [ 
        [sg.Canvas(key='osc_controls_cv')],
        [
            sg.Canvas(size=trig_canvsize, expand_x=True, expand_y=True, key='trig_canvas'),
            sg.Column([
                [sg.Checkbox("Show Current Event?", default=True, font=small_font, key='osc_currevt_check')],
                [sg.Checkbox("Hide Empty Events?",default=True, font=small_font, key='osc_hideempty_check')],
                [sg.Text("Event No.:",font=small_font), sg.Input("",size=9,font=small_font,key='osc_displayedevt')],
                [sg.Button("<",size=8,font=small_font,key='osc_left_button'), sg.Button(">",size=8,font=small_font,key='osc_right_button')],
                [sg.Text("",font=small_font)],
                [sg.Text("Start Time (ns)", font=small_font), sg.Input("", size=8, font=small_font, key='trig_start_time')],
                [sg.Text("End Time (ns)", font=small_font), sg.Input("", size=8, font=small_font, key='trig_end_time')],
            ], expand_x=True, expand_y=True)
        ]
    ], expand_x=True, expand_y=True, font=large_font)

    #----------------------------------------------------------------
    # Create the statistics window
    #----------------------------------------------------------------
    miss_width, miss_height = miss_canvsize = (0.25*window_width, 0.3*window_height) # Set the figure size in pixel width
    miss_figsize = (miss_width*figure_scale/PLOT_DPI, miss_height*figure_scale/PLOT_DPI)
    miss_fig = Figure(figsize=miss_figsize)
    miss_ax = miss_fig.add_subplot(111)

    cnt_width, cnt_height = cnt_canvsize = (0.25*window_width, 0.3*window_height) # Set the figure size in pixel width
    cnt_figsize = (cnt_width*figure_scale/PLOT_DPI, cnt_height*figure_scale/PLOT_DPI)
    cnt_fig = Figure(figsize=cnt_figsize)
    cnt_ax = cnt_fig.add_subplot(111)

    signalcounts_layout = sg.Frame("Signal Counts", [
        [sg.Canvas(size=cnt_canvsize, expand_x=True, expand_y=True, key='cnt_canvas')],
        [sg.Canvas(size=miss_canvsize, expand_x=True, expand_y=True, key='miss_canvas')],
    ], expand_x=True, expand_y=True, font=large_font)

    #----------------------------------------------------------------
    # Set up the final GUI Window
    #----------------------------------------------------------------
    layout = [[
        sg.Column([[sumxy_layout],[oscilloscope_layout]], expand_x=True, expand_y=True),
        signalcounts_layout
    ]]
    plot_dict = {
        "trig_fig":trig_fig, "trig_ax":trig_ax,
        "sumx_fig":sumx_fig, "sumx_ax":sumx_ax,
        "sumy_fig":sumy_fig, "sumy_ax":sumy_ax,
        "miss_fig":miss_fig, "miss_ax":miss_ax,
        "cnt_fig":cnt_fig, "cnt_ax":cnt_ax
    }

    return layout, plot_dict
