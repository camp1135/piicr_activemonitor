"""
@Desc: This class holds all of the timing and position information relevant to 
       a given spot/circle file for PI-ICR measurements. It is intended to only
       hold the raw spot information and no extrapolated values.
@Author: Scott Campbell, campbels@nscl.msu.edu
@Version: 22 February 2023
"""
import os
import numpy as np
import warnings

from .EvaDataFile import EvaDataFile

class DataFile:

    def __init__(self, filepath, filename, system, loadData=True):
        """ Constructor for the DataFile object
        Args:
            filepath (str): the location where the datafiles are stored, e.g. "C:/Desktop/"
            filename (str): the name of the datafile WITHOUT file extension, e.g. "myfile"
            system (System obj): the class object for the PI-ICR sytem used to collect the data
            loadData (bool, True): whether to load in the data immediately upon creation of the object
        """
        self.filepath = filepath
        self.filename = filename
        self.system = system

        self.firstLoadFinished = False
        self.CurrentEventID = 0
        self.EventsList = np.array([]) # list of Event objects ## NOTE: This is NEVER USED for this older datafile type
        self.ReconstructedEvents = np.array([]) # 2D list, [trigger_id, z_count, trigger_time, tof, y_pos, x_pos, recovered_bool]
        self.UniqueShotTrigid = None # The trigger id number for each MM8 shot
        self.UniqueShotStartIdx = None # List of indices in reconstructed events where a new MM8 shot occured
        self.UniqueShotCounts = None # Number of hits in each MM8 shot
        self.UniqueShotTimes = None # Get the trigger time for each unique event

        # Information about the MM8 settings that are being used
        self.evaDataFile = EvaDataFile()
        self.mm8info = None
        self.redcyc_accum_loc = "1:D:"
        self.mag_accum_loc = "1:C:"

        if loadData and not filename == None: self.updateDataFile()

    def loadDataMM8(self):
        """ The code in this method was adapted from the python EvaDataFile class written by Ryan Ringle,
            and adapted to read in additional information that is needed for the analysis here 
        """
        # First, check that the data file exists where the user specified it.
        fullpath = self.system.findMM8File(os.path.join(self.filepath, self.filename))
        if not os.path.isfile(fullpath):
            raise FileNotFoundError("The file "+str(fullpath)+" does not exist. Please check the location.")

        self.evaDataFile.flushData()
        self.evaDataFile.loadEvaFile(fullpath)
        header_lines = self.evaDataFile.header.split(b'\n')

        mass_dict  = { "mass":None, "charge":None }
        excit_dict = { "mass":None, "charge":None, "freq":None, "amp":None, "time":None }
        cool1_dict = { "freq":None, "amp":None, "time":None }
        cool2_dict = { "freq":None, "amp":None, "time":None }
        lomag_dict = { "freq":None, "amp":None, "time":None }
        himag_dict = { "freq":None, "amp":None, "time":None }
        scan1_dict = { "dev":None, "fct":None, "spec":None, "start":None, "stop":None, "step":None, "unit":None }
        scan2_dict = { "dev":None, "fct":None, "spec":None, "start":None, "stop":None, "step":None, "unit":None }
        timetable1_dict = { "names":None, "vals":None }
        timetable2_dict = { "names":None, "vals":None }
        timetable3_dict = { "names":None, "vals":None }
        user_header = None

        # Read in the Mass information
        mass_info = get_mm8_lines(header_lines, b'[Mass]')
        if mass_info != []:
            mass_dict["mass"]   = extract_mm8_val(mass_info, b'Mass=', b',')
            mass_dict["charge"] = extract_mm8_val(mass_info, b'Charge=', return_type=int)
        # Read in the Excitation information
        excit_info = get_mm8_lines(header_lines, b'[Excit]')
        if excit_info != []:
            excit_dict["mass"]   = extract_mm8_val(excit_info, b'Mass=', b',')
            excit_dict["charge"] = extract_mm8_val(excit_info, b'Charge=', b',')
            excit_dict["freq"]   = extract_mm8_val(excit_info, b'Freq=', b',', return_type=float)
            excit_dict["amp"]    = extract_mm8_val(excit_info, b'Amp=', b',', return_type=float)
            excit_dict["time"]   = extract_mm8_val(excit_info, b'Time=', b',', return_type=float)
        # Read in the Cool1 information
        cool1_info = get_mm8_lines(header_lines, b'[Cooling1]')
        if cool1_info != []:
            cool1_dict["freq"] = extract_mm8_val(cool1_info, b'Freq=', b',', return_type=float)
            cool1_dict["amp"]  = extract_mm8_val(cool1_info, b'Amp=', b',', return_type=float)
            cool1_dict["time"] = extract_mm8_val(cool1_info, b'Time=', b',', return_type=float)
        # Read in the Cool2 information
        cool2_info = get_mm8_lines(header_lines, b'[Cooling2]')
        if cool2_info != []:
            cool2_dict["freq"] = extract_mm8_val(cool2_info, b'Freq=', b',', return_type=float)
            cool2_dict["amp"]  = extract_mm8_val(cool2_info, b'Amp=', b',', return_type=float)
            cool2_dict["time"] = extract_mm8_val(cool2_info, b'Time=', b',', return_type=float)
        # Read in the lo magnetron information
        lomag_info = get_mm8_lines(header_lines, b'[LoMagnetron]')
        if lomag_info != []:
            lomag_dict["freq"] = extract_mm8_val(lomag_info, b'Freq=', b',', return_type=float)
            lomag_dict["amp"]  = extract_mm8_val(lomag_info, b'Amp=', b',', return_type=float)
            lomag_dict["time"] = extract_mm8_val(lomag_info, b'Time=', b',', return_type=float)
        # Read in the hi magnetron information
        himag_info = get_mm8_lines(header_lines, b'[HiMagnetron]')
        if himag_info != []:
            himag_dict["freq"] = extract_mm8_val(himag_info, b'Freq=', b',', return_type=float)
            himag_dict["amp"]  = extract_mm8_val(himag_info, b'Amp=', b',', return_type=float)
            himag_dict["time"] = extract_mm8_val(himag_info, b'Time=', b',', return_type=float)
        # Read in the Scan 1 information
        scan1_info = get_mm8_lines(header_lines, b'[SCAN0]', num_lines=2)
        if scan1_info != []:
            scan1_devinfo, scan1_valinfo = scan1_info
            scan1_dict["dev"]   = extract_mm8_val(scan1_devinfo, b'Dev=', b',')
            scan1_dict["fct"]   = extract_mm8_val(scan1_devinfo, b'Fct=', b',')
            scan1_dict["spec"]  = extract_mm8_val(scan1_devinfo, b'Spec=', b',')
            scan1_dict["start"] = extract_mm8_val(scan1_valinfo, b'Start=', b',', return_type=float)
            scan1_dict["stop"]  = extract_mm8_val(scan1_valinfo, b'Stop=', b',', return_type=float)
            scan1_dict["step"]  = extract_mm8_val(scan1_valinfo, b'Step=', b',', return_type=float)
            scan1_dict["unit"]  = extract_mm8_val(scan1_valinfo, b'Unit=', b',')
            scan1_dict["vals"]  = self.evaDataFile.numscan1val
        # Read in the Scan 2 information
        scan2_info = get_mm8_lines(header_lines, b'[SCAN1]', num_lines=2)
        if scan2_info != []:
            scan2_devinfo, scan2_valinfo = scan2_info
            scan2_dict["dev"]   = extract_mm8_val(scan2_devinfo, b'Dev=', b',')
            scan2_dict["fct"]   = extract_mm8_val(scan2_devinfo, b'Fct=', b',')
            scan2_dict["spec"]  = extract_mm8_val(scan2_devinfo, b'Spec=', b',')
            scan2_dict["start"] = extract_mm8_val(scan2_valinfo, b'Start=', b',', return_type=float)
            scan2_dict["stop"]  = extract_mm8_val(scan2_valinfo, b'Stop=', b',', return_type=float)
            scan2_dict["step"]  = extract_mm8_val(scan2_valinfo, b'Step=', b',', return_type=float)
            scan2_dict["unit"]  = extract_mm8_val(scan2_valinfo, b'Unit=', b',')
            scan2_dict["vals"]  = self.evaDataFile.numscan2val
        # Extract the User Header information
        userheader_info = get_mm8_lines(header_lines, b'[USERHEADER]')
        if userheader_info != []: user_header = userheader_info.decode('utf-8').strip()
        # Extract the Time Table 1 information
        timetable1_lines = get_mm8_lines(header_lines, b' TimeTab=0, ', num_lines=27)
        if len(timetable1_lines) > 0:
            name_list, val_list = [], []
            for line in timetable1_lines[1:]:
                name_list.append( extract_mm8_val(line, b':', b':') )
                val_list.append( extract_mm8_val(line, end_bytes=b':', reverse=True, return_type=float))
            timetable1_dict['names'] = name_list
            timetable1_dict['vals'] = val_list
        # Extract the Time Table 2 information
        timetable2_lines = get_mm8_lines(header_lines, b' TimeTab=1, ', num_lines=27)
        if len(timetable2_lines) > 0:
            name_list, val_list = [], []
            for line in timetable2_lines[1:]:
                name_list.append( extract_mm8_val(line, b':', b':') )
                val_list.append( extract_mm8_val(line, end_bytes=b':', reverse=True, return_type=float))
            timetable2_dict['names'] = name_list
            timetable2_dict['vals'] = val_list
        # Extract the Time Table 3 information
        timetable3_lines = get_mm8_lines(header_lines, b' TimeTab=2, ', num_lines=27)
        if len(timetable3_lines) > 0:
            name_list, val_list = [], []
            for line in timetable3_lines[1:]:
                name_list.append( extract_mm8_val(line, b':', b':') )
                val_list.append( extract_mm8_val(line, end_bytes=b':', reverse=True, return_type=float))
            timetable3_dict['names'] = name_list
            timetable3_dict['vals'] = val_list

        self.mm8info = {
            "file_path":fullpath,
            "start_time":self.evaDataFile.starttime,
            "end_time":self.evaDataFile.endtime,
            "mass_info":mass_dict,
            "excit_info":excit_dict,
            "cool1_info":cool1_dict,
            "cool2_info":cool2_dict,
            "lomag_info":lomag_dict,
            "himag_info":himag_dict,
            "scan1_info":scan1_dict,
            "scan2_info":scan2_dict,
            "timing1_info":timetable1_dict,
            "timing2_info":timetable2_dict,
            "timing3_info":timetable3_dict,
            "user_header":user_header
        }
    def loadDataTDC(self, start_line=0):
        """ Reads the data written by the TDC and reconstructs the events to position & tof information
            The datafile always has a header of four lines before data begins. The first 4 lines will always be discarded.
        Args:
            start_line (str): integer of line in file to start reading from AFTER THE HEADER. 
        Returns: the last line read in the datafile
        """
        header_length = 4 # number of lines for the header of the datafile
        full_path = os.path.join(self.filepath, os.path.splitext(self.filename)[0]+self.system.getDatafileExt())
        if not os.path.isfile(full_path):
            raise FileNotFoundError("The file "+str(full_path)+" does not exist. Please check the location.")
        # Read in the new data
        new_data = [] # 2d list holding all the channel ids and timestamps
        try: 
            with warnings.catch_warnings(): # supress the empty file warning
                warnings.simplefilter("ignore")
                new_data = np.loadtxt(full_path, delimiter='\t', dtype=float, skiprows=header_length+start_line)
        except: return start_line # try block suppresses a warning if datafile is completely empty  
        if len(new_data.shape) < 2 and len(new_data) > 0: # only one entry, save it
            new_data = new_data[np.newaxis]
        if len(new_data.shape) < 2: return start_line # no data, return the start line
        # Condition and process the data as necessary
        num_lines_read = len(new_data)
        new_data[:,1] = np.divide(new_data[:,1], 1e12) # convert the trigger timestamp from ps to s
        new_data[:,4] = np.divide(new_data[:,4], 1e3) # convert the TOF from ns to us
        # Create the reconstructed events data structure
        tids, x, y, tof, sumx, sumy = new_data[:,0], new_data[:,2], new_data[:,3], new_data[:,4], new_data[:,5], new_data[:,6]
        unique_trigid, count = np.unique(tids, return_counts=True)
        z_list = np.repeat(count, count)[np.newaxis].T # the last bit makes it a vertical list to insert into the data format
        recover_flag = ((sumx < 0) | (sumy < 0))[np.newaxis].T # the last bit makes it a vertical list to insert into the data format
        new_data = np.concatenate( ( new_data[:,0:1], z_list, new_data[:,1:], recover_flag ), axis=1 )
        # For the reconstructed dataset, cutout any positions that are NOT real
        pos_cut_idx = np.sqrt(np.power(x,2)+np.power(y,2)) <= 1000
        new_data = new_data[ pos_cut_idx ] 
        # Data Structure: tid, z, time, x, y, tof, sumx, sumy, recovered_flag
        if len(self.ReconstructedEvents) == 0: self.ReconstructedEvents = new_data
        else: self.ReconstructedEvents = np.concatenate((self.ReconstructedEvents, new_data))
        # Determine the indices of the unique hits
        unique_trigid, idx_start = np.unique(self.ReconstructedEvents[:,0], return_index=True)
        self.UniqueShotTrigid = unique_trigid
        self.UniqueShotStartIdx = idx_start
        self.UniqueShotTimes = self.ReconstructedEvents[idx_start,2] # Get the trigger time for each unique event
        self.UniqueShotCounts = self.ReconstructedEvents[idx_start,1] # Get the saved z count for each unique event 
        self.firstLoadFinished = True
        return start_line+num_lines_read
    def updateDataFile(self, start_line=0):
        """ Dummy method to be used by the Visualization GUI"""
        if start_line == 0:
            self.flushData()
            self.loadDataMM8() 
        try:   
            lastStartLine = self.loadDataTDC(start_line=start_line)
            return lastStartLine
        except FileNotFoundError as e:
            return start_line
    def flushData(self):
        self.firstLoadFinished = False
        self.ReconstructedEvents = [] 
        self.UniqueTrigid = None
        self.UniqueIdxStart = None
        self.UniqueShotCounts = None
        self.mm8info = None

    ##########################################################################
    # Getter methods for file/hit information
    ##########################################################################
    def getFilePath(self): return self.filepath
    def getFileName(self): return self.filename
    def getSpecies(self): return self.mm8info["mass_info"]["mass"]
    def getCharge(self): return self.mm8info["mass_info"]["charge"]
    
    def getAllEvents(self): return [] # This is not defined for this datafile type!
    def getAllReconstructedEvents(self): return self.ReconstructedEvents
    def getZ(self, unique=True): 
        """ NOTE: will return the z count for each UNIQUE trigid """
        if len(self.ReconstructedEvents) == 0: return []
        if unique: return self.UniqueShotCounts
        else: return self.ReconstructedEvents[:,1]
    def getTriggerTimes(self, unique=True):
        """ NOTE: will return the trigger time for each UNIQUE trigid """
        if len(self.ReconstructedEvents) == 0: return []
        if unique: return self.UniqueShotTimes
        else: return self.ReconstructedEvents[:,2]
    def getTriggerIDs(self, unique=True):
        if len(self.ReconstructedEvents) == 0: return []
        if unique: return self.UniqueShotTrigid
        else: return self.ReconstructedEvents[:,0]
    def getUniqueTriggerIDsAndTimes(self):
        return self.UniqueShotTrigid, self.UniqueShotTimes
    def getPositions(self):
        if len(self.ReconstructedEvents) == 0: return []
        return self.ReconstructedEvents[:,3:5]
    def getToFs(self):
        if len(self.ReconstructedEvents) == 0: return []
        return self.ReconstructedEvents[:,5]
    def getRecoveredFlag(self, indices=None):
        """ Since we have no real way of knowing if it was recovered, just assume all were not recovered """
        if len(self.ReconstructedEvents) == 0: return []
        if not indices == None and len(indices) > 0: return self.ReconstructedEvents[indices,9] 
        return self.ReconstructedEvents[:,8]
    def getRecoveredCount(self, indices=None):
        if len(self.ReconstructedEvents) == 0: return 0
        if not indices == None and len(indices) > 0: return np.count_nonzero( self.ReconstructedEvents[indices,9] )
        return np.count_nonzero( self.ReconstructedEvents[:,8] )
    def getMissingX1Count(self, indices=None):
        return 1 # this cannot be determined from this file type
    def getMissingX2Count(self, indices=None):
        return 2 # this cannot be determined from this file type
    def getMissingY1Count(self, indices=None):
        return 3 # this cannot be determined from this file type
    def getMissingY2Count(self, indices=None):
        return 4 # this cannot be determined from this file type
    def getSumXSumY(self):
        if len(self.ReconstructedEvents) == 0: return [], []
        return self.ReconstructedEvents[:,6], self.ReconstructedEvents[:,7]
    def getSignalCounts(self):
        return 1,2,3,4,5 # this cannot be determined from this file type
    def getStartTimeTDC(self):
        if len(self.ReconstructedEvents) < 1: return None 
        return self.ReconstructedEvents[0][2]

    ##########################################################################
    # Getter methods for MM8 related information
    ##########################################################################
    def setRedCycTimingLoc(self, loc_str): self.redcyc_accum_loc = loc_str
    def setMagTimingLoc(self, loc_str): self.mag_accum_loc = loc_str

    def getMM8FilePath(self): return self.mm8info['file_path']
    def getMM8Info(self): return self.mm8info
    def getMagAccumTime(self): return self.getMM8Timing(self.mag_accum_loc)
    def getRedcycAccumTime(self): return self.getMM8Timing(self.redcyc_accum_loc)
    def getMM8CyclotronFrequency(self): 
        conv_afg_loc = self.system.getConvAfgLoc()
        if conv_afg_loc == "EXCIT": return self.mm8info["excit_info"]["freq"]
        elif conv_afg_loc == "Cooling1": return self.mm8info["cool1_info"]["freq"]
        elif conv_afg_loc == "Cooling2": return self.mm8info["cool2_info"]["freq"]
        elif conv_afg_loc == "LoMagnetron": return self.mm8info["lomag_info"]["freq"]
        elif conv_afg_loc == "HiMagnetron": return self.mm8info["himag_info"]["freq"]
        else: return np.nan
    def getMM8ReducedCyclotronFrequency(self): 
        dip_afg_loc = self.system.getDipoleAfgLoc()
        if dip_afg_loc == "EXCIT": return self.mm8info["excit_info"]["freq"]
        elif dip_afg_loc == "Cooling1": return self.mm8info["cool1_info"]["freq"]
        elif dip_afg_loc == "Cooling2": return self.mm8info["cool2_info"]["freq"]
        elif dip_afg_loc == "LoMagnetron": return self.mm8info["lomag_info"]["freq"]
        elif dip_afg_loc == "HiMagnetron": return self.mm8info["himag_info"]["freq"]
        else: return np.nan
    def getMM8MagnetronFrequency(self): return self.getMM8CyclotronFrequency() - self.getMM8ReducedCyclotronFrequency()
    def getStartTime(self):
        if self.mm8info == None: return 0 
        return self.mm8info["start_time"]
    def getEndTime(self): 
        if self.mm8info == None: return 0 
        return self.mm8info["end_time"]
    def getMidTime(self): 
        if self.mm8info == None: return 0 
        return int((self.mm8info["start_time"]+self.mm8info["end_time"])/2)
    def getMM8Scan1Vals(self): 
        if self.mm8info == None or self.mm8info["scan1_info"] == None: return []
        return self.mm8info["scan1_info"]["vals"]
    def getMM8Scan2Vals(self): 
        if self.mm8info == None or self.mm8info["scan2_info"] == None: return [] 
        return self.mm8info["scan2_info"]["vals"]
    def getMM8Scan1Len(self): 
        if self.mm8info == None or self.mm8info["scan1_info"] == None: return 0 
        return len(self.mm8info['scan1_info']['vals'])
    def getMM8Scan2Len(self): 
        if self.mm8info == None or self.mm8info["scan2_info"] == None: return 0 
        return len(self.mm8info['scan2_info']['vals'])
    def getMM8Timing(self, loc_str):
        # Parse out the location where the timing is stored
        if loc_str == None or self.mm8info == None: return None
        try: 
            tablenum_end = loc_str.find(':')
            tablenum = loc_str[:tablenum_end].strip()
            tablenum = int(tablenum)
            if tablenum < 1 or tablenum > 3: tablenum = None
        except: tablenum = None
        try:
            timelett_end = loc_str.find(':', tablenum_end+1)
            timelett = loc_str[tablenum_end+1 : timelett_end].strip()
            if len(timelett) == 1 and timelett.isalpha(): timelett = timelett.lower()
            else: timelett = None
        except: timelett = None
        try: 
            timename_end = loc_str.find(':', timelett_end+1)
            timename = loc_str[timelett_end+1 : timename_end].strip()
            if timename == '': timename = None
        except: timename = None
        # fetch the value at the location specified
        if not tablenum == None: # Table is specified!
            if timelett == None and timename == None: return None
            timingtable_dict = self.mm8info['timing'+str(tablenum)+'_info']
            # check if the timename is specified/present
            if not timingtable_dict["names"] == None and timename in timingtable_dict["names"]: timename_idx = timingtable_dict["names"].index(timename)
            else: timename_idx = None
            # if the timing letter specified, check
            if not timelett == None:
                timelett_idx = ord(timelett)-97
                if (not timename == None) and timelett_idx == timename_idx: return timingtable_dict["vals"][timename_idx]
                elif timename == None: return timingtable_dict["vals"][timelett_idx]
            elif not timename == None: return timingtable_dict["vals"][timename_idx]
        else: # No table specified, have to search them all...
            if timelett == None or timename == None: return None
            tt1, tt2, tt3 = self.mm8info['timing1_info'], self.mm8info['timing2_info'], self.mm8info['timing3_info']
            timelett_idx = ord(timelett)-97
            if not tt1["names"] == None and timename in tt1["names"]: timename1_idx = tt1["names"].index(timename)
            else: timename1_idx = None
            if not tt2["names"] == None and timename in tt2["names"]: timename2_idx = tt2["names"].index(timename)
            else: timename2_idx = None
            if not tt3["names"] == None and timename in tt3["names"]: timename3_idx = tt3["names"].index(timename)
            else: timename3_idx = None
            # If any match, return that value. Fist match wins if multiple exist
            if timelett_idx == timename1_idx: return tt1["vals"][timelett_idx]
            elif timelett_idx == timename2_idx: return tt2["vals"][timelett_idx]
            elif timelett_idx == timename3_idx: return tt3["vals"][timelett_idx]
        return None

##########################################################################
# Helper methods for extracting MM8 related information
##########################################################################
def get_mm8_lines(data_list, header_bytes, num_lines=1):
    try:
        header_idx = data_list.index(header_bytes)
        if header_idx == -1: return []
        elif num_lines == 1: return data_list[header_idx+1]
        else: return data_list[header_idx+1 : header_idx+1+num_lines]
    except: return []
def extract_mm8_val(line, start_bytes=None, end_bytes=None, return_type=str, reverse=False):
    if reverse:
        start_idx = 0 if end_bytes == None else line.rfind(end_bytes)
        start_len = 0 if end_bytes == None else len(end_bytes)
        end_idx = len(line) if start_bytes == None else line.rfind(start_bytes)
    else:
        start_idx = 0 if start_bytes == None else line.find(start_bytes)
        start_len = 0 if start_bytes == None else len(start_bytes)
        end_idx = len(line) if end_bytes == None else line.find(end_bytes, start_idx+1)
    return return_type( line[start_idx+start_len : end_idx].decode('utf-8').strip() )
