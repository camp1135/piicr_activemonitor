
import os
from struct import unpack
from numpy import zeros, int16

""" Class definition for a datafile object containing all the relevent MM8 header 
    information. This is a modified version of Ryan Ringle's (ringle@frib.msu.edu) 
    EvaDataFile data structure. 
"""
class MM8DataFile:
    #############################################################################
    # Constructor
    #############################################################################
    def __init__(self):
        """ Constructor for the object. Creates empty datafile information objects """
        self.flushData()

    #############################################################################
    # Public Methods
    #   User should call these methods to operate/access the stored information
    #############################################################################
    def flushData(self):
        """ Clears the datafile objects """
        # raw header objects
        self.filename = 'N/A'
        self.header = 'N/A'
        self.numscan1 = -1
        self.numscan2 = -1
        self.numscan1val = []
        self.numscan2val = []
        self.starttime = 0
        self.endtime = 0
        # parsed header objects
        self.mm8HeaderInfo = {
            "mass_info":{},
            "excit_info":{},
            "cool1_info":{},
            "cool2_info":{},
            "lomag_info":{},
            "himag_info":{},
            "scan1_info":{},
            "scan2_info":{},
            "timing1_info":{},
            "timing2_info":{},
            "timing3_info":{},
            "user_header":None
        }      
    
    def loadFile(self, filename):
        """ Reads the MM8 datafile and extracts the header information 
        Args: 
            filename (str): the full datafile path for the MM8 datafile
        """
        if not os.path.isfile(filename):
            raise FileNotFoundError("The file "+str(filename)+" does not exist. Please check the location.")
        self.filename = filename
        self.__loadHeader__(filename)
        self.__parseHeader__()
    
    #----------------------------------------------------------------------------
    # Access methods for the loaded MM8 information
    #----------------------------------------------------------------------------
    def getFileName(self): return self.filename
    def getMM8Info(self): return self.mm8HeaderInfo
    def getSpecies(self): return self.mm8HeaderInfo["mass_info"]["mass"]
    def getCharge(self): return self.mm8HeaderInfo["mass_info"]["charge"]
    def getStartTime(self): return self.starttime
    def getEndTime(self): return self.endtime
    def getTimingValue(self, loc_str):
        """ Parses the MM8 timing tables to retrieve a stored time value
        Args:
            loc_str (str): search pattern string of the format 'X:Y:Z' where X is 
                           table number (1,2,3), Y is the timing letter (A,...,Z),
                           and Z is the name of the timing channel. Only X or Y are
                           required to find the timing, e.g. '1:A:' and '1::MyTimeName'
                           and '1:A:MyTimeName' are all valid 
        Returns: (float, None) timing value stored at the location, or None if no match is found
        """
        # Parse out the location where the timing is stored
        if loc_str == None or self.mm8HeaderInfo == None: return None
        try: 
            tablenum_end = loc_str.find(':')
            tablenum = loc_str[:tablenum_end].strip()
            tablenum = int(tablenum)
            if tablenum < 1 or tablenum > 3: tablenum = None
        except: raise ValueError("Could not parse a table number from the search pattern string!")
        try:
            timelett_end = loc_str.find(':', tablenum_end+1)
            timelett = loc_str[tablenum_end+1 : timelett_end].strip()
            if len(timelett) == 1 and timelett.isalpha(): timelett = timelett.lower()
            else: timelett = None
        except: timelett = None
        try: 
            timename_end = loc_str.find(':', timelett_end+1)
            timename = loc_str[timelett_end+1 : timename_end].strip()
            if timename == '': timename = None
        except: timename = None
        # fetch the value at the location specified
        if timelett == None and timename == None: return None
        timingtable_dict = self.mm8HeaderInfo['timing'+str(tablenum)+'_info']
        # check if the timename is specified/present
        if not timingtable_dict["names"] == None and timename in timingtable_dict["names"]: timename_idx = timingtable_dict["names"].index(timename)
        else: timename_idx = None
        # if the timing letter specified, check
        if not timelett == None:
            timelett_idx = ord(timelett)-97
            if (not timename == None) and timelett_idx == timename_idx: return timingtable_dict["vals"][timename_idx]
            elif timename == None: return timingtable_dict["vals"][timelett_idx]
        elif not timename == None: return timingtable_dict["vals"][timename_idx]
    def getFrequencyValue(self, afg_source):
        """ Fetches the frequency set for a given afg excitation channel
        Args: 
            afg_source (str): the name of the afg channel to fetch the frequency from.
                              Can be: EXCIT, Cooling1, Cooling2, LoMagnetron, HiMagnetron
        Returns: (float): the frequency set for the channel
        """ 
        if   afg_source == "EXCIT": return self.mm8HeaderInfo["excit_info"]["freq"]
        elif afg_source == "Cooling1": return self.mm8HeaderInfo["cool1_info"]["freq"]
        elif afg_source == "Cooling2": return self.mm8HeaderInfo["cool2_info"]["freq"]
        elif afg_source == "LoMagnetron": return self.mm8HeaderInfo["lomag_info"]["freq"]
        elif afg_source == "HiMagnetron": return self.mm8HeaderInfo["himag_info"]["freq"]
        else: raise ValueError("Invalid frequency afg source name provided.")
    def getMM8Scan1Vals(self): 
        if self.mm8HeaderInfo == None or self.mm8HeaderInfo["scan1_info"] == None: return []
        return self.mm8HeaderInfo["scan1_info"]["vals"]
    def getMM8Scan2Vals(self): 
        if self.mm8HeaderInfo == None: return [] 
        return self.mm8HeaderInfo["scan2_info"]["vals"]
    def getMM8Scan1Len(self): 
        if self.mm8HeaderInfo == None or self.mm8HeaderInfo["scan1_info"] == None: return 0 
        return len(self.mm8HeaderInfo['scan1_info']['vals'])   
    def getMM8Scan2Len(self): 
        if self.mm8HeaderInfo == None or self.mm8HeaderInfo["scan2_info"] == None: return 0 
        return len(self.mm8HeaderInfo['scan2_info']['vals'])

    #############################################################################
    # Pseudo-private Methods
    #   User should not call or modify these methods, unless they know what they
    #   are doing. These are called internally by the 'public' methods
    #############################################################################
    def __getMM8Lines__(self, data_list, header_bytes, num_lines=1):
        try:
            header_idx = data_list.index(header_bytes)
            if header_idx == -1: return []
            elif num_lines == 1: return data_list[header_idx+1]
            else: return data_list[header_idx+1 : header_idx+1+num_lines]
        except: return []

    def __extractMM8Val__(self, line, start_bytes=None, end_bytes=None, return_type=str, reverse=False):
        if reverse:
            start_idx = 0 if end_bytes == None else line.rfind(end_bytes)
            start_len = 0 if end_bytes == None else len(end_bytes)
            end_idx = len(line) if start_bytes == None else line.rfind(start_bytes)
        else:
            start_idx = 0 if start_bytes == None else line.find(start_bytes)
            start_len = 0 if start_bytes == None else len(start_bytes)
            end_idx = len(line) if end_bytes == None else line.find(end_bytes, start_idx+1)
        return return_type( line[start_idx+start_len : end_idx].decode('utf-8').strip() )

    def __loadHeader__(self, filename):
        try:
            datafile = open(self.filename,'rb')
            datafile.seek(0,2)
            eof = int(datafile.tell())
            datafile.seek(0)
            (headerlength,) = unpack('=l',datafile.read(4))
            (datastart,) = unpack('=l',datafile.read(4))
            self.header = datafile.read(headerlength)
            #	datafile.seek(8+self.headerlength)
            (self.numscan1,) = unpack('=l',datafile.read(4))
            self.numscan1val = []
            for _ in range(self.numscan1):
                (value,) = unpack('=d',datafile.read(8))
                self.numscan1val.append(value)
            self.numscan1val = list(set(self.numscan1val)) # some NoScans will have duplicate [0,0], this removes that...
            (self.numscan2,) = unpack('=l',datafile.read(4))
            for _ in range(self.numscan2):
                (value,) = unpack('=d',datafile.read(8))
                self.numscan2val.append(value)
            self.numscan2val = list(set(self.numscan2val)) # some NoScans will have duplicate [0,0], this removes that...

            #determine the number of channels and bin width
            mcaindex         = self.header.find(b'[MCA]')
            tpcindex         = self.header.find(b'TimePerChannel',mcaindex)
            endindex         = self.header.find(b'\xb5',tpcindex)
            channelbinwidth  = float(self.header[tpcindex+15:endindex])
            chanindex        = self.header.find(b'Channels=',mcaindex)
            endindex         = self.header.find(b',',chanindex)
            self.numchannels = int(self.header[chanindex+9:endindex])
            
            #expand binary bin data into self.bindata
            self.bindata=[]
            if self.numscan1<1 or self.numchannels<1:
                print('Invalid array size.')
                print('Number of scans = '+self.numscan1)
                print('Number of channels = '+self.numchannels)
                return

            # Determine the MM8 start and end times
            temparray = zeros((self.numscan1,self.numchannels),dtype=int16)
            run, start = True, True
            while run:
                for i in range(self.numscan1):
                    #-----------------------------------------------
                    # Read the timestamp for the scan
                    if int(datafile.tell())+6>eof:
                        self.endtime = rectime
                        run=False
                        break
                    else:
                        (reclength,)=unpack('h', datafile.read(2))
                        (rectime,)=unpack('l', datafile.read(4))
                        if start:
                            self.starttime = rectime
                            start=False
                    #-----------------------------------------------
                    # Read the binary data for that scan
                    # Need to do this to accurately get the timestamps!
                    if reclength==4: continue
                    elif reclength>=4+self.numchannels:
                        for j in range(self.numchannels):
                            if int(datafile.tell())+2>eof:
                                run=False
                                break
                            else: (temparray[i,j],) = unpack('=h',datafile.read(2))
                    else:
                        for j in range(int((reclength-4)/4)):
                            if int(datafile.tell())+4>eof:
                                run=False
                                break
                            else:
                                (binnum,)=unpack('=h',datafile.read(2))
                                (counts,)=unpack('=h',datafile.read(2))
                                temparray[i,binnum] = counts

            # close the opened file object
            datafile.close()
        except:
            raise IOError('Could not open '+filename+' for binary reading.')

    def __parseHeader__(self):
        header_lines = self.header.split(b'\n')
        # create empty objects to be filled below
        mass_dict  = { "mass":None, "charge":None }
        excit_dict = { "mass":None, "charge":None, "freq":None, "amp":None, "time":None }
        cool1_dict = { "freq":None, "amp":None, "time":None }
        cool2_dict = { "freq":None, "amp":None, "time":None }
        lomag_dict = { "freq":None, "amp":None, "time":None }
        himag_dict = { "freq":None, "amp":None, "time":None }
        scan1_dict = { "dev":None, "fct":None, "spec":None, "start":None, "stop":None, "step":None, "unit":None }
        scan2_dict = { "dev":None, "fct":None, "spec":None, "start":None, "stop":None, "step":None, "unit":None }
        timetable1_dict = { "names":None, "vals":None }
        timetable2_dict = { "names":None, "vals":None }
        timetable3_dict = { "names":None, "vals":None }
        user_header = None
        # read in the mass information
        mass_info = self.__getMM8Lines__(header_lines, b'[Mass]')
        if mass_info != []:
            mass_dict["mass"]   = self.__extractMM8Val__(mass_info, b'Mass=', b',')
            mass_dict["charge"] = self.__extractMM8Val__(mass_info, b'Charge=', return_type=int)
        # read in the excitation information
        excit_info = self.__getMM8Lines__(header_lines, b'[Excit]')
        if excit_info != []:
            excit_dict["mass"]   = self.__extractMM8Val__(excit_info, b'Mass=', b',')
            excit_dict["charge"] = self.__extractMM8Val__(excit_info, b'Charge=', b',')
            excit_dict["freq"]   = self.__extractMM8Val__(excit_info, b'Freq=', b',', return_type=float)
            excit_dict["amp"]    = self.__extractMM8Val__(excit_info, b'Amp=', b',', return_type=float)
            excit_dict["time"]   = self.__extractMM8Val__(excit_info, b'Time=', b',', return_type=float)
        # read in the Cool1 information
        cool1_info = self.__getMM8Lines__(header_lines, b'[Cooling1]')
        if cool1_info != []:
            cool1_dict["freq"] = self.__extractMM8Val__(cool1_info, b'Freq=', b',', return_type=float)
            cool1_dict["amp"]  = self.__extractMM8Val__(cool1_info, b'Amp=', b',', return_type=float)
            cool1_dict["time"] = self.__extractMM8Val__(cool1_info, b'Time=', b',', return_type=float)
        # read in the Cool2 information
        cool2_info = self.__getMM8Lines__(header_lines, b'[Cooling2]')
        if cool2_info != []:
            cool2_dict["freq"] = self.__extractMM8Val__(cool2_info, b'Freq=', b',', return_type=float)
            cool2_dict["amp"]  = self.__extractMM8Val__(cool2_info, b'Amp=', b',', return_type=float)
            cool2_dict["time"] = self.__extractMM8Val__(cool2_info, b'Time=', b',', return_type=float)
        # read in the lo magnetron information
        lomag_info = self.__getMM8Lines__(header_lines, b'[LoMagnetron]')
        if lomag_info != []:
            lomag_dict["freq"] = self.__extractMM8Val__(lomag_info, b'Freq=', b',', return_type=float)
            lomag_dict["amp"]  = self.__extractMM8Val__(lomag_info, b'Amp=', b',', return_type=float)
            lomag_dict["time"] = self.__extractMM8Val__(lomag_info, b'Time=', b',', return_type=float)
        # read in the hi magnetron information
        himag_info = self.__getMM8Lines__(header_lines, b'[HiMagnetron]')
        if himag_info != []:
            himag_dict["freq"] = self.__extractMM8Val__(himag_info, b'Freq=', b',', return_type=float)
            himag_dict["amp"]  = self.__extractMM8Val__(himag_info, b'Amp=', b',', return_type=float)
            himag_dict["time"] = self.__extractMM8Val__(himag_info, b'Time=', b',', return_type=float)
        # read in the Scan 1 information
        scan1_info = self.__getMM8Lines__(header_lines, b'[SCAN0]', num_lines=2)
        if scan1_info != []:
            scan1_devinfo, scan1_valinfo = scan1_info
            scan1_dict["dev"]   = self.__extractMM8Val__(scan1_devinfo, b'Dev=', b',')
            scan1_dict["fct"]   = self.__extractMM8Val__(scan1_devinfo, b'Fct=', b',')
            scan1_dict["spec"]  = self.__extractMM8Val__(scan1_devinfo, b'Spec=', b',')
            scan1_dict["start"] = self.__extractMM8Val__(scan1_valinfo, b'Start=', b',', return_type=float)
            scan1_dict["stop"]  = self.__extractMM8Val__(scan1_valinfo, b'Stop=', b',', return_type=float)
            scan1_dict["step"]  = self.__extractMM8Val__(scan1_valinfo, b'Step=', b',', return_type=float)
            scan1_dict["unit"]  = self.__extractMM8Val__(scan1_valinfo, b'Unit=', b',')
            scan1_dict["vals"]  = self.numscan1val
        # read in the Scan 2 information
        scan2_info = self.__getMM8Lines__(header_lines, b'[SCAN1]', num_lines=2)
        if scan2_info != []:
            scan2_devinfo, scan2_valinfo = scan2_info
            scan2_dict["dev"]   = self.__extractMM8Val__(scan2_devinfo, b'Dev=', b',')
            scan2_dict["fct"]   = self.__extractMM8Val__(scan2_devinfo, b'Fct=', b',')
            scan2_dict["spec"]  = self.__extractMM8Val__(scan2_devinfo, b'Spec=', b',')
            scan2_dict["start"] = self.__extractMM8Val__(scan2_valinfo, b'Start=', b',', return_type=float)
            scan2_dict["stop"]  = self.__extractMM8Val__(scan2_valinfo, b'Stop=', b',', return_type=float)
            scan2_dict["step"]  = self.__extractMM8Val__(scan2_valinfo, b'Step=', b',', return_type=float)
            scan2_dict["unit"]  = self.__extractMM8Val__(scan2_valinfo, b'Unit=', b',')
            scan2_dict["vals"]  = self.numscan2val
        # extract the User Header information
        userheader_info = self.__getMM8Lines__(header_lines, b'[USERHEADER]')
        if userheader_info != []: user_header = userheader_info.decode('utf-8').strip()
        # extract the Time Table 1 information
        timetable1_lines = self.__getMM8Lines__(header_lines, b' TimeTab=0, ', num_lines=27)
        if len(timetable1_lines) > 0:
            name_list, val_list = [], []
            for line in timetable1_lines[1:]:
                name_list.append( self.__extractMM8Val__(line, b':', b':') )
                val_list.append( self.__extractMM8Val__(line, end_bytes=b':', reverse=True, return_type=float))
            timetable1_dict['names'] = name_list
            timetable1_dict['vals'] = val_list
        # extract the Time Table 2 information
        timetable2_lines = self.__getMM8Lines__(header_lines, b' TimeTab=1, ', num_lines=27)
        if len(timetable2_lines) > 0:
            name_list, val_list = [], []
            for line in timetable2_lines[1:]:
                name_list.append( self.__extractMM8Val__(line, b':', b':') )
                val_list.append( self.__extractMM8Val__(line, end_bytes=b':', reverse=True, return_type=float))
            timetable2_dict['names'] = name_list
            timetable2_dict['vals'] = val_list
        # extract the Time Table 3 information
        timetable3_lines = self.__getMM8Lines__(header_lines, b' TimeTab=2, ', num_lines=27)
        if len(timetable3_lines) > 0:
            name_list, val_list = [], []
            for line in timetable3_lines[1:]:
                name_list.append( self.__extractMM8Val__(line, b':', b':') )
                val_list.append( self.__extractMM8Val__(line, end_bytes=b':', reverse=True, return_type=float))
            timetable3_dict['names'] = name_list
            timetable3_dict['vals'] = val_list

        self.mm8HeaderInfo = {
            "mass_info":mass_dict,
            "excit_info":excit_dict,
            "cool1_info":cool1_dict,
            "cool2_info":cool2_dict,
            "lomag_info":lomag_dict,
            "himag_info":himag_dict,
            "scan1_info":scan1_dict,
            "scan2_info":scan2_dict,
            "timing1_info":timetable1_dict,
            "timing2_info":timetable2_dict,
            "timing3_info":timetable3_dict,
            "user_header":user_header
        }

