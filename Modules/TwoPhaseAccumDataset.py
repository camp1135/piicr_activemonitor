"""
@Desc: This class holds all information relevant to the spot files
       which have undergone some amount of red. cyc. accumulation.
       Each instance should have only files related to each other, 
       such as all being of the same ion. Trends WILL be infered and
       mixing together different ions may give incorrect results.
@Author: Scott Campbell, campbels@nscl.msu.edu
@Version: 22 February 2023
"""
import numpy as np
from time import localtime
import re
import os
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')
import copy

from .PIICR_Utilities import getEllipseAccumPhase, calculateBirgeRatio

class TwoPhaseAccumDataset:
    
    def __init__(self, analysisfile_list, startDataset, magDataset, print_results=True, plot_results=True, initial_Fm_guess=None, initial_Fp_guess=None):
        """ Constructor for the Accumulation Dataset of a 'Indirect Fm/Fp' PI-ICR measurement, NOT the direct Fc method
        Args:
            datafile_list (list of DataSet obj): list of all PI-ICR datafiles to be used as accum redcyc spots
            startDataset (StartDataset obj): the reference spot dataset relevant to these accum redcyc spots
            magDataset (MagDataset obj): the magnetron spot dataset relevant to these accum redcyc spots
            print_results (bool): whether to print out the results of the dataset
            initial_Fm_guess (float): allows you to provide a starting guess of Fm for the analysis code. If None, the MM8 value will be used
            initial_Fp_guess (float): allows you to provide a starting guess of Fp for the analysis code. If None, the MM8 value will be used
        """
        self.startDataset = startDataset
        self.magDataset = magDataset
        if len(analysisfile_list) == 0: raise ValueError("Dataset is empty. Cannot calculate frequencies.")
        #----------------------------------------------------------------------
        # Sort datafile_list by their midtimes
        self.analysisfile_list = sorted(analysisfile_list, key=lambda x: x.getMidTime(), reverse=False)
        self.midtimes = [file.getMidTime() for file in analysisfile_list]
        #----------------------------------------------------------------------
        # Extract the relevent information
        self.MagFreqGuess = initial_Fm_guess if not initial_Fm_guess == None else analysisfile_list[0].getMM8MagnetronFrequency()
        self.RedFreqGuess = initial_Fp_guess if not initial_Fp_guess == None else analysisfile_list[0].getMM8ReducedCyclotronFrequency()
        self.magAccumTime         = np.array([self.__calcNetMagAccumTime__(file) for file in self.analysisfile_list])
        self.redcycAccumTime      = np.array([self.__calcNetRedcycAccumTime__(file) for file in self.analysisfile_list])
        self.correctedRedcycPhase = np.array([self.__calcCorrectedRedcycPhase__(kk) for kk in range(len(self.analysisfile_list))])
        self.nTurnsFreqList       = np.array([self.__calcNTurns__(kk) for kk in range(len(self.analysisfile_list))])
        self.cyclotronFreqs       = np.array([self.__calcCyclotronFreq__(kk) for kk in range(len(self.analysisfile_list))])
        #----------------------------------------------------------------------
        # Plot and print the relevant information
        redcycFreqs = np.array([val[1] for val in self.nTurnsFreqList])
        redcycFreqErrs = np.array([val[2] for val in self.nTurnsFreqList])
        cycFreqs = np.array([val[0] for val in self.cyclotronFreqs])
        cycFreqErrs = np.array([val[1] for val in self.cyclotronFreqs])
        if plot_results:
            elapsed_time = copy.deepcopy(self.midtimes)
            elapsed_time = (elapsed_time-np.amin(elapsed_time))/3600
            self.__plotFrequencyData__(elapsed_time, redcycFreqs, redcycFreqErrs, cycFreqs, cycFreqErrs)
        # print a short summary of the results
        if print_results: self.printSummary()    
            
    def printSummary(self):
        for kk in range(len(self.nTurnsFreqList)):
            vals = self.nTurnsFreqList[kk]
            cycs = self.cyclotronFreqs[kk]
            analysisfile = self.analysisfile_list[kk]
            print("-"*80)
            print("File:", analysisfile.getFileName(),"    Species:", analysisfile.getSpecies())
            print("-"*80)
            print("\t" + "{0:>25} {1}".format( "Counts:", str(int(analysisfile.getCounts()))) )
            print("\t" + "{0:>25} {1}".format( "Net F+ Accum Time [μs]:", str(round(self.redcycAccumTime[kk],3))) )
            #print("\t" + "{0:>25} {1}".format( "Net F- Accum Time [μs]:", str(round(self.magAccumTime[kk],3))) )
            print("\t" + "{0:>25} {1}".format( "Ang Location [deg]:", str(round(analysisfile.getSpotPhi(),3))+" ± "+str(round(analysisfile.getSpotPhiErr(),3))) )
            #print("\t" + "{0:>25} {1}".format( "F+ Full Turns Guess:", str(int(vals[0]))) )
            print("\t" + "{0:>25} {1}, (n±1: {2} Hz)".format( "F+ Frequency [Hz]:", str(round(vals[1],3))+" ± "+str(round(vals[2],3)), str(round((1e6)/self.redcycAccumTime[kk],3))) )
            print("\t" + "{0:>25} {1}".format( "Fc Frequency [Hz]:", str(round(cycs[0],3))+" ± "+str(round(cycs[1],3))) )

    def getReducedCyclotronFrequencies(self):
        all_vals = []
        for line in self.nTurnsFreqList:
            all_vals.append(line[1:])
        return all_vals

    def getCyclotronFrequencies(self):
        return self.cyclotronFreqs
     
    def exportToFT2(self, PIDataPath):
        for kk in range(len(self.analysisfile_list)):
            analysisfile = self.analysisfile_list[kk]

            ft2filename = os.path.join(PIDataPath, analysisfile.getFileName()[:-4] + '.ft2')
            ft2 = open(ft2filename,'w')
            ft2name = ' ' + analysisfile.getFileName() 
            ft2spec = analysisfile.getSpecies()
            isotopes = ft2spec.split(':')
            r = r"(?P<first>[0-9]*)(?P<num>[a-z]*)(?P<last>[0-9]*)" #Pattern chemical elements are presented in for RegEx parsing
            mass = 0
            for i in isotopes:
                n,e,a = re.findall(r,i,re.I)[0]
                if not n:
                    n = 1
                mass += int(n)*int(a)
            fmass = str(mass)
            fcharge = str(analysisfile.getCharge())
            ft2StartTime, ft2EndTime = self.__timeToFt2Form__(analysisfile)
            ft2ScanStart = str(analysisfile.getRedcycAccumTime())
            ft2ScanStop = '2'
            ft2NumScans = '3'
            ft2counts = str(analysisfile.getCounts())
            ft2Fc = str(round(self.cyclotronFreqs[kk][0],4))
            ft2FcErr = str(round(self.cyclotronFreqs[kk][1],4))
            ft2WholeFile = ' '.join((ft2name,ft2spec,fmass,fcharge,ft2StartTime,ft2EndTime,ft2ScanStart,ft2ScanStop,ft2NumScans,ft2counts,ft2Fc,ft2FcErr))
            ft2.write(ft2WholeFile)
            print("Wrote FT2 file to", ft2filename)
            ft2.close()
 

    def __calcNetMagAccumTime__(self, analysisfile):
        return analysisfile.getMagAccumTime() - self.startDataset.getMagWaitTime()
    
    def __calcNetRedcycAccumTime__(self, analysisfile):
        return analysisfile.getRedcycAccumTime() - self.startDataset.getPhaseAccumTime()
    
    def __calcCorrectedRedcycPhase__(self, file_idx):
        # First, correct the angular positon by any magnetron motion that may have accumulated
        analysisfile = self.analysisfile_list[file_idx]

        spotPhi, spotRho = analysisfile.getSpotPhi(), analysisfile.getSpotRho()
        # Find the new angle for the spot
        interpMagFreq, interpMagFreqErr = self.magDataset.interpMagFreq(analysisfile.getMidTime())
        magFreqDirection = analysisfile.getMagnetronDirection()
        magPhase = ((self.magAccumTime[file_idx]/10.**6)*interpMagFreq*magFreqDirection*360)%360
        spotPhi = (spotPhi - magPhase)%360
        redcycPhaseDiff, redcycPhaseDiffErr = self.__calcNetPhaseTravelled__(file_idx, spotPhi)
    
        return [redcycPhaseDiff, redcycPhaseDiffErr]
    
    def __calcNetPhaseTravelled__(self, file_idx, spotPhi):
        """ Determine the amount of phase accumulated over the ellipse for the spot,
            accounting for the elliptisity
        Args:
            datafile (DataFile obj): a PI-ICR datafile object containing magnetron spot data
        """
        analysisfile = self.analysisfile_list[file_idx]
        startPhasePhi, startPhasePhiErr = self.startDataset.interpStartPhase(analysisfile.getMidTime()) 
        freqSign = analysisfile.getReducedCyclotronDirection()

        ellipseParams = analysisfile.getEllipseParams()
        if not ellipseParams == None: # If there is an ellipse, use it to determine the phase
            # Determine the arc length between the two angles, ellipse assumption
            ellipse_a, ellipse_b, ellipse_theta = ellipseParams['a'], ellipseParams['b'], ellipseParams['theta']
            phaseDiff = getEllipseAccumPhase(startPhasePhi, spotPhi, freqSign, ellipse_a, ellipse_b, ellipse_theta)
        else: # Old method, use circle assumption
            phaseDiff = ((spotPhi-startPhasePhi)*freqSign)%360 # Depending on the direction of the frequency, you have to adjust sign!  
        
        # https://math.stackexchange.com/questions/1472138/determining-percentage-errors-for-inverse-trig-functions-in-conjunction-with-oth
        phaseDiffErr = np.sqrt(startPhasePhiErr**2 + analysisfile.getSpotPhiErr()**2)

        return [phaseDiff, phaseDiffErr]
    
    def __calcNTurns__(self, file_idx, n_diff=3, return_all=False):
        """ Calculates the number of turns that would correspond to 
            the observed magnetron spot
        Args:
            datafile (Datafile obj): the datafile with spot information for which number of turns is to be calculated
            n_diff (int, default 2): number of turns to consider beyond expected MM8 predicted value (e.g. n-2, ..., n+2)
        Returns: 
            number of turns that most closely agrees between observed frequency and MM8 value
        """
        analysisfile = self.analysisfile_list[file_idx]
        
        # Determine the number of turns to check
        n = np.floor(self.redcycAccumTime[file_idx]*self.RedFreqGuess/10.**6)
        n_list = n_list = [n-(n_diff-kk) for kk in range(n_diff)] + [n+kk for kk in range(n_diff+1)]
        # Determine the magnetron frequencies corresp to the num of turns
        fp_PI_list = [(n_val + self.correctedRedcycPhase[file_idx][0]/360)/(self.redcycAccumTime[file_idx]/10.**6) for n_val in n_list]
        fpErr_PI = np.abs(self.correctedRedcycPhase[file_idx][1]/(360*self.redcycAccumTime[file_idx]/10.**6))
        
        if return_all:
            all_arr = np.transpose([n_list, fp_PI_list, fpErr_PI*np.ones(len(n_list))])
            return all_arr
        
        # See how far each freq is from MM8 value, find the one which agrees the closest
        interpMagFreq, interpMagFreqErr = self.magDataset.interpMagFreq(analysisfile.getMidTime())
        MM8RedCycF = (self.MagFreqGuess + self.RedFreqGuess) - interpMagFreq
        fp_diff_list = [np.abs(fp_PI-MM8RedCycF) for fp_PI in fp_PI_list]
        closest_idx = fp_diff_list.index(np.amin(fp_diff_list))
        
        # print("\tn guess:", n)
        # print("\tacc phase (deg):", self.correctedRedcycPhase[file_idx][0], self.correctedRedcycPhase[file_idx][1])
        # print("\tacc time (us):", self.redcycAccumTime[file_idx])
        # print("\Red Cyc Freq (Hz):", fp_PI_list[closest_idx], fpErr_PI)

        # return the guess that best agrees with MM8 value
        return [n_list[closest_idx], fp_PI_list[closest_idx], fpErr_PI]

    def __calcCyclotronFreq__(self, file_idx):
        analysisfile = self.analysisfile_list[file_idx]

        interpMagFreq, interpMagFreqErr = self.magDataset.interpMagFreq(analysisfile.getMidTime())
        fc_PI = self.nTurnsFreqList[file_idx][1] + interpMagFreq
        fc_PIerr = np.sqrt( self.nTurnsFreqList[file_idx][2]**2 + interpMagFreqErr**2 )
        return [fc_PI, fc_PIerr]

    def __timeToFt2Form__(self, analysisfile):
        """ Converts start and stop time of data aquisition to the format used in .ft2 data files
        """
        start = localtime(analysisfile.getStartTime())
        end = localtime(analysisfile.getEndTime())
        months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        startTime = str(start.tm_mday).zfill(2)+'-'+months[start.tm_mon-1]+'-'+str(start.tm_year)+' '+str(start.tm_hour).zfill(2)+':'+str(start.tm_min).zfill(2)+':'+str(start.tm_sec).zfill(2)
        endTime = str(end.tm_mday).zfill(2)+'-'+months[end.tm_mon-1]+'-'+str(end.tm_year)+' '+str(end.tm_hour).zfill(2)+':'+str(end.tm_min).zfill(2)+':'+str(end.tm_sec).zfill(2)
        return(startTime,endTime)

    def __plotFrequencyData__(self, midtimes, redcyc_freqs, redcyc_errs, cyc_freqs, cyc_errs):
        fig, axs = plt.subplots(1, 2, figsize=(15, 4))

        # Calculate the birge ratio and weighted average for plotting
        redcyc_weights = np.divide(1, np.power(redcyc_errs, 2))
        redcyc_wav = np.sum( np.multiply(redcyc_weights, redcyc_freqs) )/np.sum(redcyc_weights)
        redcyc_wav_err = 1 / np.sqrt( np.sum( redcyc_weights ) )
        birge, birge_err = calculateBirgeRatio(redcyc_freqs, redcyc_errs)

        axs[0].set_title('Birge Ratio: '+str(np.round(birge,3))+'('+str(np.round(birge_err,3))+')')
        axs[0].errorbar(midtimes, redcyc_freqs, redcyc_errs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        axs[0].plot([np.amin(midtimes), np.amax(midtimes)], [redcyc_wav,redcyc_wav], '--k')
        axs[0].fill_between([np.amin(midtimes), np.amax(midtimes)], redcyc_wav+redcyc_wav_err, redcyc_wav-redcyc_wav_err, color='indianred', alpha=0.3)
        axs[0].set_xlabel("Measurement Time [hrs]")
        axs[0].set_ylabel("Red. Cyc. Frequency [Hz]")

        # Calculate the birge ratio and weighted average for plotting
        cyc_weights = np.divide(1, np.power(cyc_errs, 2))
        cyc_wav = np.sum( np.multiply(cyc_weights, cyc_freqs) )/np.sum(cyc_weights)
        cyc_wav_err = 1 / np.sqrt( np.sum( cyc_weights ) )
        birge, birge_err = calculateBirgeRatio(redcyc_freqs, redcyc_errs)

        axs[1].set_title('Birge Ratio: '+str(np.round(birge,3))+'('+str(np.round(birge_err,3))+')')
        axs[1].errorbar(midtimes, cyc_freqs, cyc_errs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        axs[1].plot([np.amin(midtimes), np.amax(midtimes)], [cyc_wav,cyc_wav], '--k')
        axs[1].fill_between([np.amin(midtimes), np.amax(midtimes)], cyc_wav+cyc_wav_err, cyc_wav-cyc_wav_err, color='indianred', alpha=0.3)
        axs[1].set_xlabel("Measurement Time [hrs]")
        axs[1].set_ylabel("Cyclotron Frequency [Hz]")
        plt.show()
