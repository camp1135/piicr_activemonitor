"""
@Desc: This class holds all information relevant to the spot files
       which have undergone some amount of red. cyc. accumulation.
       Each instance should have only files related to each other, 
       such as all being of the same ion. Trends WILL be infered and
       mixing together different ions may give incorrect results.
@Author: Scott Campbell, campbels@nscl.msu.edu
@Version: 22 February 2023
"""
import numpy as np
from time import localtime
import re

class OnePhaseAccumDataset:
    def __init__(self, analysisfile_list, startDataset, printResults=True):
        """ Constructor for the Accumulation Dataset of an 'Direct Fc' method PI-ICR measurement
        Args:
            datafile_list (list of DataSet obj): list of all PI-ICR datafiles to be used as accum redcyc spots
            startDataset (StartDataset obj): the reference spot dataset relevant to these accum redcyc spots
        """

        raise Exception("This class is no longer up to date with the rest of the code base. If you wish to use it, you will need to update everything accordingly!s")

        self.startDataset = startDataset
        
        # Sort datafile_list by their midtimes
        self.analysisfile_list = sorted(analysisfile_list, key=lambda x: x.getMidTime(), reverse=False)
        # extract the sorted midtimes
        self.midtimes = [file.getMidTime() for file in analysisfile_list]
        # Extract the relevent information
        self.runCalculations()
        # print a short summary of the results
        if printResults:
            self.printSummary()
        
    def runCalculations(self):
        self.nTurnsFreqList = [self.calcNTurnsFreq(file) for file in self.analysisfile_list]
        self.cyclotronFreqs = [vals[1:] for vals in self.nTurnsFreqList]

    def printSummary(self):
        for kk in range(len(self.nTurnsFreqList)):
            vals = self.nTurnsFreqList[kk]
            analysisfile = self.analysisfile_list[kk]
            print("-"*50)
            print("File:", analysisfile.getFileNum(),"    Species:", analysisfile.getSpecies())
            print("-"*50)
            print("\t" + "{0:>20} {1}".format( "Counts:",              str(int(analysisfile.getCounts()))) )
            print("  " + "{0:>20} {1}".format( "Ang Location:",        str(round(analysisfile.getSpotPhi(),3))+" ± "+str(round(analysisfile.getSpotPhiErr(),3))) )
            print("  " + "{0:>20} {1}".format( "Fc Full Turns Guess:", str(int(vals[0]))) )
            print("  " + "{0:>20} {1}".format( "Fc Frequency:",        str(round(vals[1],3))+" ± "+str(round(vals[2],3))) )

    def calcNTurnsFreq(self, analysisfile, n_diff=4):
        """ Calculates the number of turns that would correspond to 
            the observed magnetron spot
        Args:
            datafile (Datafile obj): the datafile with spot information for which number of turns is to be calculated
            n_diff (int, default 2): number of turns to consider beyond expected MM8 predicted value (e.g. n-2, ..., n+2)
        Returns: 
            number of turns that most closely agrees between observed frequency and MM8 value
        """
        file_idx = self.analysisfile_list.index(analysisfile)

        # This time is from the condition for the Orford method: diff in phase accum = diff in mag-wait!
        T = (analysisfile.getPhaseAccumTime() - self.startDataset.interpPhaseAccumTime(analysisfile.getMidTime()))/10.**6
        # Determine the expected number of cycles
        n = np.floor(np.abs(T*analysisfile.getMM8CyclotronFrequency()))
        n_list = [n-(n_diff-kk) for kk in range(n_diff)] + [n+kk for kk in range(n_diff+1)]
        # Determine the cyclotron frequencies corresp to the num of turns
        spotPhi, spotRho = analysisfile.getSpotPhi(), analysisfile.getSpotRho()
        cyc_phase, cyc_phase_err = self.calcNetPhaseTravelled(analysisfile, spotPhi, spotRho)
        fc_PI_list = [ (n_val - cyc_phase/360)/T for n_val in n_list]
        fcErr_PI_list = [cyc_phase_err/(360*T) for n_val in n_list]
        # See how far each freq is from MM8 value, find the one which agrees the closest
        MM8CycF = analysisfile.getMM8CyclotronFrequency()
        fc_diff_list = [np.abs(fc_PI-MM8CycF) for fc_PI in fc_PI_list]
        closest_idx = fc_diff_list.index(np.amin(fc_diff_list))

        return [n_list[closest_idx], fc_PI_list[closest_idx], fcErr_PI_list[closest_idx]]

    def calcNetPhaseTravelled(self, analysisfile, spotPhi, spotRho):
        """ Use the dot-product method to determine the amount of phase accumulated over
            the ellipse for the spot
        Args:
            datafile (DataFile obj): a PI-ICR datafile object containing magnetron spot data
        """
        file_idx = self.analysisfile_list.index(analysisfile)

        startPhasePhi, startPhasePhiErr = self.startDataset.interpStartPhase(analysisfile.getMidTime())
        startPhaseRho, startPhaseRhoErr = self.startDataset.interpStartRho(analysisfile.getMidTime())
      
        # Get the ellipse fit parametetrs
        ellipseParams = self.circleDataset.interpEllipseParams(analysisfile.getMidTime())
        a, b, theta = ellipseParams['a'], ellipseParams['b'], ellipseParams['theta']
        
        xA, yA = startPhaseRho*np.cos((startPhasePhi-theta)*np.pi/180), startPhaseRho*np.sin((startPhasePhi-theta)*np.pi/180)
        xB, yB = spotRho*np.cos((spotPhi-theta)*np.pi/180), spotRho*np.sin((spotPhi-theta)*np.pi/180)
        
        # Calculate the phase difference. NOTE: gives the abs value of the phase diff!!!!
        startAngle = (180/np.pi)*np.arctan2(yA*a, xA*b)
        spotAngle = (180/np.pi)*np.arctan2(yB*a, xB*b)
        
        phaseDiff = spotAngle - startAngle        
        # https://math.stackexchange.com/questions/1472138/determining-percentage-errors-for-inverse-trig-functions-in-conjunction-with-oth
        phaseDiffErr = np.sqrt(startPhasePhiErr**2 + self.angData[file_idx]["phiErr"]**2)
        
        return [phaseDiff%360, phaseDiffErr]

    def getCyclotronFrequencies(self):
        return self.cyclotronFreqs
    
    def TimeToFt2Form(self, analysisfile):
        """ Converts start and stop time of data aquisition to the format used in .ft2 data files
        """
        start = localtime(analysisfile.getStartTime())
        end = localtime(analysisfile.getEndTime())
        months = ['Jan','Feb','Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        startTime = str(start.tm_mday).zfill(2)+'-'+months[start.tm_mon-1]+'-'+str(start.tm_year)+' '+str(start.tm_hour).zfill(2)+':'+str(start.tm_min).zfill(2)+':'+str(start.tm_sec).zfill(2)
        endTime = str(end.tm_mday).zfill(2)+'-'+months[end.tm_mon-1]+'-'+str(end.tm_year)+' '+str(end.tm_hour).zfill(2)+':'+str(end.tm_min).zfill(2)+':'+str(end.tm_sec).zfill(2)
        return(startTime,endTime)
    
    def exportToFT2(self, PIDataPath):
        for kk in range(len(self.analysisfile_list)):
            analysisfile = self.analysisfile_list[kk]

            ft2filename = PIDataPath + analysisfile.getFileName()[:-4] + '.ft2'
            ft2 = open(ft2filename,'w')
            ft2name = ' ' + analysisfile.getFileName() 
            ft2spec = analysisfile.getSpecies()
            isotopes = ft2spec.split(':')
            r = r"(?P<first>[0-9]*)(?P<num>[a-z]*)(?P<last>[0-9]*)" #Pattern chemical elements are presented in for RegEx parsing
            mass = 0
            for i in isotopes:
                n,e,a = re.findall(r,i,re.I)[0]
                if not n:
                    n = 1
                mass += int(n)*int(a)
            fmass = str(mass)
            fcharge = str(analysisfile.getCharge())
            ft2StartTime, ft2EndTime = self.TimeToFt2Form(analysisfile)
            ft2ScanStart = str(analysisfile.getPhaseAccumTime())
            ft2ScanStop = '2'
            ft2NumScans = '3'
            ft2counts = str(analysisfile.getCounts())
            ft2Fc = str(round(self.cyclotronFreqs[kk][0],4))
            ft2FcErr = str(round(self.cyclotronFreqs[kk][1],4))
            ft2WholeFile = ' '.join((ft2name,ft2spec,fmass,fcharge,ft2StartTime,ft2EndTime,ft2ScanStart,ft2ScanStop,ft2NumScans,ft2counts,ft2Fc,ft2FcErr))
            ft2.write(ft2WholeFile)
            ft2.close()
    