"""
@Desc: This class holds all information relevant to the spot files
       which serve as the reference to spots with accumulated phase.
       Each instance should have only files related to each other, 
       such as all being of the same ion. Trends WILL be infered and
       mixing together different ions may give incorrect results.
@Author: Scott Campbell, campbels@nscl.msu.edu
@Version: 22 February 2023
"""
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')
import copy

from .FitLinear import LinearFitter
from .PIICR_Utilities import calculateBirgeRatio, getSurroundingIdx, getNearestUncertainty

class StartDataset:
    def __init__(self, datafile_list, interp_method='linear', print_results=True, plot_results=True):
        """
        Args:
            datafile_list (list of DataSet obj): list of all PI-ICR datafiles to be used as circles
            interp_method (str): how to interpolate the values: 'constant', 'linear' (in time), 'nearest' (weighted by time)
            print_results (bool): whether to print out the results of the dataset
        """
        #-----------------------------------------------------------
        # Sort datafile_list by their midtimes
        if len(datafile_list) == 0: raise ValueError("Dataset is empty. Cannot run any interpolation.")
        self.datafile_list = sorted(datafile_list, key=lambda x: x.getMidTime(), reverse=False)
        #-----------------------------------------------------------
        # extract the sorted midtimes
        self.midtimes = np.array([file.getMidTime() for file in datafile_list])
        self.spotPhi = np.array([file.getSpotPhi() for file in datafile_list])
        self.spotPhiErr = np.array([file.getSpotPhiErr() for file in datafile_list])
        self.spotRho = np.array([file.getSpotRho() for file in datafile_list])
        self.spotRhoErr = np.array([file.getSpotRhoErr() for file in datafile_list])
        #-----------------------------------------------------------
        # Check the requested interpolation method with the dataset size 
        if len(self.datafile_list) < 3 and not interp_method == 'constant':
            print("\n*** WARNING: Dataset contains less than 3 samples. Defaulting interpolation method to \'constant\'.\n")
            self.interp_method = 'constant'
        else: self.interp_method = interp_method
        #-----------------------------------------------------------
        # Check that for every file, the same phase_accum and mag_wait times are used
        curr_phase_accum, curr_mag_wait = None, None
        have_same_timings = True
        for file in self.datafile_list:
            if curr_phase_accum == None: curr_phase_accum = file.getRedcycAccumTime()
            if curr_mag_wait == None: curr_mag_wait = file.getMagAccumTime()
            # Check if the timings are the same
            if not (curr_phase_accum == file.getRedcycAccumTime() and curr_mag_wait == file.getMagAccumTime()): have_same_timings = False
        if not have_same_timings: raise ValueError("Not all datafiles in the start dataset have the same red. cyc. accumulation times and mag accumulation times. Cannot Interpolate!")
        #-----------------------------------------------------------
        # If the interpolation method is the linear regression, create a fit object for both the spot rho and phi
        if self.interp_method == 'linear':
            # Perform the linear regression for Spot Phi values
            self.linearFit_spotPhi = LinearFitter()
            self.linearFit_spotPhi.fit(self.midtimes, self.spotPhi, self.spotPhiErr)
            # Perform the linear regression for the Spot Rho values
            self.linearFit_spotRho = LinearFitter()
            self.linearFit_spotRho.fit(self.midtimes, self.spotRho, self.spotRhoErr)
            # Plot and print
            if plot_results: self.__plotLinearFits__(self.linearFit_spotPhi, self.linearFit_spotRho)
            if print_results:
                print("-"*50)
                print("Aggregate Dataset Fit:")
                print("-"*50)
                print("\tFitted Spot Phi Slope [deg/hr]:", np.round(self.linearFit_spotPhi.slope*3600,6))
                print("\tFitted Spot Rho Slope [deg/hr]:", np.round(self.linearFit_spotRho.slope*3600,6))
                print("")
        #-----------------------------------------------------------
        # If the interpolation method is the constant, determine the value by weighted average
        elif self.interp_method == 'constant':
            if len(self.datafile_list) == 1:
                self.constant_spotPhi = self.spotPhi[0]
                self.constant_spotRho = self.spotRho[0]
                self.constant_spotPhiErr = self.spotPhiErr[0]
                self.constant_spotRhoErr = self.spotRhoErr[0]
            else:
                phi_weights, rho_weights = np.divide(1, np.power(self.spotPhiErr, 2)), np.divide(1, np.power(self.spotRhoErr, 2))
                self.constant_spotPhi = np.sum( np.multiply(phi_weights, self.spotPhi) )/np.sum(phi_weights)
                self.constant_spotRho = np.sum( np.multiply(rho_weights, self.spotRho) )/np.sum(rho_weights)
                self.constant_spotPhiErr = 1 / np.sqrt( np.sum( phi_weights ) )
                self.constant_spotRhoErr = 1 / np.sqrt( np.sum( rho_weights ) )
            # Plot and print
            if plot_results: 
                self.__plotConstantValues__(self.midtimes, self.spotPhi, self.spotRho, self.spotPhiErr, self.spotRhoErr, 
                                            self.constant_spotPhi, self.constant_spotRho, self.constant_spotPhiErr, self.constant_spotRhoErr)
            if print_results:
                print("-"*50)
                print("Aggregate Dataset Fit:")
                print("-"*50)
                print("\tWeighted Spot Phi:", np.round(self.constant_spotPhi,3), np.round(self.constant_spotPhiErr,3))
                print("\tWeighted Spot Rho:", np.round(self.constant_spotRho,3), np.round(self.constant_spotRhoErr,3))
                print("")
        #-----------------------------------------------------------
        # If the interpolation method is the nearest, determine the value using the two closest values (one on either side) weighted by time difference
        # This will be done on a case by case basis later in the code
        elif self.interp_method == 'nearest':
            if plot_results: self.__plotRawData__(self.midtimes, self.spotPhi, self.spotRho, self.spotPhiErr, self.spotRhoErr)
        else: raise ValueError("The interpolation method was not one of the defined values... Expected \'constant\', \'linear\' or \'nearest_neighbors\'") 
        # print a short summary of the results
        if print_results: self.printSummary()

    def printSummary(self):
        for datafile in self.datafile_list:
            print("-"*50)
            print("File:", datafile.getFileName(), "    Species:", datafile.getSpecies() )
            print("-"*50)
            print("\t" + "{0:>20} {1}".format( "Counts:", str(int(datafile.getCounts()))) )
            print("\t" + "{0:>20} {1}".format( "Ang Location:", str(round(datafile.getSpotPhi(),3))+" ± "+str(round(datafile.getSpotPhiErr(),3))) )

    def interpStartPhase(self, midtime):
        """ Interpolates the start phase for a given time
        Args:
            midtime (float): the time for which the center should be interpolated at
        Returns:
            interpolated phase, interp phase err
        """
        if self.interp_method == 'constant':
            return self.constant_spotPhi%360, self.constant_spotPhiErr
        elif self.interp_method == 'linear':
            return self.linearFit_spotPhi.get_y(midtime)
        elif self.interp_method == 'nearest':
            # check if there are measurements on either side of the time
            # from the constructor, we are gauranteed to have at least two measurements
            lo_idx, hi_idx = getSurroundingIdx(midtime, self.midtimes)
            phi1, phi1err = self.spotPhi[lo_idx], self.spotPhiErr[lo_idx]
            t1 = self.midtimes[lo_idx]
            phi2, phi2err = self.spotPhi[hi_idx], self.spotPhiErr[hi_idx]
            t2 = self.midtimes[hi_idx]
            # Fit and get the predicted center x value
            slope = (phi2-phi1)/(t2-t1)
            inter = phi1 - slope*t1
            pred_phi = slope*midtime + inter
            pred_phiErr = getNearestUncertainty(midtime, [t1,t2], [phi1err,phi2err])
            return pred_phi, pred_phiErr
            
    def interpStartRho(self, midtime):
        """ Interpolates the start rho for a given time
        Args:
            midtime (float): the time for which the center should be interpolated at
        Returns:
            interpolated phase, interp phase err
        """
        if self.interp_method == 'constant':
            return self.constant_spotRho%360, self.constant_spotRhoErr
        elif self.interp_method == 'linear':
            return self.linearFit_spotRho.get_y(midtime)
        elif self.interp_method == 'nearest':
            # check if there are measurements on either side of the time
            # from the constructor, we are gauranteed to have at least two measurements
            lo_idx, hi_idx = getSurroundingIdx(midtime, self.midtimes)
            rho1, rho1err = self.spotRho[lo_idx], self.spotRhoErr[lo_idx]
            t1 = self.midtimes[lo_idx]
            rho2, rho2err = self.spotRho[hi_idx], self.spotRhoErr[hi_idx]
            t2 = self.midtimes[hi_idx]
            # Fit and get the predicted center x value
            slope = (rho2-rho1)/(t2-t1)
            inter = rho1 - slope*t1
            pred_rho = slope*midtime + inter
            pred_rhoErr = getNearestUncertainty(midtime, [t1,t2], [rho1err,rho2err])
            return pred_rho, pred_rhoErr
    
    def getPhaseAccumTime(self):
        """ Gets the Phase Accumulation Time for the Reduced Cyclotron Motion of the dataset
        Returns:
            reduced cyclotron phase accum time
        """
        return self.datafile_list[0].getRedcycAccumTime()

    def getMagWaitTime(self):
        """ Gets the Phase Accumulation Time for the Magnetron Motion of the dataset
        Returns:
            magnetron phase accum time
        """
        return self.datafile_list[0].getMagAccumTime()
    
    def __plotLinearFits__(self, linearFit_spotPhi, linearFit_spotRho):
        #------------------------------------------------------------
        # Plot the spot phi data and fit first
        if not linearFit_spotPhi.fit_completed: return 
        x_pred = np.linspace(np.amin(linearFit_spotPhi.x_data),np.amax(linearFit_spotPhi.x_data),500)
        y_pred = linearFit_spotPhi.slope * x_pred + linearFit_spotPhi.intercept  # Predicted y value at x_new
        se_pred = linearFit_spotPhi.__prediction_std_error__(x_pred)  # Standard error of prediction 
        y_lower = y_pred - se_pred
        y_upper = y_pred + se_pred

        # transform the epoch times to elapsed time in hrs
        x_pred = (x_pred - np.amin(x_pred))/3600
        raw_x = copy.deepcopy(linearFit_spotPhi.x_data)
        raw_x = (raw_x - np.amin(raw_x))/3600

        fig, axs = plt.subplots(1, 2, figsize=(12, 3))
        axs[0].errorbar(raw_x, linearFit_spotPhi.y_data, linearFit_spotPhi.y_errs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        axs[0].fill_between(x_pred, y_upper, y_lower, color='indianred', alpha=0.3)
        axs[0].plot(x_pred, y_pred, '--k')
        axs[0].set_xlabel("Measurement Time [hrs]")
        axs[0].set_ylabel("Spot Phi [deg]")

        #------------------------------------------------------------
        # Plot the spot rho data and fit next
        if not linearFit_spotRho.fit_completed: return 
        x_pred = np.linspace(np.amin(linearFit_spotRho.x_data),np.amax(linearFit_spotRho.x_data),500)
        y_pred = linearFit_spotRho.slope * x_pred + linearFit_spotRho.intercept  # Predicted y value at x_new
        se_pred = linearFit_spotRho.__prediction_std_error__(x_pred)  # Standard error of prediction 
        y_lower = y_pred - se_pred
        y_upper = y_pred + se_pred

        # transform the epoch times to elapsed time in hrs
        # transform the epoch times to elapsed time in hrs
        x_pred = (x_pred - np.amin(x_pred))/3600
        raw_x = copy.deepcopy(linearFit_spotRho.x_data)
        raw_x = (raw_x - np.amin(raw_x))/3600

        axs[1].errorbar(raw_x, linearFit_spotRho.y_data, linearFit_spotRho.y_errs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        axs[1].fill_between(x_pred, y_upper, y_lower, color='indianred', alpha=0.3)
        axs[1].plot(x_pred, y_pred, '--k')
        axs[1].set_xlabel("Measurement Time [hrs]")
        axs[1].set_ylabel("Spot Rho [arb.]")
        plt.show()

    def __plotConstantValues__(self, mid_times, spotPhi_data, spotRho_data, spotPhiErrs, spotRhoErrs, spotPhi_wav, spotRho_wav, spotPhi_wavErr, spotRho_wavErr):
        # convert the times from epoch time to elapsed time in hrs
        raw_x = copy.deepcopy(mid_times)
        raw_x -= np.amin(raw_x)
        raw_x /= 3600

        fig, axs = plt.subplots(1, 2, figsize=(12, 3))

        birge, birge_err = calculateBirgeRatio(spotPhi_data, spotPhiErrs)
        axs[0].set_title('Birge Ratio: '+str(np.round(birge,3))+'('+str(np.round(birge_err,3))+')')
        axs[0].errorbar(raw_x, spotPhi_data, spotPhiErrs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        axs[0].plot([np.amin(raw_x), np.amax(raw_x)], [spotPhi_wav,spotPhi_wav], '--k')
        axs[0].fill_between([np.amin(raw_x), np.amax(raw_x)], spotPhi_wav+spotPhi_wavErr, spotPhi_wav-spotPhi_wavErr, color='indianred', alpha=0.3)
        axs[0].set_xlabel("Measurement Time [hrs]")
        axs[0].set_ylabel("Spot Phi [deg]")

        birge, birge_err = calculateBirgeRatio(spotRho_data, spotRhoErrs)
        axs[1].set_title('Birge Ratio: '+str(np.round(birge,3))+'('+str(np.round(birge_err,3))+')')
        axs[1].errorbar(raw_x, spotRho_data, spotRhoErrs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        axs[1].plot([np.amin(raw_x), np.amax(raw_x)], [spotRho_wav,spotRho_wav], '--k')
        axs[1].fill_between([np.amin(raw_x), np.amax(raw_x)], spotRho_wav+spotRho_wavErr, spotRho_wav-spotRho_wavErr, color='indianred', alpha=0.3)
        axs[1].set_xlabel("Measurement Time [hrs]")
        axs[1].set_ylabel("Spot Rho [arb.]")
        plt.show()

    def __plotRawData__(self, mid_times, spotPhi_data, spotRho_data, spotPhiErrs, spotRhoErrs):
        # convert the times from epoch time to elapsed time in hrs
        raw_x = copy.deepcopy(mid_times)
        raw_x -= np.amin(raw_x)
        raw_x /= 3600

        fig, axs = plt.subplots(1, 2, figsize=(12, 3))

        birge, birge_err = calculateBirgeRatio(spotPhi_data, spotPhiErrs)
        axs[0].set_title('Birge Ratio: '+str(np.round(birge,3))+'('+str(np.round(birge_err,3))+')')
        axs[0].errorbar(raw_x, spotPhi_data, spotPhiErrs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        axs[0].set_xlabel("Measurement Time [hrs]")
        axs[0].set_ylabel("Spot Phi [deg]")

        birge, birge_err = calculateBirgeRatio(spotRho_data, spotRhoErrs)
        axs[1].set_title('Birge Ratio: '+str(np.round(birge,3))+'('+str(np.round(birge_err,3))+')')
        axs[1].errorbar(raw_x, spotRho_data, spotRhoErrs, 
                     mfc='k', marker='o', color='k', 
                     ecolor='k', elinewidth=1, capsize=2.5,
                     linestyle='None')
        axs[1].set_xlabel("Measurement Time [hrs]")
        axs[1].set_ylabel("Spot Rho [arb.]")
        plt.show()

