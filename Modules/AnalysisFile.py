"""
@Desc: This class holds all of the extrapolated values for a PI-ICR 
       datafile, and determine which positions meet any limits/thresholds
       specified by the user
@Author: Scott Campbell, campbels@nscl.msu.edu
@Version: 31 August 2023
"""
import numpy as np
import os

class AnalysisFile:

    def __init__(self, dataFile, limits):
        self.dataFile = dataFile
        self.limits = limits
        self.system = dataFile.system
        
        self.Center        = [None, None] # center of motion, i.e. ellipse center
        self.CenterErr     = [None, None]
        self.EllipseParams = None # parameters of the ellipse dictionary. 
        self.spotRho       = None # radius of the spot, relative to Center
        self.spotRhoErr    = None
        self.spotPhi       = None # angle of the spot, relative to Center. This should always be in degrees!
        self.spotPhiErr    = None
        self.Counts        = None
        self.ClusterPos    = [None]
        self.NoisePos      = [None]

    def setLimits(self, limits):
        self.limits = limits
    def setDataFile(self, dataFile):
        self.dataFile = dataFile

    def getLimits(self):
        return self.limits
    def getDataFile(self):
        return self.dataFile
    
    def saveReconstructedData(self):
        data = self.dataFile.getAllReconstructedEvents()
        # Save the data to a txt file
        save_name = os.path.join(self.dataFile.getFilePath(), os.path.splitext(self.dataFile.getFileName())[0]+"_Recons.txt")
        np.savetxt(save_name, data.astype(np.float32), delimiter=',', newline='\n')

    def saveFilteredData(self, limits, applyTOF=True, applyZ=True, applyRadial=False, applyAngular=False, applyEvent=False, allowRecovered=True, filterCenter=[0,0]):
        """ Saves the reconstruced MCP data (subject to filters) to a txt file
        """
        # There are some issues with multi-threading... Make sure we only consider up to the same number of datapoints!
        max_len = len(self.dataFile.getAllReconstructedEvents())
        # Apply the gates/filters
        gated_indices = self.__getGatedDataIndices__(limits, applyTOF, applyZ, applyRadial, applyAngular, filterCenter, max_len=max_len)
        if len(gated_indices) == 0: return
        # Get the event indices in the filter
        if applyEvent: gated_evt_indices = self.__applyEventGate__(limits['eventmin'], limits['eventmax'])[:max_len]
        else: gated_evt_indices = np.array( np.ones(len(gated_indices)), dtype=bool)
        gated_recovered_indices = self.__applyRecoveredPosGate__(allowRecovered)[:max_len]
        # Apply the gates to the event data
        all_events = self.dataFile.getAllReconstructedEvents()[:max_len]
        filtered_events = all_events[ gated_indices & gated_evt_indices & gated_recovered_indices ]
        # Save the data to a txt file
        save_name = os.path.join(self.dataFile.getFilePath(), os.path.splitext(self.dataFile.getFileName())[0]+"_filteredRecons.txt")
        np.savetxt(save_name, filtered_events.astype(np.float32), delimiter=',', newline='\n')

    ##########################################################################
    # Methods for applying cuts to data
    ##########################################################################
    def getGatedPositions(self, limits, applyTOF=True, applyZ=True, applyRadial=False, applyAngular=False, applyEvent=False, allowRecovered=True, filterCenter=[0,0], returnRejected=False):
        """ Applies a number of gates to a list of reconstructed events. Returns the list of POSITIONS which
            satisfy all the provided criteria
        """
        # There are some issues with multi-threading... Make sure we only consider up to the same number of datapoints!
        max_len = len(self.dataFile.getAllReconstructedEvents())
        # Apply the gates/filters
        gated_indices = self.__getGatedDataIndices__(limits, applyTOF, applyZ, applyRadial, applyAngular, filterCenter, max_len=max_len)
        if len(gated_indices) == 0:
            if returnRejected: return [], []
            else: return []
        # Get the event indices in the filter
        if applyEvent: gated_evt_indices = self.__applyEventGate__(limits['eventmin'], limits['eventmax'])[:max_len]
        else: gated_evt_indices = np.array( np.ones(len(gated_indices)), dtype=bool)
        gated_recovered_indices = self.__applyRecoveredPosGate__(allowRecovered)[:max_len]
        # Apply the gates to the position data
        positions = self.dataFile.getPositions()[:max_len]
        gated_positions = positions[ gated_indices & gated_evt_indices & gated_recovered_indices ]
        rejected_positions = positions[ ~gated_indices & gated_evt_indices & gated_recovered_indices ]

        if returnRejected: return gated_positions, rejected_positions
        return gated_positions
    def getGatedPositionsWithScan(self, limits, applyTOF, applyZ, applyRadial, applyAngular, applyEvent, allowRecovered, filterCenter, scan1idx, scan2idx=[], doublePattern=None, returnRejected=False):
        """ Applys gates and also allows you to apply gates to the MM8 scans for the datapoints
        Args:
            scan1_idx (list of int): the indices of scan 1 to include in the cut.
            scan2_idx (list of int, []): the indices of scan 2 to include in the cut. None ignores the scan
            double_pattern (int, None): an int representing if a double-pattern excitation is being used. 
                if None, then no double-pattern is used. If 1 -> first value in the double pattern is 
                returned. If 2 -> the second value is returned. Double pattern: for each MM8 scan, multiple 
                measurements are made in sucession. So two trigger events per scan value
        """
        # There are some issues with multi-threading... Make sure we only consider up to the same number of datapoints!
        max_len = len(self.dataFile.getAllReconstructedEvents())
        # Apply the gates/filters
        scan2_len = self.dataFile.getMM8Scan2Len()
        # There has to be at least one scan1 value to get any positions. Additionally, if
        # there are scan2 vals, but none are selected it should return empty
        if len(scan1idx) < 1 or (scan2_len > 0 and len(scan2idx) < 1):
            if returnRejected: return [], []
            return []
        # Get the event indices that have the standard filter values
        gated_indices = self.__getGatedDataIndices__(limits, applyTOF, applyZ, applyRadial, applyAngular, filterCenter, max_len=max_len)
        if len(gated_indices) == 0:
            if returnRejected: return [], []
            else: return []
        # Get the event indices that meet the MM8 scan selections, and/or allow for recovered positions
        gated_scan_indices = self.__applyScanCutsMM8__(scan1idx, scan2_idx=scan2idx, double_pattern=doublePattern)[:max_len]
        gated_recovered_indices = self.__applyRecoveredPosGate__(allowRecovered)[:max_len]
        # Get the event indices in the filter
        if applyEvent: gated_evt_indices = self.__applyEventGate__(limits['eventmin'], limits['eventmax'])[:max_len]
        else: gated_evt_indices = np.array([True for __ in range(len(gated_indices))])
        # Pull the position information based on the gates
        all_positions = self.dataFile.getPositions()[:max_len]
        positions = all_positions[gated_indices & gated_scan_indices & gated_evt_indices & gated_recovered_indices]
        if returnRejected:
            rejects = all_positions[~gated_indices & gated_scan_indices & gated_evt_indices & gated_recovered_indices]
            return positions, rejects
        return positions

    def __getGatedDataIndices__(self, limits, applyTOF, applyZ, applyRadial, applyAngular, filterCenter, max_len=None):
        if max_len == None: max_len = len(self.dataFile.getToFs())
        all_event_indices = np.ones(max_len, dtype=bool)
        if len(all_event_indices) == 0: return []
        if applyTOF:
            gated_indices = self.__applyTOFGate__(limits['tofmin'], limits['tofmax'])
            if not max_len == None: gated_indices = gated_indices[:max_len]
            all_event_indices = gated_indices & all_event_indices # Take the intersection
        if applyZ:
            gated_indices = self.__applyZGate__(limits['zmin'], limits['zmax'])
            if not max_len == None: gated_indices = gated_indices[:max_len]
            all_event_indices = gated_indices & all_event_indices # Take the intersection
        if applyRadial:
            gated_indices = self.__applyRadialGate__(limits['radialmin'], limits['radialmax'], filterCenter=filterCenter)
            if not max_len == None: gated_indices = gated_indices[:max_len]
            all_event_indices = gated_indices & all_event_indices # Take the intersection
        if applyAngular:
            gated_indices = self.__applyAngularGate__(limits['angmin'], limits['angmax'], filterCenter=filterCenter)
            if not max_len == None: gated_indices = gated_indices[:max_len]
            all_event_indices = gated_indices & all_event_indices # Take the intersection
        return all_event_indices
    def __applyScanCutsMM8__(self, scan1_idx, scan2_idx=[], double_pattern=None):
        """ Applies cuts to the dataset based on whether a datapoint was taken on a given mm8 scan or not
        Args:
            scan1_idx (list of int): the indices of scan 1 to include in the cut.
            scan2_idx (list of int, []): the indices of scan 2 to include in the cut. None ignores the scan
            double_pattern (int, None): an int representing if a double-pattern excitation is being used. 
                if None, then no double-pattern is used. If 1 -> first value in the double pattern is 
                returned. If 2 -> the second value is returned. Double pattern: for each MM8 scan, multiple 
                measurements are made in sucession. So two trigger events per scan value
        Returns:
            indices in the datafile which satisfy the cuts
        """
        scan1_len, scan2_len = self.dataFile.getMM8Scan1Len(), self.dataFile.getMM8Scan2Len()
        trig_ids = self.dataFile.getTriggerIDs(unique=False)
        if (scan2_len == None or scan2_len == 0) or (None in scan2_idx or len(scan2_idx) == 0):
            # Just have a 1D scan, return the indices that satisfy the request for scan 1
            if double_pattern == None or double_pattern == 0: # No double-pattern is used
                steps_per_cycle = scan1_len
                trigid_per_cycle = scan1_idx
            else: # the user is specifying a double pattern
                steps_per_cycle = scan1_len*2
                trigid_per_cycle = scan1_idx*2
        else: 
            # The user wants to find indices in a 2D scan
            perms = np.array([[ii, jj] for ii in scan1_idx for jj in scan2_idx])
            if double_pattern == None or double_pattern == 0: # No double-pattern is used
                steps_per_cycle = scan1_len*scan2_len
                trigid_per_cycle = perms[:,0] + perms[:,1]*scan1_len
            else: # the user is specifying a double pattern
                steps_per_cycle = scan1_len*scan2_len*2
                if double_pattern > 2 or double_pattern < 1: double_pattern = 1 # safeguard!
                trigid_per_cycle = (perms[:,0] + perms[:,1]*scan1_len)*2 + (double_pattern-1)
        # Determine which reconstructed events satisfy the conditions
        event_idxs = [ True if trigid%steps_per_cycle in trigid_per_cycle else False for trigid in trig_ids ]
        return event_idxs
    def __applyEventGate__(self, eventMin, eventMax):
        """ Applies an 'event gate' where only events within a certain range are allowed
        Args:
            eventMin (int, None): staring event number. If None, the first available is used
            eventMax (int, None): ending event number. If None, the last available is used
        """
        event_ids = self.dataFile.getTriggerIDs(unique=False)
        # Check the limits
        if eventMin == None: eventMin = 0
        if eventMax == None or eventMax < 0: eventMax = np.amax(event_ids)
        # Get the indices
        event_indices = (event_ids >= eventMin) & (event_ids <= eventMax)
        return event_indices
    def __applyRecoveredPosGate__(self, allowRecovered):
        """ Looks at whether a given position was recovered using the fitted sumX/xumY
            distribution. Returns a list of the indices which meet the  
        """
        recoveredFlags = self.dataFile.getRecoveredFlag()
        if allowRecovered: return np.array( np.ones(len(recoveredFlags)), dtype=bool)
        else: return recoveredFlags == False
    def __applyTOFGate__(self, tofMin, tofMax):
        """ Looks at the DataFile events list any returns a list of the same length,
            where each index is another list containing any indices that meet the tof
            gate for that trigger event
        """
        tofs = self.dataFile.getToFs()
        event_indices = (tofs >= tofMin) & (tofs <= tofMax)
        return event_indices
    def __applyZGate__(self, zMin, zMax):
        """ Looks at the DataFile events list any returns a list of the same length,
            where each index is another list containing any indices that meet the z
            gate for that trigger event
        """
        zs = self.dataFile.getZ(unique=False)
        event_indices = (zs >= zMin) & (zs <= zMax)
        return event_indices
    def __applyRadialGate__(self, radialMin, radialMax, filterCenter=[0,0]):
        positions = self.dataFile.getPositions()
        x, y = positions[:,0]-filterCenter[0], positions[:,1]-filterCenter[1]
        x, y = x.astype('float'), y.astype('float')
        rho = np.sqrt(np.power(x,2) + np.power(y,2))
        # Determine whether each index meets the criteria
        event_indices = (rho >= radialMin) & (rho <= radialMax)
        return event_indices
    def __applyAngularGate__(self, angStart, angEnd, filterCenter=[0,0]):
        """ Applies an angular gate to the data, allowing for any points within angStart to angEnd.
        Args:
            angStart (float): starting angle, in degrees
            angEnd (float): ending angle, in degrees
        """
        angStart, angEnd = angStart*(np.pi/180)%(2*np.pi), angEnd*(np.pi/180)%(2*np.pi)
        positions = self.dataFile.getPositions()
        x, y = positions[:,0]-filterCenter[0], positions[:,1]-filterCenter[1]
        x, y = x.astype('float'), y.astype('float')
        phi = np.arctan2(y, x)
        phi %= (2*np.pi)
        # Check if the gate crosses the 0 angle
        if angStart > angEnd:
            event_indices = ((phi >= angStart) & (phi <= 2*np.pi)) | ((phi >= 0) & (phi <= angEnd)) 
        else:
            event_indices = (phi >= angStart) & (phi <= angEnd)
        return event_indices

    ##########################################################################
    # Getter/Setter methods for cluster related information
    ##########################################################################

    def setCenter(self, center): self.Center = center
    def setCenterErr(self, centerErr): self.CenterErr = centerErr
    def setSpotRho(self, rho): self.spotRho = rho
    def setSpotRhoErr(self, rhoErr): self.spotRhoErr = rhoErr
    def setSpotPhi(self, phi): self.spotPhi = phi
    def setSpotPhiErr(self, phiErr): self.spotPhiErr = phiErr
    def setCounts(self, counts): self.Counts = counts
    def setClusterPos(self, clusterPos): self.ClusterPos = clusterPos
    def setNoisePos(self, noisePos): self.NoisePos = noisePos
    def setEllipseParams(self, ellipseParams):
        """ ellipseParams should be a dictionary with the following sets:
            'x', 'y', 'xErr', 'yErr' -> ellipse center and uncs
            'a', 'b', 'theta' -> semi-major and -minor axes, and rotation angle
        """
        self.EllipseParams = ellipseParams

    def getCenter(self): return self.Center
    def getCenterErr(self): return self.CenterErr
    def getSpotRho(self): return self.spotRho
    def getSpotRhoErr(self): return self.spotRhoErr
    def getSpotPhi(self): return self.spotPhi
    def getSpotPhiErr(self): return self.spotPhiErr
    def getCounts(self): return self.Counts
    def getClusterPos(self): return self.ClusterPos
    def getNoisePos(self): return self.NoisePos
    def getEllipseParams(self): return self.EllipseParams

    ##########################################################################
    # Getter proxys for the datafile object
    ##########################################################################

    def getFilePath(self): return self.dataFile.getFilePath()
    def getFileName(self): return self.dataFile.getFileName()
    def getFileType(self): return self.dataFile.getFileType()
    def getFileNum(self): return self.dataFile.getFileNum()
    def getFileSpecies(self): return self.dataFile.getFileSpecies()
    def getSpecies(self): return self.dataFile.getSpecies()
    def getCharge(self): return self.dataFile.getCharge()
    def getMagAccumTime(self): return self.dataFile.getMagAccumTime()
    def getRedcycAccumTime(self): return self.dataFile.getRedcycAccumTime()

    def getStartTime(self): return self.dataFile.getStartTime()
    def getEndTime(self): return self.dataFile.getEndTime()
    def getMidTime(self):  return self.dataFile.getMidTime()
    def getMM8CyclotronFrequency(self): return self.dataFile.mm8DataFile.getFrequencyValue(self.system.getConvAfgLoc())
    def getMM8ReducedCyclotronFrequency(self): return self.dataFile.mm8DataFile.getFrequencyValue(self.system.getDipoleAfgLoc())
    def getMM8MagnetronFrequency(self): return self.getMM8CyclotronFrequency() - self.getMM8ReducedCyclotronFrequency()
    def getMagnetronDirection(self): return -self.system.getDirectionOfRadialMotion()
    def getReducedCyclotronDirection(self): return self.system.getDirectionOfRadialMotion()
