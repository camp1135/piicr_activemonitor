import os
import glob
import numpy as np
from pathlib import Path
import re

class System:
    """
    @Author: Scott Campbell
    @Version: 23 June 2023

    This class holds all the relevant information for a particular detector system/data stograge.
    For example, this will be different for LEBIT as opposed to SHIPTRAP.
    """
    def __init__(self, mm8DataDir, mcpDataDir, mm8MainXMLDir, mm8SetsDir, amePath, 
                 tdcInfo, absTofMin, absTofMax, mcpRadius, radial_motion_direction, 
                 dataExtName, dataExtFormat, useRawDatafile, dipole_afg_loc, conv_afg_loc,
                 redcyc_accum_timing, mag_accum_timing):
        """ Initializer for System Class """
        # Save general information relevant to the system
        self.mm8DataDir = mm8DataDir
        self.mcpDataDir = mcpDataDir
        self.mm8MainXMLDir = mm8MainXMLDir
        self.mm8SetsDir = mm8SetsDir
        self.tdcInfo = tdcInfo
        self.absTofMin = absTofMin
        self.absTofMax = absTofMax
        self.mcpRadius = mcpRadius
        self.radial_motion_direction = radial_motion_direction
        self.dataExtName = dataExtName # This should be 'TEXT files' or 'RAW files' etc.
        self.dataExtFormat = dataExtFormat # This should be '*.txt' or '*.raw' etc.
        self.useRawDataFile = useRawDatafile
        self.datafileExt = dataExtFormat.split("*", maxsplit=2)[-1]
        self.dipole_afg_loc = dipole_afg_loc
        self.conv_afg_loc = conv_afg_loc
        self.redcyc_accum_timing = redcyc_accum_timing
        self.mag_accum_timing = mag_accum_timing
        # Save information relevent to the AME database
        self.amePath = amePath
        self.ameDataTable = None
        self.ameUniqueElements = None
        self.ameNonUniqueElements = None
        self.__loadAME__()

  
    def getMM8DataDirectory(self):
        """ Gets the location of the folder where the data files for the
            ***MM8 runs*** of the different PI-ICR spots are stored
        Returns:
            (str) path of the data folder
        """
        return self.mm8DataDir
    def getMcpDataDirectory(self):
        """ Gets the location of the folder where the data files for the
            ***MCP Positions*** of the different PI-ICR spots are stored
        Returns:
            (str) path of the data folder
        """
        return self.mcpDataDir
    def getMM8MainXMLDirectory(self):
        """ Gets the location of the folder where the main MM8 XML files
            are located (i.e. SavedMainXML)
        Returns:
            (str) path of the xml folder
        """
        return self.mm8MainXMLDir
    def getMM8SetsDirectory(self):
        """ Gets the location of the folder where the timing XML files
            are located (i.e. mm8sets\memories)
        Returns:
            (str) path of the xml folder
        """
        return self.mm8SetsDir
    def getTDCInfo(self, key=None):
        """ Define some of the key information for the TDC such that
            the raw data can be reconstructed to position/tof data.
        Args:
            key (str or None): if a string is given, the element in the
                dictionary corresp. to that key is returned. If None, the whole
                dictionary is returned
        """
        if key == None: return self.tdcInfo
        elif key in self.tdcInfo.keys(): return self.tdcInfo[key]
        else: return None
    def getAbsTofLimits(self): return self.absTofMin, self.absTofMax
    def getRadiusMCP(self): return self.mcpRadius
    def getDirectionOfRadialMotion(self): return self.radial_motion_direction
    def getDatafileExtName(self): return self.dataExtName
    def getDatafileExtFormat(self): return self.dataExtFormat
    def getDatafileExt(self): return self.datafileExt
    def getUsesRawDataFile(self): return self.useRawDataFile
    def getDipoleAfgLoc(self): return self.dipole_afg_loc
    def getConvAfgLoc(self): return self.conv_afg_loc
    def getRedCycAccumTimingLoc(self): return self.redcyc_accum_timing
    def getMagAccumTimingLoc(self): return self.mag_accum_timing
    
    #################################################################
    # Methods related to getting/finding MM8 Datafiles
    #################################################################
    def getLatestMCPDataFile(self, dir2search=None):
        """ Gets the most recent *.raw file in the specified directory
        """
        if dir2search == None: dir2search = self.getMcpDataDirectory()
        file_format = os.path.join(dir2search, self.dataExtFormat)
        tdcFiles = glob.glob(file_format)
        if len(tdcFiles) == 0: return "None"
        tdcFilePath = max(tdcFiles, key=os.path.getctime) # use the latest file
        return tdcFilePath # Return the file without the extension
    def findMM8File(self, mcp_dataname):
        """ Searches for the MM8 Datafile that corresponds to
            a given position datafile set. If there is a datafile
            with the same name as the mcp datafile, this is used.
            Otherwise the following (!!!SKETCHY!!!) alg is used:
            The MM8 file created most recently before the position 
            data file is returned
        Args:
            mcp_dataname (str): full filepath to the mcp datafile
        """
        # Get all possible mm8 datafiles and sort by ctime
        mm8Files = [os.path.join(self.getMM8DataDirectory(), filename.name) for filename in Path(self.getMM8DataDirectory()).rglob('*.dat')]
        if len(mm8Files) == 0: return "None"
        # Check if there is an MM8 file with the same name
        mcp_basename = os.path.splitext(os.path.basename(mcp_dataname))[0]
        for file in mm8Files:
            if os.path.splitext(os.path.basename(file))[0] == mcp_basename: 
                return file
        print("**Warning** Could not find an MM8 datafile with the name "+mcp_basename+". Looking for MM8 file created most recently before this MCP datafile.")
        # Get the creation time of the MCP Datafile
        mm8Files.sort(key=lambda x: os.path.getctime(x)) # oldest file to newest file
        mm8Files_ctimes = np.array([os.path.getctime(f) for f in mm8Files])
        mcp_ctime = os.path.getctime(mcp_dataname)
        # Find the mm8 file which was created most recently just before the mcp datafile
        # larger ctime means it was made more recently
        mm8_file_idxs = np.where(mm8Files_ctimes < mcp_ctime)[0]
        if len(mm8_file_idxs) <= 0: 
            print("\t...No MM8 files could be found with a creation time before the current datadfile")
            return "None"
        idx = mm8_file_idxs[-1] # get the most recent file, ctime earlier than the MCP datafile
        print("\tFound MM8 File:", mm8Files[idx])
        return mm8Files[idx]
    
    #################################################################
    # Methods related to getting/finding AME mass information
    #################################################################
    def __loadAME__(self):
        # Load in the AME data
        if not os.path.isfile(self.amePath):
            raise FileNotFoundError("The AME datafile "+str(self.amePath)+" does not exist. Please check the location.")
        with open(self.amePath, 'r') as file: ame_data = file.read()
        ame_lines = ame_data.split("\n")
        self.ameDataTable = np.array([line.split("\t") for line in ame_lines if line != ""]) 
        self.ameUniqueElements = np.array([val.lower() for val in set(self.ameDataTable[:,2])])
        self.ameNonUniqueElements = np.array([val.lower() for val in self.ameDataTable[:,2]])

    def getIonMass(self, name, chargestate):
        """ Gets the mass for a molecular ion, in atomic mass units
        Args: 
            name (str): expected format is "elemNumber elemName elemMassNum", additional elements separated by colon (e.g. 2H1:1O16)
            chargestate (int): charge state, e.g. 1 -> 1+
        Returns:
            The (1) molecule mass and (2) mass uncertainty in units of u
        """
        if not type(chargestate) == int: raise ValueError("Expected the charge state to be represented as an integer to calculate the molecule mass.")

        elem_list = name.split(":")
        summed_mass = 0
        sum_sq_mass_unc = 0
        for elem in elem_list:
            # find indecies to parse elem info
            name_start_idx = re.search(r"[A-Za-z]", elem).start()
            name_end_idx = re.search(r"[0-9]", elem[name_start_idx:]).start() + name_start_idx
            # get each piece of the elem info
            num_elem = elem[:name_start_idx]
            try: num_elem = int(num_elem)
            except: num_elem = 1
            elem_name = elem[name_start_idx:name_end_idx]
            if not elem_name.lower() in self.ameUniqueElements: raise ValueError("Could not parse the element name in "+str(elem)+".") 
            mass_num = elem[name_end_idx:]
            try: mass_num = int(mass_num)
            except: raise ValueError("Could not parse the isotope mass number in "+str(elem)+".")
            # try and find the specified isotope in the ame table
            elem_name_idx = np.where(self.ameNonUniqueElements == elem_name.lower())[0]
            try: found_mass_nums = self.ameDataTable[elem_name_idx,1].astype(int)
            except: raise ValueError("Could not parse all mass numbers in the AME table!")
            if not mass_num in found_mass_nums: raise ValueError("Could not find the specified isotope in AME from "+str(elem)+".")
            isotope_idx = np.where(found_mass_nums == mass_num)[0][0]
            ame_table_idx = elem_name_idx[isotope_idx]
            ame_mass_excess = self.ameDataTable[ame_table_idx,3].replace("#","")
            ame_mass_excess_unc = self.ameDataTable[ame_table_idx,4].replace("#","")
            try: 
                ame_mass_excess = float(ame_mass_excess) # Value is in keV!
                ame_mass_excess_unc = float(ame_mass_excess_unc) # Value is in keV!
            except: raise ValueError("Could not parse the ame table mass value for "+str(elem_name)+str(mass_num)+".")
            # Calculate the mass of the isotope
            mass_val = (ame_mass_excess/1000 + mass_num*931.49410242)/931.49410242 # Calculates in u!
            summed_mass += num_elem*mass_val
            # calculate the mass unc of the isotope
            mass_unc = (ame_mass_excess_unc/1000)/931.49410242 # Calculates in u!
            sum_sq_mass_unc += mass_unc**2
        # Adjust the molecule mass by the charge state
        electron_mass = 0.0005485799090648417 # mass in u
        ion_mass = summed_mass - chargestate*electron_mass 
        ion_mass_unc = np.sqrt(sum_sq_mass_unc)
        return ion_mass, ion_mass_unc
    
    def getIonCyclotronFrequency(self, name, chargestate, ref_name, ref_chargestate, ref_freq, ref_freq_unc):
        """ Gets the cyclotron frequency for a molecular ion based on a given reference measurement
        Args: 
            name (str): ion to calculate frequency for. expected format is "elemNumber elemName elemMassNum", 
                        additional elements separated by colon (e.g. 2H1:1O16)
            chargestate (int): charge state of the ion to calculate the frequency for, e.g. 1 -> 1+
            ref_name (str): name of the (molecular) ion used as a reference. Same format as above
            ref_chargestate (int): charge state of the reference ion, e.g. 1 -> 1+
            ref_freq (float): cyclotron frequency of the reference species, in Hz
            ref_freq_unc (float): uncertainty in the reference frequency, in Hz
        Returns:
            (1) frequency and (2) frequency uncertainty, in Hz, for the ion of interest
        """
        if not type(chargestate) == int: raise ValueError("Expected the charge state to be represented as an integer to calculate the molecule mass.")
        if not type(ref_chargestate) == int: raise ValueError("Expected the charge state to be represented as an integer to calculate the molecule mass.")
        electron_charge = 1.602176634e-19
        
        # Step 1: get the reference mass and uncertainty
        ref_mass, ref_mass_unc = self.getIonMass(ref_name, ref_chargestate) # gets the mass in units of u!
        # Step 2: calculate the magnetic field strength and uncertainty. NOTE that these units are NOT teslas!
        # B = freq*(2pi m)/(q*e), delB = 2*pi/(q*e) * sqrt( freq^2 * delm^2 + m^2 * delfreq^2 )
        B = ref_freq*(2*np.pi*ref_mass)/(ref_chargestate*electron_charge)
        B_unc = (2*np.pi/(ref_chargestate*electron_charge)) * np.sqrt( (ref_freq**2)*(ref_mass_unc**2) + (ref_mass**2)*(ref_freq_unc**2) )
        # Step 3: get the IOI mass and uncertainty, and calculate its frequency
        # freq = qeB/(2pi m), delfreq = e*q/(2*pim^2) * sqrt( B^2 delm^2 + m^2 delB^2 )
        ioi_mass, ioi_mass_unc = self.getIonMass(name, chargestate)
        freq = chargestate*electron_charge*B/(2*np.pi*ioi_mass)
        freq_unc = (chargestate*electron_charge/(2*np.pi*(ioi_mass**2))) * np.sqrt( (B**2)*(ioi_mass_unc**2) + (ioi_mass**2)*(B_unc**2) )
        return freq, freq_unc

if __name__ == "__main__":
    test_sys = System("", "", "", "", "I:\\projects\\lebit\\Data Exchange\\MM8\\PImm8sets\\ame2020.txt", 
                 "", "", "", "", "", "", "", "", "", "", "", "")
    
    freq, unc = test_sys.getIonCyclotronFrequency("1Ti50",1,"1V51",1,2819540.91, 0.05)
    # Expect to get freq of 2875947.9514
    print(freq, unc)