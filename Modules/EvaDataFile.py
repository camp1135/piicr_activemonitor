import os
from struct import unpack
import numpy as np

class EvaDataFile:

    def __init__(self):
        self.flushData()

    def flushData(self):
        self.sourcetype='N/A'
        self.filename='N/A'
        self.header='N/A'
        self.headerlength=-1
        self.datastart=-1
        self.numscan1=-1
        self.numscan2=-1
        self.numscan1val=[]
        self.numscan2val=[]
        self.numchannels='N/A'
        self.channelbinwidth='N/A' #normally in us
        self.bindata=[]
        self.summatrix=0
        self.starttime=0
        self.endtime=0
    
    def loadEvaFile(self,filename):
        if os.path.isfile(filename) and filename.endswith('.dat'):
            self.filename= filename
            self.sourcetype='Eva'
            try:
                datafile=open(self.filename,'rb')
                datafile = datafile
                datafile.seek(0,2)
                eof=int(datafile.tell())
                datafile.seek(0)
                (self.headerlength,)=unpack('=l',datafile.read(4))
                (self.datastart,)=unpack('=l',datafile.read(4))
                self.header=datafile.read(self.headerlength)
                #	datafile.seek(8+self.headerlength)
                (self.numscan1,)=unpack('=l',datafile.read(4))
                self.numscan1val=[]
                for i in range(self.numscan1):
                    (value,)=unpack('=d',datafile.read(8))
                    self.numscan1val.append(value)
                (self.numscan2,)=unpack('=l',datafile.read(4))
                for i in range(self.numscan2):
                    (value,)=unpack('=d',datafile.read(8))
                    self.numscan2val.append(value)
					
                #determine the number of channels and bin width
                mcaindex=self.header.find(b'[MCA]')
                tpcindex=self.header.find(b'TimePerChannel',mcaindex)
                endindex=self.header.find(b'\xb5',tpcindex)
                self.channelbinwidth=float(self.header[tpcindex+15:endindex])
                chanindex=self.header.find(b'Channels=',mcaindex)
                endindex=self.header.find(b',',chanindex)
                self.numchannels=int(self.header[chanindex+9:endindex])

                #expand binary bin data into self.bindata
                self.bindata=[]
                if self.numscan1<1 or self.numchannels<1:
                    print('Invalid array size.')
                    print('Number of scans = '+self.numscan1)
                    print('Number of channels = '+self.numchannels)
                    return
                else:
                    temparray=np.zeros((self.numscan1,self.numchannels),dtype=np.int16)
                
                run=True
                start=True
                while run:
                    for i in range(self.numscan1):
                        if int(datafile.tell())+6>eof:
                            self.endtime = rectime
                            run=False
                            break
                        else:
                            (reclength,)=unpack('=h',datafile.read(2))
                            (rectime,)=unpack('=l',datafile.read(4))
                            if start:
                                self.starttime = rectime
                                start=False
                        if reclength==4:
                            continue
                        elif reclength>=4+self.numchannels:
                            for j in range(self.numchannels):
                                if int(datafile.tell())+2>eof:
                                    run=False
                                    break
                                else:
                                    (temparray[i,j],)=unpack('=h',datafile.read(2))
                        else:
                            for j in range(int((reclength-4)/4)):
                                if int(datafile.tell())+4>eof:
                                    run=False
                                    break
                                else:
                                    (binnum,)=unpack('=h',datafile.read(2))
                                    (counts,)=unpack('=h',datafile.read(2))
                                    temparray[i,binnum]=counts
                    if run:
                        self.bindata.append(temparray)
                        temparray=np.zeros((self.numscan1,self.numchannels),dtype=np.int16)
                    else: break
                
                datafile.close()
            except IOError:
                print('Could not open '+filename+' for binary reading.')
                return
                
        else: print(filename+' is not a valid Eva file.')
