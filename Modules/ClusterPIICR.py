"""
This code allows you to use the NN clustering model to predict clusters in PI-ICR data
@Author: Scott Campbell
@Version: 22 January 2023
"""

import os
from keras.models import model_from_json
from scipy.ndimage.filters import gaussian_filter
import numpy as np

class ClusterPIICR:

    def __init__(self, mcp_radius, nx=50): # 50x50 grid seems reasonable (0.6 ns resolution)
        """
        Args: 
            mcp_radius (double): the radius of MCP active area in ns
            nx (int, default 50): the number of pixels along each axis. default to 50x50 grid
        """
        self.nx = nx
        # create the grid
        x_list = np.linspace(-mcp_radius,mcp_radius,self.nx)
        self.mesh_X, self.mesh_Y = np.meshgrid(x_list, x_list)
        self.mcp_radius = mcp_radius
        self.dx = x_list[1] - x_list[0]

        currpath = os.path.dirname(os.path.realpath(__file__))
        modelpath = os.path.join(currpath, "..", "ClusteringNN")

        # load in the cluster map model json file and create model
        json_file = open(os.path.join(modelpath,'PI-ICR_SingleSpot_Cluster_Model.json'), 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        self.spot_model = model_from_json(loaded_model_json)
        self.spot_model.load_weights(os.path.join(modelpath,'PI-ICR_SingleSpot_Cluster_Model.h5'))

        # load in the ellipse model json file and create model
        json_file = open(os.path.join(modelpath,'PI-ICR_Ellipse_Cluster_Model.json'), 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        self.ellipse_model = model_from_json(loaded_model_json)
        self.ellipse_model.load_weights(os.path.join(modelpath,'PI-ICR_Ellipse_Cluster_Model.h5'))
        
    def __get_data_for_NN__(self, shot_x, shot_y, normalize=True):
        """ This method take a list of shots, discretizes to the domain, and normalizes so that the grid sums to one
        Args: 
            shot_x (list): list of x coords of each shot. Shots outside MCP radius are ignored
            shot_y (list): list of y coords of each shot. Shots outside MCP radius are ignored
        Returns:
            a 1D list (row-wise) corresponding to the normalized MCP hits
        """
        Z = np.zeros(self.mesh_X.shape) # Create a blank array to store shot info
        for kk in range(len(shot_x)):
            if np.sqrt(shot_x[kk]**2 + shot_y[kk]**2) < self.mcp_radius:
                x_idx, y_idx = int((shot_x[kk] + self.mcp_radius)/self.dx), int((shot_y[kk] + self.mcp_radius)/self.dx)
                Z[y_idx][x_idx] += 1
        if normalize: 
            Z = Z / np.sum(Z) # Normalize so that Z sums to 1
        return Z.reshape((self.nx**2))
    
    def __get_data_from_cluster__(self, shot_x, shot_y, cluster_map):
        """ This method take a list of shots, and a discretized cluster map, and returns the cluster shot info
        Args: 
            shot_x (list): list of x coords of each shot. Shots outside MCP radius are ignored
            shot_y (list): list of y coords of each shot. Shots outside MCP radius are ignored
            cluster_map (2D list): matrix of whether a point is in the cluster or not
            cluster_thresh (double 0->1): threshold of whether to include a point in the cluster or not
        Returns:
            coordinates of the points in the cluster
        """
        cluster_x, cluster_y = [], []
        noise_x, noise_y = [], []
        for kk in range(len(shot_x)):
            if np.sqrt(shot_x[kk]**2 + shot_y[kk]**2) < self.mcp_radius:
                x_idx, y_idx = int((shot_x[kk] + self.mcp_radius)/self.dx), int((shot_y[kk] + self.mcp_radius)/self.dx)
                if cluster_map[y_idx][x_idx] > 0:
                    cluster_x.append(shot_x[kk])
                    cluster_y.append(shot_y[kk])
                else:
                    noise_x.append(shot_x[kk])
                    noise_y.append(shot_y[kk])
        return cluster_x, cluster_y, noise_x, noise_y
    
    def __apply_filters_to_map__(self, cluster_map, cluster_thresh, smoothing_sigma):
        # Apply some amount of Gaussian smoothing to the cluster map
        # This helps with the potential overfitting from the NN
        if smoothing_sigma != None:
            cluster_map = gaussian_filter(cluster_map, sigma=smoothing_sigma)
        # Apply a hard prediction cutoff
        if (np.amax(cluster_map)-np.amin(cluster_map)) != 0:
            cluster_map = (cluster_map - np.amin(cluster_map))/(np.amax(cluster_map)-np.amin(cluster_map))
        else:
            cluster_map = np.zeros(cluster_map.shape)
        
        cluster_map[cluster_map < cluster_thresh] = 0
        cluster_map[cluster_map > 0] = 1
        
        return cluster_map
    
    def cluster_data(self, shot_x, shot_y, cluster_thresh=0.35, smoothing_sigma=None, return_map=False):
        """
        Args: 
            shot_x (1D list): x coords of the data to cluster
            shot_y (1D list): y coords of the data to cluster
            cluster_thresh (double 0->1, default 0.35): how hard to cluster, 1 means 'nothing', 0 means 'everything' from the NN prediction
            smoothing_sigma (int, default None): applys a gaussian blurring on the cluster map. standard deviation for Gaussian kernel
                                                 only really important to increase if you are dealing with small samples (~20 ions in cluster)
            n_clusters (int, default None): determines the number of clusters to find. If None, this will be determined automatically (up to 3).                                    
        Returns:
            all_clusters (3D list): list of all identified cluster shots. 1st index = cluster num. for 0th cluster: clust0 = all_clusters[0] -> list of x and y coords 
            noise (2D list): list of all identified noise x and y coordinates
        """
        # Discretize the shot data and scale for the NN
        nn_input = self.__get_data_for_NN__(shot_x, shot_y)
        nn_input *= self.nx**2 # this makes it so each pixel is ~1 -- This MUST be done for the network to predict properly

        # get the cluster map prediction, reshape to matrix
        nn_output = self.spot_model.predict(np.array( [nn_input,] ), verbose=0) # verbose=0 will suppress any/all printing when a prediction is made)
        cluster_map = nn_output[0].reshape((self.nx, self.nx))
        filtered_cluster_map = self.__apply_filters_to_map__(cluster_map, cluster_thresh, smoothing_sigma)
        
        # TODO: Implement the multispot clustering when needed

        cluster_x, cluster_y, noise_x, noise_y = self.__get_data_from_cluster__(shot_x, shot_y, filtered_cluster_map)

        all_clusters = [np.concatenate((np.transpose([cluster_x]),np.transpose([cluster_y])),axis=1)]
        try:
            noise = np.concatenate((np.transpose([noise_x]),np.transpose([noise_y])),axis=1)
        except:
            noise = [None]
        
        if return_map:
            return all_clusters, noise, cluster_map 
        return all_clusters, noise
    
    def cluster_ellipse(self, shot_x, shot_y, cluster_thresh=0.35, smoothing_sigma=None, return_map=False):
        """ Clusters an ellipse
        Args: 
            shot_x (1D list): x coords of the data to cluster
            shot_y (1D list): y coords of the data to cluster
            cluster_thresh (double 0->1, default 0.35): how hard to cluster, 1 means 'nothing', 0 means 'everything' from the NN prediction
            smoothing_sigma (int, default None): applys a gaussian blurring on the cluster map. standard deviation for Gaussian kernel
                                                 only really important to increase if you are dealing with small samples (~20 ions in cluster)
        
        Returns:
            cluster (2D list): list of all identified cluster x and y coordinates
            noise (2D list): list of all identified noise x and y coordinates
        """
        # Discretize the shot data and scale for the NN
        nn_input = self.__get_data_for_NN__(shot_x, shot_y)
        nn_input *= self.nx**2 # this makes it so each pixel is ~1 -- This MUST be done for the network to predict properly

        # get the cluster map prediction, reshape to matrix
        nn_output = self.ellipse_model.predict(np.array( [nn_input,] ), verbose=0) # verbose=0 will suppress any/all printing when a prediction is made
        cluster_map = nn_output[0].reshape((self.nx, self.nx))
        filtered_cluster_map = self.__apply_filters_to_map__(cluster_map, cluster_thresh, smoothing_sigma)

        # Convert the cluster map to the shots
        cluster_x, cluster_y, noise_x, noise_y = self.__get_data_from_cluster__(shot_x, shot_y, filtered_cluster_map)

        cluster = np.concatenate((np.transpose([cluster_x]),np.transpose([cluster_y])),axis=1)
        noise = np.concatenate((np.transpose([noise_x]),np.transpose([noise_y])),axis=1)
        
        if return_map:
            return cluster, noise, cluster_map
        return cluster, noise
        
################################################################################
# These methods are for testing/generating a simulated data to demonstrate
# You don't need to include these to do the clustering of real data
################################################################################

def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

def generate_spot_data(num_clusters):
    if num_clusters > 3:
        num_clusters = 3

    mcp_radius = 15 # radius of MCP in mm

    max_ellipse_offset = 0.1*mcp_radius # allow the ellipse to be offset by up to X% mcp radius
    max_ellipse_ab_diff = 0.15 # the ellipse params can't be more than this percent diff
    min_percent_noise = 0.75 # min amount of noise relative to the cluster data
    max_percent_noise = 1.25 # max amount of noise relative to the cluster data
    min_cluster_radius = 0.10*mcp_radius # min 'radius' of the cluster data
    max_cluster_radius = 0.20*mcp_radius # max 'radius' of the cluster data
    min_num_cluster_data = 80 
    max_num_cluster_data = 300 # up to this many shots in a cluster

    #####################################################
    # Define the ellipse
    #####################################################
    # define an offset
    off_x, off_y = np.random.uniform(low=0, high=max_ellipse_offset), np.random.uniform(low=0, high=max_ellipse_offset)
    # define the elipse major/minor axes
    max_ellipse_ab = mcp_radius - np.sqrt(off_x**2 + off_y**2) - (max_cluster_radius+min_cluster_radius)/2
    ellipse_a = np.random.uniform(low=0.5*max_ellipse_ab, high=max_ellipse_ab)
    ellipse_b = np.random.uniform(low=(1-max_ellipse_ab_diff)*ellipse_a, high=ellipse_a) 
    # define angle of ellipse
    ellipse_theta = np.random.uniform(low=0, high=2*np.pi)

    #####################################################
    # Define each cluster on the ellipse
    #####################################################
    """ There are different scenarios for different numbers of clusters
    One cluster:
        Simuilate the cluster on the ellipse somewhere. Could also choose to put it at the center...
    Two clusters: 
        Case 1: Put one cluster at origin, other on the ellipse
        Case 2: Put both on the ellipse -> make them mostly separated though
    Three clusters:
        Only one way for this to happen: one on the origin, two mostly separated on the ellipse

    Note that all of these have at least one cluster on the ellipse
    """
    all_cluster_theta, all_cluster_rho, all_cluster_radius = [], [], []
    if num_clusters >= 1:
        # Put the cluster somewhere on the ellipse
        all_cluster_theta.append( np.random.uniform(low=0, high=2*np.pi) ) # cluster angle on un-rotated ellipse
        all_cluster_radius.append( np.random.uniform(low=min_cluster_radius, high=max_cluster_radius) ) # spread of the cluster
        all_cluster_rho.append( np.sqrt((ellipse_a*np.cos(all_cluster_theta[0]))**2 + (ellipse_b*np.sin(all_cluster_theta[0]))**2) ) # dist from ellipse center to cluster center
    if num_clusters == 2:
        # If we have a second cluster, place the next spot on the ellipse half the time, other half put at origin
        if np.random.uniform(low=0, high=1) < 0.5: # Place at origin
            all_cluster_theta.append( 0 ) # doesn't really matter for this case
            all_cluster_radius.append( np.random.uniform(low=min_cluster_radius, high=max_cluster_radius) )
            all_cluster_rho.append( 0 )
        else: # place on the ellipse mostly away from the other ellipse
            all_cluster_radius.append( np.random.uniform(low=min_cluster_radius, high=max_cluster_radius) ) # spread of the cluster
            temp_theta = np.random.uniform(low=0, high=2*np.pi)
            sep_ang = min( (temp_theta-all_cluster_theta[0])%(2*np.pi), 2*np.pi - (temp_theta-all_cluster_theta[0])%(2*np.pi) ) # angle between two clusters
            min_ang = (all_cluster_radius[0]+all_cluster_radius[1])/all_cluster_rho[0]  # approx min angle between two clusters if rigid circles touching
            if sep_ang < 0.95 * min_ang: # If the clusters are two close, move the other cluster to the min separation distance
                all_cluster_theta.append( all_cluster_theta[0] + min_ang )
            else:
                all_cluster_theta.append(temp_theta)
            all_cluster_rho.append( np.sqrt((ellipse_a*np.cos(all_cluster_theta[1]))**2 + (ellipse_b*np.sin(all_cluster_theta[1]))**2) ) # dist from ellipse center to cluster center     
    if num_clusters == 3: 
        # First, place one cluster on the origin
        all_cluster_theta.append( 0 ) # doesn't really matter for this case
        all_cluster_radius.append( np.random.uniform(low=min_cluster_radius, high=max_cluster_radius) )
        all_cluster_rho.append( 0 )
        # Now, place the other cluster on the ellipse
        all_cluster_radius.append( np.random.uniform(low=min_cluster_radius, high=max_cluster_radius) ) # spread of the cluster
        temp_theta = np.random.uniform(low=0, high=2*np.pi)
        sep_ang = min( (temp_theta-all_cluster_theta[0])%(2*np.pi), 2*np.pi - (temp_theta-all_cluster_theta[0])%(2*np.pi) ) # angle between two clusters
        min_ang = (all_cluster_radius[0]+all_cluster_radius[2])/all_cluster_rho[0]  # approx min angle between two clusters if rigid circles touching
        if sep_ang < 0.85 * min_ang: # If the clusters are two close, move the other cluster to the min separation distance
            all_cluster_theta.append( all_cluster_theta[0] + min_ang )
        else:
            all_cluster_theta.append(temp_theta)
        all_cluster_rho.append( np.sqrt((ellipse_a*np.cos(all_cluster_theta[2]))**2 + (ellipse_b*np.sin(all_cluster_theta[2]))**2) ) # dist from ellipse center to cluster center     
    
    #####################################################
    # Compute each cluster on the ellipse
    #####################################################
    all_cluster_x, all_cluster_y = [], []
    all_noise_x, all_noise_y = [], []
    all_x, all_y = [], []
    for kk in range(num_clusters):
        # define the cluster
        n_cluster = np.random.randint(min_num_cluster_data, high=max_num_cluster_data)
        cluster_theta = all_cluster_theta[kk] # cluster angle on un-rotated ellipse
        cluster_radius = all_cluster_radius[kk]
        # 80% random, 20% normal dist
        cluster_xy = np.random.uniform(low=-cluster_radius, high=cluster_radius, size=(int(.8*n_cluster),2))
        cluster_xy = np.concatenate((cluster_xy, np.random.normal(loc=0, scale=0.65*cluster_radius, size=(int(.2*n_cluster),2))))
        cluster_x, cluster_y = cluster_xy[:,0], cluster_xy[:,1]

        # Put the cluster on the ellipse 
        r_at_theta = all_cluster_rho[kk] # dist from ellipse center to cluster
        cluster_x += r_at_theta*np.cos(cluster_theta)
        cluster_y += r_at_theta*np.sin(cluster_theta)
        
        # translate and rotate the ellipse in space
        cluster_rho, cluster_phi = cart2pol(cluster_x, cluster_y)
        cluster_phi += ellipse_theta
        cluster_x, cluster_y = pol2cart(cluster_rho, cluster_phi)
        cluster_x, cluster_y = cluster_x + off_x, cluster_y + off_y

        # Generate noise along the ellipse, strongest near the cluster
        # 70% on the ellipse, 30% random
        n_noise = n_cluster / num_clusters * np.random.uniform(low=min_percent_noise, high=max_percent_noise)
        noise_sigma = np.random.uniform(low=0.5, high=1) # from ~30deg to ~60%
        noise_phi = np.random.normal(loc=cluster_theta, scale=noise_sigma, size=int(0.7*n_noise))

        noise_x = np.array([off_x+ellipse_a*np.cos(t) for t in noise_phi])
        noise_y = np.array([off_y+ellipse_b*np.sin(t) for t in noise_phi])
        noise_rho, noise_phi = cart2pol(noise_x, noise_y)
        noise_rho = np.add(noise_rho, np.random.normal(loc=0, scale=0.32*cluster_radius, size=len(noise_rho)))
        noise_phi += ellipse_theta
        noise_x, noise_y = pol2cart(noise_rho, noise_phi)
        # add in the random noise as well 
        other_noise_rho = np.random.uniform(low=0, high=mcp_radius, size=int(0.3*n_noise))
        otehr_noise_theta = np.random.uniform(low=0, high=2*np.pi, size=int(0.3*n_noise))
        other_noise_x, other_noise_y = pol2cart(other_noise_rho, otehr_noise_theta)
        # Concatentate all the noise
        noise_x, noise_y = np.concatenate((noise_x,other_noise_x)), np.concatenate((noise_y,other_noise_y))

        # Append to master lists
        all_cluster_x = np.concatenate((all_cluster_x,cluster_x))
        all_cluster_y = np.concatenate((all_cluster_y,cluster_y))
        all_noise_x = np.concatenate((all_noise_x,noise_x))
        all_noise_y = np.concatenate((all_noise_y,noise_y))
        all_x, all_y = np.concatenate((all_x,cluster_x,noise_x)), np.concatenate((all_y,cluster_y,noise_y))
    return all_cluster_x, all_cluster_y, all_noise_x, all_noise_y, all_x, all_y

def generate_ellipse_data():
    mcp_radius = 15 # radius of MCP in mm

    max_ellipse_offset = 0.15*mcp_radius # allow the ellipse to be offset by up to X% mcp radius
    max_ellipse_ab_diff = 0.25 # the ellipse params can't be more than this percent diff
    min_percent_noise = 0.25 # min amount of noise relative to the cluster data
    max_percent_noise = 1 # max amount of noise relative to the cluster data
    min_ellipse_rho_sigma = 0.01*mcp_radius # we add normal noise to the radii of ellipse. This is min std dev
    max_ellipse_rho_sigma = 0.06*mcp_radius # we add normal noise to the radii of ellipse. This is max std dev
    min_num_ellipse_data = 80 
    max_num_ellipse_data = 200 # up to this many shots in a cluster

    n_ellipse_points = np.random.randint(min_num_ellipse_data, high=max_num_ellipse_data)
    # define an offset
    off_x, off_y = np.random.uniform(low=0, high=max_ellipse_offset), np.random.uniform(low=0, high=max_ellipse_offset)
    # define the elipse major/minor axes
    max_ellipse_ab = mcp_radius - np.sqrt(off_x**2 + off_y**2)
    ellipse_a = np.random.uniform(low=0.5*max_ellipse_ab, high=max_ellipse_ab)
    ellipse_b = np.random.uniform(low=(1-max_ellipse_ab_diff)*ellipse_a, high=ellipse_a) 
    # Create points on the ellipse
    ellipse_phi = np.random.uniform(low=0, high=2*np.pi, size=n_ellipse_points)
    ellipse_rho = np.sqrt(np.power(ellipse_a*np.cos(ellipse_phi),2) + np.power(ellipse_b*np.sin(ellipse_phi),2)) # calculate the radius of ellipse at angle phi
    # add some randomness to the radii
    rho_sigma = np.random.uniform(low=min_ellipse_rho_sigma, high=max_ellipse_rho_sigma)
    ellipse_rho += np.random.normal(loc=0, scale=rho_sigma, size=n_ellipse_points)

    # translate and rotate the ellipse in space
    ellipse_theta = np.random.uniform(low=0, high=2*np.pi) # define rotation angle of ellipse
    ellipse_phi += ellipse_theta
    ellipse_x, ellipse_y = pol2cart(ellipse_rho, ellipse_phi)
    ellipse_x, ellipse_y = ellipse_x + off_x, ellipse_y + off_y

    # Generate random noise
    n_noise = int(n_ellipse_points * np.random.uniform(low=min_percent_noise, high=max_percent_noise))
    noise_rho = np.random.uniform(low=0, high=mcp_radius, size=n_noise)
    noise_phi = np.random.uniform(low=0, high=2*np.pi, size=n_noise)
    noise_x, noise_y = pol2cart(noise_rho, noise_phi)

    # Finally, get the neural network input
    all_x, all_y = np.concatenate((ellipse_x,noise_x)), np.concatenate((ellipse_y,noise_y))
    
    return ellipse_x, ellipse_y, noise_x, noise_y, all_x, all_y


if __name__ == "__main__":
    # import matplotlib.pyplot as plt

    # num_spots = 3
    # # Simulate an input and see how well the classifier does
    # spot_x, spot_y, spot_noise_x, spot_noise_y, spot_all_x, spot_all_y = generate_spot_data(num_spots)
    # ellipse_x, ellipse_y, ellipse_noise_x, ellipse_noise_y, ellipse_all_x, ellipse_all_y = generate_ellipse_data()

    # PI_DataObj = ClusterPIICR(15) # create mcp with 15 ns radius
    # pred_spot_xy, pred_spot_noise_xy = PI_DataObj.cluster_data(spot_all_x, spot_all_y, cluster_thresh=0.33, smoothing_sigma=2)
    # pred_ellipse_xy, pred_ellipse_noise_xy = PI_DataObj.cluster_ellipse(ellipse_all_x, ellipse_all_y, cluster_thresh=0.33, smoothing_sigma=2)

    # plt.figure(figsize=(12,8))
    # plt.subplot(2,3,1)
    # plt.title("Noisy Spot Data")
    # plt.plot(spot_x, spot_y, 'go', alpha=0.5, markersize=5, label="data")
    # plt.plot(spot_noise_x, spot_noise_y, 'ko', alpha=0.5, markersize=5, label="noise")
    # plt.xlim([-15, 15])
    # plt.ylim([-15, 15])
    # plt.legend()

    # plt.subplot(2,3,2)
    # plt.title("True Spot Cluster")
    # plt.plot(spot_x, spot_y, 'go', alpha=0.5, markersize=5)
    # plt.xlim([-15, 15])
    # plt.ylim([-15, 15])

    # plt.subplot(2,3,3)
    # plt.title("Predicted Spot Cluster")
    # plt.plot(pred_spot_xy[:,0], pred_spot_xy[:,1], 'go', alpha=0.5, markersize=5)
    # plt.xlim([-15, 15])
    # plt.ylim([-15, 15])

    # plt.subplot(2,3,4)
    # plt.title("Noisy Ellipse Data")
    # plt.plot(ellipse_x, ellipse_y, 'go', alpha=0.5, markersize=5, label="data")
    # plt.plot(ellipse_noise_x, ellipse_noise_y, 'ko', alpha=0.5, markersize=5, label="noise")
    # plt.xlim([-15, 15])
    # plt.ylim([-15, 15])
    # plt.legend()

    # plt.subplot(2,3,5)
    # plt.title("True Ellipse Cluster")
    # plt.plot(ellipse_x, ellipse_y, 'go', alpha=0.5, markersize=5)
    # plt.xlim([-15, 15])
    # plt.ylim([-15, 15])

    # plt.subplot(2,3,6)
    # plt.title("Predicted Ellipse Cluster")
    # plt.plot(pred_ellipse_xy[:,0], pred_ellipse_xy[:,1], 'go', alpha=0.5, markersize=5,)
    # plt.xlim([-15, 15])
    # plt.ylim([-15, 15])
    # plt.show()
    pass