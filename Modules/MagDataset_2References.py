"""
@Desc: This class is a custom implementation of the Magnetron Dataset
       which uses two MagDataset objects (from two different reference
       species) to interpotate the magnetron frequency. This is useful,
       for example, if you are measuring A=50, and have reference 
       measurements at A=49 and A=51
@Author: Scott Campbell, campbels@nscl.msu.edu
@Version: 13 November 2024
"""
import numpy as np

class MagDataset2Refs:
    def __init__(self, mag_dataset1, mag_dataset2, averaging_weight=0.5):
        """
        Args:
            mag_dataset1 (MagDataset obj): the dataset of magnetron frequencies for your first reference species
            mag_dataset2 (MagDataset obj): the dataset of magnetron frequencies for your second reference species
            averaging_weight (float): weighting factor for mag_dataset1 value, (1-averaging_weight) is used for mag_dataset2
        """        
        self.mag_dataset1 = mag_dataset1
        self.mag_dataset2 = mag_dataset2
        self.averaging_weight = averaging_weight
                     
    def printSummary(self):
        pass

    def getMagnetronFrequencies(self):
        return []

    def interpMagFreq(self, midtime):
        """ Interpolates the magnetron frequency for a given time
        Args:
            midTime (float): the time for which the frequency should be interpolated at
        Returns:
            interpolated frequency and uncertainty
        """
        dataset1_val, dataset1_err = self.mag_dataset1.interpMagFreq(midtime)
        dataset2_val, dataset2_err = self.mag_dataset2.interpMagFreq(midtime)
        # take the average, using the weighting factor
        avg_mag_freq = dataset1_val*self.averaging_weight + dataset2_val*(1-self.averaging_weight)
        avg_mag_err = np.sqrt( (dataset1_err**2)*(self.averaging_weight**2) + (dataset2_err**2)*(1-self.averaging_weight)**2  )
        return avg_mag_freq, avg_mag_err
        
    

