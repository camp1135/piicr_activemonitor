# PI-ICR Active Monitoring App

## Overview
The visualization application is intended to provide a visual status for PI-ICR measurements:
1. Display beam status information (count rate, position, TOF)
1. Perform preliminary analysis (clustering spots/elipses, fitting, angular information)
1. Save datasets

The analysis application is intended to provide a means of analyzing PI-ICR data accounting for various known systematics, such as:
1. Elliplical projections of ion motion on the MCP coupled with spots not fully aligned
1. Time-dependent linear magnetic field drifts
1. Spot 'tailing'
1. Others!

## Dependencies
This code requires a pre-existing application to read the TDC which aquires the MCP/anode signals. The output of the TDC can then be read and interpreted by the this code in several different forms. As of now, development is solely focused on the RAW datafile: the timestamp for each mcp/anode signal is saved to the text file. An unmaintained datafile for reading in pre-reconstructed position information is available, but not guaranteed to interface smoothly with the visualization/analysis software.

This graphical user interface (GUI) is run with python. The following python versions have been tested and verified: **3.9.13**. A tested conda virtual environment package and/or list of dependencies can be provided upon request.

Some of the features of this visualization use the "Mass Measurement 8" (MM8) application maintained by Ryan Ringle at FRIB/MSU to record information about the current measurementy being run. The following versions of MM8 have been tested with this repository: **8.2.3** to **8.2.6**

## How to Install
The following 'non-standard' python libraries are required. This list is not exhaustive, and may depend on your python intallation
1. PySimpleGUI: (conda) "conda install -c conda-forge pysimplegui",  (pip) "pip install pysimplegui"
1. Tensorflow: (conda) "conda install -c conda-forge tensorflow", (pip) "pip install tensorflow"
1. scikit-learn: (conda) "conda install scikit-learn", (pip) "pip install scikit-learn"
1. scikit-image: (conda) "conda install scikit-image", (pip) "pip install scikit-image"
1. uncertainties: (conda) "conda install -c conda-forge uncertainties", (pip) "pip install uncertainties"
1. iminuit: (conda) "conda install -c conda-forge iminuit", (pip) "pip install iminuit"  

## Considerations for PS-MCP Analysis
This code base includes analysis codes for determining position information from a MCP delay-line anode detector stack. The ion signals are amplified, discriminated, then digitized with a TDC. There are some important considerations to note about the custom algorithm used for this purpose:

* The algorithm has been designed for a use-case suitable for the Phase-Imaging Ion Cyclotron Resonance (PI-ICR) mass spectrometry technique. This means relatively low ion hit rates in a spatially dense area. For example, ~10 hits/40 microseconds is a reasonable rate for this algorithm. Higher hit rates may result in un-reliable reconstructions.
* The mcp hit time spread should be much larger than the summed time for the delay line anode. For example, an mcp time spread FHWM of ~20 microseconds, and a delay-line anode time spread FWHM of ~2 nanoseconds.

## How to Set-up
- The code should be ready-to-run (in theory). To adapt to the needs/specificities of your system, you will need to update the INI file for the program, **VisualizationGUI.ini**. Importantly, the PySimpleGUI framework used to create this code is an on-going open source project.

## How to Use
- Both of the GUI applications can be run by executing their respective python files. Additionally, the code can be run via an executable file created with PyInstaller. The necessary commands to do so are located in **pyinstaller_VisualizationGUI_command.txt**. Once the executable has been created, the INI file must be placed in the same directory as the EXE file.
- The analysis code requires some additional steps to be adjusted in the main loop, future updates to come to make this straightforward for the user as a part of the GUI. Please direct questions to Scott Campbell if you have questions in the interim.

## Authors
- Scott Campbell (FRIB/MSU) campbels@frib.msu.edu
- Isaac Yandow (no longer active)

## Versions
October 2023: version 1.7
August 2024:  version 1.8