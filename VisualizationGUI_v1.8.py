"""
@Desc: Graphical user interface for data collection/analysis of Phase Imaging
       Ion Cyclotron Resonance (PI-ICR) precision Penning trap mass meas. 
       Developed for the Low Energy Beam Ion Trap (LEBIT) facility at the 
       Facility for Rare Isotope Beams (FRIB).
@Version: 1.8 March 2024
@Author: Scott Campbell (campbels@frib.msu.edu)
"""
import PySimpleGUI as sg
from time import strftime, localtime
import os 
import glob
import sys
sys.path.insert(0, os.getcwd()) # Ensure the code knows how to find Modules folder
import xml.etree.ElementTree as ET
import threading
import traceback
import numpy as np

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.patches as patches
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from Modules.FitSpot import SpotFitter
from Modules.FitEllipse import EllipseFitter
from Modules.FitGaussian import GaussianFitter
from Modules.ClusterPIICR import ClusterPIICR
from Modules.VisGUI_Utilities import create_main_page, create_adv_page, create_calib_page, \
                                     draw_figure, draw_figure_w_toolbar, get_resolution, plot_signal_xy
from Modules.PIICR_Utilities import cart2pol, pol2cart, get_hist_plot_points, extract_configuration, get_mcp_colormap

#################################################################
# Read in settings from the INI file
#################################################################
if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'): CWD = os.path.dirname(sys.executable) # for running from exe
else: CWD = os.path.dirname(os.path.abspath(__file__)) # for running from code
# Open the ini file to read the settings for the GUI
ini_path = os.path.join(CWD, 'VisualizationGUI.ini')
try:
    # Load in the important info from the INI file
    (tdcInfo, piSystem, limits, 
        piDataFile, piAnalysisFile,
        nbinsPosn, cntrateHistory, 
        user_fig_scale, user_font_scale) = extract_configuration(ini_path)
    # Create fitting and clustering objects
    PIClusterObj = ClusterPIICR(piSystem.getRadiusMCP())
    ellipseFitter = EllipseFitter()
    spotFitter = SpotFitter()
    gaussianFitter = GaussianFitter()
    # Define dictionaries to hold the current state of the system/settings
    main_runstate = {
        'lastreadline':0,
        'min_cnts_per_bin':1,
        'centerx':0, 'centery':0, 'target_angle':0,
        'app_update_time':0, 'tdc_update_time':0,
        'cluster_type':None, 'cluster_thresh':0.01, 'cluster_smooth':1,
        'tof_filter':True, 'z_filter':True ,'radial_filter':False, 'angular_filter':False, 
        'event_filter':False, 'allow_recovered_pos':True,
        'hide_rejected_points':False,
        'plot_style':'image', 'raw_colorbar':None,
        'last_spot_clustmap':[None], 'last_ellipse_clustmap':[None], 'last_clustmap_count':0,
        'fitting_type':None, 'last_fitting_count':0,
        'last_ellipse_params':None, 'last_cartspot_params':None, 'last_polarspot_params':None,
        'pi_data_directory': piSystem.getMcpDataDirectory(),
        'pi_filename': piDataFile.getFileName(), 
        'mm8_data_directory': piSystem.getMM8DataDirectory(),
        'mm8_filename':piDataFile.getMM8FilePath(),
        'alignment_angles':None
    } # piSystem.findMM8File(piSystem.getLatestMCPDataFile())}
    # adv1_runstate = {
    #     'fitting_type':None, 'last_fitting_count':0,
    #     'last_ellipse_params':None, 'last_cartspot_params':None, 'last_polarspot_params':None,
    #     'cluster_type':None,
    #     'last_spot_clustmap':[None], 'last_ellipse_clustmap':[None], 'last_clustmap_count':0 }
    # adv2_runstate = {
    #     'fitting_type':None, 'last_fitting_count':0,
    #     'last_ellipse_params':None, 'last_cartspot_params':None, 'last_polarspot_params':None,
    #     'cluster_type':None,
    #     'last_spot_clustmap':[None], 'last_ellipse_clustmap':[None], 'last_clustmap_count':0 }
    calib_runstate = {
        "trig_start_time":None,
        "trig_end_time":None,
        "curr_event_num":0,
        "event_to_plot":None,
        "sumx_min_plot_time":None,
        "sumx_max_plot_time":None,
        "sumy_min_plot_time":None,
        "sumy_max_plot_time":None,
        "oscil_event_num":None }
    main_information = {
        'trigger_events':"0",
        'reconstr_hits':"0",
        'recovered_hits':"0",
        'filtered_hits':"0",
        'clustered_hits':"0",
        'fit_info':"",
        'fit_info_terse':"" }
    # adv1_information = {
    #     'trigger_events':"0",
    #     'reconstr_hits':"0",
    #     'recovered_hits':"0",
    #     'filtered_hits':"0",
    #     'clustered_hits':"0",
    #     'fit_info':"",
    #     'fit_info_terse':"" }
    # adv2_information = {
    #     'trigger_events':"0",
    #     'reconstr_hits':"0",
    #     'recovered_hits':"0",
    #     'filtered_hits':"0",
    #     'clustered_hits':"0",
    #     'fit_info':"",
    #     'fit_info_terse':"" }
except:
    sg.Popup("System Error", "The following error occured and could not be recovered from:\n\n"+str(traceback.format_exc()))
    sys.exit()

#################################################################
# Helper methods for doing calculations
#################################################################
def refresh_cartspot_fit(positions):
    """ """
    #try:
    fit_dict = spotFitter.fit2DCartesianGaussian(positions)
    fit_params = {'fit_dict':fit_dict}
    fit_summary_terse = "\nCart. Spot Fit:\n"+ \
                " {:<3} {:>6} ±{:>5}\n".format("x",round(fit_dict['mean_x'],2), round(fit_dict['mean_x_err'],2)) + \
                " {:<3} {:>6} ±{:>5}\n".format("σx:", round(fit_dict['sigma_x'],2), round(fit_dict['sigma_x_err'],2)) + \
                " {:<3} {:>6} ±{:>5}\n".format("y:", round(fit_dict['mean_y'],2), round(fit_dict['mean_y_err'],2)) + \
                " {:<3} {:>6} ±{:>5}".format("σy:", round(fit_dict['sigma_y'],2), round(fit_dict['sigma_y_err'],2))
    fit_summary = "\nCartesian Spot Fit Summary:\n"+ \
                "  {:<10} {:>6} ± {:>6}\n".format("Mean x:",round(fit_dict['mean_x'],2), round(fit_dict['mean_x_err'],2)) + \
                "  {:<10} {:>6} ± {:>6}\n".format("Sigma x:", round(fit_dict['sigma_x'],2), round(fit_dict['sigma_x_err'],2)) + \
                "  {:<10} {:>6} ± {:>6}\n".format("Mean y:", round(fit_dict['mean_y'],2), round(fit_dict['mean_y_err'],2)) + \
                "  {:<10} {:>6} ± {:>6}".format("Sigma y:", round(fit_dict['sigma_y'],2), round(fit_dict['sigma_y_err'],2))
    x, y = fit_dict['mean_x']-main_runstate['centerx'], fit_dict['mean_y']-main_runstate['centery']
    rho, phi = np.sqrt(x**2 + y**2), np.arctan2(y, x)
    piAnalysisFile.setSpotRho(rho)
    piAnalysisFile.setSpotPhi(phi)
    #except: fit_summary, fit_summary_terse, fit_params = "\nFit Failed!", "\nFit Failed!", None
    return fit_summary, fit_summary_terse, fit_params

def refresh_polarspot_fit(positions):
    try:
        center = [main_runstate['centerx'], main_runstate['centery']]
        rho, phi = cart2pol(positions[:,0]-center[0], positions[:,1]-center[1])
        RP_data = np.transpose(np.concatenate(([rho],[phi]), axis=0))
        fit_dict = spotFitter.fit2DPolarGaussian(RP_data)
        # Save the fit params
        fit_params = {'fit_dict':fit_dict, 'center':center}
        # write a summary to display
        fit_summary_terse = "\nPolar Spot Fit:\n"+ \
                    " {:<3} {:>6} ±{:>5}\n".format("ρ:", round(fit_dict['mean_rho'],2), round(fit_dict['mean_rho_err'],2)) + \
                    " {:<3} {:>6} ±{:>5}\n".format("σρ:", round(fit_dict['sigma_rho'],2), round(fit_dict['sigma_rho_err'],2)) + \
                    " {:<3} {:>6} ±{:>5}\n".format("ϕ:", round(fit_dict['mean_phi']*180/np.pi,2), round(fit_dict['mean_phi_err']*180/np.pi,2)) + \
                    " {:<3} {:>6} ±{:>5}".format("σϕ:", round(fit_dict['sigma_phi']*180/np.pi,2), round(fit_dict['sigma_phi_err']*180/np.pi,2))
        fit_summary = "\nPolar Spot Fit Summary:\n"+ \
                    "  {:<10} {:>7} ± {:>6}\n".format("Mean rho:", round(fit_dict['mean_rho'],2), round(fit_dict['mean_rho_err'],2)) + \
                    "  {:<10} {:>7} ± {:>6}\n".format("Sigma rho:", round(fit_dict['sigma_rho'],2), round(fit_dict['sigma_rho_err'],2)) + \
                    "  {:<10} {:>7} ± {:>6}\n".format("Mean phi:", round(fit_dict['mean_phi']*180/np.pi,2), round(fit_dict['mean_phi_err']*180/np.pi,2)) + \
                    "  {:<10} {:>7} ± {:>6}".format("Sigma phi:", round(fit_dict['sigma_phi']*180/np.pi,2), round(fit_dict['sigma_phi_err']*180/np.pi,2))
    except Exception as e: fit_summary, fit_summary_terse, fit_params = "\nFit Failed!", "\nFit Failed!", None
    return fit_summary, fit_summary_terse, fit_params
    
def refresh_ellipse_fit(positions):
    """ """
    try:
        positions.astype(float)
        # Get the coefficeints of the conic form
        params = ellipseFitter.fit(positions[:,0], positions[:,1])
        # Calculate the errors of the fitted coefficents
        params = ellipseFitter.errors(positions[:,0], positions[:,1], params) # params are a ufloat object (uncertainties package). -> params[0].n gets the val, params[0].s gets the uncertainty
        # Convert the conic coeffs to the parametrized cartesian form
        center, a, b, theta = ellipseFitter.convert(params) # params are a ufloat object (uncertainties package). -> params[0].n gets the val, params[0].s gets the uncertainty
        # Calculate the confidence interval for the fit
        c_up, c_do = ellipseFitter.confidence_area(positions[:,0], positions[:,1], [i.n for i in params], f=2) # f tells how many sigma to go out
        # Save the fit params
        fit_params = {
            "params":params,
            "c_up":c_up,
            "c_do":c_do
        }
        # write a summary to display
        fit_summary_terse = "\nEllipse Fit:\n"+ \
                        " x: {:>6} ±{:>5}\n".format(round(center[0].n,2),round(center[0].s,2)) + \
                        " y: {:>6} ±{:>5}\n".format(round(center[1].n,2),round(center[1].s,2)) + \
                        " a: {:>6} ±{:>5}\n".format(round(a.n,2),round(a.s,2)) + \
                        " b: {:>6} ±{:>5}\n".format(round(b.n,2),round(b.s,2)) + \
                        " θ: {:>6} ±{:>5}".format(round(theta.n,2), round(theta.s,2))
        fit_summary = "\nEllipse Fit Summary:\n"+ \
                    "  Center: ({:>5} ±{:>5},{:>5} ±{:>5})\n".format(round(center[0].n,2),round(center[0].s,2),round(center[1].n,2),round(center[1].s,2)) + \
                    "  (a,b):  ({:>5} ±{:>5},{:>5} ±{:>5})\n".format(round(a.n,2),round(a.s,2),round(b.n,2),round(b.s,2)) + \
                    "  Rot. angle (deg): {:>6} ±{:>6}".format(round(theta.n,2), round(theta.s,2))
    except: fit_summary, fit_summary_terse, fit_params = "\nFit Failed!", "\nFit Failed!", None
    return fit_summary, fit_summary_terse, fit_params
   
def read_main_settings(gui_values):
    #----------------------------------------------------------------
    # Determine whether or not to plot the cluster on the raw data plot
    if gui_values['enable_cluster_check']: main_runstate['cluster_type'] = gui_values['cluster_combo'].lower()
    else: main_runstate['cluster_type'] = None
    #----------------------------------------------------------------
    # Determine whether or not to plot a fit on the raw data plot
    if gui_values['enable_fitting_check']: 
        if not str(main_runstate['fitting_type']).lower() == gui_values['fitting_combo'].lower():
            main_runstate['last_fitting_count'] = 0
        main_runstate['fitting_type'] = gui_values['fitting_combo'].lower()
    else: main_runstate['fitting_type'] = None
    #----------------------------------------------------------------
    # Determine what style of plot to make
    if event == 'imageplot_button':     main_runstate['plot_style'] = 'image'
    elif event == 'contourplot_button': main_runstate['plot_style'] = 'contour'
    elif event == 'scatterplot_button': main_runstate['plot_style'] = 'scatter'
    try: 
        temp = int(gui_values['min_cnts_per_bin'])
        main_runstate['min_cnts_per_bin'] = temp if temp > 0 else 0
    except: pass
    #----------------------------------------------------------------
    # Parse out the filters and limits
    try: limits['zmax'] = int(gui_values['z_max'])
    except: pass
    try: limits['angmin'] = float(gui_values['ang_min'])
    except: pass
    try: limits['angmax'] = float(gui_values['ang_max'])
    except: pass
    try: limits['radialmin'] = float(gui_values['radial_min'])
    except: pass
    try: limits['radialmax'] = float(gui_values['radial_max'])
    except: pass
    try: limits['tofmin'] = float(gui_values['gated_tof_min'])
    except: pass
    try: limits['tofmax'] = float(gui_values['gated_tof_max'])
    except: pass
    try: limits['eventmin'] = int(gui_values['event_range_min'])
    except: limits['eventmin'] = None
    try: limits['eventmax'] = int(gui_values['event_range_max'])
    except: limits['eventmax'] = None
    try: main_runstate['centerx'] = float(gui_values['trap_center_x'])
    except: pass
    try: main_runstate['centery'] = float(gui_values['trap_center_y'])
    except: pass
    main_runstate['cluster_thresh'] = float(gui_values['threshold'])
    main_runstate['cluster_smooth'] = float(gui_values['smoothing'])
    main_runstate['angular_filter'] = gui_values['ang_check'] and not (limits['angmin']%360 == limits['angmax']%360)
    main_runstate['radial_filter'] = gui_values['radial_check'] 
    main_runstate['tof_filter'] = gui_values['tof_bounds_check']
    main_runstate['z_filter'] = gui_values['z_limit_check']
    main_runstate['event_filter'] = gui_values['event_range_check']
    main_runstate['allow_recovered_pos'] = gui_values['allow_recovered_check']
    main_runstate['hide_rejected_points'] = gui_values['hide_rejected_points']
    #----------------------------------------------------------------
    # Update the analysis file as needed
    piAnalysisFile.setLimits(limits)
    piAnalysisFile.setCenter([main_runstate['centerx'],main_runstate['centery']])
    piAnalysisFile.setCenterErr([0,0])

def load_calib_info(calib_filepath=None):
    if calib_filepath == None:
        # use the most recently created file
        list_of_files = glob.glob(os.path.join(piSystem.getMM8SetsDirectory(),"calib","*")) # * means all if need specific format then *.csv
        calib_filepath = max(list_of_files, key=os.path.getctime)
    # verify that the calib file exists
    if not os.path.exists(calib_filepath):
        raise FileNotFoundError("The MM8 calib file could not be located at the specified location: "+str(calib_filepath))
    
    calibXMLTree = ET.parse(calib_filepath)
    calibXML = calibXMLTree.getroot()
    results_dict = { 
        "nuclide": calibXML.find('nuclide').text,
        "charge": calibXML.find('charge').text,
        "main_nuc_val": calibXML.find('freq0').find("freq").text,
        "main_nup_val": calibXML.find('freq1').find("freq").text,
        "main_num_val": calibXML.find('freq2').find("freq").text,
    }
    return results_dict

def calc_spot_alignment(gui_window, gui_values):
    results_dict = {
        "ioi_angle":None,
        "ioi_angle_unc":None,
        "cont0_angle":None,
        "cont0_angle_unc":None,
        "cont1_angle":None,
        "cont1_angle_unc":None,
        "cont2_angle":None,
        "cont2_angle_unc":None,
    }
    #----------------------------------------------------------------
    # Read in all settings for the spot alignment reference measurement
    try:
        if gui_values['use_mm8_calib']:
            # Use the latest mm8 calibration file to get the reference species and frequencies
            calib_dict = load_calib_info()
            gui_window['align_ref_name'].update(str(calib_dict['nuclide']), disabled=True)
            gui_window['align_ref_charge'].update(str(calib_dict['charge']), disabled=True)
            gui_window['align_ref_fp'].update(str(calib_dict['main_nup_val']), disabled=True)
            gui_window['align_ref_fm'].update(str(calib_dict['main_num_val']), disabled=True)
        else:
            gui_window['align_ref_name'].update(disabled=False)
            gui_window['align_ref_charge'].update(disabled=False)
            gui_window['align_ref_fp'].update(disabled=False)
            gui_window['align_ref_fm'].update(disabled=False)
        gui_window.refresh()
        ref_name     = gui_values['align_ref_name']
        ref_charge   = int(gui_values['align_ref_charge'])
        ref_fp       = float(gui_values['align_ref_fp'])
        ref_fm       = float(gui_values['align_ref_fm'])
        ref_fp_unc   = float(gui_values['align_ref_fp_unc'])
        ref_fm_unc   = float(gui_values['align_ref_fm_unc'])
        ref_freqc    = ref_fp + ref_fm
        ref_freqcunc = np.sqrt(ref_fp_unc**2 + ref_fm_unc**2)
    except: 
        print("Could not read the Reference Measurement information!",str(traceback.format_exc()))
        main_runstate['alignment_angles'] = None
        return
    #----------------------------------------------------------------
    # Read in all settings for the spot alignment ion of interest
    #   and calculate the spot alignment information
    try:
        # Calculate the ion frequencies and the expected spot location
        if gui_values['align_ioi_freq_override']:
            gui_window['align_ioi_fp'].update(disabled=False)
            gui_window['align_ioi_fp_unc'].update(disabled=False)
            ioi_fp = float(gui_values['align_ioi_fp'])
            ioi_fp_unc = float(gui_values['align_ioi_fp_unc'])
        else:
            ioi_name = gui_values['align_ioi_name']
            ioi_charge = int(gui_values['align_ioi_charge'])
            ioi_cycfreq, ioi_cycfrequnc = piSystem.getIonCyclotronFrequency(ioi_name, ioi_charge, ref_name, ref_charge, ref_freqc, ref_freqcunc)
            ioi_fp, ioi_fp_unc = ioi_cycfreq - ref_fm, np.sqrt(ioi_cycfrequnc**2 + ref_fm_unc**2)
            gui_window['align_ioi_fp'].update(str(np.round(ioi_fp,2)), disabled=True)
            gui_window['align_ioi_fp_unc'].update(str(np.round(ioi_fp_unc,2)), disabled=True)
        gui_window.refresh()
        # Use the angle and accumulation times to calculate the spot position
        ioi_start_angle = float(gui_values['align_start_angle'])
        ioi_start_angle_unc = float(gui_values['align_start_angle_unc'])
        ioi_magacc = float(gui_values['align_ioi_magacc'])
        ioi_redcycacc = float(gui_values['align_ioi_redcycacc'])
        net_angle = ((ioi_redcycacc*1e-6)*ioi_fp*piAnalysisFile.getReducedCyclotronDirection() +\
                    (ioi_magacc*1e-6)*ref_fm*piAnalysisFile.getMagnetronDirection())*360
        net_angle_unc = 360*np.sqrt( (ioi_fp_unc**2)*(ioi_redcycacc*1e-6)**2 + (ref_fm_unc**2)*(ioi_magacc*1e-6)**2 )
        results_dict['ioi_angle'] = (ioi_start_angle + net_angle)%360.0
        results_dict['ioi_angle_unc']  = np.sqrt(ioi_start_angle_unc**2 + net_angle_unc**2)
        gui_window['ioi_exp_angle'].update(str(int(np.round(results_dict['ioi_angle'])))+'('+str(int(np.round(results_dict['ioi_angle_unc'])))+')')
        gui_window.refresh()
    except: 
        main_runstate['alignment_angles'] = None
        return
    
    #----------------------------------------------------------------
    # Read in all settings for the contaminant spot alignment
    try:
        if gui_values['align_cont0_check']:
            cont0_name = gui_values['align_cont0_name']
            cont0_charge = int(gui_values['align_cont0_charge'])
            # Calculate the ion frequencies and the expected spot location
            cont_cycfreq, cont_cycfrequnc = piSystem.getIonCyclotronFrequency(cont0_name, cont0_charge, ref_name, ref_charge, ref_freqc, ref_freqcunc)
            cont_fp, cont_fp_unc = cont_cycfreq - ref_fm, np.sqrt(cont_cycfrequnc**2 + ref_fm_unc**2)
            net_angle = ((ioi_redcycacc*1e-6)*cont_fp*piAnalysisFile.getReducedCyclotronDirection() +\
                        (ioi_magacc*1e-6)*ref_fm*piAnalysisFile.getMagnetronDirection())*360
            net_angle_unc = 360*np.sqrt( (cont_fp_unc**2)*(ioi_redcycacc*1e-6)**2 + (ref_fm_unc**2)*(ioi_magacc*1e-6)**2 )
            results_dict['cont0_angle']  = (ioi_start_angle + net_angle)%360.0
            results_dict['cont0_angle_unc'] = np.sqrt(ioi_start_angle_unc**2 + net_angle_unc**2)
            gui_window['cont0_exp_angle'].update(str(int(np.round(results_dict['cont0_angle']))))
            gui_window.refresh()
    except: 
        pass
        #print("Could not read the Contaminants 0 information!",str(traceback.format_exc()))
    try:
        if gui_values['align_cont1_check']:
            cont1_name = gui_values['align_cont1_name']
            cont1_charge = int(gui_values['align_cont1_charge'])
            # Calculate the ion frequencies and the expected spot location
            cont_cycfreq, cont_cycfrequnc = piSystem.getIonCyclotronFrequency(cont1_name, cont1_charge, ref_name, ref_charge, ref_freqc, ref_freqcunc)
            cont_fp, cont_fp_unc = cont_cycfreq - ref_fm, np.sqrt(cont_cycfrequnc**2 + ref_fm_unc**2)
            net_angle = ((ioi_redcycacc*1e-6)*cont_fp*piAnalysisFile.getReducedCyclotronDirection() +\
                        (ioi_magacc*1e-6)*ref_fm*piAnalysisFile.getMagnetronDirection())*360
            net_angle_unc = 360*np.sqrt( (cont_fp_unc**2)*(ioi_redcycacc*1e-6)**2 + (ref_fm_unc**2)*(ioi_magacc*1e-6)**2 )
            results_dict['cont1_angle'] = (ioi_start_angle + net_angle)%360.0
            results_dict['cont1_angle_unc'] = np.sqrt(ioi_start_angle_unc**2 + net_angle_unc**2)
            gui_window['cont1_exp_angle'].update(str(int(np.round(results_dict['cont1_angle']))))
            gui_window.refresh()
    except: 
        pass
        #print("Could not read the Contaminants 1 information!",str(traceback.format_exc()))
    try:
        if gui_values['align_cont2_check']:
            cont2_name = gui_values['align_cont2_name']
            cont2_charge = int(gui_values['align_cont2_charge'])
            # Calculate the ion frequencies and the expected spot location
            cont_cycfreq, cont_cycfrequnc = piSystem.getIonCyclotronFrequency(cont2_name, cont2_charge, ref_name, ref_charge, ref_freqc, ref_freqcunc)
            cont_fp, cont_fp_unc = cont_cycfreq - ref_fm, np.sqrt(cont_cycfrequnc**2 + ref_fm_unc**2)
            net_angle = ((ioi_redcycacc*1e-6)*cont_fp*piAnalysisFile.getReducedCyclotronDirection() +\
                        (ioi_magacc*1e-6)*ref_fm*piAnalysisFile.getMagnetronDirection())*360
            net_angle_unc = 360*np.sqrt( (cont_fp_unc**2)*(ioi_redcycacc*1e-6)**2 + (ref_fm_unc**2)*(ioi_magacc*1e-6)**2 )
            results_dict['cont2_angle'] = (ioi_start_angle + net_angle)%360.0
            results_dict['cont2_angle_unc'] = np.sqrt(ioi_start_angle_unc**2 + net_angle_unc**2)
            gui_window['cont2_exp_angle'].update(str(int(np.round(results_dict['cont2_angle']))))
            gui_window.refresh()
    except: 
        pass
        #print("Could not read the Contaminants 2 information!",str(traceback.format_exc()))
    main_runstate['alignment_angles'] = results_dict

#################################################################
# Helper methods for plotting/updating the GUI
#################################################################

def plot_MCP_data(fig, ax, ang_fig, ang_ax, positions, rejects, runstate_dict, runinfo_dict, update_rate=3):
    """ 
    Plots the position data for the MCP hits which meet the thresholds set elsewhere 
    in the GUI for TOF, Z, radius, etc. If the dynamic clustering option is 
    selected, then the plot will show the current state of the clustered information
    as well as any fits (e.g. Ellipse) made to the data
    """
    ax.cla()
    plot_MCP_boundary(ax)
    
    clustered_positions = np.array(positions, dtype=float)
    rejected_positions = np.array(rejects, dtype=float)
    runinfo_dict['clustered_hits'] = None
    
    #---------------------------------------------------------------------
    # If the user specified clustering, apply it
    #---------------------------------------------------------------------
    if not runstate_dict['cluster_type'] == None and len(positions) > 3:
        # Only fully recluster every so often (every 'update_rate' iterations)
        # Note that a previous cluster will be used to interpolate information in the interim
        if runstate_dict['last_clustmap_count']%int(update_rate) == 0:
            nn_input = PIClusterObj.__get_data_for_NN__(positions[:,0], positions[:,1])
            nn_input *= PIClusterObj.nx**2 
            if runstate_dict['cluster_type'] == 'spot':
                nn_output = PIClusterObj.spot_model.predict(np.array( [nn_input,] ), verbose=0)
                runstate_dict['last_spot_clustmap'] = nn_output[0].reshape((PIClusterObj.nx, PIClusterObj.nx))
            elif runstate_dict['cluster_type']== 'ellipse':
                nn_output = PIClusterObj.ellipse_model.predict(np.array( [nn_input,] ), verbose=0)
                runstate_dict['last_ellipse_clustmap'] = nn_output[0].reshape((PIClusterObj.nx, PIClusterObj.nx))
        runstate_dict['last_clustmap_count'] = (1+runstate_dict['last_clustmap_count'])%int(update_rate)
        if runstate_dict['cluster_type'] == 'spot': cluster_map = runstate_dict['last_spot_clustmap']
        elif runstate_dict['cluster_type'] == 'ellipse': cluster_map = runstate_dict['last_ellipse_clustmap']
        else: cluster_map = [None]
        if None in cluster_map: cluster_map = np.ones((PIClusterObj.nx, PIClusterObj.nx))
        # Apply the most recent cluster map to get the cluster data
        filtered_cluster_map = PIClusterObj.__apply_filters_to_map__(cluster_map, runstate_dict['cluster_thresh'], runstate_dict['cluster_smooth'])
        cluster_x, cluster_y, noise_x, noise_y = PIClusterObj.__get_data_from_cluster__(positions[:,0], positions[:,1], filtered_cluster_map)
        # Update the positions variable to be the clustered positions
        clustered_positions = np.transpose([cluster_x, cluster_y])
        runinfo_dict['clustered_hits'] = len(clustered_positions)
        # Append the noise to the spatially rejected data
        if len(noise_x) > 0 and len(rejects) > 0:
            rejects_x = np.concatenate((noise_x, rejects[:,0]))
            rejects_y = np.concatenate((noise_y, rejects[:,1]))
            rejected_positions = np.transpose([rejects_x, rejects_y])
        elif len(noise_x) > 0: 
            rejected_positions = np.transpose([noise_x, noise_y])        

    #---------------------------------------------------------------------
    # Update all the related plots
    #---------------------------------------------------------------------
    plot_position_data(fig, ax, clustered_positions, rejected_positions, n_bins=nbinsPosn)
    plot_filter_locations(fig, ax)
    plot_spot_alignments(fig, ax)
    if main_runstate['angular_filter']: plot_ang_spectrum(ang_fig, ang_ax, positions, ang_start=limits['angmin'], ang_end=limits['angmax'])
    else: plot_ang_spectrum(ang_fig, ang_ax, positions)

    #---------------------------------------------------------------------
    # If the user specified fitting, apply it
    #---------------------------------------------------------------------
    if not runstate_dict['fitting_type'] == None and len(clustered_positions) > 3:
        # Only refit every so often (every 'update_rate' iterations)
        if runstate_dict['fitting_type'] == 'polar spot':
            if runstate_dict['last_fitting_count']%int(update_rate) == 0 or runstate_dict['last_polarspot_fit'] == None:
                summary, summary_terse, params = refresh_polarspot_fit(clustered_positions)
                runinfo_dict['fit_info'], runinfo_dict['fit_info_terse'] = summary, summary_terse
                runstate_dict['last_polarspot_params'] = params
            plot_spot_fit(ax, runstate_dict['fitting_type'], runstate_dict)
        elif runstate_dict['fitting_type'] == 'cart spot':
            if runstate_dict['last_fitting_count']%int(update_rate) == 0 or runstate_dict['last_cartspot_fit'] == None:
                summary, summary_terse, params = refresh_cartspot_fit(clustered_positions)
                runinfo_dict['fit_info'], runinfo_dict['fit_info_terse'] = summary, summary_terse
                runstate_dict['last_cartspot_params'] = params
            plot_spot_fit(ax, runstate_dict['fitting_type'], runstate_dict)
        elif runstate_dict['fitting_type'] == 'ellipse':
            if runstate_dict['last_fitting_count']%int(update_rate) == 0 or runstate_dict['last_ellipse_fit'] == None:
                summary, summary_terse, params = refresh_ellipse_fit(clustered_positions)
                runinfo_dict['fit_info'], runinfo_dict['fit_info_terse'] = summary, summary_terse
                runstate_dict['last_ellipse_params'] = params
            plot_ellipse_fit(ax, runstate_dict)

def plot_position_data(fig, ax, positions, rejects, n_bins=nbinsPosn):
    """
    Draws the valid position information on the MCP, including the clustered
    information (and fits)
    """
    myrange = [[limits['xmin'], limits['xmax']], [limits['ymin'], limits['ymax']]]
    extent = (limits['xmin'], limits['xmax'], limits['ymin'], limits['ymax'])
    # We need to check if either the positions and/or noise have no data. If so, need to handle the errors
    position_data, noise_data = np.zeros((nbinsPosn,nbinsPosn)), np.zeros((nbinsPosn,nbinsPosn))
    if len(positions) > 0:
        position_data, xbins, ybins = np.histogram2d(positions[:,0], positions[:,1], bins=n_bins, range=myrange)
        # Check if the user wants to cut additional data (e.g. min counts/bin)
        valid_idx_rows, valid_idx_cols = np.where(position_data < main_runstate['min_cnts_per_bin'])
        noise_data[valid_idx_rows, valid_idx_cols] += position_data[valid_idx_rows, valid_idx_cols]
        position_data[valid_idx_rows, valid_idx_cols] = 0
    if len(rejects) > 0:
        reject_data, xbins, ybins = np.histogram2d(rejects[:,0], rejects[:,1], bins=n_bins, range=myrange)
        noise_data += reject_data
    position_data = np.transpose(position_data) # NOTE: numpy dist DOES NOT follow cartesian convention. This fixes that
    noise_data    = np.transpose(noise_data)    # NOTE: numpy dist DOES NOT follow cartesian convention. This fixes that
    # If the user wants to hide the rejected points, update that here
    if main_runstate['hide_rejected_points']: noise_data *= 0
    # Find the difference of the position and noise data
    # Use the position data to form a 'map', and ignore noise on this map
    # That way, ANY position data is shown, and unique noise is still highlighted.
    # This is more truthful to the real data that we are analysing
    nonzero_pos_idrow, nonzero_pos_idcol = np.where(position_data > 0)
    noise_data[nonzero_pos_idrow, nonzero_pos_idcol] = 0
    pos_minus_noise = position_data-noise_data

    my_cmap = get_mcp_colormap(np.amax(position_data), np.amax(noise_data), full_range_color=True)
    if main_runstate['plot_style'] == 'contour':
        #xcenters, ycenters = 0.5*(xbins[1:] + xbins[:-1]), 0.5*(ybins[1:] + ybins[:-1])
        #CS = ax.contourf(xcenters, ycenters, pos_minus_noise, cmap=my_cmap, origin='lower', extent=extent)
        xcenters, ycenters = 0.5*(xbins[1:] + xbins[:-1]), 0.5*(ybins[1:] + ybins[:-1])
        xgrid, ygrid = np.meshgrid(xcenters, ycenters)
        temp_x_list, temp_y_list, temp_z_list = xgrid.ravel(), ygrid.ravel(), pos_minus_noise.ravel()
        valid_idx = np.where(temp_z_list)[0]
        temp_x_list, temp_y_list, temp_z_list = temp_x_list[valid_idx], temp_y_list[valid_idx], temp_z_list[valid_idx]
        CS = ax.tricontour(temp_x_list, temp_y_list, temp_z_list, cmap=my_cmap, origin='lower', extent=extent)
    elif main_runstate['plot_style'] == 'image':
        pos_minus_noise[pos_minus_noise == 0] = np.nan # This way the colorbar won't show up for zeros
        CS = ax.imshow(pos_minus_noise, cmap=my_cmap, origin='lower', extent=extent)
    else: # by default just do a scatter plot
        if len(positions) > 0: 
            ax.plot(positions[:,0], positions[:,1], 'o', markeredgewidth='0', markersize=4, alpha=0.3, color='indianred', label='Data')
        if not None in rejects and len(rejects) > 0 and not main_runstate['hide_rejected_points']:
            ax.plot(rejects[:,0], rejects[:,1], 'o', markeredgewidth='0', markersize=4, alpha=0.3, color='black', label='Noise')
        ax.legend()
    # Remove the previous colorbar (if there is any)
    try:  fig.delaxes(fig.axes[1])
    except: pass
    # Add in a new colorbar if the plot style needs one
    if main_runstate['plot_style'] == 'image' or main_runstate['plot_style'] == 'contour':
        divider = make_axes_locatable(ax)
        cax = divider.append_axes('bottom', size='3%', pad=0.05)
        temp = position_data-noise_data
        abs_min, abs_max = np.amin(temp), np.amax(temp)
        if abs_min == 0 and abs_max == 0: bounds = np.arange(-1.5,2,1)
        elif len(rejects) == 0 and len(positions) > 0: bounds = np.arange(0.5, abs_max+1, 1) # No noise present
        elif len(rejects) > 0 and len(positions) == 0: bounds = np.arange(abs_min-0.5, 0, 1) # only noise present
        elif len(rejects) == 0 and len(positions) == 0: bounds = np.arange(-1.5,2,1) # no noise or signal
        else: bounds = np.arange(abs_min-0.5, abs_max+1, 1) # both noise and position are present

        norm = matplotlib.colors.BoundaryNorm(bounds, my_cmap.N)
        cbar_ticks = np.linspace( abs_min, abs_max, 9)
        cbar_ticks = list(set([int(t) for t in cbar_ticks]))
        cbar = fig.colorbar(matplotlib.cm.ScalarMappable(norm=norm, cmap=my_cmap), cax=cax, shrink=0.9, orientation='horizontal', ticks=cbar_ticks)
        main_runstate['raw_colorbar'] = cbar

def plot_MCP_boundary(ax):
    """
    Draws the MCP boundary on the plot as well as directions of f+/f-
    """
    # draw the initial plot in the window
    MCPBounds = plt.Circle((0.0,0.0), piSystem.getRadiusMCP(), color='cadetblue', linewidth=4, fill=False)
    ax.add_patch(MCPBounds)
    style = "Simple, tail_width=3, head_width=10, head_length=12"
    kw = dict(arrowstyle=style, color="r")
    scale = piSystem.getRadiusMCP()/15
    a3 = patches.FancyArrowPatch((-13.27*scale, -7*scale), (-7*scale, -13.27*scale), connectionstyle="arc3,rad=.149", **kw)
    a4 = patches.FancyArrowPatch((13.27*scale, -7*scale), (7*scale, -13.27*scale), connectionstyle="arc3,rad=-.149", **kw)
    ax.add_patch(a3)
    ax.add_patch(a4)
    if piSystem.getDirectionOfRadialMotion() > 0: # redcyc motion is in the counterclockwise direction
        ax.text(-12*scale, -12*scale, r"$\mathcal{\phi}_{p}$", ha='center', va='center', rotation=0, color='k', fontsize=int(user_font_scale*30))
        ax.text(12*scale, -12*scale, r"$\mathcal{\phi}_{m}$", ha='center', va='center', rotation=0, color='k', fontsize=int(user_font_scale*30))
        ax.text(12*scale, -14*scale, r"$(apparent)$", ha='center', va='center', rotation=0, color='k', fontsize=int(user_font_scale*14))
    else: # redcyc motion is in the clockwise direction
        ax.text(-12*scale, -12*scale, r"$\mathcal{\phi}_{m}$", ha='center', va='center', rotation=0, color='k', fontsize=int(user_font_scale*30))
        ax.text(-12*scale, -14*scale, r"$(apparent)$", ha='center', va='center', rotation=0, color='k', fontsize=int(user_font_scale*14))
        ax.text(12*scale, -12*scale, r"$\mathcal{\phi}_{p}$", ha='center', va='center', rotation=0, color='k', fontsize=int(user_font_scale*30))
    
    axis_adjust = 0.05*piSystem.getRadiusMCP()
    ax.set_xlim([limits['xmin'] - axis_adjust, limits['xmax'] + axis_adjust])
    ax.set_ylim([limits['ymin'] - axis_adjust, limits['ymax'] + axis_adjust])
    ax.set_position([0.05, 0.0, 0.9, 1.0])

def plot_ellipse_fit(ax, runstate_dict):
    fit_dict = runstate_dict['last_ellipse_params']
    if fit_dict == None: return # ensure that there is fit data to plot
    params, c_up, c_do = fit_dict['params'], fit_dict['c_up'], fit_dict['c_do']
    # Convert the conic coeffs to the parametrized cartesian form
    center, a, b, theta = ellipseFitter.convert(params) # params are a ufloat object (uncertainties package). -> params[0].n gets the val, params[0].s gets the uncertainty
    center_up, a_up, b_up, theta_up = ellipseFitter.convert(c_up) 
    center_do, a_do, b_do, theta_do = ellipseFitter.convert(c_do) 
    # plot the fit
    phi = np.linspace(0, 2*np.pi, 1000)
    x = a.n*np.cos(phi)*np.cos(theta.n) - b.n*np.sin(phi)*np.sin(theta.n) + center[0].n
    y = a.n*np.cos(phi)*np.sin(theta.n) + b.n*np.sin(phi)*np.cos(theta.n) + center[1].n
    x_do = a_do*np.cos(phi)*np.cos(theta_do) - b_do*np.sin(phi)*np.sin(theta_do) + center_do[0]
    y_do = a_do*np.cos(phi)*np.sin(theta_do) + b_do*np.sin(phi)*np.cos(theta_do) + center_do[1]
    x_up = a_up*np.cos(phi)*np.cos(theta_up) - b_up*np.sin(phi)*np.sin(theta_up) + center_up[0]
    y_up = a_up*np.cos(phi)*np.sin(theta_up) + b_up*np.sin(phi)*np.cos(theta_up) + center_up[1]
    ax.plot(x, y, '-', color='mediumseagreen', linewidth=4, alpha=1, label='Fit')
    ax.plot(x_do, y_do, '-', color='yellowgreen', linewidth=4, alpha=1, label=r'2$\sigma$')
    ax.plot(x_up, y_up, '-', color='yellowgreen', linewidth=4, alpha=1)
    ax.legend()

def plot_spot_fit(ax, fit_type, runstate_dict):
    if fit_type == 'cart spot':
        if runstate_dict['last_cartspot_params'] == None: return
        fit_dict = runstate_dict['last_cartspot_params']['fit_dict']
        if fit_dict == None: return
        phi = np.linspace(0, 2*np.pi, 500)
        theta_rad = fit_dict['rot_theta'] * np.pi/180
        x_list = fit_dict['sigma_x']*np.cos(phi)*np.cos(theta_rad) - fit_dict['sigma_y']*np.sin(phi)*np.sin(theta_rad) + fit_dict['mean_x']
        y_list = fit_dict['sigma_x']*np.cos(phi)*np.sin(theta_rad) + fit_dict['sigma_y']*np.sin(phi)*np.cos(theta_rad) + fit_dict['mean_y']
        ax.plot(fit_dict['mean_x'], fit_dict['mean_y'], 'x', color='mediumseagreen', markersize=10, markeredgewidth=5, alpha=1, label='Center')
        ax.plot(x_list, y_list, '-', color='yellowgreen', linewidth=4, alpha=1, label = r'1$\sigma$')
        ax.legend()
    elif fit_type == 'polar spot':
        if runstate_dict['last_polarspot_params'] == None: return
        fit_dict = runstate_dict['last_polarspot_params']['fit_dict']
        if fit_dict == None: return
        center = runstate_dict['last_polarspot_params']['center']
        mean_rho, mean_phi, sigma_rho, sigma_phi = \
            np.abs(fit_dict['mean_rho']), fit_dict['mean_phi']%(2*np.pi), \
            fit_dict['sigma_rho'], fit_dict['sigma_phi']
        mean_x, mean_y = mean_rho*np.cos(mean_phi)+center[0], mean_rho*np.sin(mean_phi)+center[1]
        t_list = np.linspace(0, 2*np.pi, 500)
        phi_list = mean_phi + sigma_phi*np.cos(t_list)
        rho_list = mean_rho + sigma_rho*np.sin(t_list)
        x_list, y_list = pol2cart(rho_list, phi_list)
        x_list, y_list = x_list+center[0], y_list+center[1]
        ax.plot(mean_x, mean_y, 'x', color='mediumseagreen', markersize=10, markeredgewidth=5, alpha=1, label='Center')
        ax.plot(x_list, y_list, '-', color='yellowgreen', linewidth=4, alpha=1, label = r'1$\sigma$')
        ax.legend()

def plot_ang_spectrum(fig, ax, positions, ang_start=0, ang_end=360):
    """ Plots the angular spectrum histogram
    Args: 
        angles_list (1D list): angles for the filtered positions, in deg
        ang_min: the minimum angle to show in plot
        ang_max: the maximum angle to show in plot
    """
    ax.cla()
    ax.grid()


    if len(positions) < 2: return
    phi = [(np.arctan2(xy[1], xy[0])*180/np.pi)%360 for xy in positions]
    # check if there is a zero crossing
    if ang_start > ang_end:
        bins1, bins2 =  np.arange(ang_start, 360, 0.5), np.arange(0, ang_end, 0.5)
        vals1, _ = np.histogram(phi, bins=bins1)
        vals2, _ = np.histogram(phi, bins=bins2)
        vals = np.concatenate((vals1, vals2))
        bins = np.concatenate((bins1[:-1]-360, bins2))
        ax.set_xlim(ang_start-360, ang_end)
    else:
        bins = np.arange(ang_start, ang_end, 0.5)
        vals, _ = np.histogram(phi, bins)
        ax.set_xlim(ang_start, ang_end)
    plot_x, plot_y = get_hist_plot_points(vals, (bins[:-1]+bins[1:])/2)
    ax.plot(plot_x, plot_y, '-', color='dimgray')
    ax.set_xlabel("Angle (deg)")
    ax.set_ylim(ymin=0, ymax=np.amax(plot_y)+1)
    
def plot_tof_spectrum(fig, ax, mca_ax, mca_min, mca_max, tof_min_view, tof_max_view):
    """
    Plots the TOF spectrum histogram
    
    Args: 
        mca_min: the minimum mca bin/tof value to filter data
        mca_max: the maximum mca bin/tof value to filter data
        tof_min_view: the minimum tof to show in the plot
        tof_max_view: the maximum tof to show in the plot
    """
    ax.cla()
    ax.grid()
    ax.set_xlim([tof_min_view,tof_max_view])
    # Update the mca gate
    mca_ax.cla()
    mca_ymax = ax.get_ylim()[1]
    mca_ax.fill_between([tof_min_view, mca_min], [0,0], [mca_ymax,mca_ymax], color='indianred', alpha=0.3, zorder=0)
    mca_ax.fill_between([mca_max, tof_max_view], [0,0], [mca_ymax,mca_ymax], color='indianred', alpha=0.3, zorder=0)
    mca_ax.set_xlim([tof_min_view,tof_max_view])
    mca_ax.set_ylim(ax.get_ylim())
    mca_ax.get_yaxis().set_visible(False)
    # update the histograms
    tofs = piDataFile.getToFs()
    if len(tofs) > 0:
        try:
            bins = np.arange(tof_min_view, tof_max_view, 0.01)
            heights, bins = np.histogram(tofs, bins=bins)
            plot_x, plot_y = get_hist_plot_points(heights, bins)
            ax.plot(plot_x, plot_y, '-', color='dimgray', zorder=1)
            ax.set_ylim(ymin=0, ymax=np.amax(plot_y)+1)
        except: pass
    ax.set_xlabel(r"TOF ($\mu$s)")

def plot_filter_locations(fig, ax):
    apply_ang_filter = main_runstate['angular_filter']
    if (limits['angmin']%360) == (limits['angmax']%360): apply_ang_filter = False
    apply_radial_filter = main_runstate['radial_filter']
    if (limits['radialmin'] == limits['radialmax']): apply_radial_filter = False

    if apply_radial_filter and apply_ang_filter:
        filterWedgeA = patches.Wedge(
            (main_runstate['centerx'],main_runstate['centery']), 2.5*piSystem.getRadiusMCP(), 
            limits['angmax'], limits['angmin'], 
            facecolor='indianred', edgecolor=None, 
            fill=True, alpha=0.3, linewidth=None)
        filterWedgeB = patches.Wedge(
            (main_runstate['centerx'],main_runstate['centery']), 2.5*piSystem.getRadiusMCP(), 
            limits['angmin'], limits['angmax'], 
            width=(2.5*piSystem.getRadiusMCP()-limits['radialmax']), 
            facecolor='indianred', edgecolor=None, 
            fill=True, alpha=0.3, linewidth=None)
        filterWedgeC = patches.Wedge(
            (main_runstate['centerx'],main_runstate['centery']), limits['radialmin'], 
            limits['angmin'], limits['angmax'], 
            facecolor='indianred', edgecolor=None, 
            fill=True, alpha=0.3, linewidth=None)
        ax.add_patch(filterWedgeA)
        ax.add_patch(filterWedgeB)
        ax.add_patch(filterWedgeC)
    elif apply_radial_filter:
        filterWedgeA = patches.Wedge(
            (main_runstate['centerx'],main_runstate['centery']), limits['radialmin'], 
            0, 360, 
            facecolor='indianred', edgecolor=None, 
            fill=True, alpha=0.3, linewidth=None)
        filterWedgeB = patches.Wedge(
            (main_runstate['centerx'],main_runstate['centery']), 2.5*piSystem.getRadiusMCP(), 
            0, 360, width=(2.5*piSystem.getRadiusMCP()-limits['radialmax']),
            facecolor='indianred', edgecolor=None, 
            fill=True, alpha=0.3, linewidth=None)
        ax.add_patch(filterWedgeA)
        ax.add_patch(filterWedgeB)
    elif apply_ang_filter:
        filterWedge = patches.Wedge(
            (main_runstate['centerx'],main_runstate['centery']), 2.5*piSystem.getRadiusMCP(), 
            limits['angmax'], limits['angmin'], 
            facecolor='indianred', edgecolor=None, 
            fill=True, alpha=0.3, linewidth=None)
        ax.add_patch(filterWedge)

def plot_spot_alignments(fig, ax):
    angles_dict = main_runstate['alignment_angles']
    if angles_dict == None: return 

    # Define colors for the different regions
    sigma1_color = 'limegreen'
    sigma2_color = 'gold'
    sigma3_color = 'orange'
    cont_color = 'slategrey'

    # Determine what radial gates may be imposed to determine how to plot this info
    upper_radius, width = 2.5*piSystem.getRadiusMCP(), 2.5*piSystem.getRadiusMCP()
    if main_runstate['radial_filter'] and (limits['radialmin'] != limits['radialmax']):
        upper_radius, width = limits['radialmax'], limits['radialmax']-limits['radialmin']

    # First check if the user wants to plot the ioi expected location
    if not angles_dict['ioi_angle'] == None and not angles_dict['ioi_angle_unc'] == None:
        # Check if the angle uncertainty to 3 sigma is below the 360 degree resolution window
        if 6*angles_dict['ioi_angle_unc'] >= 360:
            sigma_wedge = patches.Wedge(
                (main_runstate['centerx'],main_runstate['centery']), upper_radius, 0, 360, width=width, 
                facecolor=sigma3_color, edgecolor=None, 
                fill=True, alpha=0.3, linewidth=None)
            ax.add_patch(sigma_wedge)
        else:
            #----------------------------------------------------
            # Plot the 1 Sigma error wedge
            sigma1_lo, sigma1_hi = (angles_dict['ioi_angle']-angles_dict['ioi_angle_unc'])%360, (angles_dict['ioi_angle']+angles_dict['ioi_angle_unc'])%360
            sigma1_wedge = patches.Wedge(
                (main_runstate['centerx'],main_runstate['centery']), upper_radius, 
                sigma1_lo, sigma1_hi, width=width, 
                facecolor=sigma1_color, edgecolor=None, 
                fill=True, alpha=0.3, linewidth=None)
            ax.add_patch(sigma1_wedge)
            #----------------------------------------------------
            # Plot the 2 Sigma error wedge
            sigma2_lo1, sigma2_hi1 = (sigma1_lo-angles_dict['ioi_angle_unc'])%360, sigma1_lo%360
            sigma2_wedge_lo = patches.Wedge(
                (main_runstate['centerx'],main_runstate['centery']), upper_radius, 
                sigma2_lo1, sigma2_hi1, width=width, 
                facecolor=sigma2_color, edgecolor=None, 
                fill=True, alpha=0.3, linewidth=None)
            ax.add_patch(sigma2_wedge_lo)
            sigma2_lo2, sigma2_hi2 = sigma1_hi%360, (sigma1_hi+angles_dict['ioi_angle_unc'])%360
            sigma2_wedge_hi = patches.Wedge(
                (main_runstate['centerx'],main_runstate['centery']), upper_radius, 
                sigma2_lo2, sigma2_hi2, width=width, 
                facecolor=sigma2_color, edgecolor=None, 
                fill=True, alpha=0.3, linewidth=None)
            ax.add_patch(sigma2_wedge_hi)
            #----------------------------------------------------
            # Plot the 3 Sigma error wedge
            sigma3_lo1, sigma3_hi1 = (sigma2_lo1-angles_dict['ioi_angle_unc'])%360, sigma2_lo1%360
            sigma3_wedge_lo = patches.Wedge(
                (main_runstate['centerx'],main_runstate['centery']), upper_radius, 
                sigma3_lo1, sigma3_hi1, width=width, 
                facecolor=sigma3_color, edgecolor=None, 
                fill=True, alpha=0.3, linewidth=None)
            ax.add_patch(sigma3_wedge_lo)
            sigma3_lo2, sigma3_hi2 = sigma2_hi2%360, (sigma2_hi2+angles_dict['ioi_angle_unc'])%360
            sigma3_wedge_hi = patches.Wedge(
                (main_runstate['centerx'],main_runstate['centery']), upper_radius, 
                sigma3_lo2, sigma3_hi2, width=width, 
                facecolor=sigma3_color, edgecolor=None, 
                fill=True, alpha=0.3, linewidth=None)
            ax.add_patch(sigma3_wedge_hi)

    # Next, check if the user wants to plot the contaminant 0 info
    if not angles_dict['cont0_angle'] == None and not angles_dict['cont0_angle_unc'] == None:
        center_angle, angle_err = angles_dict['cont0_angle'],angles_dict['cont0_angle_unc']
        # For the contaminants, just plot the center line and then 3sigma error bands (in grey)
        x0, y0 = main_runstate['centerx'],main_runstate['centery']
        rho1, phi1 = 2.5*piSystem.getRadiusMCP(), center_angle*np.pi/180
        x1, y1 = rho1*np.cos(phi1) + main_runstate['centerx'], rho1*np.sin(phi1) + main_runstate['centery']
        ax.plot([x0, x1], [y0, y1], '--k')
        # plot the 3sigma error band
        if 6*angle_err >= 360:
            cont0_wedge = patches.Wedge(
                (main_runstate['centerx'],main_runstate['centery']), upper_radius, 0, 360, width=width, 
                facecolor=cont_color, edgecolor=None, 
                fill=True, alpha=0.3, linewidth=None)
        else:
            cont0_wedge = patches.Wedge(
                (main_runstate['centerx'],main_runstate['centery']), upper_radius, 
                (center_angle-3*angle_err)%360, (center_angle+3*angle_err)%360, width=width, 
                facecolor=cont_color, edgecolor=None,
                fill=True, alpha=0.3, linewidth=None)
        ax.add_patch(cont0_wedge)
    
    # Next, check if the user wants to plot the contaminant 1 info
    if not angles_dict['cont1_angle'] == None and not angles_dict['cont1_angle_unc'] == None:
        center_angle, angle_err = angles_dict['cont1_angle'],angles_dict['cont1_angle_unc']
        # For the contaminants, just plot the center line and then 3sigma error bands (in grey)
        x0, y0 = main_runstate['centerx'],main_runstate['centery']
        rho1, phi1 = 2.5*piSystem.getRadiusMCP(), center_angle*np.pi/180
        x1, y1 = rho1*np.cos(phi1) + main_runstate['centerx'], rho1*np.sin(phi1) + main_runstate['centery']
        ax.plot([x0, x1], [y0, y1], ':k')
        # plot the 3sigma error band
        if 6*angle_err >= 360:
            cont1_wedge = patches.Wedge(
                (main_runstate['centerx'],main_runstate['centery']), upper_radius, 0, 360, width=width, 
                facecolor=cont_color, edgecolor=None, 
                fill=True, alpha=0.3, linewidth=None)
        else:
            cont1_wedge = patches.Wedge(
                (main_runstate['centerx'],main_runstate['centery']), upper_radius, 
                (center_angle-3*angle_err)%360, (center_angle+3*angle_err)%360, width=width, 
                facecolor=cont_color, edgecolor=None, 
                fill=True, alpha=0.3, linewidth=None)
        ax.add_patch(cont1_wedge)

    # Next, check if the user wants to plot the contaminant 2 info
    if not angles_dict['cont2_angle'] == None and not angles_dict['cont2_angle_unc'] == None:
        center_angle, angle_err = angles_dict['cont0_angle'],angles_dict['cont2_angle_unc']
        # For the contaminants, just plot the center line and then 3sigma error bands (in grey)
        x0, y0 = main_runstate['centerx'],main_runstate['centery']
        rho1, phi1 = 2.5*piSystem.getRadiusMCP(), center_angle*np.pi/180
        x1, y1 = rho1*np.cos(phi1) + main_runstate['centerx'], rho1*np.sin(phi1) + main_runstate['centery']
        ax.plot([x0, x1], [y0, y1], '-.k')
        # plot the 3sigma error band
        if 6*angle_err >= 360:
            cont0_wedge = patches.Wedge(
                (main_runstate['centerx'],main_runstate['centery']), upper_radius, 0, 360, width=width, 
                facecolor=cont_color, edgecolor=None, 
                fill=True, alpha=0.3, linewidth=None)
        else:
            cont0_wedge = patches.Wedge(
                (main_runstate['centerx'],main_runstate['centery']), upper_radius, 
                (center_angle-3*angle_err)%360, (center_angle+3*angle_err)%360, width=width, 
                facecolor=cont_color, edgecolor=None, 
                fill=True, alpha=0.3, linewidth=None)
        ax.add_patch(cont0_wedge)

def plot_count_rates(fig, ax):
    """ Plot the count rate as a line graph
    Args:
        fig: figure object for the plot
        ax: axes object for the plot
    """
    ax.cla()    
    ax.grid()
    
    # Get the shot/count information
    shot_counts = piDataFile.getZ(unique=True)
    shot_trigids = piDataFile.getTriggerIDs(unique=True)
    curr_event_num = piDataFile.CurrentEventID

    if len(shot_trigids) < 1:
        ax.set_ylabel('MM8 Shot Counts')
        ax.set_xlabel('Elapsed time [min]')
        ax.set_xlim(xmin=0, xmax=cntrateHistory)
        ax.set_ylim(ymin=0)
        ax.invert_xaxis()
        ax.plot([0, cntrateHistory], [0,0], '-', color='dimgray')
    elif piSystem.getUsesRawDataFile(): # The datafile with the raw tdc data is being read
        ax.set_ylabel('MM8 Shot Counts')
        ax.set_xlabel('Elaspsed Shot Number')
        ax.set_xlim(xmin=0, xmax=cntrateHistory)
        ax.invert_xaxis()
        # get indices of all shots in the history time window
        idx2plot = np.where( shot_trigids > (curr_event_num-cntrateHistory) )[0]
        if len(idx2plot) == 0: ax.plot([0, cntrateHistory], [0,0], '-', color='dimgray')
        else:
            cnts2plot, trigids2plot = shot_counts[idx2plot], shot_trigids[idx2plot]
            trigids2plot = curr_event_num - trigids2plot
            # Convert times to elapsed time from current time
            hist_vals = np.arange(0, cntrateHistory+1, 1)
            hist_cnts = np.zeros(len(hist_vals))
            for kk in range(len(trigids2plot)):
                hist_cnts[int(trigids2plot[kk])] = cnts2plot[kk] 
            # Plot the count rate
            plot_x, plot_y = get_hist_plot_points(hist_cnts, hist_vals)
            ax.plot(plot_x, plot_y, '-', color='dimgray')
    else: # The datafile with the reconstructed data is being read
        # There is no really good way to show this plot. I've tried a lot. Instead, I will just show a histogram of the counts/shot
        ax.set_xlim(xmin=0, xmax=10)
        ax.set_ylabel('Bin Counts')
        ax.set_xlabel('Counts per Shot')
        ax.set_xlim(xmin=0.5, xmax=10.5)

        bins = np.arange(0.5,11,1)
        heights, _ = np.histogram(shot_counts, bins=bins)
        plot_x, plot_y =  get_hist_plot_points(heights, bins)
        ax.plot(plot_x, plot_y, '-', color='dimgray')
        ax.set_xticks(ticks=range(1,11))
    rate_fig_agg.draw()

def plot_triggers(fig, ax):
    ax.cla()
    ax.set_yticks([]) # Hide the y-axes ticks & labels
    ax.tick_params(axis="x", bottom=True, top=True, labelbottom=True, labeltop=True)    
    ax.set_ylim(0,5)
    ax.set_xlabel("Time from Start Signal (ns)")
    ax.grid(visible=True, which='major', axis='x') # Add a grid for the x-axis
    # If there is no data, don't draw anything
    if len(piDataFile.getAllEvents()) < 1 or len(piDataFile.getAllReconstructedEvents()) < 1: 
        trig_fig_agg.draw()
        return

    # Choose colors for the different signal 'traces'
    signal_colors = {"X1":'lightslategray',
                     "X2":'gold',
                     "Y1":'orange',
                     "Y2":'indianred',
                     "MCP":'peru'}

    evt = piDataFile.EventsList[ calib_runstate["oscil_event_num"] ]
    # First, find the earliest and latest times in the signal traces
    min_mcp = np.inf if len(evt.mcp_list) == 0 else np.amin(evt.mcp_list)
    min_x1 = np.inf if len(evt.x1_list) == 0 else np.amin(evt.x1_list)
    min_x2 = np.inf if len(evt.x2_list) == 0 else np.amin(evt.x2_list)
    min_y1 = np.inf if len(evt.y1_list) == 0 else np.amin(evt.y1_list)
    min_y2 = np.inf if len(evt.y2_list) == 0 else np.amin(evt.y2_list)
    min_val = np.amin([min_mcp, min_x1, min_x2, min_y1, min_y2])
    earliest_time = None if min_val == np.inf else min_val 
    max_mcp = 0 if len(evt.mcp_list) == 0 else np.amax(evt.mcp_list)
    max_x1 = 0 if len(evt.x1_list) == 0 else np.amax(evt.x1_list)
    max_x2 = 0 if len(evt.x2_list) == 0 else np.amax(evt.x2_list)
    max_y1 = 0 if len(evt.y1_list) == 0 else np.amax(evt.y1_list)
    max_y2 = 0 if len(evt.y2_list) == 0 else np.amax(evt.y2_list)
    max_val = np.amax([max_mcp, max_x1, max_x2, max_y1, max_y2])
    latest_time = None if max_val == 0 else max_val
    if earliest_time == None or latest_time == None: return # No signal traces to plot

    evt_start_time = 0 if evt.start_time == None else evt.start_time
    if calib_runstate["trig_start_time"] == None: trig_left_time = earliest_time - evt_start_time - 50
    else: trig_left_time = calib_runstate["trig_start_time"]
    if calib_runstate["trig_end_time"] == None: trig_right_time = latest_time - evt_start_time + 50
    else: trig_right_time = calib_runstate["trig_end_time"]

    plot_signal_xy(ax, evt.mcp_list-evt_start_time, trig_left_time, trig_right_time, signal_colors['MCP'], y_offset=4, hi_val=0.75, signal_width=3)
    plot_signal_xy(ax, evt.x1_list-evt_start_time,  trig_left_time, trig_right_time, signal_colors['X1'],  y_offset=3, hi_val=0.75, signal_width=3)
    plot_signal_xy(ax, evt.x2_list-evt_start_time,  trig_left_time, trig_right_time, signal_colors['X2'],  y_offset=2, hi_val=0.75, signal_width=3)
    plot_signal_xy(ax, evt.y1_list-evt_start_time,  trig_left_time, trig_right_time, signal_colors['Y1'],  y_offset=1, hi_val=0.75, signal_width=3)
    plot_signal_xy(ax, evt.y2_list-evt_start_time,  trig_left_time, trig_right_time, signal_colors['Y2'],  y_offset=0, hi_val=0.75, signal_width=3)
    
    ax.set_xlim((trig_left_time,trig_right_time))
    ax.set_yticks(ticks=[0.375, 1.375, 2.375, 3.375, 4.375], labels=[r"$Y_2$",r"$Y_1$",r"$X_2$",r"$X_1$","MCP"])
    trig_fig_agg.draw()

def plot_sumxsumy():
    sumx_ax.cla()
    sumx_ax.grid()
    sumy_ax.cla()
    sumy_ax.grid()

    sumx_ax.set_title("Sum X Signals")
    sumy_ax.set_title("Sum Y Signals")
    sumx_ax.set_xlabel("Time (ns)")
    sumy_ax.set_xlabel("Time (ns)")

    def gaussian(x_list, amp, mu, sigma):
        return amp * np.exp(-0.5 * np.power(x_list-mu, 2) / sigma**2)

    # First, plot the histograms
    sumx_list, sumy_list = piDataFile.getSumXSumY()
    if len(sumx_list) > 1:
        if calib_runstate["sumx_min_plot_time"] == None: sumx_start_time = np.amin(sumx_list) - 0.1
        else: sumx_start_time = calib_runstate["sumx_min_plot_time"] - 0.1
        if calib_runstate["sumx_max_plot_time"] == None: sumx_end_time = np.amax(sumx_list) + 0.1
        else: sumx_end_time = calib_runstate["sumx_max_plot_time"] + 0.1

        try:
            bins = np.arange(sumx_start_time, sumx_end_time, 0.025)
            heights, bins = np.histogram(sumx_list, bins=bins)
            plot_x, plot_y = get_hist_plot_points(heights, bins)
            sumx_ax.plot(plot_x, plot_y, '-', color='cornflowerblue')
            # Next, plot the gaussian fits
            if np.sum(heights) > 5:
                avg_bins = (bins[1:] + bins[:-1])/2
                errs = np.sqrt(heights)
                errs[errs == 0] = 1
                params = gaussianFitter.fit1DGaussian(avg_bins, heights, errs)
                plt_x = np.linspace(sumx_start_time, sumx_end_time, 200)
                plt_y = gaussian(plt_x, params["amp"], params["mean"], params["sigma"])
                sumx_ax.plot(plt_x, plt_y, '-r')
                window['sumx_avg_val'].update(value=np.round(params["mean"],3))
                window['sumx_sig_val'].update(value=np.round(params["sigma"],3))
        except: pass

    if len(sumy_list) > 1:
        if calib_runstate["sumy_min_plot_time"] == None: sumy_start_time = np.amin(sumy_list) - 0.1
        else: sumy_start_time = calib_runstate["sumy_min_plot_time"] - 0.1
        if calib_runstate["sumy_max_plot_time"] == None: sumy_end_time = np.amax(sumy_list) + 0.1
        else: sumy_end_time = calib_runstate["sumy_max_plot_time"] + 0.1

        try:
            bins = np.arange(sumy_start_time, sumy_end_time, 0.025)
            heights, bins = np.histogram(sumy_list, bins=bins)
            plot_x, plot_y = get_hist_plot_points(heights, bins)
            sumy_ax.plot(plot_x, plot_y, '-', color='cornflowerblue')
            # Next, plot the gaussian fits
            if np.sum(heights) > 5:
                avg_bins = (bins[1:] + bins[:-1])/2
                errs = np.sqrt(heights)
                errs[errs == 0] = 1
                params = gaussianFitter.fit1DGaussian(avg_bins, heights, errs)
                plt_x = np.linspace(sumy_start_time, sumy_end_time, 200)
                plt_y = gaussian(plt_x, params["amp"], params["mean"], params["sigma"])
                sumy_ax.plot(plt_x, plt_y, '-r')
                window['sumy_avg_val'].update(value=round(params["mean"],3))
                window['sumy_sig_val'].update(value=round(params["sigma"],3))
        except: pass

    sumx_fig_agg.draw()
    sumy_fig_agg.draw()

def plot_signal_stats():
    miss_ax.cla()
    miss_ax.yaxis.grid()
    cnt_ax.cla()
    cnt_ax.yaxis.grid()

    missing_x1 = piDataFile.getMissingX1Count()
    missing_x2 = piDataFile.getMissingX2Count()
    missing_y1 = piDataFile.getMissingY1Count()
    missing_y2 = piDataFile.getMissingY2Count()
    x1_cnt, x2_cnt, y1_cnt, y2_cnt, mcp_cnt = piDataFile.getSignalCounts()

    cnt_bar = cnt_ax.bar([1,2,3,4,5], [mcp_cnt, x1_cnt, x2_cnt, y1_cnt, y2_cnt], 
        width=0.75, 
        tick_label=["MCP",r"$X_1$",r"$X_2$",r"$Y_1$",r"$Y_2$"],
        color=["peru","lightslategray","gold","orange","indianred"])
    cnt_ax.bar_label(cnt_bar)
    cnt_ax.set_title("Signal Count")
    cnt_ax.set_ylabel("Count")
    cnt_fig_agg.draw()

    miss_bar = miss_ax.bar( [1,2,3,4], [missing_x1, missing_x2, missing_y1, missing_y2], 
        width=0.75, 
        tick_label=[r"$X_1$",r"$X_2$",r"$Y_1$",r"$Y_2$"],
        color=["lightslategray","gold","orange","indianred"])
    miss_ax.bar_label(miss_bar)
    miss_ax.set_title("Signal Missing from Reconstruction")
    miss_ax.set_ylabel("Count")
    miss_fig_agg.draw()

###############################################################################
# Wrapper functions for multi-threading
###############################################################################

def update_datafile(work_id, window, refresh_mm8_scans=False, event_key='datafile_update_done'):
    try:
        newfileline = piDataFile.updateDataFile(start_line=main_runstate['lastreadline'])
        main_runstate['lastreadline'] = newfileline
        # if refresh_mm8_scans:
        #     window['adv1_scan1_listbox'].update(set_to_index=[])
        #     window['adv1_scan2_listbox'].update(set_to_index=[])
        #     window['adv2_scan1_listbox'].update(set_to_index=[])
        #     window['adv2_scan2_listbox'].update(set_to_index=[])
        #     window['adv1_scan1_listbox'].update(values=[])
        #     window['adv1_scan2_listbox'].update(values=[])
        #     window['adv2_scan1_listbox'].update(values=[])
        #     window['adv2_scan2_listbox'].update(values=[])
        #     window['adv1_scan1_listbox'].update(values=piDataFile.getMM8Scan1Vals())
        #     window['adv1_scan2_listbox'].update(values=piDataFile.getMM8Scan2Vals())
        #     window['adv2_scan1_listbox'].update(values=piDataFile.getMM8Scan1Vals())
        #     window['adv2_scan2_listbox'].update(values=piDataFile.getMM8Scan2Vals())
    except: sg.Popup("System Error", "The following error occured updating the datafile:\n\n"+str(traceback.format_exc()))
    window.write_event_value(event_key, work_id)

def update_mcp_data(work_id, window, fig, ax, ang_fig, ang_ax, positions, rejects, runstate_dict, runinfo_dict, event_key='mcp_update_done'):
    try: plot_MCP_data(fig, ax, ang_fig, ang_ax, positions, rejects, runstate_dict, runinfo_dict)
    except: sg.Popup("System Error", "The following error occured updating the MCP plot:\n\n"+str(traceback.format_exc()))
    window.write_event_value(event_key, work_id)

def update_tof_spectrum(work_id, window, runstate_dict, event_key='tof_update_done'):
    tof_min, tof_max = limits['tofmin'], limits['tofmax']
    if not runstate_dict['tof_filter']: tof_min, tof_max = limits['tofminview'], limits['tofmaxview']

    try: plot_tof_spectrum(tof_fig, tof_ax, tof_ax2, tof_min, tof_max, limits['tofminview'], limits['tofmaxview'])  
    except: sg.Popup("System Error", "The following error occured updating the TOF plot:\n\n"+str(traceback.format_exc()))
    window.write_event_value(event_key, work_id)

def update_rate(work_id, window, event_key='rate_update_done'):
    try: plot_count_rates(rate_fig, rate_allax)
    except: sg.Popup("System Error", "The following error occured updating the count rate plot:\n\n"+str(traceback.format_exc()))
    window.write_event_value(event_key, work_id)

###########################################################################
# Deal with window scaling for different computers
###########################################################################
# Find the number of the original screen where the GUI was designed
my_resolution = 1.3333333333333 # call get_resolution() method
my_width, my_height = 1440, 900 # call sg.Window.get_screen_size()
# Get the scaling and size numbers for new screen
curr_resolution = get_resolution()
width, height = sg.Window.get_screen_size()
window_scaling = min((width/my_width),(height/my_height))
# Determine the matplotlib figure DPI scaling
PLOT_DPI = (90*curr_resolution/my_resolution)
plt.rcParams['figure.dpi'] = PLOT_DPI # Change the default dpi for all plots
sg.set_options(scaling=PLOT_DPI/72)
# Determine the font scaling
user_font_scale = user_font_scale*(window_scaling*curr_resolution/my_resolution)
# Do a baseline figure scaling to get things reasonable
user_fig_scale *= 0.95
# lastly, determine the final size of the GUI
window_width  = window_scaling*my_width*0.80
window_height = window_scaling*my_height*0.80

###########################################################################
# Create the tab pages of the GUI
###########################################################################
main_layout, plot_dict = create_main_page(piSystem, window_width, window_height, limits, main_runstate, PLOT_DPI, user_font_scale, user_fig_scale)
raw_fig, raw_ax = plot_dict["raw_fig"], plot_dict["raw_ax"]
rate_fig, rate_allax = plot_dict["rate_fig"], plot_dict["rate_allax"]
tof_fig, tof_ax, tof_ax2 = plot_dict["tof_fig"], plot_dict["tof_ax"], plot_dict["tof_ax2"]
ang_fig, ang_ax = plot_dict["ang_fig"], plot_dict["ang_ax"]
# adv_layout, adv_plot_dict = create_adv_page(piSystem, piDataFile.getMM8Scan1Vals(), piDataFile.getMM8Scan2Vals(), \
#     window_width, window_height, user_fig_scale, user_font_scale, PLOT_DPI)
# adv1_fig, adv1_ax = adv_plot_dict["adv1_fig"], adv_plot_dict["adv1_ax"]
# adv2_fig, adv2_ax = adv_plot_dict["adv2_fig"], adv_plot_dict["adv2_ax"]
# adv1ang_fig, adv1ang_ax = adv_plot_dict["adv1ang_fig"], adv_plot_dict["adv1ang_ax"]
# adv2ang_fig, adv2ang_ax = adv_plot_dict["adv2ang_fig"], adv_plot_dict["adv2ang_ax"]
calib_layout, calib_plot_dict = create_calib_page(window_width, window_height, user_fig_scale, user_font_scale, PLOT_DPI)
sumx_fig, sumx_ax = calib_plot_dict["sumx_fig"], calib_plot_dict["sumx_ax"]
sumy_fig, sumy_ax = calib_plot_dict["sumy_fig"], calib_plot_dict["sumy_ax"]
trig_fig, trig_ax = calib_plot_dict["trig_fig"], calib_plot_dict["trig_ax"]
miss_fig, miss_ax = calib_plot_dict["miss_fig"], calib_plot_dict["miss_ax"]
cnt_fig, cnt_ax = calib_plot_dict["cnt_fig"], calib_plot_dict["cnt_ax"]

tab_layout = [[
    sg.TabGroup([[
        sg.Tab('Visualization', main_layout, key='main_tab'), 
        #sg.Tab('Advanced', adv_layout, key='adv_tab'),
        sg.Tab('Calibration', calib_layout, key='calib_tab'),
    ]], font=("Helvetica",int(14*user_font_scale),"bold"), expand_x=True, expand_y=True, enable_events=True, key='tab_group') 
]]

# Actually create the GUI window now
icon_path = os.path.join(CWD, 'piicr_vis.ico')
sg.theme('DarkBlue')
window = sg.Window('PI-ICR Active Data Visualization', tab_layout, icon=icon_path, finalize=True, resizable=True)
# draw the initial plots in the main window
raw_fig_agg  = draw_figure(window['raw_canvas'].TKCanvas, raw_fig)
tof_fig_agg  = draw_figure(window['tof_canvas'].TKCanvas, tof_fig)
ang_fig_agg  = draw_figure(window['ang_canvas'].TKCanvas, ang_fig)
rate_fig_agg = draw_figure(window['rate_canvas'].TKCanvas, rate_fig)
# draw the initial plots in the calibration windows
sumx_fig_agg = draw_figure(window['sumx_canvas'].TKCanvas, sumx_fig)
sumy_fig_agg = draw_figure(window['sumy_canvas'].TKCanvas, sumy_fig)
trig_fig_agg = draw_figure_w_toolbar(window['trig_canvas'].TKCanvas, trig_fig, window['osc_controls_cv'].TKCanvas)
# draw the initial plots in the advanced window
# adv1_fig_agg    = draw_figure(window['adv1_canvas'].TKCanvas, adv1_fig)
# adv2_fig_agg    = draw_figure(window['adv2_canvas'].TKCanvas, adv2_fig)
# adv1ang_fig_agg = draw_figure(window['adv1ang_canvas'].TKCanvas, adv1ang_fig)
# adv2ang_fig_agg = draw_figure(window['adv2ang_canvas'].TKCanvas, adv2ang_fig)
miss_fig_agg    = draw_figure(window['miss_canvas'].TKCanvas, miss_fig)
cnt_fig_agg     = draw_figure(window['cnt_canvas'].TKCanvas, cnt_fig)

# Create some flags to reduce unnecessary threads
processing_datafile = False
processing_mcpplot  = False
processing_tofplot  = False
processing_rate     = False
processing_adv1     = False
processing_adv2     = False
# Additional flags
reload_data_requested = True # flag if the user requested to load a new datafile
update_mm8_scans = True # flag for updating the mm8 scan info

# enter the loop
is_waiting = True
work_id = 0
while is_waiting:
    ###############################################################################
    # Check if any of the long operations were finished and ready to restart
    ###############################################################################
    event, values = window.read(timeout=10)

    if event in (sg.WIN_CLOSED, '_EXIT_', 'Exit') or values['tab_group'] == None:
        is_waiting = False
        continue
    elif event == "datafile_update_done": 
        processing_datafile = False
    elif event == "mcp_update_done":
        raw_fig_agg.draw()
        ang_fig_agg.draw()
        processing_mcpplot = False
        window.refresh()
    elif event == "tof_update_done":
        tof_fig_agg.draw()
        processing_tofplot = False
        window.refresh()
    elif event == "rate_update_done":
        rate_fig_agg.draw()
        processing_rate = False
        window.refresh()
    # elif event == "adv1_update_done":
    #     adv1_fig_agg.draw()
    #     adv1ang_fig_agg.draw()
    #     processing_adv1 = False
    #     window.refresh()
    # elif event == "adv2_update_done":
    #     adv2_fig_agg.draw()
    #     adv2ang_fig_agg.draw()
    #     processing_adv2 = False
    #     window.refresh()

    ###############################################################################
    # Update the Datafile Object
    ###############################################################################
    # open a thread to update the datafile
    if not processing_datafile:
        if reload_data_requested:
            main_runstate['lastreadline'] = 0
            main_runstate['app_update_time'] = 0
            piDataFile.filepath, piDataFile.filename = os.path.split(main_runstate['pi_filename'])
            update_mm8_scans = True
            reload_data_requested = False
        thread_id = threading.Thread(
            target=update_datafile,
            args=(work_id, window, update_mm8_scans),
            daemon=True)
        thread_id.start()
        work_id += 1
        update_mm8_scans = False
        processing_datafile = True

    ###############################################################################
    # Handle the updates on the MAIN TAB of the GUI
    ###############################################################################
    if values['tab_group'] == 'main_tab':
        window['thresh_slider_val'].update(round(main_runstate['cluster_thresh'],2))
        window['smooth_slider_val'].update(round(main_runstate['cluster_smooth'],2))
        bkgnd_color = 'indianred' if main_runstate['hide_rejected_points'] else None
        window['hide_rejected_points'].update(background_color=bkgnd_color)

        # Get any positions that meet the thresholds specified by the user
        positions, rejects = piAnalysisFile.getGatedPositions(
            limits, 
            applyTOF       = main_runstate['tof_filter'], 
            applyZ         = main_runstate['z_filter'], 
            applyRadial    = main_runstate['radial_filter'], 
            applyAngular   = main_runstate['angular_filter'], 
            applyEvent     = main_runstate['event_filter'],
            allowRecovered = main_runstate['allow_recovered_pos'],
            filterCenter   = [main_runstate['centerx'],main_runstate['centery']], 
            returnRejected = True)
        
        # Update the run info window
        try:
            main_information['trigger_events'] = piDataFile.CurrentEventID
            main_information['reconstr_hits'] = len(piDataFile.ReconstructedEvents)
            if values['allow_recovered_check']: main_information['recovered_hits'] = piDataFile.getRecoveredCount()
            else: main_information['recovered_hits'] = None
            main_information['filtered_hits'] = len(positions)
            line0 = "MM8 Start Time: "+strftime('%Y-%m-%d %H:%M:%S', localtime(piDataFile.getStartTime()))+"\n"
            line1 = "{0:<26} {1:>10}".format("Total trigger events:", main_information['trigger_events'])
            line2 = "{0:<26} {1:>10}".format("Total reconstructed hits:", main_information['reconstr_hits'])
            line3 = "{0:<26} {1:>10}".format(" → recovered hits:", str(main_information['recovered_hits']))
            line4 = "{0:<26} {1:>10}".format("Number of filtered hits:", main_information['filtered_hits'])
            line5 = "{0:<26} {1:>10}".format("Number of clustered hits:", str(main_information['clustered_hits']))
            line6 = main_information['fit_info']
            window['run_info'].update(value=line0+"\n"+line1+"\n"+line2+"\n"+line3+"\n"+line4+"\n"+line5+"\n"+line6)
            read_main_settings(values)
        except: 
            if not is_waiting: sg.Popup("System Error", "The following error occured updating the main tab:\n\n"+str(traceback.format_exc()))
            else: pass

        calc_spot_alignment(window, values)

        # open a thread to update the mcp plot
        if piDataFile.firstLoadFinished and not processing_mcpplot:
            processing_mcpplot = True
            thread_id = threading.Thread(
                target=update_mcp_data,
                args=(work_id, window, raw_fig, raw_ax , ang_fig, ang_ax, \
                      positions, rejects, main_runstate, main_information),
                daemon=True)
            thread_id.start()
            work_id += 1
        # Check if new data has come in. If so, update the variable indicating such
        if piDataFile.firstLoadFinished and not processing_rate:
            processing_rate = True
            thread_id = threading.Thread(
                target=update_rate,
                args=(work_id, window,),
                daemon=True)
            thread_id.start()
            work_id += 1
        # open a thread to update the tof spectrum
        if piDataFile.firstLoadFinished and not processing_tofplot:
            processing_tofplot = True
            thread_id = threading.Thread(
                target=update_tof_spectrum,
                args=(work_id, window, main_runstate),
                daemon=True)
            thread_id.start()
            work_id += 1
   
        if event == "load_latest_button":
            try:
                piDataDirTemp = values['load_dir']
                piFilenameTemp = piSystem.getLatestMCPDataFile(dir2search=piDataDirTemp)
                if piFilenameTemp == "None": 
                    window['load_filepath'].update("***No Available Files***")
                    window['mm8_datafile'].update("None")
                elif (not piDataDirTemp == main_runstate['pi_data_directory']) or (not piFilenameTemp == main_runstate['pi_filename']): 
                    # only update if the file request is different from the currently loaded file
                    main_runstate['pi_data_directory'] = piDataDirTemp
                    main_runstate['pi_filename'] = piFilenameTemp
                    window['load_filepath'].update(os.path.split(piFilenameTemp)[1])
                    main_runstate['mm8_filename'] = piSystem.findMM8File(os.path.join(piDataDirTemp,piFilenameTemp))
                    window['mm8_datafile'].update(os.path.split(main_runstate['mm8_filename'])[1])
                    reload_data_requested = True
            except: sg.Popup("System Error", "The following error occured trying to load the latest datafile:\n\n"+str(traceback.format_exc()))
        elif event == 'load_filepath':
            try:
                piDataDirTemp, piFilenameTemp = os.path.split(values['load_file_browser'])
                main_runstate['pi_data_directory'] = piDataDirTemp
                main_runstate['pi_filename'] = values['load_file_browser']
                main_runstate['mm8_filename'] = piSystem.findMM8File(os.path.join(piDataDirTemp,piFilenameTemp))
                window['mm8_datafile'].update(os.path.split(main_runstate['mm8_filename'])[1])
                window['load_dir'].update(piDataDirTemp)
                window['load_filepath'].update(piFilenameTemp)
                reload_data_requested = True
            except: sg.Popup("System Error", "The following error occured trying to load the specified datafile:\n\n"+str(traceback.format_exc()))
        elif event == "save_all_button":
            piAnalysisFile.saveReconstructedData() 
        elif event == "save_filtered_button":
            piAnalysisFile.saveFilteredData(
                limits, 
                applyTOF       = main_runstate['tof_filter'], 
                applyZ         = main_runstate['z_filter'], 
                applyRadial    = main_runstate['radial_filter'], 
                applyAngular   = main_runstate['angular_filter'], 
                applyEvent     = main_runstate['event_filter'],
                allowRecovered = main_runstate['allow_recovered_pos'],
                filterCenter   = [main_runstate['centerx'],main_runstate['centery']] )    
    ###############################################################################
    # Handle the updates on the ADVANCED TAB of the GUI
    ###############################################################################
    # if values['tab_group'] == 'adv_tab':
    #     # Update the run info window
    #     try:
    #         line1 = "{0:<14} {1:>6}".format("# trigs:", adv1_information['trigger_events'])
    #         line2 = "{0:<14} {1:>6}".format("# recons:", adv1_information['reconstr_hits'])
    #         line3 = "{0:<14} {1:>6}".format(" → recov.:", str(adv1_information['recovered_hits']))
    #         line4 = "{0:<14} {1:>6}".format("# filter:", adv1_information['filtered_hits'])
    #         line5 = "{0:<14} {1:>6}".format("# cluster:", str(adv1_information['clustered_hits']))
    #         line6 = adv1_information['fit_info_terse']
    #         window['adv1_info'].update(value=line1+"\n"+line2+"\n"+line3+"\n"+line4+"\n"+line5+"\n"+line6)
    #     except: sg.Popup("System Error", "The following error occured updating the adv1 info:\n\n"+str(traceback.format_exc()))
    #     # Update the run info window
    #     try:
    #         line1 = "{0:<14} {1:>6}".format("# trigs:", adv2_information['trigger_events'])
    #         line2 = "{0:<14} {1:>6}".format("# recons:", adv2_information['reconstr_hits'])
    #         line3 = "{0:<14} {1:>6}".format(" → recov.:", str(adv2_information['recovered_hits']))
    #         line4 = "{0:<14} {1:>6}".format("# filter:", adv2_information['filtered_hits'])
    #         line5 = "{0:<14} {1:>6}".format("# cluster:", str(adv2_information['clustered_hits']))
    #         line6 = adv2_information['fit_info_terse']
    #         window['adv2_info'].update(value=line1+"\n"+line2+"\n"+line3+"\n"+line4+"\n"+line5+"\n"+line6)
    #     except: sg.Popup("System Error", "The following error occured updating the adv2 info:\n\n"+str(traceback.format_exc()))

    #     # Update the data scan selection information
    #     if event == 'adv1_scan1_selectall':
    #         window['adv1_scan1_listbox'].update(set_to_index=list(range(piDataFile.getMM8Scan1Len())))
    #     elif event == 'adv1_scan2_selectall':
    #         window['adv1_scan2_listbox'].update(set_to_index=list(range(piDataFile.getMM8Scan2Len())))
    #     elif event == 'adv2_scan1_selectall':
    #         window['adv2_scan1_listbox'].update(set_to_index=list(range(piDataFile.getMM8Scan1Len())))
    #     elif event == 'adv2_scan2_selectall':
    #        window['adv2_scan2_listbox'].update(set_to_index=list(range(piDataFile.getMM8Scan2Len())))
        
    #     if values['adv2_scan1_oppadv1_check']:
    #         # the user wants the second plot to have scan1 vals the opp of first
    #         adv1_scan1_vals = values['adv1_scan1_listbox']
    #         scan1_vals = piDataFile.getMM8Scan1Vals()
    #         selected_idx = [ kk for kk in range(piDataFile.getMM8Scan1Len()) if scan1_vals[kk] in adv1_scan1_vals ]
    #         opposite_idx = [ kk for kk in range(piDataFile.getMM8Scan1Len()) if not kk in selected_idx ]
    #         window['adv2_scan1_listbox'].update(set_to_index=opposite_idx)
    #     if values['adv2_scan2_oppadv1_check']:
    #         # the user wants the second plot to have scan2 vals the opp of first
    #         adv1_scan2_vals = values['adv1_scan2_listbox']
    #         scan2_vals = piDataFile.getMM8Scan2Vals()
    #         selected_idx = [ kk for kk in range(piDataFile.getMM8Scan2Len()) if scan2_vals[kk] in adv1_scan2_vals ]
    #         opposite_idx = [ kk for kk in range(piDataFile.getMM8Scan2Len()) if not kk in selected_idx ]
    #         window['adv2_scan2_listbox'].update(set_to_index=opposite_idx)

    #     if event == 'adv1_clearselection':
    #         window['adv1_scan1_listbox'].update(set_to_index=[])
    #         window['adv1_scan2_listbox'].update(set_to_index=[])
    #     elif event == 'adv2_clearselection':
    #         window['adv2_scan1_listbox'].update(set_to_index=[])
    #         window['adv2_scan2_listbox'].update(set_to_index=[])

    #     # update the fitting settigns
    #     if values['adv1_fitting_check']: 
    #         if not str(adv1_runstate['fitting_type']).lower() == values['adv1_fitting_combo'].lower():
    #             adv1_runstate['last_fitting_count'] = 0
    #         adv1_runstate['fitting_type'] = values['adv1_fitting_combo'].lower()
    #     else: adv1_runstate['fitting_type'] = None
    #     if values['adv2_fitting_check']: 
    #         if not str(adv2_runstate['fitting_type']).lower() == values['adv2_fitting_combo'].lower():
    #             adv2_runstate['last_fitting_count'] = 0
    #         adv2_runstate['fitting_type'] = values['adv2_fitting_combo'].lower()
    #     else: adv2_runstate['fitting_type'] = None

    #     # Update the cluster settings
    #     if values['adv1_cluster_check']: adv1_runstate['cluster_type'] = values['adv1_cluster_combo'].lower()
    #     else: adv1_runstate['cluster_type'] = None
    #     if values['adv2_cluster_check']: adv2_runstate['cluster_type'] = values['adv2_cluster_combo'].lower()
    #     else: adv2_runstate['cluster_type'] = None
    #     adv1_runstate['cluster_thresh'] = main_runstate['cluster_thresh']
    #     adv1_runstate['cluster_smooth'] = main_runstate['cluster_smooth']
    #     adv2_runstate['cluster_thresh'] = main_runstate['cluster_thresh']
    #     adv2_runstate['cluster_smooth'] = main_runstate['cluster_smooth']

    #     # open a thread to update the mcp plot
    #     if piDataFile.firstLoadFinished and not processing_adv1:
    #         if values['adv1_dbl_check']: dblpattern_setting = 2 if values['adv1_dbl_combo'] == 'Second' else 1
    #         else: dblpattern_setting = None
    #         selected_scan1_vals, selected_scan2_vals = values['adv1_scan1_listbox'], values['adv1_scan2_listbox']
    #         scan1_vals, scan2_vals = piDataFile.getMM8Scan1Vals(), piDataFile.getMM8Scan2Vals()

    #         scan1_idx = [kk for kk in range(piDataFile.getMM8Scan1Len()) if scan1_vals[kk] in selected_scan1_vals]
    #         scan2_idx = [kk for kk in range(piDataFile.getMM8Scan2Len()) if scan2_vals[kk] in selected_scan2_vals]
    #         filterCenter = [main_runstate['centerx'],main_runstate['centery']]
    #         positions, rejects = piAnalysisFile.getGatedPositionsWithScan( \
    #             limits, applyTOF=True, applyZ=True,
    #             applyRadial=main_runstate['radial_filter'], 
    #             applyAngular=main_runstate['angular_filter'], 
    #             applyEvent=main_runstate['event_filter'],
    #             allowRecovered=main_runstate['allow_recovered_pos'],
    #             filterCenter=[main_runstate['centerx'],main_runstate['centery']], 
    #             scan1idx=scan1_idx,
    #             scan2idx=scan2_idx,
    #             doublePattern=dblpattern_setting,
    #             returnRejected=True)
    #         # Update the run information    
    #         if values['adv1_dbl_check']: adv1_information['trigger_events'] = int(piDataFile.CurrentEventID/2)
    #         else: adv1_information['trigger_events'] = piDataFile.CurrentEventID
    #         adv1_information['reconstr_hits'] = len(positions) + len(rejects)
    #         if values['allow_recovered_check'] and len(positions) > 0: 
    #             trig_ids = piDataFile.getTriggerIDs(unique=False)
    #             if not dblpattern_setting == None: indices = np.where(np.mod(trig_ids,2) == (dblpattern_setting-1))
    #             else: indices = None
    #             adv1_information['recovered_hits'] = piDataFile.getRecoveredCount(indices=indices)
    #         else: adv1_information['recovered_hits'] = None
            
    #         adv1_information['filtered_hits'] = len(positions)
    #         # start the thread to plot
    #         processing_adv1 = True
    #         thread_id = threading.Thread(
    #             target=update_mcp_data,
    #             args=(work_id, window, adv1_fig, adv1_ax, \
    #                   adv1ang_fig, adv1ang_ax, positions, rejects, \
    #                   adv1_runstate, adv1_information, 'adv1_update_done'),
    #             daemon=True)
    #         thread_id.start()
    #         work_id += 1

    #     # open a thread to update the mcp plot
    #     if piDataFile.firstLoadFinished and not processing_adv2:
    #         if values['adv2_dbl_check']: dblpattern_setting = 2 if values['adv2_dbl_combo'] == 'Second' else 1
    #         else: dblpattern_setting = None
    #         selected_scan1_vals, selected_scan2_vals = values['adv2_scan1_listbox'], values['adv2_scan2_listbox']
    #         scan1_vals, scan2_vals = piDataFile.getMM8Scan1Vals(), piDataFile.getMM8Scan2Vals()
    #         scan1_idx = [kk for kk in range(piDataFile.getMM8Scan1Len()) if scan1_vals[kk] in selected_scan1_vals]
    #         scan2_idx = [kk for kk in range(piDataFile.getMM8Scan2Len()) if scan2_vals[kk] in selected_scan2_vals]
    #         filterCenter = [main_runstate['centerx'],main_runstate['centery']]
    #         positions, rejects = piAnalysisFile.getGatedPositionsWithScan( \
    #             limits, applyTOF=True, applyZ=True,
    #             applyRadial=main_runstate['radial_filter'], 
    #             applyAngular=main_runstate['angular_filter'], 
    #             applyEvent=main_runstate['event_filter'],
    #             allowRecovered=main_runstate['allow_recovered_pos'],
    #             filterCenter=[main_runstate['centerx'],main_runstate['centery']], 
    #             scan1idx=scan1_idx,
    #             scan2idx=scan2_idx,
    #             doublePattern=dblpattern_setting,
    #             returnRejected=True)
    #         # update the run information
    #         if values['adv2_dbl_check']: adv2_information['trigger_events'] = int(piDataFile.CurrentEventID/2)
    #         else: adv2_information['trigger_events'] = piDataFile.CurrentEventID
    #         adv2_information['reconstr_hits'] = len(positions) + len(rejects)
    #         if values['allow_recovered_check'] and len(positions) > 0: 
    #             trig_ids = piDataFile.getTriggerIDs(unique=False)
    #             if not dblpattern_setting == None: indices = np.where(np.mod(trig_ids,2) == (dblpattern_setting-1))
    #             else: indices = None
    #             adv2_information['recovered_hits'] = piDataFile.getRecoveredCount(indices=indices)
    #         else: adv2_information['recovered_hits'] = None
    #         adv2_information['filtered_hits'] = len(positions)
    #         # start the thread to plot
    #         processing_adv2 = True
    #         thread_id = threading.Thread(
    #             target=update_mcp_data,
    #             args=(work_id, window, adv2_fig, adv2_ax, \
    #                   adv2ang_fig, adv2ang_ax, positions, rejects, \
    #                   adv2_runstate, adv2_information, 'adv2_update_done'),
    #             daemon=True)
    #         thread_id.start()
    #         work_id += 1

    #     window.refresh()

    ###############################################################################
    # Handle the updates on the CALIBRATION TAB of the GUI
    ###############################################################################
    if values['tab_group'] == 'calib_tab':
        num_events = len(piDataFile.getAllEvents())
        update_osc_plot = values['osc_currevt_check'] # bool to track if we should update the plot
        if values['osc_currevt_check']:
            calib_runstate["oscil_event_num"] = num_events-1 # saving the INDEX of the event in the event list
            window['osc_displayedevt'].update("*Latest*")
        else:
            try: 
                temp = int(values['osc_displayedevt'])
                if temp != calib_runstate["oscil_event_num"]:  
                    calib_runstate["oscil_event_num"] = temp
                    update_osc_plot = True
            except: calib_runstate["oscil_event_num"] = num_events
        
        if event == 'osc_left_button':
            update_osc_plot = True
            window['osc_currevt_check'].update(False)
            if values["osc_hideempty_check"]: # only show events with at least one signal present
                is_event_empty = piDataFile.getEventIsEmpty()
                next_left = np.where(is_event_empty[:calib_runstate["oscil_event_num"]])[0]
                if len(next_left) == 0: calib_runstate["oscil_event_num"] = calib_runstate["oscil_event_num"]
                else: calib_runstate["oscil_event_num"] = next_left[-1]
            else: calib_runstate["oscil_event_num"] = calib_runstate["oscil_event_num"]-1 if calib_runstate["oscil_event_num"] > 1 else 1
            window['osc_displayedevt'].update(calib_runstate["oscil_event_num"])
        elif event == 'osc_right_button':
            update_osc_plot = True
            window['osc_currevt_check'].update(False)
            if values["osc_hideempty_check"]: # only show events with at least one signal present
                is_event_empty = piDataFile.getEventIsEmpty()
                next_right = np.where(is_event_empty[calib_runstate["oscil_event_num"]+1:])[0] + calib_runstate["oscil_event_num"]+1
                if len(next_right) == 0: calib_runstate["oscil_event_num"] = calib_runstate["oscil_event_num"]
                else: calib_runstate["oscil_event_num"] = next_right[0]
            else: calib_runstate["oscil_event_num"] = calib_runstate["oscil_event_num"]+1 if calib_runstate["oscil_event_num"] < num_events else num_events
            window['osc_displayedevt'].update(calib_runstate["oscil_event_num"])

        # Update the settings set by the user 
        try: calib_runstate["trig_start_time"] = float(values["trig_start_time"])
        except: calib_runstate["trig_start_time"] = None
        try: calib_runstate["trig_end_time"] = float(values["trig_end_time"])
        except: calib_runstate["trig_end_time"] = None
        try: calib_runstate["sumx_min_plot_time"] = float(values["sumxy_min_time"])
        except: calib_runstate["sumx_min_plot_time"] = None
        try: calib_runstate["sumx_max_plot_time"] = float(values["sumxy_max_time"])
        except: calib_runstate["sumx_max_plot_time"] = None
        try: calib_runstate["sumy_min_plot_time"] = float(values["sumxy_min_time"])
        except: calib_runstate["sumy_min_plot_time"] = None
        try: calib_runstate["sumy_max_plot_time"] = float(values["sumxy_max_time"])
        except: calib_runstate["sumy_max_plot_time"] = None
        #--------------------------------------------------------------------------
        # Update the Sum X and Sum Y histograms and the Oscilloscope view
        #--------------------------------------------------------------------------
        if piDataFile.firstLoadFinished:
            try:
                plot_signal_stats()
                plot_sumxsumy()
                if update_osc_plot: plot_triggers(trig_fig, trig_ax)
                window.refresh()
            except: sg.Popup("System Error", "The following error occured updating the calibration plots:\n\n"+str(traceback.format_exc()))

window.close()
sys.exit()