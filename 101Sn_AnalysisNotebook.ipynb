{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# $^{101}\\text{Sn}^{2+}$ Analysis Workflow\n",
    "\n",
    "This notebook is an adaptation of the traditional analysis notebook. The most important difference being that the this accounts for two reference measurements, $^{50}\\text{Ti}$ and $^{51}\\text{V}$, to determine the cyclotron frequency for $^{101}\\text{Sn}^{2+}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the necessary imports for the analysis code\n",
    "import numpy as np\n",
    "import matplotlib\n",
    "from copy import deepcopy\n",
    "import os\n",
    "\n",
    "from Modules.PIICR_Utilities import loadAllDataFiles, cart2pol, pol2cart, cart2Pol_withUnc, extract_configuration\n",
    "from Modules.FitSpot import SpotFitter\n",
    "from Modules.FitEllipse import EllipseFitter\n",
    "from Modules.ClusterPIICR import ClusterPIICR\n",
    "from Modules.AnalysisGUI_Utilities import launchFitGUI\n",
    "\n",
    "from Modules.CircleDataset import CircleDataset\n",
    "from Modules.MagDataset import MagDataset\n",
    "from Modules.StartDataset import StartDataset\n",
    "from Modules.TwoPhaseAccumDataset import TwoPhaseAccumDataset\n",
    "from Modules.MagDataset_2References import MagDataset2Refs\n",
    "\n",
    "# Extract the user settings from the ini file\n",
    "CWD = os.path.abspath(\"\") # for running from code\n",
    "ini_path = os.path.join(CWD, 'VisualizationGUI.ini')\n",
    "(tdcInfo, piSystem, loose_limits, piDataFile, piAnalysisFile, nbinsPosn, \n",
    "    cntrateHistory, user_fig_scale, user_font_scale) = extract_configuration(ini_path)\n",
    "\n",
    "# Define some objects for running the analysis\n",
    "piCluster = ClusterPIICR(piSystem.getRadiusMCP())\n",
    "spotFitter = SpotFitter()\n",
    "ellipseFitter = EllipseFitter()\n",
    "\n",
    "# These limits will be used during the fitting to specify additional restrictions on the dataset. \n",
    "limits = deepcopy(loose_limits)\n",
    "tofmin, tofmax = piSystem.getAbsTofLimits()\n",
    "settings = {\n",
    "    'tofwindowmin':tofmin, 'tofwindowmax':tofmax, # range of the visible tof sprectrum to plot\n",
    "    'toffilter':True, # Whether or not to use the tof filter\n",
    "    'zfilter':True, # Whether or not to use the count/shot filter\n",
    "    'radialfilter':False, # Whether or not to use the radial filter\n",
    "    'angularfilter':False, # Whether or not to use the angular filter\n",
    "    'allowRecovered':True, # Whether to allow positions recovered from incomplete timing info\n",
    "    'overrideFilterCenter':False, # Whether to allow user to choose center for spatial filters\n",
    "    'hideRejectedPoints':False, # Whether to hide the points rejected by filters, etc.\n",
    "    'filterCenterXY':[0,0], # The center for the filters, used only if overrideFilterCenter is True\n",
    "    'clusterthresh':0.15, # cluster threshold, (min of 0.01, max of 0.99)\n",
    "    'clustersmooth':1, # cluster smoothing, (min of 0.01, max of 5)\n",
    "    'clustertype':'spot', # 'spot' or 'ellipse' to determine how to cluster the data\n",
    "    'plotstyle':'image', # How to plot the position data, 'image', 'contour', or 'scatter'\n",
    "    'fitfunctionkey':'Cartesian Spot' # key for the default fit method, defined in dictionary below!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load in the Data Files to Analyize"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#---------------------------------------------------------------\n",
    "# Provide a path to the directrory containing the data files\n",
    "#---------------------------------------------------------------\n",
    "posDataDir = piSystem.getMcpDataDirectory() # Use default specified in ini (e.g. I:\\projects\\lebit/DATA/currentPI-ICRData)\n",
    "# posDataDir = r'I:\\\\projects\\\\lebit\\\\DATA\\\\dataBackupPI-ICR\\\\2023\\\\PI-ICR_Data\\\\230420_230728\\\\' # use files from a backup location\n",
    "\n",
    "#---------------------------------------------------------------\n",
    "# If you wish to provide your own frequency guesses, do so here\n",
    "#---------------------------------------------------------------\n",
    "initial_Fm_guess = 1873 # None: uses the frequencies from the MM8 file\n",
    "ti50_Fp_guess    = 2874055 # None: uses the frequencies from the MM8 file\n",
    "v51_Fp_guess     = 2817648 # None: uses the frequencies from the MM8 file\n",
    "sn101_Fp_guess   = None # None: uses the frequencies from the MM8 file\n",
    "\n",
    "#---------------------------------------------------------------------\n",
    "# Define all the files corresponding to the ellipse measurments\n",
    "#---------------------------------------------------------------\n",
    "centerFileNums = [387]\n",
    "\n",
    "#---------------------------------------------------------------------\n",
    "# Define all the files corresponding to the 50Ti reference measurements\n",
    "#   If there are no datafiles yet, use []\n",
    "#---------------------------------------------------------------\n",
    "ti50_startFileNums = [391, 397, 403, 409, 415]\n",
    "ti50_magFileNums   = [392, 398, 404, 410, 416]\n",
    "ti50_redFileNums   = [393, 399, 405, 411, 417]\n",
    "\n",
    "#---------------------------------------------------------------------\n",
    "# Define all the files corresponding to the 51V reference measurements\n",
    "#   If there are no datafiles yet, use []\n",
    "#---------------------------------------------------------------\n",
    "v51_startFileNums = [388, 394, 400, 406, 412, 418]\n",
    "v51_magFileNums   = [389, 395, 401, 407, 413, 419]\n",
    "v51_redFileNums   = [390, 396, 402, 408, 414, 420]\n",
    "\n",
    "#---------------------------------------------------------------------\n",
    "# Define all the files corresponding to the 101Sn2+ measurements\n",
    "#   If there are no datafiles yet, use []\n",
    "#---------------------------------------------------------------\n",
    "sn101_startFileNums = []\n",
    "sn101_redFileNums   = []"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#---------------------------------------------------------------\n",
    "# Read in all files corresponding to the ellipse measurments\n",
    "#---------------------------------------------------------------\n",
    "centerFiles = loadAllDataFiles(posDataDir, centerFileNums, piSystem, loose_limits)\n",
    "\n",
    "#---------------------------------------------------------------\n",
    "# Read in all files corresponding to the 50Ti reference measurements\n",
    "#---------------------------------------------------------------\n",
    "ti50_startFiles = loadAllDataFiles(posDataDir, ti50_startFileNums, piSystem, loose_limits)\n",
    "ti50_magFiles   = loadAllDataFiles(posDataDir, ti50_magFileNums,   piSystem, loose_limits)\n",
    "ti50_accumFiles = loadAllDataFiles(posDataDir, ti50_redFileNums,   piSystem, loose_limits)\n",
    "\n",
    "#---------------------------------------------------------------\n",
    "# Read in all files corresponding to the 51V reference measurements\n",
    "#---------------------------------------------------------------\n",
    "v51_startFiles = loadAllDataFiles(posDataDir, v51_startFileNums, piSystem, loose_limits)\n",
    "v51_magFiles   = loadAllDataFiles(posDataDir, v51_magFileNums,   piSystem, loose_limits)\n",
    "v51_accumFiles = loadAllDataFiles(posDataDir, v51_redFileNums,   piSystem, loose_limits)\n",
    "\n",
    "#---------------------------------------------------------------\n",
    "# Read in all files corresponding to the 101Sn2+ measurements\n",
    "#---------------------------------------------------------------\n",
    "sn101_startFiles = loadAllDataFiles(posDataDir, v51_startFileNums, piSystem, loose_limits)\n",
    "sn101_magFiles   = loadAllDataFiles(posDataDir, v51_magFileNums,   piSystem, loose_limits)\n",
    "sn101_accumFiles = loadAllDataFiles(posDataDir, v51_redFileNums,   piSystem, loose_limits)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define functions for performing the fitting\n",
    "\n",
    "These functions are used to fit the PI-ICR data. You can create new methods, though you must follow the style guideline so that the functions interface properly with the plotting GUI\n",
    "\n",
    "The launchFitGUI method requires the fit function to have the following structure: \n",
    "\n",
    "> def func(data, (init param tuple), center, centerErr=[0,0]):  \n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...  \n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return (param name tuple), (fit param tuple), (fit param err tuple), plot_function_handle\n",
    "\n",
    "The required arguments are:  \n",
    "1. data is a 2D list of the points to fit (x in first col, y in second)\n",
    "1. the paramter tuple allows you to pass as many parameters as needed to perform the fit (so long as they are contained inside of the tuple!). Some of the init params may be provided as 'None' type, and the fit method will need to be able to handle this\n",
    "1. The center value is subtracted off the positions BEFORE fitting. This is the center of motion for the ions on the MCP.\n",
    "\n",
    "The required returns are (all values are RELATIVE to the provided center):\n",
    "1. a tuple of strings for each parameter name (same length as the init param tuple!)\n",
    "1. a tuple of the fitted parameter values (same length as the init param tuple!)\n",
    "1. a tuple of the fitted parameter errors (same length as the init param tuple!)\n",
    "1. a function handle which plots the fit, who has only one argument (figure axes object), and returns the 2D artist object (e.g. ax.plot(...))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def cartSpotFit(data, init_param_tuple, center, centerErr=[0,0]):\n",
    "    \"\"\" Note: as is, this method does not use inital guesses! \n",
    "        This method needs to be able to handle None values in\n",
    "            init_param_tuple!\n",
    "    \"\"\"\n",
    "    fit_dict = spotFitter.fit2DCartesianGaussian(np.array(data) - center, dataErr=centerErr)\n",
    "    def plot_fit(ax):\n",
    "        phi = np.linspace(0, 2*np.pi, 1000)\n",
    "        theta_rad = fit_dict['rot_theta'] * np.pi/180\n",
    "        x_list = fit_dict['sigma_x']*np.cos(phi)*np.cos(theta_rad) - fit_dict['sigma_y']*np.sin(phi)*np.sin(theta_rad) + fit_dict['mean_x'] + center[0]\n",
    "        y_list = fit_dict['sigma_x']*np.cos(phi)*np.sin(theta_rad) + fit_dict['sigma_y']*np.sin(phi)*np.cos(theta_rad) + fit_dict['mean_y'] + center[1]\n",
    "        pl1 = ax.plot(fit_dict['mean_x']+center[0], fit_dict['mean_y']+center[1], 'x', color='mediumseagreen', markersize=7, markeredgewidth=3, alpha=1, label='Center')\n",
    "        pl2 = ax.plot(x_list, y_list, '-', color='yellowgreen', linewidth=2, alpha=1, label = r'1$\\sigma$')\n",
    "        ax.legend()\n",
    "        return pl1, pl2\n",
    "    \n",
    "    return ('Mean x', 'Mean y', 'Sigma x', 'Sigma y', 'Rot Angle'), \\\n",
    "           (fit_dict['mean_x'], fit_dict['mean_y'], fit_dict['sigma_x'], fit_dict['sigma_y'], fit_dict['rot_theta']), \\\n",
    "           (fit_dict['mean_x_err'], fit_dict['mean_y_err'], fit_dict['sigma_x_err'], fit_dict['sigma_y_err'], fit_dict['rot_theta_err']), \\\n",
    "           plot_fit\n",
    "\n",
    "def polarSpotFit(data, init_param_tuple, center, centerErr=[0,0]):\n",
    "    \"\"\" Note: as is, this method does not use inital guesses! \n",
    "        This method needs to be able to handle None values in\n",
    "            init_param_tuple!\n",
    "        Args:\n",
    "            data (2D list): positions in CARTESIAN coords!\n",
    "    \"\"\"\n",
    "    rho, phi = cart2pol(data[:,0]-center[0], data[:,1]-center[1])\n",
    "    # Determine radius and angle uncertainty based on center error\n",
    "    rhoErr = np.sqrt( centerErr[0]**2 + centerErr[1]**2 )\n",
    "    phiErr = 0\n",
    "\n",
    "    RP_data = np.transpose(np.concatenate(([rho],[phi]), axis=0))\n",
    "    fit_dict = spotFitter.fit2DPolarGaussian(RP_data, dataErr=[rhoErr, phiErr])\n",
    "    def plot_fit(ax):\n",
    "        mean_rho, mean_phi, sigma_rho, sigma_phi = fit_dict['mean_rho'], fit_dict['mean_phi'], fit_dict['sigma_rho'], fit_dict['sigma_phi']\n",
    "        mean_x, mean_y = mean_rho*np.cos(mean_phi), mean_rho*np.sin(mean_phi)\n",
    "        t_list = np.linspace(0,2*np.pi,500)\n",
    "        phi_list, rho_list = mean_phi + sigma_phi*np.cos(t_list), mean_rho + sigma_rho*np.sin(t_list)\n",
    "        x_list, y_list = pol2cart(rho_list, phi_list)\n",
    "        pl1 = ax.plot(mean_x+center[0], mean_y+center[1], 'x', color='mediumseagreen', markersize=7, markeredgewidth=3, alpha=1, label='Center')\n",
    "        pl2 = ax.plot(np.array(x_list)+center[0], np.array(y_list)+center[1], '-', color='yellowgreen', linewidth=2, alpha=1, label = r'1$\\sigma$')\n",
    "        ax.legend()\n",
    "        return pl1, pl2\n",
    "    \n",
    "    return ('Mean rho', 'Mean phi', 'Sigma rho', 'Sigma phi'), \\\n",
    "           (fit_dict['mean_rho'], (180/np.pi)*fit_dict['mean_phi'], fit_dict['sigma_rho'], (180/np.pi)*fit_dict['sigma_phi']), \\\n",
    "           (fit_dict['mean_rho_err'], (180/np.pi)*fit_dict['mean_phi_err'], fit_dict['sigma_rho_err'], (180/np.pi)*fit_dict['sigma_phi_err']), \\\n",
    "           plot_fit\n",
    "\n",
    "def ellipseFit(data, init_param_tuple, center, centerErr=[0,0]):\n",
    "    \"\"\" Note: as is, this method does not use inital guesses! \n",
    "        This method needs to be able to handle None values in\n",
    "            init_param_tuple!\n",
    "    \"\"\"\n",
    "    datatemp = np.array(data) - center\n",
    "    # Get the coefficeints of the conic form\n",
    "    params = ellipseFitter.fit(datatemp[:,0], datatemp[:,1])\n",
    "    # Calculate the errors of the fitted coefficents\n",
    "    params = ellipseFitter.errors(datatemp[:,0], datatemp[:,1], params) # params are a ufloat object (uncertainties package). -> params[0].n gets the val, params[0].s gets the uncertainty\n",
    "    # Convert the conic coeffs to the parametrized cartesian form\n",
    "    ell_center, a, b, theta = ellipseFitter.convert(params) # params are a ufloat object (uncertainties package). -> params[0].n gets the val, params[0].s gets the uncertainty\n",
    "    # Calculate the parameters for the upper and lower confidence intervals\n",
    "    c_up, c_do = ellipseFitter.confidence_area(datatemp[:,0], datatemp[:,1], [i.n for i in params], f=1) # f tells how many sigma to go out\n",
    "    center_up, a_up, b_up, theta_up = ellipseFitter.convert(c_up) \n",
    "    center_do, a_do, b_do, theta_do = ellipseFitter.convert(c_do) \n",
    "\n",
    "    def plot_fit(ax):\n",
    "        phi = np.linspace(0, 2*np.pi, 1000)\n",
    "        x = a.n*np.cos(phi)*np.cos(theta.n) - b.n*np.sin(phi)*np.sin(theta.n) + ell_center[0].n + center[0]\n",
    "        y = a.n*np.cos(phi)*np.sin(theta.n) + b.n*np.sin(phi)*np.cos(theta.n) + ell_center[1].n + center[1]\n",
    "        x_do = a_do*np.cos(phi)*np.cos(theta_do) - b_do*np.sin(phi)*np.sin(theta_do) + center_do[0] + center[0]\n",
    "        y_do = a_do*np.cos(phi)*np.sin(theta_do) + b_do*np.sin(phi)*np.cos(theta_do) + center_do[1] + center[1]\n",
    "        x_up = a_up*np.cos(phi)*np.cos(theta_up) - b_up*np.sin(phi)*np.sin(theta_up) + center_up[0] + center[0]\n",
    "        y_up = a_up*np.cos(phi)*np.sin(theta_up) + b_up*np.sin(phi)*np.cos(theta_up) + center_up[1] + center[1]\n",
    "        pl1, = ax.plot(x, y, '-', color='mediumseagreen', linewidth=2, alpha=1, label='Fit')\n",
    "        pl2, = ax.plot(x_do, y_do, '-', color='yellowgreen', linewidth=2, alpha=1, label=r'1$\\sigma$')\n",
    "        pl3, = ax.plot(x_up, y_up, '-', color='yellowgreen', linewidth=2, alpha=1)\n",
    "        ax.legend()\n",
    "        return pl1, pl2, pl3\n",
    "\n",
    "    return ('Center x', 'Center y', 'Major (a)', 'Minor (b)', 'Rot Angle'), \\\n",
    "           (ell_center[0].n, ell_center[1].n, a.n, b.n, theta.n*180/np.pi), \\\n",
    "           (ell_center[0].s, ell_center[1].s, a.s, b.s, theta.s*180/np.pi), \\\n",
    "           plot_fit\n",
    "    \n",
    "fitmethods = {\n",
    "    \"Ellipse\": ellipseFit,\n",
    "    \"Polar Spot\": polarSpotFit,\n",
    "    \"Cartesian Spot\": cartSpotFit\n",
    "}\n",
    "\n",
    "def determineFileCenters(centerDataset, analysisfile_list):\n",
    "    \"\"\" \n",
    "    Args:\n",
    "        centerDataset (CenterDataset obj): dataset to interpolate the ion center of motion for\n",
    "        analysisfile_list (list of AnalysisFile obj): all files to update their 'center' value (center of ion motion in trap)\n",
    "    Returns: none (files are passed by reference and updated dynamically)\n",
    "    \"\"\"\n",
    "    for file in analysisfile_list:\n",
    "        interpCenter, interpCenterErr = centerDataset.interpCenter(file.getMidTime())\n",
    "        file.setCenter(interpCenter)\n",
    "        file.setCenterErr(interpCenterErr)\n",
    "        # If the center dataset was made using ellipse fits, we will save those for the spots here as well\n",
    "        # NOTE: this code will use 'None' if an ellipse fit wasn't used.\n",
    "        interpParams = centerDataset.interpEllipseParams(file.getMidTime())\n",
    "        file.setEllipseParams(interpParams)\n",
    "\n",
    "def extractSpotLocationFromFit(analysisfile_list, fitparam_dicts, fitparamerr_dicts, data_type='spot'):\n",
    "    \"\"\" Takes one of the fits from the above functions and extracts the \n",
    "        spot location in polar coordinates \n",
    "    Args:\n",
    "        analysisfile_list (list of AnalysisFile obj): all files which have been fit\n",
    "        fitparam_dicts (list of Dict): dictionaries containing all the fit parameter values\n",
    "        firparamerr_dicts (list of Dict): dictionaries containing all the fit parameter errors\n",
    "        data_type ('spot' or 'center'): determines how to interpret the fit\n",
    "    Returns:\n",
    "        list of AnalysisFile obj with the center/spot location saved\n",
    "    \"\"\"\n",
    "    saved_analysisfiles = []\n",
    "    #------------------------------------------------------------------------\n",
    "    # If the datatype is for a center of motion measurement, we only use the\n",
    "    #   ellipse fit or the cartesian spot fit!\n",
    "    if data_type == 'center':\n",
    "        for kk, currfile in enumerate(analysisfile_list):\n",
    "            if fitparam_dicts[kk] == None: continue # skip this datafile if no fit was performed\n",
    "            fitmethod = fitmethods[fitparam_dicts[kk]['fitmethod']]\n",
    "            # If you used a cartesian spot fit\n",
    "            if fitmethod == cartSpotFit:\n",
    "                currfile.setCenter( [fitparam_dicts[kk]['param1'], fitparam_dicts[kk]['param2']] )\n",
    "                currfile.setCenterErr( [fitparamerr_dicts[kk]['param1err'], fitparamerr_dicts[kk]['param2err']] )\n",
    "            # Used an ellipse fit to determine the center\n",
    "            elif fitmethod == ellipseFit: \n",
    "                currfile.setCenter( [fitparam_dicts[kk]['param1'], fitparam_dicts[kk]['param2']] )\n",
    "                currfile.setCenterErr( [fitparamerr_dicts[kk]['param1err'], fitparamerr_dicts[kk]['param2err']] )\n",
    "            else: print(\"WARNING: only a cartesian spot fit or a ellipse fit is valid for the center datasets! File \"+str(analysisfile_list[kk].getFileName())+\" will be ignored.\")\n",
    "            saved_analysisfiles.append(currfile)\n",
    "    #------------------------------------------------------------------------\n",
    "    # If the datatype is for a center of motion measurement, we only use the\n",
    "    #   ellipse fit or the cartesian spot fit!\n",
    "    else:\n",
    "        for kk, currfile in enumerate(analysisfile_list):\n",
    "            if fitparam_dicts[kk] == None: continue # skip this datafile if no fit was performed\n",
    "            fitmethod = fitmethods[fitparam_dicts[kk]['fitmethod']]\n",
    "            # If you used a cartesian spot fit\n",
    "            if fitmethod == cartSpotFit:\n",
    "                xy = [fitparam_dicts[kk]['param1'], fitparam_dicts[kk]['param2']]\n",
    "                xyErr = [fitparamerr_dicts[kk]['param1err'], fitparamerr_dicts[kk]['param2err']]\n",
    "                center, centerErr = currfile.getCenter(), currfile.getCenterErr()\n",
    "                rho, rhoErr, phi, phiErr = cart2Pol_withUnc(xy, xyErr, center=center, centerErr=centerErr)\n",
    "            # If you used a polar fit\n",
    "            elif fitmethod == polarSpotFit:\n",
    "                rho, phi = fitparam_dicts[kk]['param1'], fitparam_dicts[kk]['param2']\n",
    "                rhoErr, phiErr = fitparamerr_dicts[kk]['param1err'], fitparamerr_dicts[kk]['param2err']\n",
    "            else: print(\"WARNING: only a cartesian spot fit or a polar spot fit is valid for the excited spot datasets! File \"+str(analysisfile_list[kk].getFileName())+\" will be ignored.\")\n",
    "        \n",
    "            currfile.setSpotRho(rho)\n",
    "            currfile.setSpotRhoErr(rhoErr)\n",
    "            currfile.setSpotPhi(phi) # should always be in degrees!\n",
    "            currfile.setSpotPhiErr(phiErr) # should always be in degrees!\n",
    "            saved_analysisfiles.append(currfile)\n",
    "\n",
    "    return saved_analysisfiles"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Determine location for the ions' center of motion\n",
    "\n",
    "Fitting and analyzing the data may require knowing where the center of the elliptical/circular motion is (specifically if fitting in polar coordinates). If this is to be done, you need to run this code to fit an ellipse or center spot from the dataset. By default, the code will use (0,0) as the center if you do not provide any datafiles."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "#--------------------------------------------------------\n",
    "# Load the files, and perform the fits\n",
    "centerParamDicts, centerParamErrDicts = launchFitGUI(centerFiles, piSystem, limits, settings, piCluster, fitmethods, nbinsPosn)\n",
    "#--------------------------------------------------------\n",
    "# Update the datafiles to reflect the fits\n",
    "savedCenterFiles = extractSpotLocationFromFit(centerFiles, centerParamDicts, centerParamErrDicts, data_type='center')\n",
    "#--------------------------------------------------------\n",
    "# Create the dataset with the saved files\n",
    "circleDataset = CircleDataset(savedCenterFiles, interp_method=\"constant\") # interp_method = 'linear', 'constant', or 'nearest'"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run Measurements for $^{50}\\text{Ti}$\n",
    "\n",
    "### Fit all Start Spots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "#--------------------------------------------------------\n",
    "# Determine the center of motion for each spot datafile\n",
    "determineFileCenters(circleDataset, ti50_startFiles)\n",
    "#--------------------------------------------------------\n",
    "# Load the files, and perform the fits NOTE: all fit params are RELATIVE to the ions center of motion! Important when using cartesian fit!\n",
    "ti50_startParamDicts, ti50_startParamErrDicts = launchFitGUI(ti50_startFiles, piSystem, limits, settings, piCluster, fitmethods, nbinsPosn)\n",
    "#--------------------------------------------------------\n",
    "# Update the datafiles to reflect the fits\n",
    "ti50_savedStartFiles = extractSpotLocationFromFit(ti50_startFiles, ti50_startParamDicts, ti50_startParamErrDicts, data_type='spot')\n",
    "#--------------------------------------------------------\n",
    "# Create the dataset with the saved files\n",
    "ti50_startDataset = StartDataset(ti50_savedStartFiles, interp_method=\"linear\") # interp_method = 'linear', 'constant', or 'nearest'"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fit all Magnetron Spots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "#--------------------------------------------------------\n",
    "# Determine the center of motion for each spot datafile\n",
    "determineFileCenters(circleDataset, ti50_magFiles)\n",
    "#--------------------------------------------------------\n",
    "# Load the files, and perform the fits NOTE: all fit params are RELATIVE to the ions center of motion! Important when using cartesian fit!\n",
    "ti50_magParamDicts, ti50_magParamErrDicts = launchFitGUI(ti50_magFiles, piSystem, limits, settings, piCluster, fitmethods, nbinsPosn)\n",
    "#--------------------------------------------------------\n",
    "# Update the datafiles to reflect the fits\n",
    "ti50_savedMagFiles = extractSpotLocationFromFit(ti50_magFiles, ti50_magParamDicts, ti50_magParamErrDicts, data_type='spot')\n",
    "#--------------------------------------------------------\n",
    "# Create the dataset with the saved files\n",
    "ti50_magDataset = MagDataset(ti50_savedMagFiles, ti50_startDataset, interp_method=\"linear\",\n",
    "                             initial_Fm_guess=initial_Fm_guess, initial_Fp_guess=ti50_Fp_guess) # interp_method = 'linear', 'constant', or 'nearest'"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fit all Red. Cyc. Spots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "#--------------------------------------------------------\n",
    "# Determine the center of motion for each spot datafile\n",
    "determineFileCenters(circleDataset, ti50_accumFiles)\n",
    "#--------------------------------------------------------\n",
    "# Load the files, and perform the fits NOTE: all fit params are RELATIVE to the ions center of motion! Important when using cartesian fit!\n",
    "ti50_accumParamDicts, ti50_accumParamErrDicts = launchFitGUI(ti50_accumFiles, piSystem, limits, settings, piCluster, fitmethods, nbinsPosn)\n",
    "#--------------------------------------------------------\n",
    "# Update the datafiles to reflect the fits\n",
    "ti50_savedAccumFiles = extractSpotLocationFromFit(ti50_accumFiles, ti50_accumParamDicts, ti50_accumParamErrDicts, data_type='spot')\n",
    "#--------------------------------------------------------\n",
    "# Create the dataset with the saved files\n",
    "ti50_accumDataset = TwoPhaseAccumDataset(ti50_savedAccumFiles, ti50_startDataset, ti50_magDataset,\n",
    "                                         initial_Fm_guess=initial_Fm_guess, initial_Fp_guess=ti50_Fp_guess)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Save the Results to FT2 files for SOMA"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "saveFilePath = posDataDir\n",
    "ti50_accumDataset.exportToFT2(saveFilePath)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run Measurements for $^{51}\\text{V}$\n",
    "\n",
    "### Fit all Start Spots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "#--------------------------------------------------------\n",
    "# Determine the center of motion for each spot datafile\n",
    "determineFileCenters(circleDataset, v51_startFiles)\n",
    "#--------------------------------------------------------\n",
    "# Load the files, and perform the fits NOTE: all fit params are RELATIVE to the ions center of motion! Important when using cartesian fit!\n",
    "v51_startParamDicts, v51_startParamErrDicts = launchFitGUI(v51_startFiles, piSystem, limits, settings, piCluster, fitmethods, nbinsPosn)\n",
    "#--------------------------------------------------------\n",
    "# Update the datafiles to reflect the fits\n",
    "v51_savedStartFiles = extractSpotLocationFromFit(v51_startFiles, v51_startParamDicts, v51_startParamErrDicts, data_type='spot')\n",
    "#--------------------------------------------------------\n",
    "# Create the dataset with the saved files\n",
    "v51_startDataset = StartDataset(v51_savedStartFiles, interp_method=\"linear\") # interp_method = 'linear', 'constant', or 'nearest'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fit all Magnetron Spots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "#--------------------------------------------------------\n",
    "# Determine the center of motion for each spot datafile\n",
    "determineFileCenters(circleDataset, v51_magFiles)\n",
    "#--------------------------------------------------------\n",
    "# Load the files, and perform the fits NOTE: all fit params are RELATIVE to the ions center of motion! Important when using cartesian fit!\n",
    "v51_magParamDicts, v51_magParamErrDicts = launchFitGUI(v51_magFiles, piSystem, limits, settings, piCluster, fitmethods, nbinsPosn)\n",
    "#--------------------------------------------------------\n",
    "# Update the datafiles to reflect the fits\n",
    "v51_savedMagFiles = extractSpotLocationFromFit(v51_magFiles, v51_magParamDicts, v51_magParamErrDicts, data_type='spot')\n",
    "#--------------------------------------------------------\n",
    "# Create the dataset with the saved files\n",
    "v51_magDataset = MagDataset(v51_savedMagFiles, v51_startDataset, interp_method=\"linear\",\n",
    "                            initial_Fm_guess=initial_Fm_guess, initial_Fp_guess=v51_Fp_guess) # interp_method = 'linear', 'constant', or 'nearest'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fit all Red. Cyc. Spots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "#--------------------------------------------------------\n",
    "# Determine the center of motion for each spot datafile\n",
    "determineFileCenters(circleDataset, v51_accumFiles)\n",
    "#--------------------------------------------------------\n",
    "# Load the files, and perform the fits NOTE: all fit params are RELATIVE to the ions center of motion! Important when using cartesian fit!\n",
    "v51_accumParamDicts, v51_accumParamErrDicts = launchFitGUI(v51_accumFiles, piSystem, limits, settings, piCluster, fitmethods, nbinsPosn)\n",
    "#--------------------------------------------------------\n",
    "# Update the datafiles to reflect the fits\n",
    "v51_savedAccumFiles = extractSpotLocationFromFit(v51_accumFiles, v51_accumParamDicts, v51_accumParamErrDicts, data_type='spot')\n",
    "#--------------------------------------------------------\n",
    "# Create the dataset with the saved files\n",
    "v51_accumDataset = TwoPhaseAccumDataset(v51_savedAccumFiles, v51_startDataset, v51_magDataset,\n",
    "                                        initial_Fm_guess=initial_Fm_guess, initial_Fp_guess=v51_Fp_guess)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Save the Results to FT2 files for SOMA"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "saveFilePath = posDataDir\n",
    "v51_accumDataset.exportToFT2(saveFilePath)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run Measurements for $^{101}\\text{Sn}^{2+}$\n",
    "\n",
    "### Fit all Start Spots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "#--------------------------------------------------------\n",
    "# Determine the center of motion for each spot datafile\n",
    "determineFileCenters(circleDataset, sn101_startFiles)\n",
    "#--------------------------------------------------------\n",
    "# Load the files, and perform the fits NOTE: all fit params are RELATIVE to the ions center of motion! Important when using cartesian fit!\n",
    "sn101_startParamDicts, sn101_startParamErrDicts = launchFitGUI(sn101_startFiles, piSystem, limits, settings, piCluster, fitmethods, nbinsPosn)\n",
    "#--------------------------------------------------------\n",
    "# Update the datafiles to reflect the fits\n",
    "sn101_savedStartFiles = extractSpotLocationFromFit(sn101_startFiles, sn101_startParamDicts, sn101_startParamErrDicts, data_type='spot')\n",
    "#--------------------------------------------------------\n",
    "# Create the dataset with the saved files\n",
    "sn101_startDataset = StartDataset(sn101_savedStartFiles, interp_method=\"linear\") # interp_method = 'linear', 'constant', or 'nearest'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create A Magnetron Dataset out of the Two Reference Sets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# averaging_weight (float): weighting factor for mag_dataset1 value, (1-averaging_weight) is used for mag_dataset2\n",
    "#   Using the AME masses for 50Ti, 51V, and 101Sn, we can determine that the Sn101 will be closer to 51V and if \n",
    "#   we use mag_dataset1 = t150, then the averaging_weight should be 0.4767...  \n",
    "sn101_magDataset = MagDataset2Refs(ti50_magDataset, v51_magDataset, averaging_weight=0.4767)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fit all Red. Cyc. Spots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "#--------------------------------------------------------\n",
    "# Determine the center of motion for each spot datafile\n",
    "determineFileCenters(circleDataset, sn101_accumFiles)\n",
    "#--------------------------------------------------------\n",
    "# Load the files, and perform the fits NOTE: all fit params are RELATIVE to the ions center of motion! Important when using cartesian fit!\n",
    "sn101_accumParamDicts, sn101_accumParamErrDicts = launchFitGUI(sn101_accumFiles, piSystem, limits, settings, piCluster, fitmethods, nbinsPosn)\n",
    "#--------------------------------------------------------\n",
    "# Update the datafiles to reflect the fits\n",
    "sn101_savedAccumFiles = extractSpotLocationFromFit(sn101_accumFiles, sn101_accumParamDicts, sn101_accumParamErrDicts, data_type='spot')\n",
    "#--------------------------------------------------------\n",
    "# Create the dataset with the saved files\n",
    "sn101_accumDataset = TwoPhaseAccumDataset(sn101_savedAccumFiles, sn101_startDataset, sn101_magDataset, \n",
    "                                          initial_Fm_guess=initial_Fm_guess, initial_Fp_guess=sn101_Fp_guess)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Save the Results to FT2 files for SOMA"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "saveFilePath = posDataDir\n",
    "sn101_accumDataset.exportToFT2(saveFilePath)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.11.2 ('PIICR_311_v2')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "09f976f5976ec5ef7979cbf49af2adbba98ae5178567dc3291e0f2365105daea"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
